<?php

namespace Base;

use \Config as ChildConfig;
use \ConfigQuery as ChildConfigQuery;
use \Exception;
use \PDO;
use Map\ConfigTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'config' table.
 *
 *
 *
 * @method     ChildConfigQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildConfigQuery orderByQuizMode($order = Criteria::ASC) Order by the quiz_mode column
 * @method     ChildConfigQuery orderByQuestionTime($order = Criteria::ASC) Order by the question_time column
 * @method     ChildConfigQuery orderByAnswerTime($order = Criteria::ASC) Order by the answer_time column
 * @method     ChildConfigQuery orderByAnnouncedRightUsers($order = Criteria::ASC) Order by the announced_right_users column
 * @method     ChildConfigQuery orderByChallengeUpdated($order = Criteria::ASC) Order by the challenge_updated column
 * @method     ChildConfigQuery orderByPackages($order = Criteria::ASC) Order by the packages column
 * @method     ChildConfigQuery orderByBlackList($order = Criteria::ASC) Order by the black_list column
 * @method     ChildConfigQuery orderByDifficultySequence($order = Criteria::ASC) Order by the difficulty_sequence column
 * @method     ChildConfigQuery orderByMinUsersForUpdates($order = Criteria::ASC) Order by the min_users_for_updates column
 * @method     ChildConfigQuery orderByTqi($order = Criteria::ASC) Order by the tqi column
 * @method     ChildConfigQuery orderByTournamentNumber($order = Criteria::ASC) Order by the tournament_number column
 * @method     ChildConfigQuery orderByTempUploadedQuestionId($order = Criteria::ASC) Order by the temp_uploaded_question_id column
 * @method     ChildConfigQuery orderByReserveString($order = Criteria::ASC) Order by the reserve_string column
 * @method     ChildConfigQuery orderByReserveInt($order = Criteria::ASC) Order by the reserve_int column
 * @method     ChildConfigQuery orderByReserve5($order = Criteria::ASC) Order by the reserve5 column
 * @method     ChildConfigQuery orderByMinTimeBetweenEqualQuestions($order = Criteria::ASC) Order by the min_time_between_equal_questions column
 *
 * @method     ChildConfigQuery groupById() Group by the id column
 * @method     ChildConfigQuery groupByQuizMode() Group by the quiz_mode column
 * @method     ChildConfigQuery groupByQuestionTime() Group by the question_time column
 * @method     ChildConfigQuery groupByAnswerTime() Group by the answer_time column
 * @method     ChildConfigQuery groupByAnnouncedRightUsers() Group by the announced_right_users column
 * @method     ChildConfigQuery groupByChallengeUpdated() Group by the challenge_updated column
 * @method     ChildConfigQuery groupByPackages() Group by the packages column
 * @method     ChildConfigQuery groupByBlackList() Group by the black_list column
 * @method     ChildConfigQuery groupByDifficultySequence() Group by the difficulty_sequence column
 * @method     ChildConfigQuery groupByMinUsersForUpdates() Group by the min_users_for_updates column
 * @method     ChildConfigQuery groupByTqi() Group by the tqi column
 * @method     ChildConfigQuery groupByTournamentNumber() Group by the tournament_number column
 * @method     ChildConfigQuery groupByTempUploadedQuestionId() Group by the temp_uploaded_question_id column
 * @method     ChildConfigQuery groupByReserveString() Group by the reserve_string column
 * @method     ChildConfigQuery groupByReserveInt() Group by the reserve_int column
 * @method     ChildConfigQuery groupByReserve5() Group by the reserve5 column
 * @method     ChildConfigQuery groupByMinTimeBetweenEqualQuestions() Group by the min_time_between_equal_questions column
 *
 * @method     ChildConfigQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildConfigQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildConfigQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildConfigQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildConfigQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildConfigQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildConfig findOne(ConnectionInterface $con = null) Return the first ChildConfig matching the query
 * @method     ChildConfig findOneOrCreate(ConnectionInterface $con = null) Return the first ChildConfig matching the query, or a new ChildConfig object populated from the query conditions when no match is found
 *
 * @method     ChildConfig findOneById(int $id) Return the first ChildConfig filtered by the id column
 * @method     ChildConfig findOneByQuizMode(string $quiz_mode) Return the first ChildConfig filtered by the quiz_mode column
 * @method     ChildConfig findOneByQuestionTime(int $question_time) Return the first ChildConfig filtered by the question_time column
 * @method     ChildConfig findOneByAnswerTime(int $answer_time) Return the first ChildConfig filtered by the answer_time column
 * @method     ChildConfig findOneByAnnouncedRightUsers(int $announced_right_users) Return the first ChildConfig filtered by the announced_right_users column
 * @method     ChildConfig findOneByChallengeUpdated(int $challenge_updated) Return the first ChildConfig filtered by the challenge_updated column
 * @method     ChildConfig findOneByPackages(string $packages) Return the first ChildConfig filtered by the packages column
 * @method     ChildConfig findOneByBlackList(string $black_list) Return the first ChildConfig filtered by the black_list column
 * @method     ChildConfig findOneByDifficultySequence(string $difficulty_sequence) Return the first ChildConfig filtered by the difficulty_sequence column
 * @method     ChildConfig findOneByMinUsersForUpdates(int $min_users_for_updates) Return the first ChildConfig filtered by the min_users_for_updates column
 * @method     ChildConfig findOneByTqi(int $tqi) Return the first ChildConfig filtered by the tqi column
 * @method     ChildConfig findOneByTournamentNumber(string $tournament_number) Return the first ChildConfig filtered by the tournament_number column
 * @method     ChildConfig findOneByTempUploadedQuestionId(int $temp_uploaded_question_id) Return the first ChildConfig filtered by the temp_uploaded_question_id column
 * @method     ChildConfig findOneByReserveString(string $reserve_string) Return the first ChildConfig filtered by the reserve_string column
 * @method     ChildConfig findOneByReserveInt(int $reserve_int) Return the first ChildConfig filtered by the reserve_int column
 * @method     ChildConfig findOneByReserve5(int $reserve5) Return the first ChildConfig filtered by the reserve5 column
 * @method     ChildConfig findOneByMinTimeBetweenEqualQuestions(int $min_time_between_equal_questions) Return the first ChildConfig filtered by the min_time_between_equal_questions column *

 * @method     ChildConfig requirePk($key, ConnectionInterface $con = null) Return the ChildConfig by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOne(ConnectionInterface $con = null) Return the first ChildConfig matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildConfig requireOneById(int $id) Return the first ChildConfig filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByQuizMode(string $quiz_mode) Return the first ChildConfig filtered by the quiz_mode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByQuestionTime(int $question_time) Return the first ChildConfig filtered by the question_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByAnswerTime(int $answer_time) Return the first ChildConfig filtered by the answer_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByAnnouncedRightUsers(int $announced_right_users) Return the first ChildConfig filtered by the announced_right_users column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByChallengeUpdated(int $challenge_updated) Return the first ChildConfig filtered by the challenge_updated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByPackages(string $packages) Return the first ChildConfig filtered by the packages column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByBlackList(string $black_list) Return the first ChildConfig filtered by the black_list column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByDifficultySequence(string $difficulty_sequence) Return the first ChildConfig filtered by the difficulty_sequence column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByMinUsersForUpdates(int $min_users_for_updates) Return the first ChildConfig filtered by the min_users_for_updates column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByTqi(int $tqi) Return the first ChildConfig filtered by the tqi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByTournamentNumber(string $tournament_number) Return the first ChildConfig filtered by the tournament_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByTempUploadedQuestionId(int $temp_uploaded_question_id) Return the first ChildConfig filtered by the temp_uploaded_question_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByReserveString(string $reserve_string) Return the first ChildConfig filtered by the reserve_string column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByReserveInt(int $reserve_int) Return the first ChildConfig filtered by the reserve_int column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByReserve5(int $reserve5) Return the first ChildConfig filtered by the reserve5 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConfig requireOneByMinTimeBetweenEqualQuestions(int $min_time_between_equal_questions) Return the first ChildConfig filtered by the min_time_between_equal_questions column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildConfig[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildConfig objects based on current ModelCriteria
 * @method     ChildConfig[]|ObjectCollection findById(int $id) Return ChildConfig objects filtered by the id column
 * @method     ChildConfig[]|ObjectCollection findByQuizMode(string $quiz_mode) Return ChildConfig objects filtered by the quiz_mode column
 * @method     ChildConfig[]|ObjectCollection findByQuestionTime(int $question_time) Return ChildConfig objects filtered by the question_time column
 * @method     ChildConfig[]|ObjectCollection findByAnswerTime(int $answer_time) Return ChildConfig objects filtered by the answer_time column
 * @method     ChildConfig[]|ObjectCollection findByAnnouncedRightUsers(int $announced_right_users) Return ChildConfig objects filtered by the announced_right_users column
 * @method     ChildConfig[]|ObjectCollection findByChallengeUpdated(int $challenge_updated) Return ChildConfig objects filtered by the challenge_updated column
 * @method     ChildConfig[]|ObjectCollection findByPackages(string $packages) Return ChildConfig objects filtered by the packages column
 * @method     ChildConfig[]|ObjectCollection findByBlackList(string $black_list) Return ChildConfig objects filtered by the black_list column
 * @method     ChildConfig[]|ObjectCollection findByDifficultySequence(string $difficulty_sequence) Return ChildConfig objects filtered by the difficulty_sequence column
 * @method     ChildConfig[]|ObjectCollection findByMinUsersForUpdates(int $min_users_for_updates) Return ChildConfig objects filtered by the min_users_for_updates column
 * @method     ChildConfig[]|ObjectCollection findByTqi(int $tqi) Return ChildConfig objects filtered by the tqi column
 * @method     ChildConfig[]|ObjectCollection findByTournamentNumber(string $tournament_number) Return ChildConfig objects filtered by the tournament_number column
 * @method     ChildConfig[]|ObjectCollection findByTempUploadedQuestionId(int $temp_uploaded_question_id) Return ChildConfig objects filtered by the temp_uploaded_question_id column
 * @method     ChildConfig[]|ObjectCollection findByReserveString(string $reserve_string) Return ChildConfig objects filtered by the reserve_string column
 * @method     ChildConfig[]|ObjectCollection findByReserveInt(int $reserve_int) Return ChildConfig objects filtered by the reserve_int column
 * @method     ChildConfig[]|ObjectCollection findByReserve5(int $reserve5) Return ChildConfig objects filtered by the reserve5 column
 * @method     ChildConfig[]|ObjectCollection findByMinTimeBetweenEqualQuestions(int $min_time_between_equal_questions) Return ChildConfig objects filtered by the min_time_between_equal_questions column
 * @method     ChildConfig[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ConfigQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ConfigQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'dchat', $modelName = '\\Config', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildConfigQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildConfigQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildConfigQuery) {
            return $criteria;
        }
        $query = new ChildConfigQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildConfig|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ConfigTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ConfigTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildConfig A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, quiz_mode, question_time, answer_time, announced_right_users, challenge_updated, packages, black_list, difficulty_sequence, min_users_for_updates, tqi, tournament_number, temp_uploaded_question_id, reserve_string, reserve_int, reserve5, min_time_between_equal_questions FROM config WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildConfig $obj */
            $obj = new ChildConfig();
            $obj->hydrate($row);
            ConfigTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildConfig|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ConfigTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ConfigTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the quiz_mode column
     *
     * Example usage:
     * <code>
     * $query->filterByQuizMode('fooValue');   // WHERE quiz_mode = 'fooValue'
     * $query->filterByQuizMode('%fooValue%'); // WHERE quiz_mode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quizMode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByQuizMode($quizMode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quizMode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $quizMode)) {
                $quizMode = str_replace('*', '%', $quizMode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_QUIZ_MODE, $quizMode, $comparison);
    }

    /**
     * Filter the query on the question_time column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestionTime(1234); // WHERE question_time = 1234
     * $query->filterByQuestionTime(array(12, 34)); // WHERE question_time IN (12, 34)
     * $query->filterByQuestionTime(array('min' => 12)); // WHERE question_time > 12
     * </code>
     *
     * @param     mixed $questionTime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByQuestionTime($questionTime = null, $comparison = null)
    {
        if (is_array($questionTime)) {
            $useMinMax = false;
            if (isset($questionTime['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_QUESTION_TIME, $questionTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($questionTime['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_QUESTION_TIME, $questionTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_QUESTION_TIME, $questionTime, $comparison);
    }

    /**
     * Filter the query on the answer_time column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerTime(1234); // WHERE answer_time = 1234
     * $query->filterByAnswerTime(array(12, 34)); // WHERE answer_time IN (12, 34)
     * $query->filterByAnswerTime(array('min' => 12)); // WHERE answer_time > 12
     * </code>
     *
     * @param     mixed $answerTime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByAnswerTime($answerTime = null, $comparison = null)
    {
        if (is_array($answerTime)) {
            $useMinMax = false;
            if (isset($answerTime['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_ANSWER_TIME, $answerTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($answerTime['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_ANSWER_TIME, $answerTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_ANSWER_TIME, $answerTime, $comparison);
    }

    /**
     * Filter the query on the announced_right_users column
     *
     * Example usage:
     * <code>
     * $query->filterByAnnouncedRightUsers(1234); // WHERE announced_right_users = 1234
     * $query->filterByAnnouncedRightUsers(array(12, 34)); // WHERE announced_right_users IN (12, 34)
     * $query->filterByAnnouncedRightUsers(array('min' => 12)); // WHERE announced_right_users > 12
     * </code>
     *
     * @param     mixed $announcedRightUsers The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByAnnouncedRightUsers($announcedRightUsers = null, $comparison = null)
    {
        if (is_array($announcedRightUsers)) {
            $useMinMax = false;
            if (isset($announcedRightUsers['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS, $announcedRightUsers['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($announcedRightUsers['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS, $announcedRightUsers['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS, $announcedRightUsers, $comparison);
    }

    /**
     * Filter the query on the challenge_updated column
     *
     * Example usage:
     * <code>
     * $query->filterByChallengeUpdated(1234); // WHERE challenge_updated = 1234
     * $query->filterByChallengeUpdated(array(12, 34)); // WHERE challenge_updated IN (12, 34)
     * $query->filterByChallengeUpdated(array('min' => 12)); // WHERE challenge_updated > 12
     * </code>
     *
     * @param     mixed $challengeUpdated The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByChallengeUpdated($challengeUpdated = null, $comparison = null)
    {
        if (is_array($challengeUpdated)) {
            $useMinMax = false;
            if (isset($challengeUpdated['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_CHALLENGE_UPDATED, $challengeUpdated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($challengeUpdated['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_CHALLENGE_UPDATED, $challengeUpdated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_CHALLENGE_UPDATED, $challengeUpdated, $comparison);
    }

    /**
     * Filter the query on the packages column
     *
     * Example usage:
     * <code>
     * $query->filterByPackages('fooValue');   // WHERE packages = 'fooValue'
     * $query->filterByPackages('%fooValue%'); // WHERE packages LIKE '%fooValue%'
     * </code>
     *
     * @param     string $packages The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByPackages($packages = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($packages)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $packages)) {
                $packages = str_replace('*', '%', $packages);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_PACKAGES, $packages, $comparison);
    }

    /**
     * Filter the query on the black_list column
     *
     * Example usage:
     * <code>
     * $query->filterByBlackList('fooValue');   // WHERE black_list = 'fooValue'
     * $query->filterByBlackList('%fooValue%'); // WHERE black_list LIKE '%fooValue%'
     * </code>
     *
     * @param     string $blackList The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByBlackList($blackList = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($blackList)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $blackList)) {
                $blackList = str_replace('*', '%', $blackList);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_BLACK_LIST, $blackList, $comparison);
    }

    /**
     * Filter the query on the difficulty_sequence column
     *
     * Example usage:
     * <code>
     * $query->filterByDifficultySequence('fooValue');   // WHERE difficulty_sequence = 'fooValue'
     * $query->filterByDifficultySequence('%fooValue%'); // WHERE difficulty_sequence LIKE '%fooValue%'
     * </code>
     *
     * @param     string $difficultySequence The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByDifficultySequence($difficultySequence = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($difficultySequence)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $difficultySequence)) {
                $difficultySequence = str_replace('*', '%', $difficultySequence);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_DIFFICULTY_SEQUENCE, $difficultySequence, $comparison);
    }

    /**
     * Filter the query on the min_users_for_updates column
     *
     * Example usage:
     * <code>
     * $query->filterByMinUsersForUpdates(1234); // WHERE min_users_for_updates = 1234
     * $query->filterByMinUsersForUpdates(array(12, 34)); // WHERE min_users_for_updates IN (12, 34)
     * $query->filterByMinUsersForUpdates(array('min' => 12)); // WHERE min_users_for_updates > 12
     * </code>
     *
     * @param     mixed $minUsersForUpdates The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByMinUsersForUpdates($minUsersForUpdates = null, $comparison = null)
    {
        if (is_array($minUsersForUpdates)) {
            $useMinMax = false;
            if (isset($minUsersForUpdates['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_MIN_USERS_FOR_UPDATES, $minUsersForUpdates['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minUsersForUpdates['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_MIN_USERS_FOR_UPDATES, $minUsersForUpdates['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_MIN_USERS_FOR_UPDATES, $minUsersForUpdates, $comparison);
    }

    /**
     * Filter the query on the tqi column
     *
     * Example usage:
     * <code>
     * $query->filterByTqi(1234); // WHERE tqi = 1234
     * $query->filterByTqi(array(12, 34)); // WHERE tqi IN (12, 34)
     * $query->filterByTqi(array('min' => 12)); // WHERE tqi > 12
     * </code>
     *
     * @param     mixed $tqi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByTqi($tqi = null, $comparison = null)
    {
        if (is_array($tqi)) {
            $useMinMax = false;
            if (isset($tqi['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_TQI, $tqi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tqi['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_TQI, $tqi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_TQI, $tqi, $comparison);
    }

    /**
     * Filter the query on the tournament_number column
     *
     * Example usage:
     * <code>
     * $query->filterByTournamentNumber('fooValue');   // WHERE tournament_number = 'fooValue'
     * $query->filterByTournamentNumber('%fooValue%'); // WHERE tournament_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tournamentNumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByTournamentNumber($tournamentNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tournamentNumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tournamentNumber)) {
                $tournamentNumber = str_replace('*', '%', $tournamentNumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_TOURNAMENT_NUMBER, $tournamentNumber, $comparison);
    }

    /**
     * Filter the query on the temp_uploaded_question_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTempUploadedQuestionId(1234); // WHERE temp_uploaded_question_id = 1234
     * $query->filterByTempUploadedQuestionId(array(12, 34)); // WHERE temp_uploaded_question_id IN (12, 34)
     * $query->filterByTempUploadedQuestionId(array('min' => 12)); // WHERE temp_uploaded_question_id > 12
     * </code>
     *
     * @param     mixed $tempUploadedQuestionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByTempUploadedQuestionId($tempUploadedQuestionId = null, $comparison = null)
    {
        if (is_array($tempUploadedQuestionId)) {
            $useMinMax = false;
            if (isset($tempUploadedQuestionId['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID, $tempUploadedQuestionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tempUploadedQuestionId['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID, $tempUploadedQuestionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID, $tempUploadedQuestionId, $comparison);
    }

    /**
     * Filter the query on the reserve_string column
     *
     * Example usage:
     * <code>
     * $query->filterByReserveString('fooValue');   // WHERE reserve_string = 'fooValue'
     * $query->filterByReserveString('%fooValue%'); // WHERE reserve_string LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reserveString The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByReserveString($reserveString = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reserveString)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $reserveString)) {
                $reserveString = str_replace('*', '%', $reserveString);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_RESERVE_STRING, $reserveString, $comparison);
    }

    /**
     * Filter the query on the reserve_int column
     *
     * Example usage:
     * <code>
     * $query->filterByReserveInt(1234); // WHERE reserve_int = 1234
     * $query->filterByReserveInt(array(12, 34)); // WHERE reserve_int IN (12, 34)
     * $query->filterByReserveInt(array('min' => 12)); // WHERE reserve_int > 12
     * </code>
     *
     * @param     mixed $reserveInt The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByReserveInt($reserveInt = null, $comparison = null)
    {
        if (is_array($reserveInt)) {
            $useMinMax = false;
            if (isset($reserveInt['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_RESERVE_INT, $reserveInt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($reserveInt['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_RESERVE_INT, $reserveInt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_RESERVE_INT, $reserveInt, $comparison);
    }

    /**
     * Filter the query on the reserve5 column
     *
     * Example usage:
     * <code>
     * $query->filterByReserve5(1234); // WHERE reserve5 = 1234
     * $query->filterByReserve5(array(12, 34)); // WHERE reserve5 IN (12, 34)
     * $query->filterByReserve5(array('min' => 12)); // WHERE reserve5 > 12
     * </code>
     *
     * @param     mixed $reserve5 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByReserve5($reserve5 = null, $comparison = null)
    {
        if (is_array($reserve5)) {
            $useMinMax = false;
            if (isset($reserve5['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_RESERVE5, $reserve5['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($reserve5['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_RESERVE5, $reserve5['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_RESERVE5, $reserve5, $comparison);
    }

    /**
     * Filter the query on the min_time_between_equal_questions column
     *
     * Example usage:
     * <code>
     * $query->filterByMinTimeBetweenEqualQuestions(1234); // WHERE min_time_between_equal_questions = 1234
     * $query->filterByMinTimeBetweenEqualQuestions(array(12, 34)); // WHERE min_time_between_equal_questions IN (12, 34)
     * $query->filterByMinTimeBetweenEqualQuestions(array('min' => 12)); // WHERE min_time_between_equal_questions > 12
     * </code>
     *
     * @param     mixed $minTimeBetweenEqualQuestions The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function filterByMinTimeBetweenEqualQuestions($minTimeBetweenEqualQuestions = null, $comparison = null)
    {
        if (is_array($minTimeBetweenEqualQuestions)) {
            $useMinMax = false;
            if (isset($minTimeBetweenEqualQuestions['min'])) {
                $this->addUsingAlias(ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS, $minTimeBetweenEqualQuestions['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minTimeBetweenEqualQuestions['max'])) {
                $this->addUsingAlias(ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS, $minTimeBetweenEqualQuestions['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS, $minTimeBetweenEqualQuestions, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildConfig $config Object to remove from the list of results
     *
     * @return $this|ChildConfigQuery The current query, for fluid interface
     */
    public function prune($config = null)
    {
        if ($config) {
            $this->addUsingAlias(ConfigTableMap::COL_ID, $config->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the config table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConfigTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ConfigTableMap::clearInstancePool();
            ConfigTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConfigTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ConfigTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ConfigTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ConfigTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ConfigQuery
