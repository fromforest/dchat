<?php

namespace Base;

use \ConfigQuery as ChildConfigQuery;
use \Exception;
use \PDO;
use Map\ConfigTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'config' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class Config implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\ConfigTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the quiz_mode field.
     *
     * Note: this column has a database default value of: 'regular'
     * @var        string
     */
    protected $quiz_mode;

    /**
     * The value for the question_time field.
     *
     * Note: this column has a database default value of: 30
     * @var        int
     */
    protected $question_time;

    /**
     * The value for the answer_time field.
     *
     * Note: this column has a database default value of: 7
     * @var        int
     */
    protected $answer_time;

    /**
     * The value for the announced_right_users field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $announced_right_users;

    /**
     * The value for the challenge_updated field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $challenge_updated;

    /**
     * The value for the packages field.
     *
     * Note: this column has a database default value of: 'tp1:tp2:tp18'
     * @var        string
     */
    protected $packages;

    /**
     * The value for the black_list field.
     *
     * Note: this column has a database default value of: 'tp18:temp_uploaded'
     * @var        string
     */
    protected $black_list;

    /**
     * The value for the difficulty_sequence field.
     *
     * Note: this column has a database default value of: 'easy:medium:easy:hard'
     * @var        string
     */
    protected $difficulty_sequence;

    /**
     * The value for the min_users_for_updates field.
     *
     * Note: this column has a database default value of: 5
     * @var        int
     */
    protected $min_users_for_updates;

    /**
     * The value for the tqi field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $tqi;

    /**
     * The value for the tournament_number field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $tournament_number;

    /**
     * The value for the temp_uploaded_question_id field.
     *
     * Note: this column has a database default value of: 1000000
     * @var        int
     */
    protected $temp_uploaded_question_id;

    /**
     * The value for the reserve_string field.
     *
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $reserve_string;

    /**
     * The value for the reserve_int field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $reserve_int;

    /**
     * The value for the reserve5 field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $reserve5;

    /**
     * The value for the min_time_between_equal_questions field.
     *
     * Note: this column has a database default value of: 360
     * @var        int
     */
    protected $min_time_between_equal_questions;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->quiz_mode = 'regular';
        $this->question_time = 30;
        $this->answer_time = 7;
        $this->announced_right_users = 1;
        $this->challenge_updated = 0;
        $this->packages = 'tp1:tp2:tp18';
        $this->black_list = 'tp18:temp_uploaded';
        $this->difficulty_sequence = 'easy:medium:easy:hard';
        $this->min_users_for_updates = 5;
        $this->tqi = 0;
        $this->tournament_number = '';
        $this->temp_uploaded_question_id = 1000000;
        $this->reserve_string = '0';
        $this->reserve_int = 0;
        $this->reserve5 = 0;
        $this->min_time_between_equal_questions = 360;
    }

    /**
     * Initializes internal state of Base\Config object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Config</code> instance.  If
     * <code>obj</code> is an instance of <code>Config</code>, delegates to
     * <code>equals(Config)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Config The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [quiz_mode] column value.
     *
     * @return string
     */
    public function getQuizMode()
    {
        return $this->quiz_mode;
    }

    /**
     * Get the [question_time] column value.
     *
     * @return int
     */
    public function getQuestionTime()
    {
        return $this->question_time;
    }

    /**
     * Get the [answer_time] column value.
     *
     * @return int
     */
    public function getAnswerTime()
    {
        return $this->answer_time;
    }

    /**
     * Get the [announced_right_users] column value.
     *
     * @return int
     */
    public function getAnnouncedRightUsers()
    {
        return $this->announced_right_users;
    }

    /**
     * Get the [challenge_updated] column value.
     *
     * @return int
     */
    public function getChallengeUpdated()
    {
        return $this->challenge_updated;
    }

    /**
     * Get the [packages] column value.
     *
     * @return string
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * Get the [black_list] column value.
     *
     * @return string
     */
    public function getBlackList()
    {
        return $this->black_list;
    }

    /**
     * Get the [difficulty_sequence] column value.
     *
     * @return string
     */
    public function getDifficultySequence()
    {
        return $this->difficulty_sequence;
    }

    /**
     * Get the [min_users_for_updates] column value.
     *
     * @return int
     */
    public function getMinUsersForUpdates()
    {
        return $this->min_users_for_updates;
    }

    /**
     * Get the [tqi] column value.
     *
     * @return int
     */
    public function getTqi()
    {
        return $this->tqi;
    }

    /**
     * Get the [tournament_number] column value.
     *
     * @return string
     */
    public function getTournamentNumber()
    {
        return $this->tournament_number;
    }

    /**
     * Get the [temp_uploaded_question_id] column value.
     *
     * @return int
     */
    public function getTempUploadedQuestionId()
    {
        return $this->temp_uploaded_question_id;
    }

    /**
     * Get the [reserve_string] column value.
     *
     * @return string
     */
    public function getReserveString()
    {
        return $this->reserve_string;
    }

    /**
     * Get the [reserve_int] column value.
     *
     * @return int
     */
    public function getReserveInt()
    {
        return $this->reserve_int;
    }

    /**
     * Get the [reserve5] column value.
     *
     * @return int
     */
    public function getReserve5()
    {
        return $this->reserve5;
    }

    /**
     * Get the [min_time_between_equal_questions] column value.
     *
     * @return int
     */
    public function getMinTimeBetweenEqualQuestions()
    {
        return $this->min_time_between_equal_questions;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[ConfigTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [quiz_mode] column.
     *
     * @param string $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setQuizMode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quiz_mode !== $v) {
            $this->quiz_mode = $v;
            $this->modifiedColumns[ConfigTableMap::COL_QUIZ_MODE] = true;
        }

        return $this;
    } // setQuizMode()

    /**
     * Set the value of [question_time] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setQuestionTime($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->question_time !== $v) {
            $this->question_time = $v;
            $this->modifiedColumns[ConfigTableMap::COL_QUESTION_TIME] = true;
        }

        return $this;
    } // setQuestionTime()

    /**
     * Set the value of [answer_time] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setAnswerTime($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->answer_time !== $v) {
            $this->answer_time = $v;
            $this->modifiedColumns[ConfigTableMap::COL_ANSWER_TIME] = true;
        }

        return $this;
    } // setAnswerTime()

    /**
     * Set the value of [announced_right_users] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setAnnouncedRightUsers($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->announced_right_users !== $v) {
            $this->announced_right_users = $v;
            $this->modifiedColumns[ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS] = true;
        }

        return $this;
    } // setAnnouncedRightUsers()

    /**
     * Set the value of [challenge_updated] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setChallengeUpdated($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->challenge_updated !== $v) {
            $this->challenge_updated = $v;
            $this->modifiedColumns[ConfigTableMap::COL_CHALLENGE_UPDATED] = true;
        }

        return $this;
    } // setChallengeUpdated()

    /**
     * Set the value of [packages] column.
     *
     * @param string $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setPackages($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->packages !== $v) {
            $this->packages = $v;
            $this->modifiedColumns[ConfigTableMap::COL_PACKAGES] = true;
        }

        return $this;
    } // setPackages()

    /**
     * Set the value of [black_list] column.
     *
     * @param string $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setBlackList($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->black_list !== $v) {
            $this->black_list = $v;
            $this->modifiedColumns[ConfigTableMap::COL_BLACK_LIST] = true;
        }

        return $this;
    } // setBlackList()

    /**
     * Set the value of [difficulty_sequence] column.
     *
     * @param string $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setDifficultySequence($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->difficulty_sequence !== $v) {
            $this->difficulty_sequence = $v;
            $this->modifiedColumns[ConfigTableMap::COL_DIFFICULTY_SEQUENCE] = true;
        }

        return $this;
    } // setDifficultySequence()

    /**
     * Set the value of [min_users_for_updates] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setMinUsersForUpdates($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->min_users_for_updates !== $v) {
            $this->min_users_for_updates = $v;
            $this->modifiedColumns[ConfigTableMap::COL_MIN_USERS_FOR_UPDATES] = true;
        }

        return $this;
    } // setMinUsersForUpdates()

    /**
     * Set the value of [tqi] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setTqi($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->tqi !== $v) {
            $this->tqi = $v;
            $this->modifiedColumns[ConfigTableMap::COL_TQI] = true;
        }

        return $this;
    } // setTqi()

    /**
     * Set the value of [tournament_number] column.
     *
     * @param string $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setTournamentNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tournament_number !== $v) {
            $this->tournament_number = $v;
            $this->modifiedColumns[ConfigTableMap::COL_TOURNAMENT_NUMBER] = true;
        }

        return $this;
    } // setTournamentNumber()

    /**
     * Set the value of [temp_uploaded_question_id] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setTempUploadedQuestionId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->temp_uploaded_question_id !== $v) {
            $this->temp_uploaded_question_id = $v;
            $this->modifiedColumns[ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID] = true;
        }

        return $this;
    } // setTempUploadedQuestionId()

    /**
     * Set the value of [reserve_string] column.
     *
     * @param string $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setReserveString($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->reserve_string !== $v) {
            $this->reserve_string = $v;
            $this->modifiedColumns[ConfigTableMap::COL_RESERVE_STRING] = true;
        }

        return $this;
    } // setReserveString()

    /**
     * Set the value of [reserve_int] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setReserveInt($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->reserve_int !== $v) {
            $this->reserve_int = $v;
            $this->modifiedColumns[ConfigTableMap::COL_RESERVE_INT] = true;
        }

        return $this;
    } // setReserveInt()

    /**
     * Set the value of [reserve5] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setReserve5($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->reserve5 !== $v) {
            $this->reserve5 = $v;
            $this->modifiedColumns[ConfigTableMap::COL_RESERVE5] = true;
        }

        return $this;
    } // setReserve5()

    /**
     * Set the value of [min_time_between_equal_questions] column.
     *
     * @param int $v new value
     * @return $this|\Config The current object (for fluent API support)
     */
    public function setMinTimeBetweenEqualQuestions($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->min_time_between_equal_questions !== $v) {
            $this->min_time_between_equal_questions = $v;
            $this->modifiedColumns[ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS] = true;
        }

        return $this;
    } // setMinTimeBetweenEqualQuestions()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->quiz_mode !== 'regular') {
                return false;
            }

            if ($this->question_time !== 30) {
                return false;
            }

            if ($this->answer_time !== 7) {
                return false;
            }

            if ($this->announced_right_users !== 1) {
                return false;
            }

            if ($this->challenge_updated !== 0) {
                return false;
            }

            if ($this->packages !== 'tp1:tp2:tp18') {
                return false;
            }

            if ($this->black_list !== 'tp18:temp_uploaded') {
                return false;
            }

            if ($this->difficulty_sequence !== 'easy:medium:easy:hard') {
                return false;
            }

            if ($this->min_users_for_updates !== 5) {
                return false;
            }

            if ($this->tqi !== 0) {
                return false;
            }

            if ($this->tournament_number !== '') {
                return false;
            }

            if ($this->temp_uploaded_question_id !== 1000000) {
                return false;
            }

            if ($this->reserve_string !== '0') {
                return false;
            }

            if ($this->reserve_int !== 0) {
                return false;
            }

            if ($this->reserve5 !== 0) {
                return false;
            }

            if ($this->min_time_between_equal_questions !== 360) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ConfigTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ConfigTableMap::translateFieldName('QuizMode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->quiz_mode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ConfigTableMap::translateFieldName('QuestionTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->question_time = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ConfigTableMap::translateFieldName('AnswerTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->answer_time = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ConfigTableMap::translateFieldName('AnnouncedRightUsers', TableMap::TYPE_PHPNAME, $indexType)];
            $this->announced_right_users = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ConfigTableMap::translateFieldName('ChallengeUpdated', TableMap::TYPE_PHPNAME, $indexType)];
            $this->challenge_updated = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ConfigTableMap::translateFieldName('Packages', TableMap::TYPE_PHPNAME, $indexType)];
            $this->packages = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ConfigTableMap::translateFieldName('BlackList', TableMap::TYPE_PHPNAME, $indexType)];
            $this->black_list = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ConfigTableMap::translateFieldName('DifficultySequence', TableMap::TYPE_PHPNAME, $indexType)];
            $this->difficulty_sequence = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ConfigTableMap::translateFieldName('MinUsersForUpdates', TableMap::TYPE_PHPNAME, $indexType)];
            $this->min_users_for_updates = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ConfigTableMap::translateFieldName('Tqi', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tqi = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ConfigTableMap::translateFieldName('TournamentNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tournament_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ConfigTableMap::translateFieldName('TempUploadedQuestionId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->temp_uploaded_question_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ConfigTableMap::translateFieldName('ReserveString', TableMap::TYPE_PHPNAME, $indexType)];
            $this->reserve_string = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ConfigTableMap::translateFieldName('ReserveInt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->reserve_int = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ConfigTableMap::translateFieldName('Reserve5', TableMap::TYPE_PHPNAME, $indexType)];
            $this->reserve5 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ConfigTableMap::translateFieldName('MinTimeBetweenEqualQuestions', TableMap::TYPE_PHPNAME, $indexType)];
            $this->min_time_between_equal_questions = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 17; // 17 = ConfigTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Config'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ConfigTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildConfigQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Config::setDeleted()
     * @see Config::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConfigTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildConfigQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConfigTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ConfigTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ConfigTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_QUIZ_MODE)) {
            $modifiedColumns[':p' . $index++]  = 'quiz_mode';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_QUESTION_TIME)) {
            $modifiedColumns[':p' . $index++]  = 'question_time';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_ANSWER_TIME)) {
            $modifiedColumns[':p' . $index++]  = 'answer_time';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS)) {
            $modifiedColumns[':p' . $index++]  = 'announced_right_users';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_CHALLENGE_UPDATED)) {
            $modifiedColumns[':p' . $index++]  = 'challenge_updated';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_PACKAGES)) {
            $modifiedColumns[':p' . $index++]  = 'packages';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_BLACK_LIST)) {
            $modifiedColumns[':p' . $index++]  = 'black_list';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_DIFFICULTY_SEQUENCE)) {
            $modifiedColumns[':p' . $index++]  = 'difficulty_sequence';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_MIN_USERS_FOR_UPDATES)) {
            $modifiedColumns[':p' . $index++]  = 'min_users_for_updates';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_TQI)) {
            $modifiedColumns[':p' . $index++]  = 'tqi';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_TOURNAMENT_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'tournament_number';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID)) {
            $modifiedColumns[':p' . $index++]  = 'temp_uploaded_question_id';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_RESERVE_STRING)) {
            $modifiedColumns[':p' . $index++]  = 'reserve_string';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_RESERVE_INT)) {
            $modifiedColumns[':p' . $index++]  = 'reserve_int';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_RESERVE5)) {
            $modifiedColumns[':p' . $index++]  = 'reserve5';
        }
        if ($this->isColumnModified(ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS)) {
            $modifiedColumns[':p' . $index++]  = 'min_time_between_equal_questions';
        }

        $sql = sprintf(
            'INSERT INTO config (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'quiz_mode':
                        $stmt->bindValue($identifier, $this->quiz_mode, PDO::PARAM_STR);
                        break;
                    case 'question_time':
                        $stmt->bindValue($identifier, $this->question_time, PDO::PARAM_INT);
                        break;
                    case 'answer_time':
                        $stmt->bindValue($identifier, $this->answer_time, PDO::PARAM_INT);
                        break;
                    case 'announced_right_users':
                        $stmt->bindValue($identifier, $this->announced_right_users, PDO::PARAM_INT);
                        break;
                    case 'challenge_updated':
                        $stmt->bindValue($identifier, $this->challenge_updated, PDO::PARAM_INT);
                        break;
                    case 'packages':
                        $stmt->bindValue($identifier, $this->packages, PDO::PARAM_STR);
                        break;
                    case 'black_list':
                        $stmt->bindValue($identifier, $this->black_list, PDO::PARAM_STR);
                        break;
                    case 'difficulty_sequence':
                        $stmt->bindValue($identifier, $this->difficulty_sequence, PDO::PARAM_STR);
                        break;
                    case 'min_users_for_updates':
                        $stmt->bindValue($identifier, $this->min_users_for_updates, PDO::PARAM_INT);
                        break;
                    case 'tqi':
                        $stmt->bindValue($identifier, $this->tqi, PDO::PARAM_INT);
                        break;
                    case 'tournament_number':
                        $stmt->bindValue($identifier, $this->tournament_number, PDO::PARAM_STR);
                        break;
                    case 'temp_uploaded_question_id':
                        $stmt->bindValue($identifier, $this->temp_uploaded_question_id, PDO::PARAM_INT);
                        break;
                    case 'reserve_string':
                        $stmt->bindValue($identifier, $this->reserve_string, PDO::PARAM_STR);
                        break;
                    case 'reserve_int':
                        $stmt->bindValue($identifier, $this->reserve_int, PDO::PARAM_INT);
                        break;
                    case 'reserve5':
                        $stmt->bindValue($identifier, $this->reserve5, PDO::PARAM_INT);
                        break;
                    case 'min_time_between_equal_questions':
                        $stmt->bindValue($identifier, $this->min_time_between_equal_questions, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ConfigTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getQuizMode();
                break;
            case 2:
                return $this->getQuestionTime();
                break;
            case 3:
                return $this->getAnswerTime();
                break;
            case 4:
                return $this->getAnnouncedRightUsers();
                break;
            case 5:
                return $this->getChallengeUpdated();
                break;
            case 6:
                return $this->getPackages();
                break;
            case 7:
                return $this->getBlackList();
                break;
            case 8:
                return $this->getDifficultySequence();
                break;
            case 9:
                return $this->getMinUsersForUpdates();
                break;
            case 10:
                return $this->getTqi();
                break;
            case 11:
                return $this->getTournamentNumber();
                break;
            case 12:
                return $this->getTempUploadedQuestionId();
                break;
            case 13:
                return $this->getReserveString();
                break;
            case 14:
                return $this->getReserveInt();
                break;
            case 15:
                return $this->getReserve5();
                break;
            case 16:
                return $this->getMinTimeBetweenEqualQuestions();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['Config'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Config'][$this->hashCode()] = true;
        $keys = ConfigTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getQuizMode(),
            $keys[2] => $this->getQuestionTime(),
            $keys[3] => $this->getAnswerTime(),
            $keys[4] => $this->getAnnouncedRightUsers(),
            $keys[5] => $this->getChallengeUpdated(),
            $keys[6] => $this->getPackages(),
            $keys[7] => $this->getBlackList(),
            $keys[8] => $this->getDifficultySequence(),
            $keys[9] => $this->getMinUsersForUpdates(),
            $keys[10] => $this->getTqi(),
            $keys[11] => $this->getTournamentNumber(),
            $keys[12] => $this->getTempUploadedQuestionId(),
            $keys[13] => $this->getReserveString(),
            $keys[14] => $this->getReserveInt(),
            $keys[15] => $this->getReserve5(),
            $keys[16] => $this->getMinTimeBetweenEqualQuestions(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Config
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ConfigTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Config
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setQuizMode($value);
                break;
            case 2:
                $this->setQuestionTime($value);
                break;
            case 3:
                $this->setAnswerTime($value);
                break;
            case 4:
                $this->setAnnouncedRightUsers($value);
                break;
            case 5:
                $this->setChallengeUpdated($value);
                break;
            case 6:
                $this->setPackages($value);
                break;
            case 7:
                $this->setBlackList($value);
                break;
            case 8:
                $this->setDifficultySequence($value);
                break;
            case 9:
                $this->setMinUsersForUpdates($value);
                break;
            case 10:
                $this->setTqi($value);
                break;
            case 11:
                $this->setTournamentNumber($value);
                break;
            case 12:
                $this->setTempUploadedQuestionId($value);
                break;
            case 13:
                $this->setReserveString($value);
                break;
            case 14:
                $this->setReserveInt($value);
                break;
            case 15:
                $this->setReserve5($value);
                break;
            case 16:
                $this->setMinTimeBetweenEqualQuestions($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ConfigTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setQuizMode($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setQuestionTime($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAnswerTime($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setAnnouncedRightUsers($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setChallengeUpdated($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPackages($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setBlackList($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDifficultySequence($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setMinUsersForUpdates($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setTqi($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setTournamentNumber($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setTempUploadedQuestionId($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setReserveString($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setReserveInt($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setReserve5($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setMinTimeBetweenEqualQuestions($arr[$keys[16]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Config The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ConfigTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ConfigTableMap::COL_ID)) {
            $criteria->add(ConfigTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_QUIZ_MODE)) {
            $criteria->add(ConfigTableMap::COL_QUIZ_MODE, $this->quiz_mode);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_QUESTION_TIME)) {
            $criteria->add(ConfigTableMap::COL_QUESTION_TIME, $this->question_time);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_ANSWER_TIME)) {
            $criteria->add(ConfigTableMap::COL_ANSWER_TIME, $this->answer_time);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS)) {
            $criteria->add(ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS, $this->announced_right_users);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_CHALLENGE_UPDATED)) {
            $criteria->add(ConfigTableMap::COL_CHALLENGE_UPDATED, $this->challenge_updated);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_PACKAGES)) {
            $criteria->add(ConfigTableMap::COL_PACKAGES, $this->packages);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_BLACK_LIST)) {
            $criteria->add(ConfigTableMap::COL_BLACK_LIST, $this->black_list);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_DIFFICULTY_SEQUENCE)) {
            $criteria->add(ConfigTableMap::COL_DIFFICULTY_SEQUENCE, $this->difficulty_sequence);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_MIN_USERS_FOR_UPDATES)) {
            $criteria->add(ConfigTableMap::COL_MIN_USERS_FOR_UPDATES, $this->min_users_for_updates);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_TQI)) {
            $criteria->add(ConfigTableMap::COL_TQI, $this->tqi);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_TOURNAMENT_NUMBER)) {
            $criteria->add(ConfigTableMap::COL_TOURNAMENT_NUMBER, $this->tournament_number);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID)) {
            $criteria->add(ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID, $this->temp_uploaded_question_id);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_RESERVE_STRING)) {
            $criteria->add(ConfigTableMap::COL_RESERVE_STRING, $this->reserve_string);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_RESERVE_INT)) {
            $criteria->add(ConfigTableMap::COL_RESERVE_INT, $this->reserve_int);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_RESERVE5)) {
            $criteria->add(ConfigTableMap::COL_RESERVE5, $this->reserve5);
        }
        if ($this->isColumnModified(ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS)) {
            $criteria->add(ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS, $this->min_time_between_equal_questions);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildConfigQuery::create();
        $criteria->add(ConfigTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Config (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setQuizMode($this->getQuizMode());
        $copyObj->setQuestionTime($this->getQuestionTime());
        $copyObj->setAnswerTime($this->getAnswerTime());
        $copyObj->setAnnouncedRightUsers($this->getAnnouncedRightUsers());
        $copyObj->setChallengeUpdated($this->getChallengeUpdated());
        $copyObj->setPackages($this->getPackages());
        $copyObj->setBlackList($this->getBlackList());
        $copyObj->setDifficultySequence($this->getDifficultySequence());
        $copyObj->setMinUsersForUpdates($this->getMinUsersForUpdates());
        $copyObj->setTqi($this->getTqi());
        $copyObj->setTournamentNumber($this->getTournamentNumber());
        $copyObj->setTempUploadedQuestionId($this->getTempUploadedQuestionId());
        $copyObj->setReserveString($this->getReserveString());
        $copyObj->setReserveInt($this->getReserveInt());
        $copyObj->setReserve5($this->getReserve5());
        $copyObj->setMinTimeBetweenEqualQuestions($this->getMinTimeBetweenEqualQuestions());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Config Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->quiz_mode = null;
        $this->question_time = null;
        $this->answer_time = null;
        $this->announced_right_users = null;
        $this->challenge_updated = null;
        $this->packages = null;
        $this->black_list = null;
        $this->difficulty_sequence = null;
        $this->min_users_for_updates = null;
        $this->tqi = null;
        $this->tournament_number = null;
        $this->temp_uploaded_question_id = null;
        $this->reserve_string = null;
        $this->reserve_int = null;
        $this->reserve5 = null;
        $this->min_time_between_equal_questions = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ConfigTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
