<?php

namespace Base;

use \Challenge as ChildChallenge;
use \ChallengeQuery as ChildChallengeQuery;
use \Package as ChildPackage;
use \PackageQuery as ChildPackageQuery;
use \Question as ChildQuestion;
use \QuestionQuery as ChildQuestionQuery;
use \SinglePlayerAnswer as ChildSinglePlayerAnswer;
use \SinglePlayerAnswerQuery as ChildSinglePlayerAnswerQuery;
use \TextSinglePlayerAnswer as ChildTextSinglePlayerAnswer;
use \TextSinglePlayerAnswerQuery as ChildTextSinglePlayerAnswerQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\ChallengeTableMap;
use Map\QuestionTableMap;
use Map\SinglePlayerAnswerTableMap;
use Map\TextSinglePlayerAnswerTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;
use Symfony\Component\Translation\IdentityTranslator;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Context\ExecutionContextFactory;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Mapping\Loader\StaticMethodLoader;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Base class that represents a row from the 'questions' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class Question implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\QuestionTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the package_id field.
     *
     * @var        int
     */
    protected $package_id;

    /**
     * The value for the type field.
     *
     * Note: this column has a database default value of: 'image'
     * @var        string
     */
    protected $type;

    /**
     * The value for the topic field.
     *
     * @var        string
     */
    protected $topic;

    /**
     * The value for the text field.
     *
     * @var        string
     */
    protected $text;

    /**
     * The value for the picture_url field.
     *
     * @var        string
     */
    protected $picture_url;

    /**
     * The value for the answer_picture_url field.
     *
     * @var        string
     */
    protected $answer_picture_url;

    /**
     * The value for the right_answer field.
     *
     * @var        string
     */
    protected $right_answer;

    /**
     * The value for the alt_answers field.
     *
     * @var        string
     */
    protected $alt_answers;

    /**
     * The value for the author field.
     *
     * @var        string
     */
    protected $author;

    /**
     * The value for the tag1 field.
     *
     * @var        string
     */
    protected $tag1;

    /**
     * The value for the tag2 field.
     *
     * @var        string
     */
    protected $tag2;

    /**
     * The value for the comment field.
     *
     * @var        string
     */
    protected $comment;

    /**
     * The value for the num_asked field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $num_asked;

    /**
     * The value for the num_answered field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $num_answered;

    /**
     * The value for the created_at field.
     *
     * @var        \DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        \DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildPackage
     */
    protected $aPackage;

    /**
     * @var        ObjectCollection|ChildChallenge[] Collection to store aggregation of ChildChallenge objects.
     */
    protected $collChallenges;
    protected $collChallengesPartial;

    /**
     * @var        ObjectCollection|ChildSinglePlayerAnswer[] Collection to store aggregation of ChildSinglePlayerAnswer objects.
     */
    protected $collSinglePlayerAnswers;
    protected $collSinglePlayerAnswersPartial;

    /**
     * @var        ObjectCollection|ChildTextSinglePlayerAnswer[] Collection to store aggregation of ChildTextSinglePlayerAnswer objects.
     */
    protected $collTextSinglePlayerAnswers;
    protected $collTextSinglePlayerAnswersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // validate behavior

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * ConstraintViolationList object
     *
     * @see     http://api.symfony.com/2.0/Symfony/Component/Validator/ConstraintViolationList.html
     * @var     ConstraintViolationList
     */
    protected $validationFailures;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildChallenge[]
     */
    protected $challengesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSinglePlayerAnswer[]
     */
    protected $singlePlayerAnswersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTextSinglePlayerAnswer[]
     */
    protected $textSinglePlayerAnswersScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->type = 'image';
        $this->num_asked = 0;
        $this->num_answered = 0;
    }

    /**
     * Initializes internal state of Base\Question object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Question</code> instance.  If
     * <code>obj</code> is an instance of <code>Question</code>, delegates to
     * <code>equals(Question)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Question The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [package_id] column value.
     *
     * @return int
     */
    public function getPackageId()
    {
        return $this->package_id;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [topic] column value.
     *
     * @return string
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Get the [text] column value.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Get the [picture_url] column value.
     *
     * @return string
     */
    public function getPictureUrl()
    {
        return $this->picture_url;
    }

    /**
     * Get the [answer_picture_url] column value.
     *
     * @return string
     */
    public function getAnswerPictureUrl()
    {
        return $this->answer_picture_url;
    }

    /**
     * Get the [right_answer] column value.
     *
     * @return string
     */
    public function getRightAnswer()
    {
        return $this->right_answer;
    }

    /**
     * Get the [alt_answers] column value.
     *
     * @return string
     */
    public function getAltAnswers()
    {
        return $this->alt_answers;
    }

    /**
     * Get the [author] column value.
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Get the [tag1] column value.
     *
     * @return string
     */
    public function getTag1()
    {
        return $this->tag1;
    }

    /**
     * Get the [tag2] column value.
     *
     * @return string
     */
    public function getTag2()
    {
        return $this->tag2;
    }

    /**
     * Get the [comment] column value.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Get the [num_asked] column value.
     *
     * @return int
     */
    public function getNumAsked()
    {
        return $this->num_asked;
    }

    /**
     * Get the [num_answered] column value.
     *
     * @return int
     */
    public function getNumAnswered()
    {
        return $this->num_answered;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTime ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTime ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[QuestionTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [package_id] column.
     *
     * @param int $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setPackageId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->package_id !== $v) {
            $this->package_id = $v;
            $this->modifiedColumns[QuestionTableMap::COL_PACKAGE_ID] = true;
        }

        if ($this->aPackage !== null && $this->aPackage->getId() !== $v) {
            $this->aPackage = null;
        }

        return $this;
    } // setPackageId()

    /**
     * Set the value of [type] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[QuestionTableMap::COL_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Set the value of [topic] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setTopic($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->topic !== $v) {
            $this->topic = $v;
            $this->modifiedColumns[QuestionTableMap::COL_TOPIC] = true;
        }

        return $this;
    } // setTopic()

    /**
     * Set the value of [text] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->text !== $v) {
            $this->text = $v;
            $this->modifiedColumns[QuestionTableMap::COL_TEXT] = true;
        }

        return $this;
    } // setText()

    /**
     * Set the value of [picture_url] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setPictureUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->picture_url !== $v) {
            $this->picture_url = $v;
            $this->modifiedColumns[QuestionTableMap::COL_PICTURE_URL] = true;
        }

        return $this;
    } // setPictureUrl()

    /**
     * Set the value of [answer_picture_url] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setAnswerPictureUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->answer_picture_url !== $v) {
            $this->answer_picture_url = $v;
            $this->modifiedColumns[QuestionTableMap::COL_ANSWER_PICTURE_URL] = true;
        }

        return $this;
    } // setAnswerPictureUrl()

    /**
     * Set the value of [right_answer] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setRightAnswer($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->right_answer !== $v) {
            $this->right_answer = $v;
            $this->modifiedColumns[QuestionTableMap::COL_RIGHT_ANSWER] = true;
        }

        return $this;
    } // setRightAnswer()

    /**
     * Set the value of [alt_answers] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setAltAnswers($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->alt_answers !== $v) {
            $this->alt_answers = $v;
            $this->modifiedColumns[QuestionTableMap::COL_ALT_ANSWERS] = true;
        }

        return $this;
    } // setAltAnswers()

    /**
     * Set the value of [author] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setAuthor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->author !== $v) {
            $this->author = $v;
            $this->modifiedColumns[QuestionTableMap::COL_AUTHOR] = true;
        }

        return $this;
    } // setAuthor()

    /**
     * Set the value of [tag1] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setTag1($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tag1 !== $v) {
            $this->tag1 = $v;
            $this->modifiedColumns[QuestionTableMap::COL_TAG1] = true;
        }

        return $this;
    } // setTag1()

    /**
     * Set the value of [tag2] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setTag2($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tag2 !== $v) {
            $this->tag2 = $v;
            $this->modifiedColumns[QuestionTableMap::COL_TAG2] = true;
        }

        return $this;
    } // setTag2()

    /**
     * Set the value of [comment] column.
     *
     * @param string $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setComment($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->comment !== $v) {
            $this->comment = $v;
            $this->modifiedColumns[QuestionTableMap::COL_COMMENT] = true;
        }

        return $this;
    } // setComment()

    /**
     * Set the value of [num_asked] column.
     *
     * @param int $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setNumAsked($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->num_asked !== $v) {
            $this->num_asked = $v;
            $this->modifiedColumns[QuestionTableMap::COL_NUM_ASKED] = true;
        }

        return $this;
    } // setNumAsked()

    /**
     * Set the value of [num_answered] column.
     *
     * @param int $v new value
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setNumAnswered($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->num_answered !== $v) {
            $this->num_answered = $v;
            $this->modifiedColumns[QuestionTableMap::COL_NUM_ANSWERED] = true;
        }

        return $this;
    } // setNumAnswered()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->created_at->format("Y-m-d H:i:s")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[QuestionTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\Question The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->updated_at->format("Y-m-d H:i:s")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[QuestionTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->type !== 'image') {
                return false;
            }

            if ($this->num_asked !== 0) {
                return false;
            }

            if ($this->num_answered !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : QuestionTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : QuestionTableMap::translateFieldName('PackageId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->package_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : QuestionTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : QuestionTableMap::translateFieldName('Topic', TableMap::TYPE_PHPNAME, $indexType)];
            $this->topic = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : QuestionTableMap::translateFieldName('Text', TableMap::TYPE_PHPNAME, $indexType)];
            $this->text = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : QuestionTableMap::translateFieldName('PictureUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->picture_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : QuestionTableMap::translateFieldName('AnswerPictureUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->answer_picture_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : QuestionTableMap::translateFieldName('RightAnswer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->right_answer = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : QuestionTableMap::translateFieldName('AltAnswers', TableMap::TYPE_PHPNAME, $indexType)];
            $this->alt_answers = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : QuestionTableMap::translateFieldName('Author', TableMap::TYPE_PHPNAME, $indexType)];
            $this->author = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : QuestionTableMap::translateFieldName('Tag1', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tag1 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : QuestionTableMap::translateFieldName('Tag2', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tag2 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : QuestionTableMap::translateFieldName('Comment', TableMap::TYPE_PHPNAME, $indexType)];
            $this->comment = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : QuestionTableMap::translateFieldName('NumAsked', TableMap::TYPE_PHPNAME, $indexType)];
            $this->num_asked = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : QuestionTableMap::translateFieldName('NumAnswered', TableMap::TYPE_PHPNAME, $indexType)];
            $this->num_answered = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : QuestionTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : QuestionTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 17; // 17 = QuestionTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Question'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aPackage !== null && $this->package_id !== $this->aPackage->getId()) {
            $this->aPackage = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(QuestionTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildQuestionQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPackage = null;
            $this->collChallenges = null;

            $this->collSinglePlayerAnswers = null;

            $this->collTextSinglePlayerAnswers = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Question::setDeleted()
     * @see Question::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(QuestionTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildQuestionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(QuestionTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(QuestionTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(QuestionTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(QuestionTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                QuestionTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPackage !== null) {
                if ($this->aPackage->isModified() || $this->aPackage->isNew()) {
                    $affectedRows += $this->aPackage->save($con);
                }
                $this->setPackage($this->aPackage);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->challengesScheduledForDeletion !== null) {
                if (!$this->challengesScheduledForDeletion->isEmpty()) {
                    \ChallengeQuery::create()
                        ->filterByPrimaryKeys($this->challengesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->challengesScheduledForDeletion = null;
                }
            }

            if ($this->collChallenges !== null) {
                foreach ($this->collChallenges as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->singlePlayerAnswersScheduledForDeletion !== null) {
                if (!$this->singlePlayerAnswersScheduledForDeletion->isEmpty()) {
                    \SinglePlayerAnswerQuery::create()
                        ->filterByPrimaryKeys($this->singlePlayerAnswersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->singlePlayerAnswersScheduledForDeletion = null;
                }
            }

            if ($this->collSinglePlayerAnswers !== null) {
                foreach ($this->collSinglePlayerAnswers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->textSinglePlayerAnswersScheduledForDeletion !== null) {
                if (!$this->textSinglePlayerAnswersScheduledForDeletion->isEmpty()) {
                    \TextSinglePlayerAnswerQuery::create()
                        ->filterByPrimaryKeys($this->textSinglePlayerAnswersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->textSinglePlayerAnswersScheduledForDeletion = null;
                }
            }

            if ($this->collTextSinglePlayerAnswers !== null) {
                foreach ($this->collTextSinglePlayerAnswers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(QuestionTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_PACKAGE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'package_id';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'type';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TOPIC)) {
            $modifiedColumns[':p' . $index++]  = 'topic';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TEXT)) {
            $modifiedColumns[':p' . $index++]  = 'text';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_PICTURE_URL)) {
            $modifiedColumns[':p' . $index++]  = 'picture_url';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_ANSWER_PICTURE_URL)) {
            $modifiedColumns[':p' . $index++]  = 'answer_picture_url';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_RIGHT_ANSWER)) {
            $modifiedColumns[':p' . $index++]  = 'right_answer';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_ALT_ANSWERS)) {
            $modifiedColumns[':p' . $index++]  = 'alt_answers';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_AUTHOR)) {
            $modifiedColumns[':p' . $index++]  = 'author';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TAG1)) {
            $modifiedColumns[':p' . $index++]  = 'tag1';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TAG2)) {
            $modifiedColumns[':p' . $index++]  = 'tag2';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_COMMENT)) {
            $modifiedColumns[':p' . $index++]  = 'comment';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_NUM_ASKED)) {
            $modifiedColumns[':p' . $index++]  = 'num_asked';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_NUM_ANSWERED)) {
            $modifiedColumns[':p' . $index++]  = 'num_answered';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(QuestionTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO questions (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'package_id':
                        $stmt->bindValue($identifier, $this->package_id, PDO::PARAM_INT);
                        break;
                    case 'type':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case 'topic':
                        $stmt->bindValue($identifier, $this->topic, PDO::PARAM_STR);
                        break;
                    case 'text':
                        $stmt->bindValue($identifier, $this->text, PDO::PARAM_STR);
                        break;
                    case 'picture_url':
                        $stmt->bindValue($identifier, $this->picture_url, PDO::PARAM_STR);
                        break;
                    case 'answer_picture_url':
                        $stmt->bindValue($identifier, $this->answer_picture_url, PDO::PARAM_STR);
                        break;
                    case 'right_answer':
                        $stmt->bindValue($identifier, $this->right_answer, PDO::PARAM_STR);
                        break;
                    case 'alt_answers':
                        $stmt->bindValue($identifier, $this->alt_answers, PDO::PARAM_STR);
                        break;
                    case 'author':
                        $stmt->bindValue($identifier, $this->author, PDO::PARAM_STR);
                        break;
                    case 'tag1':
                        $stmt->bindValue($identifier, $this->tag1, PDO::PARAM_STR);
                        break;
                    case 'tag2':
                        $stmt->bindValue($identifier, $this->tag2, PDO::PARAM_STR);
                        break;
                    case 'comment':
                        $stmt->bindValue($identifier, $this->comment, PDO::PARAM_STR);
                        break;
                    case 'num_asked':
                        $stmt->bindValue($identifier, $this->num_asked, PDO::PARAM_INT);
                        break;
                    case 'num_answered':
                        $stmt->bindValue($identifier, $this->num_answered, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = QuestionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getPackageId();
                break;
            case 2:
                return $this->getType();
                break;
            case 3:
                return $this->getTopic();
                break;
            case 4:
                return $this->getText();
                break;
            case 5:
                return $this->getPictureUrl();
                break;
            case 6:
                return $this->getAnswerPictureUrl();
                break;
            case 7:
                return $this->getRightAnswer();
                break;
            case 8:
                return $this->getAltAnswers();
                break;
            case 9:
                return $this->getAuthor();
                break;
            case 10:
                return $this->getTag1();
                break;
            case 11:
                return $this->getTag2();
                break;
            case 12:
                return $this->getComment();
                break;
            case 13:
                return $this->getNumAsked();
                break;
            case 14:
                return $this->getNumAnswered();
                break;
            case 15:
                return $this->getCreatedAt();
                break;
            case 16:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Question'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Question'][$this->hashCode()] = true;
        $keys = QuestionTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getPackageId(),
            $keys[2] => $this->getType(),
            $keys[3] => $this->getTopic(),
            $keys[4] => $this->getText(),
            $keys[5] => $this->getPictureUrl(),
            $keys[6] => $this->getAnswerPictureUrl(),
            $keys[7] => $this->getRightAnswer(),
            $keys[8] => $this->getAltAnswers(),
            $keys[9] => $this->getAuthor(),
            $keys[10] => $this->getTag1(),
            $keys[11] => $this->getTag2(),
            $keys[12] => $this->getComment(),
            $keys[13] => $this->getNumAsked(),
            $keys[14] => $this->getNumAnswered(),
            $keys[15] => $this->getCreatedAt(),
            $keys[16] => $this->getUpdatedAt(),
        );
        if ($result[$keys[15]] instanceof \DateTime) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        if ($result[$keys[16]] instanceof \DateTime) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPackage) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'package';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'packages';
                        break;
                    default:
                        $key = 'Package';
                }

                $result[$key] = $this->aPackage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collChallenges) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'challenges';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'challengess';
                        break;
                    default:
                        $key = 'Challenges';
                }

                $result[$key] = $this->collChallenges->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSinglePlayerAnswers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'singlePlayerAnswers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'single_player_answerss';
                        break;
                    default:
                        $key = 'SinglePlayerAnswers';
                }

                $result[$key] = $this->collSinglePlayerAnswers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTextSinglePlayerAnswers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'textSinglePlayerAnswers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'text_single_player_answerss';
                        break;
                    default:
                        $key = 'TextSinglePlayerAnswers';
                }

                $result[$key] = $this->collTextSinglePlayerAnswers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Question
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = QuestionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Question
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setPackageId($value);
                break;
            case 2:
                $this->setType($value);
                break;
            case 3:
                $this->setTopic($value);
                break;
            case 4:
                $this->setText($value);
                break;
            case 5:
                $this->setPictureUrl($value);
                break;
            case 6:
                $this->setAnswerPictureUrl($value);
                break;
            case 7:
                $this->setRightAnswer($value);
                break;
            case 8:
                $this->setAltAnswers($value);
                break;
            case 9:
                $this->setAuthor($value);
                break;
            case 10:
                $this->setTag1($value);
                break;
            case 11:
                $this->setTag2($value);
                break;
            case 12:
                $this->setComment($value);
                break;
            case 13:
                $this->setNumAsked($value);
                break;
            case 14:
                $this->setNumAnswered($value);
                break;
            case 15:
                $this->setCreatedAt($value);
                break;
            case 16:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = QuestionTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPackageId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setType($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTopic($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setText($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPictureUrl($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setAnswerPictureUrl($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setRightAnswer($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setAltAnswers($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setAuthor($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setTag1($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setTag2($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setComment($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setNumAsked($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setNumAnswered($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setCreatedAt($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setUpdatedAt($arr[$keys[16]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Question The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(QuestionTableMap::DATABASE_NAME);

        if ($this->isColumnModified(QuestionTableMap::COL_ID)) {
            $criteria->add(QuestionTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_PACKAGE_ID)) {
            $criteria->add(QuestionTableMap::COL_PACKAGE_ID, $this->package_id);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TYPE)) {
            $criteria->add(QuestionTableMap::COL_TYPE, $this->type);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TOPIC)) {
            $criteria->add(QuestionTableMap::COL_TOPIC, $this->topic);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TEXT)) {
            $criteria->add(QuestionTableMap::COL_TEXT, $this->text);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_PICTURE_URL)) {
            $criteria->add(QuestionTableMap::COL_PICTURE_URL, $this->picture_url);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_ANSWER_PICTURE_URL)) {
            $criteria->add(QuestionTableMap::COL_ANSWER_PICTURE_URL, $this->answer_picture_url);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_RIGHT_ANSWER)) {
            $criteria->add(QuestionTableMap::COL_RIGHT_ANSWER, $this->right_answer);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_ALT_ANSWERS)) {
            $criteria->add(QuestionTableMap::COL_ALT_ANSWERS, $this->alt_answers);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_AUTHOR)) {
            $criteria->add(QuestionTableMap::COL_AUTHOR, $this->author);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TAG1)) {
            $criteria->add(QuestionTableMap::COL_TAG1, $this->tag1);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_TAG2)) {
            $criteria->add(QuestionTableMap::COL_TAG2, $this->tag2);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_COMMENT)) {
            $criteria->add(QuestionTableMap::COL_COMMENT, $this->comment);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_NUM_ASKED)) {
            $criteria->add(QuestionTableMap::COL_NUM_ASKED, $this->num_asked);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_NUM_ANSWERED)) {
            $criteria->add(QuestionTableMap::COL_NUM_ANSWERED, $this->num_answered);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_CREATED_AT)) {
            $criteria->add(QuestionTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(QuestionTableMap::COL_UPDATED_AT)) {
            $criteria->add(QuestionTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildQuestionQuery::create();
        $criteria->add(QuestionTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Question (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setPackageId($this->getPackageId());
        $copyObj->setType($this->getType());
        $copyObj->setTopic($this->getTopic());
        $copyObj->setText($this->getText());
        $copyObj->setPictureUrl($this->getPictureUrl());
        $copyObj->setAnswerPictureUrl($this->getAnswerPictureUrl());
        $copyObj->setRightAnswer($this->getRightAnswer());
        $copyObj->setAltAnswers($this->getAltAnswers());
        $copyObj->setAuthor($this->getAuthor());
        $copyObj->setTag1($this->getTag1());
        $copyObj->setTag2($this->getTag2());
        $copyObj->setComment($this->getComment());
        $copyObj->setNumAsked($this->getNumAsked());
        $copyObj->setNumAnswered($this->getNumAnswered());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getChallenges() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChallenge($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSinglePlayerAnswers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSinglePlayerAnswer($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTextSinglePlayerAnswers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTextSinglePlayerAnswer($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Question Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildPackage object.
     *
     * @param  ChildPackage $v
     * @return $this|\Question The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPackage(ChildPackage $v = null)
    {
        if ($v === null) {
            $this->setPackageId(NULL);
        } else {
            $this->setPackageId($v->getId());
        }

        $this->aPackage = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPackage object, it will not be re-added.
        if ($v !== null) {
            $v->addQuestion($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPackage object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPackage The associated ChildPackage object.
     * @throws PropelException
     */
    public function getPackage(ConnectionInterface $con = null)
    {
        if ($this->aPackage === null && ($this->package_id !== null)) {
            $this->aPackage = ChildPackageQuery::create()->findPk($this->package_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPackage->addQuestions($this);
             */
        }

        return $this->aPackage;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Challenge' == $relationName) {
            return $this->initChallenges();
        }
        if ('SinglePlayerAnswer' == $relationName) {
            return $this->initSinglePlayerAnswers();
        }
        if ('TextSinglePlayerAnswer' == $relationName) {
            return $this->initTextSinglePlayerAnswers();
        }
    }

    /**
     * Clears out the collChallenges collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addChallenges()
     */
    public function clearChallenges()
    {
        $this->collChallenges = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collChallenges collection loaded partially.
     */
    public function resetPartialChallenges($v = true)
    {
        $this->collChallengesPartial = $v;
    }

    /**
     * Initializes the collChallenges collection.
     *
     * By default this just sets the collChallenges collection to an empty array (like clearcollChallenges());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChallenges($overrideExisting = true)
    {
        if (null !== $this->collChallenges && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChallengeTableMap::getTableMap()->getCollectionClassName();

        $this->collChallenges = new $collectionClassName;
        $this->collChallenges->setModel('\Challenge');
    }

    /**
     * Gets an array of ChildChallenge objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildQuestion is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildChallenge[] List of ChildChallenge objects
     * @throws PropelException
     */
    public function getChallenges(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collChallengesPartial && !$this->isNew();
        if (null === $this->collChallenges || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChallenges) {
                // return empty collection
                $this->initChallenges();
            } else {
                $collChallenges = ChildChallengeQuery::create(null, $criteria)
                    ->filterByQuestion($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collChallengesPartial && count($collChallenges)) {
                        $this->initChallenges(false);

                        foreach ($collChallenges as $obj) {
                            if (false == $this->collChallenges->contains($obj)) {
                                $this->collChallenges->append($obj);
                            }
                        }

                        $this->collChallengesPartial = true;
                    }

                    return $collChallenges;
                }

                if ($partial && $this->collChallenges) {
                    foreach ($this->collChallenges as $obj) {
                        if ($obj->isNew()) {
                            $collChallenges[] = $obj;
                        }
                    }
                }

                $this->collChallenges = $collChallenges;
                $this->collChallengesPartial = false;
            }
        }

        return $this->collChallenges;
    }

    /**
     * Sets a collection of ChildChallenge objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $challenges A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildQuestion The current object (for fluent API support)
     */
    public function setChallenges(Collection $challenges, ConnectionInterface $con = null)
    {
        /** @var ChildChallenge[] $challengesToDelete */
        $challengesToDelete = $this->getChallenges(new Criteria(), $con)->diff($challenges);


        $this->challengesScheduledForDeletion = $challengesToDelete;

        foreach ($challengesToDelete as $challengeRemoved) {
            $challengeRemoved->setQuestion(null);
        }

        $this->collChallenges = null;
        foreach ($challenges as $challenge) {
            $this->addChallenge($challenge);
        }

        $this->collChallenges = $challenges;
        $this->collChallengesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Challenge objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Challenge objects.
     * @throws PropelException
     */
    public function countChallenges(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collChallengesPartial && !$this->isNew();
        if (null === $this->collChallenges || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChallenges) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getChallenges());
            }

            $query = ChildChallengeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestion($this)
                ->count($con);
        }

        return count($this->collChallenges);
    }

    /**
     * Method called to associate a ChildChallenge object to this object
     * through the ChildChallenge foreign key attribute.
     *
     * @param  ChildChallenge $l ChildChallenge
     * @return $this|\Question The current object (for fluent API support)
     */
    public function addChallenge(ChildChallenge $l)
    {
        if ($this->collChallenges === null) {
            $this->initChallenges();
            $this->collChallengesPartial = true;
        }

        if (!$this->collChallenges->contains($l)) {
            $this->doAddChallenge($l);

            if ($this->challengesScheduledForDeletion and $this->challengesScheduledForDeletion->contains($l)) {
                $this->challengesScheduledForDeletion->remove($this->challengesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildChallenge $challenge The ChildChallenge object to add.
     */
    protected function doAddChallenge(ChildChallenge $challenge)
    {
        $this->collChallenges[]= $challenge;
        $challenge->setQuestion($this);
    }

    /**
     * @param  ChildChallenge $challenge The ChildChallenge object to remove.
     * @return $this|ChildQuestion The current object (for fluent API support)
     */
    public function removeChallenge(ChildChallenge $challenge)
    {
        if ($this->getChallenges()->contains($challenge)) {
            $pos = $this->collChallenges->search($challenge);
            $this->collChallenges->remove($pos);
            if (null === $this->challengesScheduledForDeletion) {
                $this->challengesScheduledForDeletion = clone $this->collChallenges;
                $this->challengesScheduledForDeletion->clear();
            }
            $this->challengesScheduledForDeletion[]= clone $challenge;
            $challenge->setQuestion(null);
        }

        return $this;
    }

    /**
     * Clears out the collSinglePlayerAnswers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSinglePlayerAnswers()
     */
    public function clearSinglePlayerAnswers()
    {
        $this->collSinglePlayerAnswers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSinglePlayerAnswers collection loaded partially.
     */
    public function resetPartialSinglePlayerAnswers($v = true)
    {
        $this->collSinglePlayerAnswersPartial = $v;
    }

    /**
     * Initializes the collSinglePlayerAnswers collection.
     *
     * By default this just sets the collSinglePlayerAnswers collection to an empty array (like clearcollSinglePlayerAnswers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSinglePlayerAnswers($overrideExisting = true)
    {
        if (null !== $this->collSinglePlayerAnswers && !$overrideExisting) {
            return;
        }

        $collectionClassName = SinglePlayerAnswerTableMap::getTableMap()->getCollectionClassName();

        $this->collSinglePlayerAnswers = new $collectionClassName;
        $this->collSinglePlayerAnswers->setModel('\SinglePlayerAnswer');
    }

    /**
     * Gets an array of ChildSinglePlayerAnswer objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildQuestion is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSinglePlayerAnswer[] List of ChildSinglePlayerAnswer objects
     * @throws PropelException
     */
    public function getSinglePlayerAnswers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSinglePlayerAnswersPartial && !$this->isNew();
        if (null === $this->collSinglePlayerAnswers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSinglePlayerAnswers) {
                // return empty collection
                $this->initSinglePlayerAnswers();
            } else {
                $collSinglePlayerAnswers = ChildSinglePlayerAnswerQuery::create(null, $criteria)
                    ->filterByQuestion($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSinglePlayerAnswersPartial && count($collSinglePlayerAnswers)) {
                        $this->initSinglePlayerAnswers(false);

                        foreach ($collSinglePlayerAnswers as $obj) {
                            if (false == $this->collSinglePlayerAnswers->contains($obj)) {
                                $this->collSinglePlayerAnswers->append($obj);
                            }
                        }

                        $this->collSinglePlayerAnswersPartial = true;
                    }

                    return $collSinglePlayerAnswers;
                }

                if ($partial && $this->collSinglePlayerAnswers) {
                    foreach ($this->collSinglePlayerAnswers as $obj) {
                        if ($obj->isNew()) {
                            $collSinglePlayerAnswers[] = $obj;
                        }
                    }
                }

                $this->collSinglePlayerAnswers = $collSinglePlayerAnswers;
                $this->collSinglePlayerAnswersPartial = false;
            }
        }

        return $this->collSinglePlayerAnswers;
    }

    /**
     * Sets a collection of ChildSinglePlayerAnswer objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $singlePlayerAnswers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildQuestion The current object (for fluent API support)
     */
    public function setSinglePlayerAnswers(Collection $singlePlayerAnswers, ConnectionInterface $con = null)
    {
        /** @var ChildSinglePlayerAnswer[] $singlePlayerAnswersToDelete */
        $singlePlayerAnswersToDelete = $this->getSinglePlayerAnswers(new Criteria(), $con)->diff($singlePlayerAnswers);


        $this->singlePlayerAnswersScheduledForDeletion = $singlePlayerAnswersToDelete;

        foreach ($singlePlayerAnswersToDelete as $singlePlayerAnswerRemoved) {
            $singlePlayerAnswerRemoved->setQuestion(null);
        }

        $this->collSinglePlayerAnswers = null;
        foreach ($singlePlayerAnswers as $singlePlayerAnswer) {
            $this->addSinglePlayerAnswer($singlePlayerAnswer);
        }

        $this->collSinglePlayerAnswers = $singlePlayerAnswers;
        $this->collSinglePlayerAnswersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SinglePlayerAnswer objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SinglePlayerAnswer objects.
     * @throws PropelException
     */
    public function countSinglePlayerAnswers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSinglePlayerAnswersPartial && !$this->isNew();
        if (null === $this->collSinglePlayerAnswers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSinglePlayerAnswers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSinglePlayerAnswers());
            }

            $query = ChildSinglePlayerAnswerQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestion($this)
                ->count($con);
        }

        return count($this->collSinglePlayerAnswers);
    }

    /**
     * Method called to associate a ChildSinglePlayerAnswer object to this object
     * through the ChildSinglePlayerAnswer foreign key attribute.
     *
     * @param  ChildSinglePlayerAnswer $l ChildSinglePlayerAnswer
     * @return $this|\Question The current object (for fluent API support)
     */
    public function addSinglePlayerAnswer(ChildSinglePlayerAnswer $l)
    {
        if ($this->collSinglePlayerAnswers === null) {
            $this->initSinglePlayerAnswers();
            $this->collSinglePlayerAnswersPartial = true;
        }

        if (!$this->collSinglePlayerAnswers->contains($l)) {
            $this->doAddSinglePlayerAnswer($l);

            if ($this->singlePlayerAnswersScheduledForDeletion and $this->singlePlayerAnswersScheduledForDeletion->contains($l)) {
                $this->singlePlayerAnswersScheduledForDeletion->remove($this->singlePlayerAnswersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSinglePlayerAnswer $singlePlayerAnswer The ChildSinglePlayerAnswer object to add.
     */
    protected function doAddSinglePlayerAnswer(ChildSinglePlayerAnswer $singlePlayerAnswer)
    {
        $this->collSinglePlayerAnswers[]= $singlePlayerAnswer;
        $singlePlayerAnswer->setQuestion($this);
    }

    /**
     * @param  ChildSinglePlayerAnswer $singlePlayerAnswer The ChildSinglePlayerAnswer object to remove.
     * @return $this|ChildQuestion The current object (for fluent API support)
     */
    public function removeSinglePlayerAnswer(ChildSinglePlayerAnswer $singlePlayerAnswer)
    {
        if ($this->getSinglePlayerAnswers()->contains($singlePlayerAnswer)) {
            $pos = $this->collSinglePlayerAnswers->search($singlePlayerAnswer);
            $this->collSinglePlayerAnswers->remove($pos);
            if (null === $this->singlePlayerAnswersScheduledForDeletion) {
                $this->singlePlayerAnswersScheduledForDeletion = clone $this->collSinglePlayerAnswers;
                $this->singlePlayerAnswersScheduledForDeletion->clear();
            }
            $this->singlePlayerAnswersScheduledForDeletion[]= clone $singlePlayerAnswer;
            $singlePlayerAnswer->setQuestion(null);
        }

        return $this;
    }

    /**
     * Clears out the collTextSinglePlayerAnswers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTextSinglePlayerAnswers()
     */
    public function clearTextSinglePlayerAnswers()
    {
        $this->collTextSinglePlayerAnswers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTextSinglePlayerAnswers collection loaded partially.
     */
    public function resetPartialTextSinglePlayerAnswers($v = true)
    {
        $this->collTextSinglePlayerAnswersPartial = $v;
    }

    /**
     * Initializes the collTextSinglePlayerAnswers collection.
     *
     * By default this just sets the collTextSinglePlayerAnswers collection to an empty array (like clearcollTextSinglePlayerAnswers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTextSinglePlayerAnswers($overrideExisting = true)
    {
        if (null !== $this->collTextSinglePlayerAnswers && !$overrideExisting) {
            return;
        }

        $collectionClassName = TextSinglePlayerAnswerTableMap::getTableMap()->getCollectionClassName();

        $this->collTextSinglePlayerAnswers = new $collectionClassName;
        $this->collTextSinglePlayerAnswers->setModel('\TextSinglePlayerAnswer');
    }

    /**
     * Gets an array of ChildTextSinglePlayerAnswer objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildQuestion is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTextSinglePlayerAnswer[] List of ChildTextSinglePlayerAnswer objects
     * @throws PropelException
     */
    public function getTextSinglePlayerAnswers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTextSinglePlayerAnswersPartial && !$this->isNew();
        if (null === $this->collTextSinglePlayerAnswers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTextSinglePlayerAnswers) {
                // return empty collection
                $this->initTextSinglePlayerAnswers();
            } else {
                $collTextSinglePlayerAnswers = ChildTextSinglePlayerAnswerQuery::create(null, $criteria)
                    ->filterByQuestion($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTextSinglePlayerAnswersPartial && count($collTextSinglePlayerAnswers)) {
                        $this->initTextSinglePlayerAnswers(false);

                        foreach ($collTextSinglePlayerAnswers as $obj) {
                            if (false == $this->collTextSinglePlayerAnswers->contains($obj)) {
                                $this->collTextSinglePlayerAnswers->append($obj);
                            }
                        }

                        $this->collTextSinglePlayerAnswersPartial = true;
                    }

                    return $collTextSinglePlayerAnswers;
                }

                if ($partial && $this->collTextSinglePlayerAnswers) {
                    foreach ($this->collTextSinglePlayerAnswers as $obj) {
                        if ($obj->isNew()) {
                            $collTextSinglePlayerAnswers[] = $obj;
                        }
                    }
                }

                $this->collTextSinglePlayerAnswers = $collTextSinglePlayerAnswers;
                $this->collTextSinglePlayerAnswersPartial = false;
            }
        }

        return $this->collTextSinglePlayerAnswers;
    }

    /**
     * Sets a collection of ChildTextSinglePlayerAnswer objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $textSinglePlayerAnswers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildQuestion The current object (for fluent API support)
     */
    public function setTextSinglePlayerAnswers(Collection $textSinglePlayerAnswers, ConnectionInterface $con = null)
    {
        /** @var ChildTextSinglePlayerAnswer[] $textSinglePlayerAnswersToDelete */
        $textSinglePlayerAnswersToDelete = $this->getTextSinglePlayerAnswers(new Criteria(), $con)->diff($textSinglePlayerAnswers);


        $this->textSinglePlayerAnswersScheduledForDeletion = $textSinglePlayerAnswersToDelete;

        foreach ($textSinglePlayerAnswersToDelete as $textSinglePlayerAnswerRemoved) {
            $textSinglePlayerAnswerRemoved->setQuestion(null);
        }

        $this->collTextSinglePlayerAnswers = null;
        foreach ($textSinglePlayerAnswers as $textSinglePlayerAnswer) {
            $this->addTextSinglePlayerAnswer($textSinglePlayerAnswer);
        }

        $this->collTextSinglePlayerAnswers = $textSinglePlayerAnswers;
        $this->collTextSinglePlayerAnswersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related TextSinglePlayerAnswer objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related TextSinglePlayerAnswer objects.
     * @throws PropelException
     */
    public function countTextSinglePlayerAnswers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTextSinglePlayerAnswersPartial && !$this->isNew();
        if (null === $this->collTextSinglePlayerAnswers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTextSinglePlayerAnswers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTextSinglePlayerAnswers());
            }

            $query = ChildTextSinglePlayerAnswerQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByQuestion($this)
                ->count($con);
        }

        return count($this->collTextSinglePlayerAnswers);
    }

    /**
     * Method called to associate a ChildTextSinglePlayerAnswer object to this object
     * through the ChildTextSinglePlayerAnswer foreign key attribute.
     *
     * @param  ChildTextSinglePlayerAnswer $l ChildTextSinglePlayerAnswer
     * @return $this|\Question The current object (for fluent API support)
     */
    public function addTextSinglePlayerAnswer(ChildTextSinglePlayerAnswer $l)
    {
        if ($this->collTextSinglePlayerAnswers === null) {
            $this->initTextSinglePlayerAnswers();
            $this->collTextSinglePlayerAnswersPartial = true;
        }

        if (!$this->collTextSinglePlayerAnswers->contains($l)) {
            $this->doAddTextSinglePlayerAnswer($l);

            if ($this->textSinglePlayerAnswersScheduledForDeletion and $this->textSinglePlayerAnswersScheduledForDeletion->contains($l)) {
                $this->textSinglePlayerAnswersScheduledForDeletion->remove($this->textSinglePlayerAnswersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTextSinglePlayerAnswer $textSinglePlayerAnswer The ChildTextSinglePlayerAnswer object to add.
     */
    protected function doAddTextSinglePlayerAnswer(ChildTextSinglePlayerAnswer $textSinglePlayerAnswer)
    {
        $this->collTextSinglePlayerAnswers[]= $textSinglePlayerAnswer;
        $textSinglePlayerAnswer->setQuestion($this);
    }

    /**
     * @param  ChildTextSinglePlayerAnswer $textSinglePlayerAnswer The ChildTextSinglePlayerAnswer object to remove.
     * @return $this|ChildQuestion The current object (for fluent API support)
     */
    public function removeTextSinglePlayerAnswer(ChildTextSinglePlayerAnswer $textSinglePlayerAnswer)
    {
        if ($this->getTextSinglePlayerAnswers()->contains($textSinglePlayerAnswer)) {
            $pos = $this->collTextSinglePlayerAnswers->search($textSinglePlayerAnswer);
            $this->collTextSinglePlayerAnswers->remove($pos);
            if (null === $this->textSinglePlayerAnswersScheduledForDeletion) {
                $this->textSinglePlayerAnswersScheduledForDeletion = clone $this->collTextSinglePlayerAnswers;
                $this->textSinglePlayerAnswersScheduledForDeletion->clear();
            }
            $this->textSinglePlayerAnswersScheduledForDeletion[]= clone $textSinglePlayerAnswer;
            $textSinglePlayerAnswer->setQuestion(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aPackage) {
            $this->aPackage->removeQuestion($this);
        }
        $this->id = null;
        $this->package_id = null;
        $this->type = null;
        $this->topic = null;
        $this->text = null;
        $this->picture_url = null;
        $this->answer_picture_url = null;
        $this->right_answer = null;
        $this->alt_answers = null;
        $this->author = null;
        $this->tag1 = null;
        $this->tag2 = null;
        $this->comment = null;
        $this->num_asked = null;
        $this->num_answered = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collChallenges) {
                foreach ($this->collChallenges as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSinglePlayerAnswers) {
                foreach ($this->collSinglePlayerAnswers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTextSinglePlayerAnswers) {
                foreach ($this->collTextSinglePlayerAnswers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collChallenges = null;
        $this->collSinglePlayerAnswers = null;
        $this->collTextSinglePlayerAnswers = null;
        $this->aPackage = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(QuestionTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildQuestion The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[QuestionTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // validate behavior

    /**
     * Configure validators constraints. The Validator object uses this method
     * to perform object validation.
     *
     * @param ClassMetadata $metadata
     */
    static public function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('author', new Length(array ('min' => 3,'max' => 64,)));
    }

    /**
     * Validates the object and all objects related to this table.
     *
     * @see        getValidationFailures()
     * @param      ValidatorInterface|null $validator A Validator class instance
     * @return     boolean Whether all objects pass validation.
     */
    public function validate(ValidatorInterface $validator = null)
    {
        if (null === $validator) {
            $validator = new RecursiveValidator(
                new ExecutionContextFactory(new IdentityTranslator()),
                new LazyLoadingMetadataFactory(new StaticMethodLoader()),
                new ConstraintValidatorFactory()
            );
        }

        $failureMap = new ConstraintViolationList();

        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            // If validate() method exists, the validate-behavior is configured for related object
            if (method_exists($this->aPackage, 'validate')) {
                if (!$this->aPackage->validate($validator)) {
                    $failureMap->addAll($this->aPackage->getValidationFailures());
                }
            }

            $retval = $validator->validate($this);
            if (count($retval) > 0) {
                $failureMap->addAll($retval);
            }

            if (null !== $this->collChallenges) {
                foreach ($this->collChallenges as $referrerFK) {
                    if (method_exists($referrerFK, 'validate')) {
                        if (!$referrerFK->validate($validator)) {
                            $failureMap->addAll($referrerFK->getValidationFailures());
                        }
                    }
                }
            }
            if (null !== $this->collSinglePlayerAnswers) {
                foreach ($this->collSinglePlayerAnswers as $referrerFK) {
                    if (method_exists($referrerFK, 'validate')) {
                        if (!$referrerFK->validate($validator)) {
                            $failureMap->addAll($referrerFK->getValidationFailures());
                        }
                    }
                }
            }
            if (null !== $this->collTextSinglePlayerAnswers) {
                foreach ($this->collTextSinglePlayerAnswers as $referrerFK) {
                    if (method_exists($referrerFK, 'validate')) {
                        if (!$referrerFK->validate($validator)) {
                            $failureMap->addAll($referrerFK->getValidationFailures());
                        }
                    }
                }
            }

            $this->alreadyInValidation = false;
        }

        $this->validationFailures = $failureMap;

        return (Boolean) (!(count($this->validationFailures) > 0));

    }

    /**
     * Gets any ConstraintViolation objects that resulted from last call to validate().
     *
     *
     * @return     object ConstraintViolationList
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
