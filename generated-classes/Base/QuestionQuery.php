<?php

namespace Base;

use \Question as ChildQuestion;
use \QuestionQuery as ChildQuestionQuery;
use \Exception;
use \PDO;
use Map\QuestionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'questions' table.
 *
 *
 *
 * @method     ChildQuestionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildQuestionQuery orderByPackageId($order = Criteria::ASC) Order by the package_id column
 * @method     ChildQuestionQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildQuestionQuery orderByTopic($order = Criteria::ASC) Order by the topic column
 * @method     ChildQuestionQuery orderByText($order = Criteria::ASC) Order by the text column
 * @method     ChildQuestionQuery orderByPictureUrl($order = Criteria::ASC) Order by the picture_url column
 * @method     ChildQuestionQuery orderByAnswerPictureUrl($order = Criteria::ASC) Order by the answer_picture_url column
 * @method     ChildQuestionQuery orderByRightAnswer($order = Criteria::ASC) Order by the right_answer column
 * @method     ChildQuestionQuery orderByAltAnswers($order = Criteria::ASC) Order by the alt_answers column
 * @method     ChildQuestionQuery orderByAuthor($order = Criteria::ASC) Order by the author column
 * @method     ChildQuestionQuery orderByTag1($order = Criteria::ASC) Order by the tag1 column
 * @method     ChildQuestionQuery orderByTag2($order = Criteria::ASC) Order by the tag2 column
 * @method     ChildQuestionQuery orderByComment($order = Criteria::ASC) Order by the comment column
 * @method     ChildQuestionQuery orderByNumAsked($order = Criteria::ASC) Order by the num_asked column
 * @method     ChildQuestionQuery orderByNumAnswered($order = Criteria::ASC) Order by the num_answered column
 * @method     ChildQuestionQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildQuestionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildQuestionQuery groupById() Group by the id column
 * @method     ChildQuestionQuery groupByPackageId() Group by the package_id column
 * @method     ChildQuestionQuery groupByType() Group by the type column
 * @method     ChildQuestionQuery groupByTopic() Group by the topic column
 * @method     ChildQuestionQuery groupByText() Group by the text column
 * @method     ChildQuestionQuery groupByPictureUrl() Group by the picture_url column
 * @method     ChildQuestionQuery groupByAnswerPictureUrl() Group by the answer_picture_url column
 * @method     ChildQuestionQuery groupByRightAnswer() Group by the right_answer column
 * @method     ChildQuestionQuery groupByAltAnswers() Group by the alt_answers column
 * @method     ChildQuestionQuery groupByAuthor() Group by the author column
 * @method     ChildQuestionQuery groupByTag1() Group by the tag1 column
 * @method     ChildQuestionQuery groupByTag2() Group by the tag2 column
 * @method     ChildQuestionQuery groupByComment() Group by the comment column
 * @method     ChildQuestionQuery groupByNumAsked() Group by the num_asked column
 * @method     ChildQuestionQuery groupByNumAnswered() Group by the num_answered column
 * @method     ChildQuestionQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildQuestionQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildQuestionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildQuestionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildQuestionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildQuestionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildQuestionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildQuestionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildQuestionQuery leftJoinPackage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Package relation
 * @method     ChildQuestionQuery rightJoinPackage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Package relation
 * @method     ChildQuestionQuery innerJoinPackage($relationAlias = null) Adds a INNER JOIN clause to the query using the Package relation
 *
 * @method     ChildQuestionQuery joinWithPackage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Package relation
 *
 * @method     ChildQuestionQuery leftJoinWithPackage() Adds a LEFT JOIN clause and with to the query using the Package relation
 * @method     ChildQuestionQuery rightJoinWithPackage() Adds a RIGHT JOIN clause and with to the query using the Package relation
 * @method     ChildQuestionQuery innerJoinWithPackage() Adds a INNER JOIN clause and with to the query using the Package relation
 *
 * @method     ChildQuestionQuery leftJoinChallenge($relationAlias = null) Adds a LEFT JOIN clause to the query using the Challenge relation
 * @method     ChildQuestionQuery rightJoinChallenge($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Challenge relation
 * @method     ChildQuestionQuery innerJoinChallenge($relationAlias = null) Adds a INNER JOIN clause to the query using the Challenge relation
 *
 * @method     ChildQuestionQuery joinWithChallenge($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Challenge relation
 *
 * @method     ChildQuestionQuery leftJoinWithChallenge() Adds a LEFT JOIN clause and with to the query using the Challenge relation
 * @method     ChildQuestionQuery rightJoinWithChallenge() Adds a RIGHT JOIN clause and with to the query using the Challenge relation
 * @method     ChildQuestionQuery innerJoinWithChallenge() Adds a INNER JOIN clause and with to the query using the Challenge relation
 *
 * @method     ChildQuestionQuery leftJoinSinglePlayerAnswer($relationAlias = null) Adds a LEFT JOIN clause to the query using the SinglePlayerAnswer relation
 * @method     ChildQuestionQuery rightJoinSinglePlayerAnswer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SinglePlayerAnswer relation
 * @method     ChildQuestionQuery innerJoinSinglePlayerAnswer($relationAlias = null) Adds a INNER JOIN clause to the query using the SinglePlayerAnswer relation
 *
 * @method     ChildQuestionQuery joinWithSinglePlayerAnswer($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SinglePlayerAnswer relation
 *
 * @method     ChildQuestionQuery leftJoinWithSinglePlayerAnswer() Adds a LEFT JOIN clause and with to the query using the SinglePlayerAnswer relation
 * @method     ChildQuestionQuery rightJoinWithSinglePlayerAnswer() Adds a RIGHT JOIN clause and with to the query using the SinglePlayerAnswer relation
 * @method     ChildQuestionQuery innerJoinWithSinglePlayerAnswer() Adds a INNER JOIN clause and with to the query using the SinglePlayerAnswer relation
 *
 * @method     ChildQuestionQuery leftJoinTextSinglePlayerAnswer($relationAlias = null) Adds a LEFT JOIN clause to the query using the TextSinglePlayerAnswer relation
 * @method     ChildQuestionQuery rightJoinTextSinglePlayerAnswer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TextSinglePlayerAnswer relation
 * @method     ChildQuestionQuery innerJoinTextSinglePlayerAnswer($relationAlias = null) Adds a INNER JOIN clause to the query using the TextSinglePlayerAnswer relation
 *
 * @method     ChildQuestionQuery joinWithTextSinglePlayerAnswer($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TextSinglePlayerAnswer relation
 *
 * @method     ChildQuestionQuery leftJoinWithTextSinglePlayerAnswer() Adds a LEFT JOIN clause and with to the query using the TextSinglePlayerAnswer relation
 * @method     ChildQuestionQuery rightJoinWithTextSinglePlayerAnswer() Adds a RIGHT JOIN clause and with to the query using the TextSinglePlayerAnswer relation
 * @method     ChildQuestionQuery innerJoinWithTextSinglePlayerAnswer() Adds a INNER JOIN clause and with to the query using the TextSinglePlayerAnswer relation
 *
 * @method     \PackageQuery|\ChallengeQuery|\SinglePlayerAnswerQuery|\TextSinglePlayerAnswerQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildQuestion findOne(ConnectionInterface $con = null) Return the first ChildQuestion matching the query
 * @method     ChildQuestion findOneOrCreate(ConnectionInterface $con = null) Return the first ChildQuestion matching the query, or a new ChildQuestion object populated from the query conditions when no match is found
 *
 * @method     ChildQuestion findOneById(int $id) Return the first ChildQuestion filtered by the id column
 * @method     ChildQuestion findOneByPackageId(int $package_id) Return the first ChildQuestion filtered by the package_id column
 * @method     ChildQuestion findOneByType(string $type) Return the first ChildQuestion filtered by the type column
 * @method     ChildQuestion findOneByTopic(string $topic) Return the first ChildQuestion filtered by the topic column
 * @method     ChildQuestion findOneByText(string $text) Return the first ChildQuestion filtered by the text column
 * @method     ChildQuestion findOneByPictureUrl(string $picture_url) Return the first ChildQuestion filtered by the picture_url column
 * @method     ChildQuestion findOneByAnswerPictureUrl(string $answer_picture_url) Return the first ChildQuestion filtered by the answer_picture_url column
 * @method     ChildQuestion findOneByRightAnswer(string $right_answer) Return the first ChildQuestion filtered by the right_answer column
 * @method     ChildQuestion findOneByAltAnswers(string $alt_answers) Return the first ChildQuestion filtered by the alt_answers column
 * @method     ChildQuestion findOneByAuthor(string $author) Return the first ChildQuestion filtered by the author column
 * @method     ChildQuestion findOneByTag1(string $tag1) Return the first ChildQuestion filtered by the tag1 column
 * @method     ChildQuestion findOneByTag2(string $tag2) Return the first ChildQuestion filtered by the tag2 column
 * @method     ChildQuestion findOneByComment(string $comment) Return the first ChildQuestion filtered by the comment column
 * @method     ChildQuestion findOneByNumAsked(int $num_asked) Return the first ChildQuestion filtered by the num_asked column
 * @method     ChildQuestion findOneByNumAnswered(int $num_answered) Return the first ChildQuestion filtered by the num_answered column
 * @method     ChildQuestion findOneByCreatedAt(string $created_at) Return the first ChildQuestion filtered by the created_at column
 * @method     ChildQuestion findOneByUpdatedAt(string $updated_at) Return the first ChildQuestion filtered by the updated_at column *

 * @method     ChildQuestion requirePk($key, ConnectionInterface $con = null) Return the ChildQuestion by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOne(ConnectionInterface $con = null) Return the first ChildQuestion matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildQuestion requireOneById(int $id) Return the first ChildQuestion filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByPackageId(int $package_id) Return the first ChildQuestion filtered by the package_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByType(string $type) Return the first ChildQuestion filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByTopic(string $topic) Return the first ChildQuestion filtered by the topic column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByText(string $text) Return the first ChildQuestion filtered by the text column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByPictureUrl(string $picture_url) Return the first ChildQuestion filtered by the picture_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByAnswerPictureUrl(string $answer_picture_url) Return the first ChildQuestion filtered by the answer_picture_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByRightAnswer(string $right_answer) Return the first ChildQuestion filtered by the right_answer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByAltAnswers(string $alt_answers) Return the first ChildQuestion filtered by the alt_answers column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByAuthor(string $author) Return the first ChildQuestion filtered by the author column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByTag1(string $tag1) Return the first ChildQuestion filtered by the tag1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByTag2(string $tag2) Return the first ChildQuestion filtered by the tag2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByComment(string $comment) Return the first ChildQuestion filtered by the comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByNumAsked(int $num_asked) Return the first ChildQuestion filtered by the num_asked column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByNumAnswered(int $num_answered) Return the first ChildQuestion filtered by the num_answered column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByCreatedAt(string $created_at) Return the first ChildQuestion filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildQuestion requireOneByUpdatedAt(string $updated_at) Return the first ChildQuestion filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildQuestion[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildQuestion objects based on current ModelCriteria
 * @method     ChildQuestion[]|ObjectCollection findById(int $id) Return ChildQuestion objects filtered by the id column
 * @method     ChildQuestion[]|ObjectCollection findByPackageId(int $package_id) Return ChildQuestion objects filtered by the package_id column
 * @method     ChildQuestion[]|ObjectCollection findByType(string $type) Return ChildQuestion objects filtered by the type column
 * @method     ChildQuestion[]|ObjectCollection findByTopic(string $topic) Return ChildQuestion objects filtered by the topic column
 * @method     ChildQuestion[]|ObjectCollection findByText(string $text) Return ChildQuestion objects filtered by the text column
 * @method     ChildQuestion[]|ObjectCollection findByPictureUrl(string $picture_url) Return ChildQuestion objects filtered by the picture_url column
 * @method     ChildQuestion[]|ObjectCollection findByAnswerPictureUrl(string $answer_picture_url) Return ChildQuestion objects filtered by the answer_picture_url column
 * @method     ChildQuestion[]|ObjectCollection findByRightAnswer(string $right_answer) Return ChildQuestion objects filtered by the right_answer column
 * @method     ChildQuestion[]|ObjectCollection findByAltAnswers(string $alt_answers) Return ChildQuestion objects filtered by the alt_answers column
 * @method     ChildQuestion[]|ObjectCollection findByAuthor(string $author) Return ChildQuestion objects filtered by the author column
 * @method     ChildQuestion[]|ObjectCollection findByTag1(string $tag1) Return ChildQuestion objects filtered by the tag1 column
 * @method     ChildQuestion[]|ObjectCollection findByTag2(string $tag2) Return ChildQuestion objects filtered by the tag2 column
 * @method     ChildQuestion[]|ObjectCollection findByComment(string $comment) Return ChildQuestion objects filtered by the comment column
 * @method     ChildQuestion[]|ObjectCollection findByNumAsked(int $num_asked) Return ChildQuestion objects filtered by the num_asked column
 * @method     ChildQuestion[]|ObjectCollection findByNumAnswered(int $num_answered) Return ChildQuestion objects filtered by the num_answered column
 * @method     ChildQuestion[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildQuestion objects filtered by the created_at column
 * @method     ChildQuestion[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildQuestion objects filtered by the updated_at column
 * @method     ChildQuestion[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class QuestionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\QuestionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'dchat', $modelName = '\\Question', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildQuestionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildQuestionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildQuestionQuery) {
            return $criteria;
        }
        $query = new ChildQuestionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildQuestion|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = QuestionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(QuestionTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildQuestion A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, package_id, type, topic, text, picture_url, answer_picture_url, right_answer, alt_answers, author, tag1, tag2, comment, num_asked, num_answered, created_at, updated_at FROM questions WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildQuestion $obj */
            $obj = new ChildQuestion();
            $obj->hydrate($row);
            QuestionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildQuestion|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(QuestionTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(QuestionTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(QuestionTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(QuestionTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the package_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPackageId(1234); // WHERE package_id = 1234
     * $query->filterByPackageId(array(12, 34)); // WHERE package_id IN (12, 34)
     * $query->filterByPackageId(array('min' => 12)); // WHERE package_id > 12
     * </code>
     *
     * @see       filterByPackage()
     *
     * @param     mixed $packageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByPackageId($packageId = null, $comparison = null)
    {
        if (is_array($packageId)) {
            $useMinMax = false;
            if (isset($packageId['min'])) {
                $this->addUsingAlias(QuestionTableMap::COL_PACKAGE_ID, $packageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($packageId['max'])) {
                $this->addUsingAlias(QuestionTableMap::COL_PACKAGE_ID, $packageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_PACKAGE_ID, $packageId, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the topic column
     *
     * Example usage:
     * <code>
     * $query->filterByTopic('fooValue');   // WHERE topic = 'fooValue'
     * $query->filterByTopic('%fooValue%'); // WHERE topic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $topic The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByTopic($topic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($topic)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $topic)) {
                $topic = str_replace('*', '%', $topic);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_TOPIC, $topic, $comparison);
    }

    /**
     * Filter the query on the text column
     *
     * Example usage:
     * <code>
     * $query->filterByText('fooValue');   // WHERE text = 'fooValue'
     * $query->filterByText('%fooValue%'); // WHERE text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $text The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByText($text = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($text)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $text)) {
                $text = str_replace('*', '%', $text);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_TEXT, $text, $comparison);
    }

    /**
     * Filter the query on the picture_url column
     *
     * Example usage:
     * <code>
     * $query->filterByPictureUrl('fooValue');   // WHERE picture_url = 'fooValue'
     * $query->filterByPictureUrl('%fooValue%'); // WHERE picture_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pictureUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByPictureUrl($pictureUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pictureUrl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pictureUrl)) {
                $pictureUrl = str_replace('*', '%', $pictureUrl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_PICTURE_URL, $pictureUrl, $comparison);
    }

    /**
     * Filter the query on the answer_picture_url column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswerPictureUrl('fooValue');   // WHERE answer_picture_url = 'fooValue'
     * $query->filterByAnswerPictureUrl('%fooValue%'); // WHERE answer_picture_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $answerPictureUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByAnswerPictureUrl($answerPictureUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($answerPictureUrl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $answerPictureUrl)) {
                $answerPictureUrl = str_replace('*', '%', $answerPictureUrl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_ANSWER_PICTURE_URL, $answerPictureUrl, $comparison);
    }

    /**
     * Filter the query on the right_answer column
     *
     * Example usage:
     * <code>
     * $query->filterByRightAnswer('fooValue');   // WHERE right_answer = 'fooValue'
     * $query->filterByRightAnswer('%fooValue%'); // WHERE right_answer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rightAnswer The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByRightAnswer($rightAnswer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rightAnswer)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $rightAnswer)) {
                $rightAnswer = str_replace('*', '%', $rightAnswer);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_RIGHT_ANSWER, $rightAnswer, $comparison);
    }

    /**
     * Filter the query on the alt_answers column
     *
     * Example usage:
     * <code>
     * $query->filterByAltAnswers('fooValue');   // WHERE alt_answers = 'fooValue'
     * $query->filterByAltAnswers('%fooValue%'); // WHERE alt_answers LIKE '%fooValue%'
     * </code>
     *
     * @param     string $altAnswers The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByAltAnswers($altAnswers = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($altAnswers)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $altAnswers)) {
                $altAnswers = str_replace('*', '%', $altAnswers);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_ALT_ANSWERS, $altAnswers, $comparison);
    }

    /**
     * Filter the query on the author column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthor('fooValue');   // WHERE author = 'fooValue'
     * $query->filterByAuthor('%fooValue%'); // WHERE author LIKE '%fooValue%'
     * </code>
     *
     * @param     string $author The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByAuthor($author = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($author)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $author)) {
                $author = str_replace('*', '%', $author);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_AUTHOR, $author, $comparison);
    }

    /**
     * Filter the query on the tag1 column
     *
     * Example usage:
     * <code>
     * $query->filterByTag1('fooValue');   // WHERE tag1 = 'fooValue'
     * $query->filterByTag1('%fooValue%'); // WHERE tag1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tag1 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByTag1($tag1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tag1)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tag1)) {
                $tag1 = str_replace('*', '%', $tag1);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_TAG1, $tag1, $comparison);
    }

    /**
     * Filter the query on the tag2 column
     *
     * Example usage:
     * <code>
     * $query->filterByTag2('fooValue');   // WHERE tag2 = 'fooValue'
     * $query->filterByTag2('%fooValue%'); // WHERE tag2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tag2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByTag2($tag2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tag2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tag2)) {
                $tag2 = str_replace('*', '%', $tag2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_TAG2, $tag2, $comparison);
    }

    /**
     * Filter the query on the comment column
     *
     * Example usage:
     * <code>
     * $query->filterByComment('fooValue');   // WHERE comment = 'fooValue'
     * $query->filterByComment('%fooValue%'); // WHERE comment LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comment The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByComment($comment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comment)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $comment)) {
                $comment = str_replace('*', '%', $comment);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_COMMENT, $comment, $comparison);
    }

    /**
     * Filter the query on the num_asked column
     *
     * Example usage:
     * <code>
     * $query->filterByNumAsked(1234); // WHERE num_asked = 1234
     * $query->filterByNumAsked(array(12, 34)); // WHERE num_asked IN (12, 34)
     * $query->filterByNumAsked(array('min' => 12)); // WHERE num_asked > 12
     * </code>
     *
     * @param     mixed $numAsked The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByNumAsked($numAsked = null, $comparison = null)
    {
        if (is_array($numAsked)) {
            $useMinMax = false;
            if (isset($numAsked['min'])) {
                $this->addUsingAlias(QuestionTableMap::COL_NUM_ASKED, $numAsked['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numAsked['max'])) {
                $this->addUsingAlias(QuestionTableMap::COL_NUM_ASKED, $numAsked['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_NUM_ASKED, $numAsked, $comparison);
    }

    /**
     * Filter the query on the num_answered column
     *
     * Example usage:
     * <code>
     * $query->filterByNumAnswered(1234); // WHERE num_answered = 1234
     * $query->filterByNumAnswered(array(12, 34)); // WHERE num_answered IN (12, 34)
     * $query->filterByNumAnswered(array('min' => 12)); // WHERE num_answered > 12
     * </code>
     *
     * @param     mixed $numAnswered The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByNumAnswered($numAnswered = null, $comparison = null)
    {
        if (is_array($numAnswered)) {
            $useMinMax = false;
            if (isset($numAnswered['min'])) {
                $this->addUsingAlias(QuestionTableMap::COL_NUM_ANSWERED, $numAnswered['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numAnswered['max'])) {
                $this->addUsingAlias(QuestionTableMap::COL_NUM_ANSWERED, $numAnswered['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_NUM_ANSWERED, $numAnswered, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(QuestionTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(QuestionTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(QuestionTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(QuestionTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(QuestionTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Package object
     *
     * @param \Package|ObjectCollection $package The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByPackage($package, $comparison = null)
    {
        if ($package instanceof \Package) {
            return $this
                ->addUsingAlias(QuestionTableMap::COL_PACKAGE_ID, $package->getId(), $comparison);
        } elseif ($package instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(QuestionTableMap::COL_PACKAGE_ID, $package->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPackage() only accepts arguments of type \Package or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Package relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function joinPackage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Package');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Package');
        }

        return $this;
    }

    /**
     * Use the Package relation Package object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PackageQuery A secondary query class using the current class as primary query
     */
    public function usePackageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPackage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Package', '\PackageQuery');
    }

    /**
     * Filter the query by a related \Challenge object
     *
     * @param \Challenge|ObjectCollection $challenge the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByChallenge($challenge, $comparison = null)
    {
        if ($challenge instanceof \Challenge) {
            return $this
                ->addUsingAlias(QuestionTableMap::COL_ID, $challenge->getQuestionId(), $comparison);
        } elseif ($challenge instanceof ObjectCollection) {
            return $this
                ->useChallengeQuery()
                ->filterByPrimaryKeys($challenge->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChallenge() only accepts arguments of type \Challenge or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Challenge relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function joinChallenge($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Challenge');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Challenge');
        }

        return $this;
    }

    /**
     * Use the Challenge relation Challenge object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ChallengeQuery A secondary query class using the current class as primary query
     */
    public function useChallengeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChallenge($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Challenge', '\ChallengeQuery');
    }

    /**
     * Filter the query by a related \SinglePlayerAnswer object
     *
     * @param \SinglePlayerAnswer|ObjectCollection $singlePlayerAnswer the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildQuestionQuery The current query, for fluid interface
     */
    public function filterBySinglePlayerAnswer($singlePlayerAnswer, $comparison = null)
    {
        if ($singlePlayerAnswer instanceof \SinglePlayerAnswer) {
            return $this
                ->addUsingAlias(QuestionTableMap::COL_ID, $singlePlayerAnswer->getQuestionId(), $comparison);
        } elseif ($singlePlayerAnswer instanceof ObjectCollection) {
            return $this
                ->useSinglePlayerAnswerQuery()
                ->filterByPrimaryKeys($singlePlayerAnswer->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySinglePlayerAnswer() only accepts arguments of type \SinglePlayerAnswer or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SinglePlayerAnswer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function joinSinglePlayerAnswer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SinglePlayerAnswer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SinglePlayerAnswer');
        }

        return $this;
    }

    /**
     * Use the SinglePlayerAnswer relation SinglePlayerAnswer object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SinglePlayerAnswerQuery A secondary query class using the current class as primary query
     */
    public function useSinglePlayerAnswerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSinglePlayerAnswer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SinglePlayerAnswer', '\SinglePlayerAnswerQuery');
    }

    /**
     * Filter the query by a related \TextSinglePlayerAnswer object
     *
     * @param \TextSinglePlayerAnswer|ObjectCollection $textSinglePlayerAnswer the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildQuestionQuery The current query, for fluid interface
     */
    public function filterByTextSinglePlayerAnswer($textSinglePlayerAnswer, $comparison = null)
    {
        if ($textSinglePlayerAnswer instanceof \TextSinglePlayerAnswer) {
            return $this
                ->addUsingAlias(QuestionTableMap::COL_ID, $textSinglePlayerAnswer->getQuestionId(), $comparison);
        } elseif ($textSinglePlayerAnswer instanceof ObjectCollection) {
            return $this
                ->useTextSinglePlayerAnswerQuery()
                ->filterByPrimaryKeys($textSinglePlayerAnswer->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTextSinglePlayerAnswer() only accepts arguments of type \TextSinglePlayerAnswer or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TextSinglePlayerAnswer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function joinTextSinglePlayerAnswer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TextSinglePlayerAnswer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TextSinglePlayerAnswer');
        }

        return $this;
    }

    /**
     * Use the TextSinglePlayerAnswer relation TextSinglePlayerAnswer object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TextSinglePlayerAnswerQuery A secondary query class using the current class as primary query
     */
    public function useTextSinglePlayerAnswerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTextSinglePlayerAnswer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TextSinglePlayerAnswer', '\TextSinglePlayerAnswerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildQuestion $question Object to remove from the list of results
     *
     * @return $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function prune($question = null)
    {
        if ($question) {
            $this->addUsingAlias(QuestionTableMap::COL_ID, $question->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the questions table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(QuestionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            QuestionTableMap::clearInstancePool();
            QuestionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(QuestionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(QuestionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            QuestionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            QuestionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(QuestionTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(QuestionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(QuestionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(QuestionTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(QuestionTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildQuestionQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(QuestionTableMap::COL_CREATED_AT);
    }

} // QuestionQuery
