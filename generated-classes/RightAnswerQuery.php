<?php

use Base\RightAnswerQuery as BaseRightAnswerQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'right_answers' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RightAnswerQuery extends BaseRightAnswerQuery
{

}
