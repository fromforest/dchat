<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Propel\Runtime\Connection\ConnectionInterface;


trait myValidation {


    public function preSave(ConnectionInterface $con = null) {
        return $this->validate();
    }

    public function save(ConnectionInterface $con = null) {
        $ret = parent::save($con);
		

        if ($ret <= 0) {
            $error_msg = "";
            $failures = $this->getValidationFailures();
			

            // count($failures) > 0 means that we have ConstraintViolation objects and validation failed
            if (count($failures) > 0) {
                foreach ($failures as $failure) {
                    $error_msg = $error_msg . $failure->getPropertyPath() . " validation failed: " . $failure->getMessage();
                }
                throw new Exception("Could not create new " . get_class() . " : " . $error_msg);
            }
        }
		

        return $ret;
    }

}
