<?php

use Base\User as BaseUser;
/**
 * Skeleton subclass for representing a row from the 'users' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
use Propel\Runtime\Connection\ConnectionInterface;

//require_once 'myvalidation.trait.php';

class User extends BaseUser {

    use myValidation;

    public function __construct(array $params = null) {
        parent::__construct();
        if ($params) {
            $this->fromArray($params);
        }
    }

}

//class
