<?php

namespace Map;

use \Question;
use \QuestionQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'questions' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class QuestionTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.QuestionTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'dchat';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'questions';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Question';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Question';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 17;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 17;

    /**
     * the column name for the id field
     */
    const COL_ID = 'questions.id';

    /**
     * the column name for the package_id field
     */
    const COL_PACKAGE_ID = 'questions.package_id';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'questions.type';

    /**
     * the column name for the topic field
     */
    const COL_TOPIC = 'questions.topic';

    /**
     * the column name for the text field
     */
    const COL_TEXT = 'questions.text';

    /**
     * the column name for the picture_url field
     */
    const COL_PICTURE_URL = 'questions.picture_url';

    /**
     * the column name for the answer_picture_url field
     */
    const COL_ANSWER_PICTURE_URL = 'questions.answer_picture_url';

    /**
     * the column name for the right_answer field
     */
    const COL_RIGHT_ANSWER = 'questions.right_answer';

    /**
     * the column name for the alt_answers field
     */
    const COL_ALT_ANSWERS = 'questions.alt_answers';

    /**
     * the column name for the author field
     */
    const COL_AUTHOR = 'questions.author';

    /**
     * the column name for the tag1 field
     */
    const COL_TAG1 = 'questions.tag1';

    /**
     * the column name for the tag2 field
     */
    const COL_TAG2 = 'questions.tag2';

    /**
     * the column name for the comment field
     */
    const COL_COMMENT = 'questions.comment';

    /**
     * the column name for the num_asked field
     */
    const COL_NUM_ASKED = 'questions.num_asked';

    /**
     * the column name for the num_answered field
     */
    const COL_NUM_ANSWERED = 'questions.num_answered';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'questions.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'questions.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PackageId', 'Type', 'Topic', 'Text', 'PictureUrl', 'AnswerPictureUrl', 'RightAnswer', 'AltAnswers', 'Author', 'Tag1', 'Tag2', 'Comment', 'NumAsked', 'NumAnswered', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'packageId', 'type', 'topic', 'text', 'pictureUrl', 'answerPictureUrl', 'rightAnswer', 'altAnswers', 'author', 'tag1', 'tag2', 'comment', 'numAsked', 'numAnswered', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(QuestionTableMap::COL_ID, QuestionTableMap::COL_PACKAGE_ID, QuestionTableMap::COL_TYPE, QuestionTableMap::COL_TOPIC, QuestionTableMap::COL_TEXT, QuestionTableMap::COL_PICTURE_URL, QuestionTableMap::COL_ANSWER_PICTURE_URL, QuestionTableMap::COL_RIGHT_ANSWER, QuestionTableMap::COL_ALT_ANSWERS, QuestionTableMap::COL_AUTHOR, QuestionTableMap::COL_TAG1, QuestionTableMap::COL_TAG2, QuestionTableMap::COL_COMMENT, QuestionTableMap::COL_NUM_ASKED, QuestionTableMap::COL_NUM_ANSWERED, QuestionTableMap::COL_CREATED_AT, QuestionTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'package_id', 'type', 'topic', 'text', 'picture_url', 'answer_picture_url', 'right_answer', 'alt_answers', 'author', 'tag1', 'tag2', 'comment', 'num_asked', 'num_answered', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PackageId' => 1, 'Type' => 2, 'Topic' => 3, 'Text' => 4, 'PictureUrl' => 5, 'AnswerPictureUrl' => 6, 'RightAnswer' => 7, 'AltAnswers' => 8, 'Author' => 9, 'Tag1' => 10, 'Tag2' => 11, 'Comment' => 12, 'NumAsked' => 13, 'NumAnswered' => 14, 'CreatedAt' => 15, 'UpdatedAt' => 16, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'packageId' => 1, 'type' => 2, 'topic' => 3, 'text' => 4, 'pictureUrl' => 5, 'answerPictureUrl' => 6, 'rightAnswer' => 7, 'altAnswers' => 8, 'author' => 9, 'tag1' => 10, 'tag2' => 11, 'comment' => 12, 'numAsked' => 13, 'numAnswered' => 14, 'createdAt' => 15, 'updatedAt' => 16, ),
        self::TYPE_COLNAME       => array(QuestionTableMap::COL_ID => 0, QuestionTableMap::COL_PACKAGE_ID => 1, QuestionTableMap::COL_TYPE => 2, QuestionTableMap::COL_TOPIC => 3, QuestionTableMap::COL_TEXT => 4, QuestionTableMap::COL_PICTURE_URL => 5, QuestionTableMap::COL_ANSWER_PICTURE_URL => 6, QuestionTableMap::COL_RIGHT_ANSWER => 7, QuestionTableMap::COL_ALT_ANSWERS => 8, QuestionTableMap::COL_AUTHOR => 9, QuestionTableMap::COL_TAG1 => 10, QuestionTableMap::COL_TAG2 => 11, QuestionTableMap::COL_COMMENT => 12, QuestionTableMap::COL_NUM_ASKED => 13, QuestionTableMap::COL_NUM_ANSWERED => 14, QuestionTableMap::COL_CREATED_AT => 15, QuestionTableMap::COL_UPDATED_AT => 16, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'package_id' => 1, 'type' => 2, 'topic' => 3, 'text' => 4, 'picture_url' => 5, 'answer_picture_url' => 6, 'right_answer' => 7, 'alt_answers' => 8, 'author' => 9, 'tag1' => 10, 'tag2' => 11, 'comment' => 12, 'num_asked' => 13, 'num_answered' => 14, 'created_at' => 15, 'updated_at' => 16, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('questions');
        $this->setPhpName('Question');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Question');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('package_id', 'PackageId', 'INTEGER', 'packages', 'id', true, null, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 32, 'image');
        $this->addColumn('topic', 'Topic', 'VARCHAR', true, 100, null);
        $this->addColumn('text', 'Text', 'VARCHAR', true, 2000, null);
        $this->addColumn('picture_url', 'PictureUrl', 'VARCHAR', true, 255, null);
        $this->addColumn('answer_picture_url', 'AnswerPictureUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('right_answer', 'RightAnswer', 'VARCHAR', true, 100, null);
        $this->addColumn('alt_answers', 'AltAnswers', 'VARCHAR', false, 255, null);
        $this->addColumn('author', 'Author', 'VARCHAR', false, 64, null);
        $this->addColumn('tag1', 'Tag1', 'VARCHAR', false, 100, null);
        $this->addColumn('tag2', 'Tag2', 'VARCHAR', false, 100, null);
        $this->addColumn('comment', 'Comment', 'VARCHAR', false, 2000, null);
        $this->addColumn('num_asked', 'NumAsked', 'INTEGER', true, null, 0);
        $this->addColumn('num_answered', 'NumAnswered', 'INTEGER', true, null, 0);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Package', '\\Package', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':package_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Challenge', '\\Challenge', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':question_id',
    1 => ':id',
  ),
), null, null, 'Challenges', false);
        $this->addRelation('SinglePlayerAnswer', '\\SinglePlayerAnswer', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':question_id',
    1 => ':id',
  ),
), null, null, 'SinglePlayerAnswers', false);
        $this->addRelation('TextSinglePlayerAnswer', '\\TextSinglePlayerAnswer', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':question_id',
    1 => ':id',
  ),
), null, null, 'TextSinglePlayerAnswers', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'validate' => array('name_length' => array ('column' => 'author','validator' => 'Length','options' => array ('min' => 3,'max' => 64,),), ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? QuestionTableMap::CLASS_DEFAULT : QuestionTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Question object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = QuestionTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = QuestionTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + QuestionTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = QuestionTableMap::OM_CLASS;
            /** @var Question $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            QuestionTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = QuestionTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = QuestionTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Question $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                QuestionTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(QuestionTableMap::COL_ID);
            $criteria->addSelectColumn(QuestionTableMap::COL_PACKAGE_ID);
            $criteria->addSelectColumn(QuestionTableMap::COL_TYPE);
            $criteria->addSelectColumn(QuestionTableMap::COL_TOPIC);
            $criteria->addSelectColumn(QuestionTableMap::COL_TEXT);
            $criteria->addSelectColumn(QuestionTableMap::COL_PICTURE_URL);
            $criteria->addSelectColumn(QuestionTableMap::COL_ANSWER_PICTURE_URL);
            $criteria->addSelectColumn(QuestionTableMap::COL_RIGHT_ANSWER);
            $criteria->addSelectColumn(QuestionTableMap::COL_ALT_ANSWERS);
            $criteria->addSelectColumn(QuestionTableMap::COL_AUTHOR);
            $criteria->addSelectColumn(QuestionTableMap::COL_TAG1);
            $criteria->addSelectColumn(QuestionTableMap::COL_TAG2);
            $criteria->addSelectColumn(QuestionTableMap::COL_COMMENT);
            $criteria->addSelectColumn(QuestionTableMap::COL_NUM_ASKED);
            $criteria->addSelectColumn(QuestionTableMap::COL_NUM_ANSWERED);
            $criteria->addSelectColumn(QuestionTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(QuestionTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.package_id');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.topic');
            $criteria->addSelectColumn($alias . '.text');
            $criteria->addSelectColumn($alias . '.picture_url');
            $criteria->addSelectColumn($alias . '.answer_picture_url');
            $criteria->addSelectColumn($alias . '.right_answer');
            $criteria->addSelectColumn($alias . '.alt_answers');
            $criteria->addSelectColumn($alias . '.author');
            $criteria->addSelectColumn($alias . '.tag1');
            $criteria->addSelectColumn($alias . '.tag2');
            $criteria->addSelectColumn($alias . '.comment');
            $criteria->addSelectColumn($alias . '.num_asked');
            $criteria->addSelectColumn($alias . '.num_answered');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(QuestionTableMap::DATABASE_NAME)->getTable(QuestionTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(QuestionTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(QuestionTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new QuestionTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Question or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Question object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(QuestionTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Question) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(QuestionTableMap::DATABASE_NAME);
            $criteria->add(QuestionTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = QuestionQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            QuestionTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                QuestionTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the questions table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return QuestionQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Question or Criteria object.
     *
     * @param mixed               $criteria Criteria or Question object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(QuestionTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Question object
        }


        // Set the correct dbName
        $query = QuestionQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // QuestionTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
QuestionTableMap::buildTableMap();
