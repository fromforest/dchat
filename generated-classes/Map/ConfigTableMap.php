<?php

namespace Map;

use \Config;
use \ConfigQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'config' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ConfigTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ConfigTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'dchat';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'config';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Config';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Config';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 17;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 17;

    /**
     * the column name for the id field
     */
    const COL_ID = 'config.id';

    /**
     * the column name for the quiz_mode field
     */
    const COL_QUIZ_MODE = 'config.quiz_mode';

    /**
     * the column name for the question_time field
     */
    const COL_QUESTION_TIME = 'config.question_time';

    /**
     * the column name for the answer_time field
     */
    const COL_ANSWER_TIME = 'config.answer_time';

    /**
     * the column name for the announced_right_users field
     */
    const COL_ANNOUNCED_RIGHT_USERS = 'config.announced_right_users';

    /**
     * the column name for the challenge_updated field
     */
    const COL_CHALLENGE_UPDATED = 'config.challenge_updated';

    /**
     * the column name for the packages field
     */
    const COL_PACKAGES = 'config.packages';

    /**
     * the column name for the black_list field
     */
    const COL_BLACK_LIST = 'config.black_list';

    /**
     * the column name for the difficulty_sequence field
     */
    const COL_DIFFICULTY_SEQUENCE = 'config.difficulty_sequence';

    /**
     * the column name for the min_users_for_updates field
     */
    const COL_MIN_USERS_FOR_UPDATES = 'config.min_users_for_updates';

    /**
     * the column name for the tqi field
     */
    const COL_TQI = 'config.tqi';

    /**
     * the column name for the tournament_number field
     */
    const COL_TOURNAMENT_NUMBER = 'config.tournament_number';

    /**
     * the column name for the temp_uploaded_question_id field
     */
    const COL_TEMP_UPLOADED_QUESTION_ID = 'config.temp_uploaded_question_id';

    /**
     * the column name for the reserve_string field
     */
    const COL_RESERVE_STRING = 'config.reserve_string';

    /**
     * the column name for the reserve_int field
     */
    const COL_RESERVE_INT = 'config.reserve_int';

    /**
     * the column name for the reserve5 field
     */
    const COL_RESERVE5 = 'config.reserve5';

    /**
     * the column name for the min_time_between_equal_questions field
     */
    const COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS = 'config.min_time_between_equal_questions';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'QuizMode', 'QuestionTime', 'AnswerTime', 'AnnouncedRightUsers', 'ChallengeUpdated', 'Packages', 'BlackList', 'DifficultySequence', 'MinUsersForUpdates', 'Tqi', 'TournamentNumber', 'TempUploadedQuestionId', 'ReserveString', 'ReserveInt', 'Reserve5', 'MinTimeBetweenEqualQuestions', ),
        self::TYPE_CAMELNAME     => array('id', 'quizMode', 'questionTime', 'answerTime', 'announcedRightUsers', 'challengeUpdated', 'packages', 'blackList', 'difficultySequence', 'minUsersForUpdates', 'tqi', 'tournamentNumber', 'tempUploadedQuestionId', 'reserveString', 'reserveInt', 'reserve5', 'minTimeBetweenEqualQuestions', ),
        self::TYPE_COLNAME       => array(ConfigTableMap::COL_ID, ConfigTableMap::COL_QUIZ_MODE, ConfigTableMap::COL_QUESTION_TIME, ConfigTableMap::COL_ANSWER_TIME, ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS, ConfigTableMap::COL_CHALLENGE_UPDATED, ConfigTableMap::COL_PACKAGES, ConfigTableMap::COL_BLACK_LIST, ConfigTableMap::COL_DIFFICULTY_SEQUENCE, ConfigTableMap::COL_MIN_USERS_FOR_UPDATES, ConfigTableMap::COL_TQI, ConfigTableMap::COL_TOURNAMENT_NUMBER, ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID, ConfigTableMap::COL_RESERVE_STRING, ConfigTableMap::COL_RESERVE_INT, ConfigTableMap::COL_RESERVE5, ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS, ),
        self::TYPE_FIELDNAME     => array('id', 'quiz_mode', 'question_time', 'answer_time', 'announced_right_users', 'challenge_updated', 'packages', 'black_list', 'difficulty_sequence', 'min_users_for_updates', 'tqi', 'tournament_number', 'temp_uploaded_question_id', 'reserve_string', 'reserve_int', 'reserve5', 'min_time_between_equal_questions', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'QuizMode' => 1, 'QuestionTime' => 2, 'AnswerTime' => 3, 'AnnouncedRightUsers' => 4, 'ChallengeUpdated' => 5, 'Packages' => 6, 'BlackList' => 7, 'DifficultySequence' => 8, 'MinUsersForUpdates' => 9, 'Tqi' => 10, 'TournamentNumber' => 11, 'TempUploadedQuestionId' => 12, 'ReserveString' => 13, 'ReserveInt' => 14, 'Reserve5' => 15, 'MinTimeBetweenEqualQuestions' => 16, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'quizMode' => 1, 'questionTime' => 2, 'answerTime' => 3, 'announcedRightUsers' => 4, 'challengeUpdated' => 5, 'packages' => 6, 'blackList' => 7, 'difficultySequence' => 8, 'minUsersForUpdates' => 9, 'tqi' => 10, 'tournamentNumber' => 11, 'tempUploadedQuestionId' => 12, 'reserveString' => 13, 'reserveInt' => 14, 'reserve5' => 15, 'minTimeBetweenEqualQuestions' => 16, ),
        self::TYPE_COLNAME       => array(ConfigTableMap::COL_ID => 0, ConfigTableMap::COL_QUIZ_MODE => 1, ConfigTableMap::COL_QUESTION_TIME => 2, ConfigTableMap::COL_ANSWER_TIME => 3, ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS => 4, ConfigTableMap::COL_CHALLENGE_UPDATED => 5, ConfigTableMap::COL_PACKAGES => 6, ConfigTableMap::COL_BLACK_LIST => 7, ConfigTableMap::COL_DIFFICULTY_SEQUENCE => 8, ConfigTableMap::COL_MIN_USERS_FOR_UPDATES => 9, ConfigTableMap::COL_TQI => 10, ConfigTableMap::COL_TOURNAMENT_NUMBER => 11, ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID => 12, ConfigTableMap::COL_RESERVE_STRING => 13, ConfigTableMap::COL_RESERVE_INT => 14, ConfigTableMap::COL_RESERVE5 => 15, ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS => 16, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'quiz_mode' => 1, 'question_time' => 2, 'answer_time' => 3, 'announced_right_users' => 4, 'challenge_updated' => 5, 'packages' => 6, 'black_list' => 7, 'difficulty_sequence' => 8, 'min_users_for_updates' => 9, 'tqi' => 10, 'tournament_number' => 11, 'temp_uploaded_question_id' => 12, 'reserve_string' => 13, 'reserve_int' => 14, 'reserve5' => 15, 'min_time_between_equal_questions' => 16, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('config');
        $this->setPhpName('Config');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Config');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('quiz_mode', 'QuizMode', 'VARCHAR', true, 1024, 'regular');
        $this->addColumn('question_time', 'QuestionTime', 'INTEGER', true, null, 30);
        $this->addColumn('answer_time', 'AnswerTime', 'INTEGER', true, null, 7);
        $this->addColumn('announced_right_users', 'AnnouncedRightUsers', 'INTEGER', true, null, 1);
        $this->addColumn('challenge_updated', 'ChallengeUpdated', 'INTEGER', true, null, 0);
        $this->addColumn('packages', 'Packages', 'VARCHAR', true, 2048, 'tp1:tp2:tp18');
        $this->addColumn('black_list', 'BlackList', 'VARCHAR', true, 1024, 'tp18:temp_uploaded');
        $this->addColumn('difficulty_sequence', 'DifficultySequence', 'VARCHAR', true, 1024, 'easy:medium:easy:hard');
        $this->addColumn('min_users_for_updates', 'MinUsersForUpdates', 'INTEGER', true, null, 5);
        $this->addColumn('tqi', 'Tqi', 'INTEGER', true, null, 0);
        $this->addColumn('tournament_number', 'TournamentNumber', 'VARCHAR', true, 1024, '');
        $this->addColumn('temp_uploaded_question_id', 'TempUploadedQuestionId', 'INTEGER', true, null, 1000000);
        $this->addColumn('reserve_string', 'ReserveString', 'VARCHAR', true, 255, '0');
        $this->addColumn('reserve_int', 'ReserveInt', 'INTEGER', true, null, 0);
        $this->addColumn('reserve5', 'Reserve5', 'INTEGER', true, null, 0);
        $this->addColumn('min_time_between_equal_questions', 'MinTimeBetweenEqualQuestions', 'INTEGER', true, null, 360);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ConfigTableMap::CLASS_DEFAULT : ConfigTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Config object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ConfigTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ConfigTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ConfigTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ConfigTableMap::OM_CLASS;
            /** @var Config $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ConfigTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ConfigTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ConfigTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Config $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ConfigTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ConfigTableMap::COL_ID);
            $criteria->addSelectColumn(ConfigTableMap::COL_QUIZ_MODE);
            $criteria->addSelectColumn(ConfigTableMap::COL_QUESTION_TIME);
            $criteria->addSelectColumn(ConfigTableMap::COL_ANSWER_TIME);
            $criteria->addSelectColumn(ConfigTableMap::COL_ANNOUNCED_RIGHT_USERS);
            $criteria->addSelectColumn(ConfigTableMap::COL_CHALLENGE_UPDATED);
            $criteria->addSelectColumn(ConfigTableMap::COL_PACKAGES);
            $criteria->addSelectColumn(ConfigTableMap::COL_BLACK_LIST);
            $criteria->addSelectColumn(ConfigTableMap::COL_DIFFICULTY_SEQUENCE);
            $criteria->addSelectColumn(ConfigTableMap::COL_MIN_USERS_FOR_UPDATES);
            $criteria->addSelectColumn(ConfigTableMap::COL_TQI);
            $criteria->addSelectColumn(ConfigTableMap::COL_TOURNAMENT_NUMBER);
            $criteria->addSelectColumn(ConfigTableMap::COL_TEMP_UPLOADED_QUESTION_ID);
            $criteria->addSelectColumn(ConfigTableMap::COL_RESERVE_STRING);
            $criteria->addSelectColumn(ConfigTableMap::COL_RESERVE_INT);
            $criteria->addSelectColumn(ConfigTableMap::COL_RESERVE5);
            $criteria->addSelectColumn(ConfigTableMap::COL_MIN_TIME_BETWEEN_EQUAL_QUESTIONS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.quiz_mode');
            $criteria->addSelectColumn($alias . '.question_time');
            $criteria->addSelectColumn($alias . '.answer_time');
            $criteria->addSelectColumn($alias . '.announced_right_users');
            $criteria->addSelectColumn($alias . '.challenge_updated');
            $criteria->addSelectColumn($alias . '.packages');
            $criteria->addSelectColumn($alias . '.black_list');
            $criteria->addSelectColumn($alias . '.difficulty_sequence');
            $criteria->addSelectColumn($alias . '.min_users_for_updates');
            $criteria->addSelectColumn($alias . '.tqi');
            $criteria->addSelectColumn($alias . '.tournament_number');
            $criteria->addSelectColumn($alias . '.temp_uploaded_question_id');
            $criteria->addSelectColumn($alias . '.reserve_string');
            $criteria->addSelectColumn($alias . '.reserve_int');
            $criteria->addSelectColumn($alias . '.reserve5');
            $criteria->addSelectColumn($alias . '.min_time_between_equal_questions');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ConfigTableMap::DATABASE_NAME)->getTable(ConfigTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ConfigTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ConfigTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ConfigTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Config or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Config object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConfigTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Config) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ConfigTableMap::DATABASE_NAME);
            $criteria->add(ConfigTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ConfigQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ConfigTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ConfigTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the config table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ConfigQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Config or Criteria object.
     *
     * @param mixed               $criteria Criteria or Config object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConfigTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Config object
        }


        // Set the correct dbName
        $query = ConfigQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ConfigTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ConfigTableMap::buildTableMap();
