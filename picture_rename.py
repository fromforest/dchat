import os
src_dir = r'C:\wamp\www\dchat\muz_mash'
theme = 'музыкальные инструменты'
tag1  = ''
tag2  = ''

text = 'Назовите музыкальный инструмент.' #используя _
alt_answers = '' #начиная с &     например &ежик&еж&ёжик
comment = 'no comment'


os.chdir(src_dir)
files = os.listdir()
for file in files:
    parts = file.split('.')
    os.rename(
        file,
        theme + bool(tag1)*('!' + tag1)  + bool(tag2)*('!' + tag2) + '!' + text + '$' + parts[0] + 
        bool(alt_answers)*alt_answers + '#' + comment + '.' + parts[-1]
    )
    