import os

def writeln (filename, element):
    f = open (filename, 'a', encoding = 'utf-8')
    f.write (str (element) + '\n')
    f.close ()

author = 'HRUST'
package_id = 39
name = 'tp' + str(package_id)
description = 'композиторы'


first_id = package_id*1000

result = 'package.xml'
src_dir = './results'
os.chdir (src_dir)
files = os.listdir()
import random
random.shuffle(files)

writeln(result, '<?xml version="1.0" encoding="UTF-8"?>')
writeln(result, '\t'+ '<package>')
writeln(result, '\t\t' + '<id>' + str (package_id) + '</id>')
writeln(result, '\t\t' + '<name>' + name + '</name>')
writeln(result, '\t\t' + '<description>' + description + '</description>')

# Города России!Алтайский_край!Герб какого российского городаvopros$ответ&barnaul_001_gerb#коммент
theme = 'композиторы'
default_text = 'Назовите имя и фамилию композитора'

#============================ основной цикл по файлам =======================
id = first_id

for file1 in files:
    try:
    
        file = file1
        #======================== парсинг топика ===============================
    
        file = file.replace ('vopros','?')
        elems = file.split('!')
        topic = elems[0]
        string = elems[-1]
        if (len(elems) == 1):
            topic = theme

       #===================== проверка и парсинг тэгов ========================
    
        if len(elems) > 2:
            tag1 = elems[1]
            if len(elems) > 3:
                tag2 = elems[2]
            else:
                tag2 = ''
        else:
            tag1 = ''
            tag2 = ''
    
    #======================== парсинг текста вопроса  =======================
    
        elems = string.split('$')
        text = elems[0]
        string = elems[-1]
        if (len(elems) == 1):
            text = default_text
    
    #======================== парсинг ответов ===============================
        if '#' not in string:
            string = string.replace('.', '#комментарий.')
        elems = string.split('#')
        string = elems[1]
        answers = elems[0].split('&')
        right_answer = answers[0]
        alt_answers = ':'.join (answers[1:])
    
    #======================== парсинг комментария ===========================
    
        elems = string.split('.')
    
        ext = elems[-1]
        comment = '.'.join(elems[0:-1])
    

    #======================== парсинг расширения  ===========================    
    
        picture_url = 'pic' + str (id) + '.' + ext
    
    #======================= запись в файл =================================                
        writeln(result, '\t\t' + '<question>')
        writeln(result, '\t\t\t'+ '<id>' + str(id) + '</id>')
        writeln(result, '\t\t\t'+ '<topic>' + topic + '</topic>')
        writeln(result, '\t\t\t'+ '<text>' + text + '</text>')
        writeln(result, '\t\t\t'+ '<picture_url>' + picture_url + '</picture_url>')
        writeln(result, '\t\t\t'+ '<right_answer>' + right_answer + '</right_answer>')
        writeln(result, '\t\t\t'+ '<comment>' + comment + '</comment>')
        if alt_answers:
            writeln(result, '\t\t\t'+ '<alt_answers>' + alt_answers + '</alt_answers>')
        writeln(result, '\t\t\t'+ '<author>' + author + '</author>')
        if tag1:
            writeln(result, '\t\t\t'+ '<tag1>' + tag1 + '</tag1>')
        if tag2:
            writeln(result, '\t\t\t'+ '<tag2>' + tag2 + '</tag2>')
        writeln(result, '\t\t' + '</question>')
    
        os.rename(file1, picture_url)
        id = id + 1
    
    except:
        print(file1)
#================== конец основного цикла по файлам ========================

writeln(result, '\t'+'</package>')
