chatAppControllers.controller('StatisticsController', ['$scope', '$routeParams',  'chatService', '$timeout', function ($scope, $routeParams, chatService, $timeout, $http) {

    $scope.header = 'СТАТИСТИКА БУДЕТ ОБНУЛЕНА';
	$scope.statistics_array  = [];
	$scope.statistics2_array = [];
	$scope.table1_choiced = true;

	$scope.setParamsForTables = function(table1_choiced) {
		$scope.statistics_array  = [];
		$scope.statistics2_array = [];
		$scope.table1_choiced = table1_choiced;
	};

	$scope.getFromResponseStatisticsInPeriod = function(response) {
		var data = response.data;
		var statistics_array = [];
        if (data.statistics_in_period_array) {
			for (var key in data.statistics_in_period_array) { //deepcopy
				statistics_array [key] = data.statistics_in_period_array[key];
			}
        }
		return statistics_array;
	};

	$scope.getFromResponseBestPeriods = function(response) {
		var data = response.data;
		var statistics2_array = [];
        if (data.best_periods) {
			for(var key in data.best_periods){ //deepcopy
				statistics2_array [key] = data.best_periods[key];
			}
        }
		return statistics2_array;
	};
	
	$scope.getStatisticsCurrentHour = function() {
			$scope.setParamsForTables(true);
            chatService.getStatistics('CurrentHour', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Статистика текущего часа';
    };

	$scope.getStatisticsPreviousHour = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('PreviousHour', 10000).then(function (response) {
                $scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Статистика предыдущего часа';
	};

	$scope.getStatisticsCurrentDay = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('CurrentDay', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Статистика текущего дня';
	};

	$scope.getStatisticsPreviousDay = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('PreviousDay', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Статистика предыдущих суток';
	};

	$scope.getStatisticsCurrentMonth = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('CurrentMonth', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Статистика текущего месяца';
	};

	$scope.getStatisticsPreviousMonth = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('PreviousMonth', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Статистика предыдущего месяца';
	};

	$scope.getStatisticsCurrentYear = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('CurrentYear', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Статистика текущего года';
	};

	$scope.getStatisticsPreviousYear = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('PreviousYear', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Статистика предыдущего года';
	};

	$scope.getStatisticsTotalScore = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('TotalScore', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Ответы за всё время';
	};

	$scope.getStatisticsBestHours = function() {
			$scope.setParamsForTables(false);
			chatService.getStatistics('BestHoursTotal', 10000).then(function (response) {
				$scope.statistics2_array = $scope.getFromResponseBestPeriods (response);
            });
			$scope.header = 'Лучшие результаты за час за всё время';
	};

	$scope.getStatisticsBestHoursCurrentMonth = function() {
			$scope.setParamsForTables(false);
			chatService.getStatistics('BestHoursCurrentMonth', 10000).then(function (response) {
				$scope.statistics2_array = $scope.getFromResponseBestPeriods (response);
            });
			$scope.header = 'Лучшие результаты за час текущего месяца';
	};

	$scope.getStatisticsBestHoursCurrentYear = function() {
			$scope.setParamsForTables(false);
			chatService.getStatistics('BestHoursCurrentYear', 10000).then(function (response) {
				$scope.statistics2_array = $scope.getFromResponseBestPeriods (response);
            });
			$scope.header = 'Лучшие результаты за час текущего года';
	};

	$scope.getStatisticsBestDaysTotal = function() {
			$scope.setParamsForTables(false);
			chatService.getStatistics('BestDaysTotal', 10000).then(function (response) {
				$scope.statistics2_array = $scope.getFromResponseBestPeriods (response);
            });
			$scope.header = 'Лучшие результаты за сутки за всё время';
	};

	$scope.getStatisticsBestDaysCurrentYear = function() {
			$scope.setParamsForTables(false);
			chatService.getStatistics('BestDaysCurrentYear', 10000).then(function (response) {
				$scope.statistics2_array = $scope.getFromResponseBestPeriods (response);
            });
			$scope.header = 'Лучшие результаты за сутки текущего года';
	};

	$scope.getStatisticsBestMonthsTotal = function() {
			$scope.setParamsForTables(false);
			chatService.getStatistics('BestMonthsTotal', 10000).then(function (response) {
				$scope.statistics2_array = $scope.getFromResponseBestPeriods (response);
            });
			$scope.header = 'Лучшие результаты за месяц за всё время';
	};

	$scope.getStatisticsBestYears = function() {
			$scope.setParamsForTables(false);
			chatService.getStatistics('BestYears', 10000).then(function (response) {
				$scope.statistics2_array = $scope.getFromResponseBestPeriods (response);
            });
			$scope.header = 'Лучшие годовые результаты за всё время';
	};

	$scope.getStatisticsChainsTotal = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('ChainsTotal', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Цепочки за всё время';
	};

	$scope.getStatisticsChainsCurrentMonth = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('ChainsCurrentMonth', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Цепочки текущего месяца';
	};

	$scope.getStatisticsChainsCurrentYear = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('ChainsCurrentYear', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Цепочки текущего года';
	};

	$scope.getStatisticsMQZ = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('MQZ', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Рейтинг по индексу MQZ';
	};

	$scope.getStatisticsOnAnyFiftyChallenges = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('OnAnyFiftyChallenges', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Топ на произвольных 50 вопросах подряд';
	};
	
	$scope.getStatisticsLastTournament = function() {
			$scope.setParamsForTables(true);
			chatService.getStatistics('LastTournament', 10000).then(function (response) {
				$scope.statistics_array = $scope.getFromResponseStatisticsInPeriod (response);
            });
			$scope.header = 'Результаты последнего турнира';
	};

	$scope.init = function () {

		$timeout(function() {
			resizeAll();
		}, 100);

		$timeout(function () {
				//пробная штука для запуска resize
                        var height = parseInt($('body').css('height'));
						$('body').css('height', height + 1);
                    }, 50);

		//$scope.getStatisticsCurrentDay();
	};//init

	$scope.init();

}]);//controller