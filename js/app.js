var chatApp = angular.module('chatApp', ['ngRoute', 'chatControllers', 'chatServices']);


chatApp.config(['$routeProvider',
function ($routeProvider) {
        $routeProvider.
        when('/first', {
            templateUrl: 'partials/_t1.html',
            controller: 'FirstController'
        }).
        when('/second', {
                templateUrl: 'partials/_t2.html',
                controller: 'SecondController'
        })
            .when('/chat', {
                templateUrl: 'partials/_chat.html',
                controller: 'ChatController'

			}).
        when('/about', {
                templateUrl: 'partials/_about.html',
                controller: 'AboutController'
        })
			.when('/statistics', {
                templateUrl: 'partials/_statistics.html',
                controller: 'StatisticsController'
            }).
		when('/singleplayer', {
				templateUrl: 'partials/_singleplayer.html',
				controller: 'SinglePlayerController'
		}).
			when('/donate', {
                templateUrl: 'partials/_donate.html',
                controller: 'DonateController'
			}).
		when('/zavalinka', {
				templateUrl: 'partials/_zavalinka.html',
				controller: 'ZavalinkaController'
		}).
			when('/send', {
				templateUrl: 'partials/_sendfile.html',
				controller: 'SendFileController'
			}).
		when('/text_singleplayer', {
				templateUrl: 'partials/_text_singleplayer.html',
				controller: 'TextSinglePlayerController'
		}).
			otherwise({
				redirectTo: '/chat'
				});
}]);