$(document).ready(function(){//ожидание формирования DOM-дерева

menu_elems         = $('#navbar ul li');
stat_menu_elems    = $('#right_statistics_menu td, #left_statistics_menu td');
singleplayer_elems = $('#singleplayer_right td');

menu_elems.click (function(eventObject){
	$.each(menu_elems, function(){$(this).removeClass('on_click_class')});
	$(this).addClass('on_click_class');
});

menu_elems.hover(//через запятую два события - наведение мышки, отведение мышки.
	function (){
		$(this).addClass('mouse_over_class');
	},
	function (){
		$(this).removeClass('mouse_over_class');
	}
);

stat_menu_elems.click (function(eventObject){
	$.each(stat_menu_elems, function(){$(this).removeClass('on_click_class')});
	$(this).addClass('on_click_class');
});

stat_menu_elems.hover(//через запятую два события - наведение мышки, отведение мышки.
	function (){
		$(this).addClass('mouse_over_class');
	},
	function (){
		$(this).removeClass('mouse_over_class');
	}
);


//делает невозможным нажатие правой кнопки мыши на изображение
document.oncontextmenu = function(e){
	var target = (typeof e !="undefined")? e.target: event.srcElement
	if (target.tagName == "IMG" || (target.tagName == 'A' && target.firstChild.tagName == 'IMG'))
		return false
}


/*
function resizeChat(chat_container_height) {
	//везде пиксели преобразуются в integer автоматом

	//------------------------------common-----------------------------------------
	$('#chatContainer').css('height', chat_container_height);
	padding_top = 20;//дубликат css. при получении значения не получилось преобразовать в число
	padding_bottom = 30;
	padding = padding_bottom + padding_top;
	table_bottom_part = 20;//нижний кусок от таблицы top5
	//------------------------------common end--------------------------------------

	//------------------------------question----------------------------------------
	question_texts    = 55;//топик, комментарий и зазор между ними.
	img_height        = chat_container_height - padding - question_texts - table_bottom_part;
	$('#img_wrapper').css('height', img_height);
	$('img.demo').css('max-height', img_height);
	//------------------------------question end ------------------------------------
	
	//------------------------------messages and forms-------------------------------
	messages_height     = img_height; //чтоб ровненько было
	$('#wrapper').css('height', messages_height);
	$('#allmessages').css('height', messages_height - 10);
	//$('#allmessages').css('max-height', messages_height);
	//------------------------------messages and forms end---------------------------

	//------------------------------users--------------------------------------------
	var top5_list_height = $('#top5_list').height();
	var users_height     = chat_container_height - padding;
	var users_clearance  = 5;//зазор, чтобы не было скролла
	$('#users').css('height', users_height);
	$('#user_list').css('height', users_height - top5_list_height - users_clearance);
	//------------------------------users end----------------------------------------
};

function resizeStatistics(statistics_container_height) {
	$('#statisticsContainer').css('height', statistics_container_height);
	padding_top = 45, padding_bottom = 36;
	padding = padding_top + padding_bottom;
	statistics_list_height = statistics_container_height - padding;
	$('#statistics_list').css('height',statistics_list_height);
	statistics_menu_height = statistics_list_height;//если экран маленький, то всё ровненько.
	$('#left_statistics_menu, #right_statistics_menu').css('height', statistics_menu_height);
};

function resizeAll(){
	var width             = $(this).width();
	var height            = $(this).height();
	var navbar_height     = $('#navbar').height();
	var footer_height     = 40;//дубликат css. Просто вытащить не получается, выдаёт 10.
	var wrapper_clearance = 10; //зазор для всяких wrapper и container, чтобы не возникало скролла.
	var diff              = navbar_height + footer_height + wrapper_clearance;
	var wrappers_height   = height - diff;
	$('.wrappers_height').css('height', wrappers_height);
	resizeChat(wrappers_height);
	resizeStatistics (wrappers_height);
};

$(function() {
$(window).resize(resizeAll);//если тут тоже сделать setTimeout, то будут скроллы и всякая скверна.
});

setTimeout(resizeAll, 150);//init (работает только с паузой)
*/

});//конец ready