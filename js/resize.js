chatAppControllers.directive('resize', function ($window) {
		return function (scope, element) {
			var w = angular.element($window);
			scope.getWindowDimensions = function () {
				return {
                'h': w.height(),
                'w': w.width()
				};
			};
			scope.$watch(scope.getWindowDimensions, function () {
				setTimeout(resizeAll,50);
			}, true);

			w.bind('resize', function () {
				scope.$apply();
			});
		}
});

function resizeAll(){
	var width             = angular.element(this).width();
	var height            = angular.element(this).height();
	var navbar_height     = angular.element('#navbar').height();
	var footer_height     = 40;//дубликат css. Просто вытащить не получается, выдаёт 10.
	var wrapper_clearance = 15; //зазор для всяких wrapper и container, чтобы не возникало скролла.
	var diff              = navbar_height + footer_height + wrapper_clearance;
	var wrappers_height   = height - diff;
	angular.element('.wrappers_height').css('height', wrappers_height);
	resizeChat(wrappers_height);
	resizeStatistics (wrappers_height);
	resizeSinglePlayer (wrappers_height);
};

function resizeChat(chat_container_height) {
	//везде пиксели преобразуются в integer автоматом

	//------------------------------common-----------------------------------------
	angular.element('#chatContainer').css('height', chat_container_height);
	var padding_top = 20;//дубликат css. при получении значения не получилось преобразовать в число
	var padding_bottom = 13;
	var padding = padding_bottom + padding_top;
	var table_bottom_part = 37;//нижний кусок от таблицы top5
	//------------------------------common end--------------------------------------

	//------------------------------question----------------------------------------
	var question_texts    = 55;//топик, комментарий и зазор между ними.
	var img_height        = chat_container_height - padding - question_texts - table_bottom_part;
	angular.element('#img_wrapper').css('height', img_height);
	angular.element('img.demo').css('max-height', img_height);
	var question_panel_clearance = 10;
	//------------------------------question end ------------------------------------
	
	//------------------------------messages-----------------------------------------
	messages_height     = img_height + question_panel_clearance; //чтоб ровненько было
	angular.element('#wrapper').css('height', messages_height);
	angular.element('#allmessages').css('height', messages_height - 10);
	//$('#allmessages').css('max-height', messages_height);
	//------------------------------messages end-------------------------------------

	//------------------------------users--------------------------------------------
	var top5_list_height = angular.element('#top5_list').height();
	var users_height     = chat_container_height - padding;
	var users_clearance  = 50;//зазор, чтобы не было скролла
	angular.element('#users').css('height', users_height);
	angular.element('#user_list').css('height', users_height - top5_list_height - users_clearance);
	//------------------------------users end----------------------------------------
};

function resizeStatistics(statistics_container_height) {
	angular.element('#statisticsContainer').css('height', statistics_container_height);
	var padding_top = 30, padding_bottom = 23;
	var padding = padding_top + padding_bottom;
	statistics_list_clearance  = 10;
	var statistics_list_height = statistics_container_height - padding - statistics_list_clearance;
	angular.element('#statistics_list').css('height',statistics_list_height);
	//zavalinka
		angular.element('#zavalinka_left').css('height', statistics_list_height);
		
	//end
	var statistics_menu_height = statistics_list_height;//если экран маленький, то всё ровненько.
	angular.element('#left_statistics_menu, #right_statistics_menu').css('height', statistics_menu_height);
};

function resizeSinglePlayer(singleplayer_container_height) {
	angular.element('#singleplayer_container, #text_singleplayer_container').css('height', singleplayer_container_height);
	
	var padding_top = 20;//дубликат css. при получении значения не получилось преобразовать в число
	var padding_bottom = 13;
	var padding = padding_bottom + padding_top;
	var question_texts    = 55;//топик, комментарий и зазор между ними.
	var form = 80;
	
	var img_height        = singleplayer_container_height - padding - question_texts - form;
	angular.element('#singleplayer_img_wrapper').css('height', img_height);
	angular.element('img.demo').css('max-height', img_height);
	
	angular.element('#singleplayer_left, #singleplayer_right, #text_singleplayer_left, #text_singleplayer_right').css('height', img_height);
}

