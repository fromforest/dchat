var chatAppServices = angular.module('chatServices', []);

chatAppServices.factory('chatService', ['$http', function ($http) {

    var svc = {};

    var urlBase = 'ajax.php?action=';

    svc.sayHello = function () {
        console.log('hello from test service 1!');
    };

    svc.login = function (name, password) {
        //        console.log('login request');
        //        console.log('name = ' + name + ' email = ' + email);
        var data = {
            name: name,
            password: password
        };

        return $http.post(urlBase + 'login', data);

        //        var req = {
        //            method: 'POST',
        //            url: urlBase + 'login',
        //            headers: {
        //                'Content-Type': 'application/x-www-form-urlencoded'
        //            },
        //            data: { name: name, email: email }
        //        }
        //        
        //        return $http(req);
    };

    svc.send = function (text) {
        return $http.post(urlBase + "submitChat", {
            chatText: text
        });
    };

    svc.logout = function () {
        return $http.post(urlBase + 'logout');
    };

    svc.checkLogin = function () {
        return $http.get(urlBase + 'checkLogged');
    };

    svc.getMessages = function (last) {
        return $http.post(urlBase + 'getChats' + '&lastID=' + last);
    };

    svc.getUsers = function () {
        //console.log(last);
        return $http.get(urlBase + 'getUsers');
    };
    
    svc.getQuestion = function() {
        return $http.get(urlBase + 'getQuestion');
    };
	
	svc.getStatistics = function (type, number) {
        var data = {
			length_of_top: number
        };

        return $http.post(urlBase + 'getStatistics' + type, data);
    };
	
	svc.getSinglePlayerRandomQuestion = function (theme) {
		var data = {
			theme: theme
		};
		return $http.post(urlBase + 'getSinglePlayerRandomQuestion', data);
	};
	
	svc.getSinglePlayerQuestion = function(theme, is_random) {
		var data = {
			theme: theme,
			is_random: is_random
        };
		return $http.post(urlBase + 'getSinglePlayerQuestion', data);
	};
	
	svc.getSinglePlayerAnswer = function(text) {
		var data = {
			text: text
        };
		
		return $http.post(urlBase + 'getSinglePlayerAnswer', data);
	};
	
	svc.getSinglePlayerLastTheme = function() {
		return $http.post(urlBase + 'getSinglePlayerLastTheme');
	}
	
	svc.getSinglePlayerAllThemes = function() {
		return $http.post(urlBase + 'getSinglePlayerAllThemes');
	}
	
	//-------- text singleplayer ------------
	
	svc.getTextSinglePlayerQuestion = function(theme, is_random) {
		var data = {
			theme: theme,
			is_random: is_random
        };
		return $http.post(urlBase + 'getTextSinglePlayerQuestion', data);
	};
	
	svc.getTextSinglePlayerAnswer = function(text, question_id) {
		var data = {
			text: text,
			question_id: question_id
        };
		
		return $http.post(urlBase + 'getTextSinglePlayerAnswer', data);
	};
	
	svc.getTextSinglePlayerLastTheme = function() {
		return $http.post(urlBase + 'getTextSinglePlayerLastTheme');
	}
	
	svc.getTextSinglePlayerAllThemes = function() {
		return $http.post(urlBase + 'getTextSinglePlayerAllThemes');
	}
	
	
	//--------------zavalinka------------------
	svc.createGame = function(type, room_name) {
		var data = {
			type: type,
			room_name: room_name
		};
		return $http.post(urlBase + 'createGame', data);
	}
	
	svc.joinGame = function(room_name) {
		var data = {
			room_name: room_name
		};
		return $http.post(urlBase + 'joinGame', data);
	}
	
	svc.sendWordsAndTrueDefinitions = function (game_room_id, words, true_definitions) {
		var data = {
			game_room_id: game_room_id,
			words: words,
			true_definitions: true_definitions
		};
		return $http.post(urlBase + 'sendWordsAndTrueDefinitions', data);
	}
	
	svc.sendWordsAndUserDefinitions = function (game_room_id, words, user_definitions) {
		var data = {
			game_room_id: game_room_id,
			words: words,
			user_definitions: user_definitions
		};
		return $http.post(urlBase + 'sendWordsAndUserDefinitions', data);
	}
	
	svc.getZavalinkaUsers = function(game_room_id) {
		var data = {
			game_room_id: game_room_id
		}
		return $http.post(urlBase + 'getZavalinkaUsers', data);
	}
	
	svc.getDefinitionsForCreatorEditing = function(game_room_id) {
		var data = {
			game_room_id: game_room_id
		}
		return $http.post(urlBase + 'getDefinitionsForCreatorEditing', data);
	}
	
	svc.getPhase = function(game_room_id) {
		var data = {
			game_room_id: game_room_id
		}
		return $http.post(urlBase + 'getPhase', data);
	}
	
	svc.getWords = function(game_room_id) {
		var data = {
			game_room_id: game_room_id
		}
		return $http.post(urlBase + 'getWords', data);
	}
	
	svc.startGame = function(game_room_id, edited_users_definitions) {
		var data = {
			game_room_id: game_room_id,
			edited_users_definitions: edited_users_definitions
		}
		return $http.post(urlBase + 'startGame', data);
	}
	
	svc.continueGame = function(game_room_id) {
		var data = {
			game_room_id: game_room_id
		}
		return $http.post(urlBase + 'continueGame', data);
	}
	
	svc.getChoices = function(game_room_id, current_word) {
		var data = {
			game_room_id: game_room_id,
			current_word: current_word
		}
		return $http.post(urlBase + 'getChoices', data);
	}
	
	svc.getChoiceResults = function(game_room_id, current_word) {
		var data = {
			game_room_id: game_room_id,
			current_word: current_word
		}
		return $http.post(urlBase + 'getChoiceResults', data);
	}
	
	svc.getShuffledDefinitions = function (game_room_id) {
		var data = {
			game_room_id: game_room_id
		}
		return $http.post(urlBase + 'getShuffledDefinitions', data);
	}
	
	svc.submitChoice = function (game_room_id, current_word, choiced_definition) {
		var data = {
			game_room_id: game_room_id,
			current_word: current_word,
			choiced_definition: choiced_definition
		}
		return $http.post(urlBase + 'submitChoice', data);
	}
	
	svc.sendFile = function() {
		console.log('lol');
		var data = {
			lol:'lol'
		}
		return $http.post(urlBase + 'sendFile', data);
	}
	
    return svc;

}]);