chatAppControllers.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});

chatAppControllers.controller('SinglePlayerController', ['$scope', '$routeParams', 'chatService', '$timeout', '$interval', '$http' , function ($scope, $routeParams, chatService, $timeout, $interval, $http) {

$scope.submitText = "";
$scope.header = 'Одиночная игра';
$scope.theme = '';
$scope.loggedIn = false;
$scope.working = false;
$scope.current_content_type = 'answer';
$scope.num_asked = 0;
$scope.num_answered = 0;
$scope.randomQuestion = false;
$scope.themes = [];
$scope.previous_index = 0; //для снятия класса,выделяющего строку в табличке по клику.
$scope.questions_count = 0;

$scope.question = {
            topic: 'topic here',
            text: 'this is the question text',
            pictureUrl: 'img/single.jpg',
			is_right: false
        };
		
		
		$scope.getPlaceholder = function() {
			if ($scope.theme) {return 'Нажмите Enter для продолжения...';}
			else return 'Выберите тему';
		};
		
		$scope.sendRequest = function() {
			
			($scope.current_content_type === 'answer') ? $scope.getQuestion() : $scope.getAnswer();
			
			//отмены интервальных запросов, соответствующих многопользовательской игре
			$interval.cancel(gettingQuestion);
			$interval.cancel(gettingStatisticsForRightTable);
			$interval.cancel(gettingMessages);
		};
				
		$scope.getQuestion = function() {
			if ($scope.working) return false;
			$scope.working = true;
			chatService.getSinglePlayerQuestion($scope.theme, $scope.randomQuestion).then(function (response) {
				$scope.submitText = "";
				// ответ сервера лежит в response.data
                var data = response.data;
				if (data.question) {
					$scope.question = data.question;
					$scope.questions_count = data.questions_count;
					$scope.question.pictureUrl = 'questions/' + $scope.question.pictureUrl;
					$scope.current_content_type = ($scope.question.topic === "Тема отсутствует" || $scope.question.topic === 'Сейчас идёт турнир')? 'answer' : 'question';
				}
				$timeout(function () {
							angular.element('#singleplayer_chatText').focus();
                    }, 50);
						
			});
			
			$scope.working = false;
			
		};
		
		$scope.getAnswer = function() {
			if ($scope.working) return false;
			$scope.working = true;
			chatService.getSinglePlayerAnswer($scope.submitText).then(function (response) {
                // ответ сервера лежит в response.data
				$scope.submitText = "";
                var data = response.data;
				if (data.answer) {
					$scope.question = data.answer;
					$scope.question.pictureUrl = 'questions/' + $scope.question.pictureUrl;			
					$scope.num_asked++;
					$scope.num_answered += (data.answer.is_right);
				}
				//обработка data
				
			});
			$scope.working = false;
			
			$scope.current_content_type = 'answer';
		};
		
		$scope.inputTheme = function() {
			angular.element('#theme_input').val('');
			$scope.current_content_type = 'answer';
			$timeout(function () {
							angular.element('#singleplayer_chatText').focus();
						}, 50);
			$scope.sendRequest();
		};

		$scope.submitLogin = function() {

            if ($scope.working) return false;
            $scope.working = true;

            return chatService.login($scope.formName, $scope.formPassword).then(function (response) {

                $scope.working = false;

                var data = response.data;
                //                
                //                console.log('data received: ');
                //                console.log(data);

                if (data.error || !(data.name)) {
                    alert(data.error);
                } else {
                    $scope.loggedIn = true;
                    $scope.myName = data.name;
                    $timeout(function () {
                        angular.element('#singleplayer_chatText').focus();
                    }, 50);
					$scope.getLastTheme();
                }
            });
        };
		
		$scope.submitLogout = function () {
            $scope.loggedIn = false;
            chatService.logout();
            $timeout(function () {
                angular.element('#singleplayer_name').focus();
            }, 50);
        };

		$scope.onFocusButtonSubmit = function() {
            //console.log('get focus');
            //1. Если при вводе сообщения случайно нажат Tab и фокус перешёл к кнопке Submit.
            //2. Если сообщение отправлено мышкой нажатием на кнопку Submit.
            $timeout(function () {
                angular.element('#singleplayer_chatText').focus();
            }, 50);
        };
		
		$scope.getLastTheme = function() {
			$scope.current_content_type = 'answer';
			chatService.getSinglePlayerLastTheme().then(function(response) {
					var theme_data = response.data;
					$scope.theme = theme_data.theme;
					if (theme_data.theme) {
						$timeout(function () {
							angular.element('#singleplayer_chatText').focus();
						}, 50);
						$scope.sendRequest();
						
					}
					else {
						$timeout(function () {
							angular.element('#theme_input').focus();
						}, 50);
					}
			});
		};


		$scope.sortFunction = function(a,b) {
			if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
			if (b.name.toLowerCase() < a.name.toLowerCase()) return 1;
			return 0;
		};
		
		$scope.getSinglePlayerAllThemes = function() {
			chatService.getSinglePlayerAllThemes().then(function(response) {
				var data = response.data;
				data.themes = data.themes.sort($scope.sortFunction);
				$scope.themes = data.themes;
			});
		};
		
		$scope.setTheme = function (index) {
			angular.element('#my_table tr :eq(' + $scope.previous_index + ')' ).removeClass('on_click_class');
			angular.element('#my_table tr :eq(' + index + ')' ).addClass('on_click_class');
			$scope.theme = $scope.themes[index].name;
			$scope.inputTheme();
			$scope.previous_index = index;
			$scope.num_asked = 0;
			$scope.num_answered = 0;
		};

		$scope.mouseEnter = function(index) { //вхождение мышки на строку таблицы
			angular.element('#my_table tr :eq(' + index + ')' ).addClass('mouse_over_class');
		};

		$scope.mouseLeave = function(index) { //уход мышки из строки таблицы
			angular.element('#my_table tr :eq(' + index + ')' ).removeClass('mouse_over_class');
		};
		
		$scope.init = function() {

			$timeout(function() {
				resizeAll();
			}, 100);

            chatService.checkLogin().then(function (response) {
                var data = response.data;
                if (data.logged) {
                    $scope.loggedIn = true;
                    $scope.myName = data.loggedAs.name;
					$timeout(function () {
                        angular.element('#singleplayer_chatText').focus();
                    }, 50);
					$scope.getLastTheme();
                } else {
                    $timeout(function () {
                        angular.element('#singleplayer_name').focus();
                    }, 50);
                }
            });

			$scope.getSinglePlayerAllThemes();
			
        }; //init

        $scope.init();
}]);//controller