var chatAppControllers = angular.module('chatControllers', ['ngAnimate']);

chatAppControllers.controller('SendFileController', ['$scope', '$http', 'chatService', function ($scope, $http, chatService) {
	
		$scope.loggedIn = false;
		$scope.formName = "";
		$scope.formPassword = "";
		$scope.logging = false;
	
		/*$scope.add = function(){
			var f = document.getElementById('file').files[0],
			r = new FileReader();
			r.onloadend = function(e){
				var file_data = e.target.result;
				chatService.sendFile(file_data).then(function (response) {
					var data = response.data;
				});
				//send you binary data via $http or $resource or do anything else with it
			}
			r.readAsBinaryString(f);
		}
		
		$scope.sendFile = function() {
			console.log('I am in');
			chatService.sendFile().then(function(response) {
				var data = response.data;
			});
		}*/
		
		$scope.submitLogin = function () {
            if ($scope.logging)
                return false;
            $scope.logging = true;
            return chatService.login($scope.formName, $scope.formPassword).then(function (response) {
                $scope.logging = false;

                var data = response.data;

                if (data.error || !(data.name)) {
                    alert(data.error);
                } else {
                    $scope.loggedIn = true;
                    $scope.myName = data.name;
                }
            });

        };
		
		$scope.submitLogout = function () {
            $scope.loggedIn = false;
            chatService.logout();
        };
		
		$scope.init = function() {

            chatService.checkLogin().then(function (response) {
                var data = response.data;
                if (data.logged) {
                    $scope.loggedIn = true;
                    $scope.myName = data.loggedAs.name;
				}
            });
			
        }; //init

        $scope.init();
	
    }]);

chatAppControllers.controller('FirstController', ['$scope', '$http', function ($scope, $http) {

        $scope.header = 'header in the first controller';

    }]);

chatAppControllers.controller('AboutController', ['$scope', '$http', function ($scope, $http) {

        $scope.header = 'header in the about controller';

    }]);
	
chatAppControllers.controller('DonateController', ['$scope', '$http', function ($scope, $http) {
		$scope.donateSum = 100;
		
		$scope.setDonateSums = function() {
			button_for_yandex_money = document.getElementById("buttonForYandexMoney");
			button_for_cards = document.getElementById("buttonForCards");
			button_for_phone = document.getElementById("buttonForPhone");
			$scope.linkForYandexMoney = "https://money.yandex.ru/embed/small.xml?account=41001265351958&quickpay=small&yamoney-payment-type=on&button-text=04&button-size=m&button-color=white&targets=%D0%B4%D0%BB%D1%8F+%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0+MQZ&default-sum=" + $scope.donateSum + "&successURL=";
			
			$scope.linkForCards = "https://money.yandex.ru/embed/small.xml?account=41001265351958&quickpay=small&any-card-payment-type=on&button-text=04&button-size=m&button-color=white&targets=%D0%B4%D0%BB%D1%8F+%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0+MQZ&default-sum=" + $scope.donateSum + "&successURL=";
			
			$scope.linkForPhone = "https://money.yandex.ru/embed/small.xml?account=41001265351958&quickpay=small&mobile-payment-type=on&button-text=04&button-size=m&button-color=white&targets=%D0%B4%D0%BB%D1%8F+%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0+MQZ&default-sum=" + $scope.donateSum + "&successURL=";
			
			button_for_yandex_money.src = $scope.linkForYandexMoney;
			button_for_cards.src = $scope.linkForCards;
			button_for_phone.src = $scope.linkForPhone;
		}
		
		
    }]);

chatAppControllers.controller('SecondController', ['$scope', '$routeParams', function ($scope, $routeParams) {

        $scope.header = "header in the second controller";
        $scope.formOfSex = 'жывотные';

    }]);

chatAppControllers.controller('ChatController', ['$scope', '$routeParams', 'chatService', '$timeout', '$interval',
    function ($scope, $routeParams, chatService, $timeout, $interval) {

        $scope.messages = [
            {
                id: 1,
                time: '12:42',
                author: 'System',
                text: 'No messages',
                type: 'public'
            },
        ];

        $scope.lastID = 0,
                $scope.noActivity = 0;

        $scope.formName = "";
        $scope.formPassword = "";

        $scope.loggedIn = false;
        $scope.myName = "undef";
        $scope.logging = false;
        $scope.chatText = "";
        $scope.showTime = false;

        $scope.total = "";

        $scope.users = [
            {
                name: 'empty'
            }
        ];

        $scope.question = {
            topic: 'topic here',
            text: 'this is the question text',
            pictureUrl: 'img/default11.jpg'
        };

        $scope.current_hour_array = [
            {user: '1', score: 0},
            {user: '2', score: 0},
            {user: '3', score: 0},
            {user: '4', score: 0},
            {user: '5', score: 0}
        ];


        $scope.submitLogin = function () {
            if ($scope.logging)
                return false;
            $scope.logging = true;
            return chatService.login($scope.formName, $scope.formPassword).then(function (response) {
                $scope.logging = false;

                var data = response.data;
                //                
                //                console.log('data received: ');
                //                console.log(data);

                if (data.error || !(data.name)) {
                    alert(data.error);
                } else {
                    $scope.loggedIn = true;
                    $scope.myName = data.name;
                    $timeout(function () {
                        angular.element('#chatText').focus();
                    }, 50);
                }
            });

        };

        $scope.sendMessage = function () {
            if ($scope.chatText.length == 0)
                return false;

            var tempID = 't' + Math.round(Math.random() * 1000000);
            var message = {
                id: tempID,
                author: $scope.myName,
                text: $scope.chatText.replace(/</g, '&lt;').replace(/>/g, '&gt;')
            };
            var chat_text = $scope.chatText;
            $scope.chatText = "";
            return chatService.send(chat_text).then(function (response) {



                //
                //                var data = response.data;
                //                message.id = data.insertID;
                //                $scope.lastID = data.insertID;
                //                message.time = $scope.processTime(data.time);

                //                var d = new Date();
                //
                //                // PHP returns the time in UTC (GMT). We use it to feed the date
                //                // object and later output it in the user's timezone. JavaScript
                //                // internally converts it for us.
                //
                //                d.setUTCHours(data.time.hours, data.time.minutes);
                //
                //
                //                message.time = (d.getHours() < 10 ? '0' : '') + d.getHours() + ':' +
                //                    (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();

                // $scope.messages.push(message);

                //$scope.scrollTimeout();
                setTimeout($scope.getMessages(), 50);

                //$scope.lastID++;
            });


        };

        $scope.onFocusButtonSubmit = function () {
            //console.log('get focus');
            //1. Если при вводе сообщения случайно нажат Tab и фокус перешёл к кнопке Submit.
            //2. Если сообщение отправлено мышкой нажатием на кнопку Submit.
            $timeout(function () {
                angular.element('#chatText').focus();
            }, 50);
        };


        $scope.submitLogout = function () {
            $scope.loggedIn = false;
            chatService.logout();
            $timeout(function () {
                angular.element('#name').focus();
            }, 50);
        };


        $scope.getMessageClass = function (msg) {

            //            $timeout(function() {
            //                var selector = '#msg-' + msg.id;
            //                console.log('----------- selector = ' + selector);
            //                angular.element(selector).removeClass("bigmessage");
            //            }, 2000);  

            //return msg.author == 'System' ? 'sysmessage bigmessage' : 'bigmessage';
            var cls = '';
            if (msg.author == 'System') {
                cls += ' sysmessage';
            }
            if (msg.type == 'private') {
                cls += ' private';
            }
            return cls;
        };

        $scope.setPrivate = function (name) {
            $scope.chatText = '@$' + name + '$';
            angular.element('#chatText').focus();
        };

        $scope.get2d = function (num) { //постановка нуля перед одноразрядными числами при выводе времени
            return (num.toString().length < 2 ? "0" + num : num).toString();
        };

        $scope.getMessages = function () {
            //console.log('lastid = ' + $scope.lastID);
            chatService.getMessages($scope.lastID).then(function (response) {
                // ответ сервера лежит в response.data

                var data = response.data;

                var i = 0;

                for (i = 0; i < data.chats.length; i++) {
                    var m = data.chats[i];
                    var local_date = new Date(m.time + ' UTC');//преобразование в местный часовой пояс
                    m.time = $scope.get2d(local_date.getHours()) + ':' +
                            $scope.get2d(local_date.getMinutes()) + ':' + $scope.get2d(local_date.getSeconds());

                    // проверяет, что в последних сообщениях нет такого id
                    // создаем и вызываем анонимную функцию
                    // это хак. dirty hack
                    // область видимости переменной i - функция
                    // жирафы грустят
                    if ( (function(id, ms) {
                        for (var i = ms.length > 50 ? ms.length - 20 : 0; i < ms.length; i++)
                            if (ms[i].id === id) return false;
                        return true;
                    })(m.id, $scope.messages) ) {
                        $scope.messages.push(m);
                    } else {
                        //console.log('dup');
                    }
                    //$scope.messages[m.id] = m;
                }

                //$scope.scrollDown();
                $scope.scrollTimeout();

                if (data.chats.length) {
                    $scope.noActivity = 0;
                    //$scope.lastID = data.chats[i - i].id;
                    $scope.lastID = data.chats[i - 1].id;
                }
                /*else {
                 
                 // If no chats were received, increment
                 // the noActivity counter.
                 
                 $scope.noActivity++;
                 }
                 // Setting a timeout for the next request,
                 // depending on the chat activity:
                 
                 var nextRequest = 1000;
                 
                 // 2 seconds
                 if ($scope.noActivity > 3) {
                 nextRequest = 2000;
                 }
                 
                 if ($scope.noActivity > 10) {
                 nextRequest = 5000;
                 }
                 
                 // 15 seconds
                 if ($scope.noActivity > 20) {
                 nextRequest = 15000;
                 }
                 
                 setTimeout(callback, nextRequest * 3);*/

            });
            // console.log($scope.messages);
        };

        $scope.scrollTimeout = function () {
            setTimeout($scope.scrollDown, 50);
        };

        $scope.scrollDown = function () {
            //var chatDiv = document.getElementById('messages')[0]; //I assume you only have one chat box!
            //chatDiv.scrollTop = chatDiv.scrollHeight;

            //$('#messages').scrollTop(window.Number.MAX_SAFE_INTEGER * 0.001);
            //$('#wrapper').scrollTop($('#messages')[0].scrollHeight);
            $('#wrapper').scrollTop(window.Number.MAX_SAFE_INTEGER * 0.001);
        };

        $scope.getUsers = function () {
            //console.log('begin getUsers');
            //console.log($scope.users);

            chatService.getUsers().then(function (response) {

                var data = response.data;

                $scope.users = [];

                if (data.users) {
                    for (var i = 0; i < data.users.length; i++) {
                        if (data.users[i]) {
                            $scope.users.push(data.users[i]);
                        }
                    }
                }

                $scope.total = "";
                if ($scope.users.length < 1) {
                    $scope.total = "No one is online";
                } else {
                    $scope.total = $scope.users.length + ' ' + ($scope.users.length == 1 ? 'person' : 'people') + ' online';
                }

                //setTimeout(callback, 15000);

            });

            //console.log('end getUsers');
            //console.log($scope.users);
        };

        $scope.getQuestion = function () {

            chatService.getQuestion().then(function (response) {

                var data = response.data;
                //console.log('received from server');
                //console.log(data);

                if (data.question) {
                    $scope.question = data.question;

                    $scope.question.pictureUrl = 'questions/' + $scope.question.pictureUrl;
                }

                //setTimeout(callback, 5000);

            });
        };

        $scope.getStatisticsForRightTable = function () {

            chatService.getStatistics('ForRightTable', 5).then(function (response) {

                var data = response.data;
                if (data.statistics_in_period_array) {
                    var size_of_response = data.statistics_in_period_array.length;
                    for (var key in data.statistics_in_period_array) { //deepcopy
                        $scope.current_hour_array [key] = data.statistics_in_period_array[key];
                    }
                    for (var i = 0; i < (5 - size_of_response); i++) {
                        $scope.current_hour_array [size_of_response + i] = {user: size_of_response + i + 1, score: 0};
                    }
                }

                //alert ($scope.current_hour_array.length);

                //setTimeout(callback, 10000);
            });
        };


        $scope.init = function () {

            $timeout(function () {
                resizeAll();
            }, 100);

            chatService.checkLogin().then(function (response) {
                var data = response.data;
                if (data.logged) {
                    $scope.loggedIn = true;
                    $scope.myName = data.loggedAs.name;
                    $timeout(function () {
                        angular.element('#chatText').focus();
                    }, 50);
                } else {
                    $timeout(function () {
                        angular.element('#name').focus();
                    }, 50);
                }
            });

            /*(function getMessagesTimeoutFunction() {
             $scope.getMessages(getMessagesTimeoutFunction);
             })();*/
			 
			$interval(function () { $scope.getUsers(); }, 10000);
            gettingMessages = 
				$interval(function () { $scope.getMessages(); }, 5000);
            gettingQuestion =
				$interval(function () { $scope.getQuestion(); }, 3000);
            gettingStatisticsForRightTable = 
				$interval(function () { $scope.getStatisticsForRightTable(); }, 20000);

        }; //init

        $scope.init();

    }]); //controller