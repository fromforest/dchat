chatAppControllers.controller('ZavalinkaController', ['$scope', '$routeParams', 'chatService', '$timeout', '$interval',
function ($scope, $routeParams, chatService, $timeout, $interval) {
	
	$scope.loggedIn = false;
	$scope.working = false;
	$scope.preGameButtonsShow = true;
	$scope.createGameFormShow = false;
	$scope.joinGameFormShow = false;
	$scope.backButtonShow   = false;
	$scope.roomCreated = false; //создана ли комната
	$scope.creator_words = ["", ""];
	$scope.words = [];
	$scope.true_definitions = [];
	$scope.creator_definitions = [];
	$scope.user_definitions = [];
	$scope.definitions_for_creator_editing = [];
	$scope.zavalinka_users = [];
	$scope.roomName = "";
	$scope.joined = false;
	$scope.isCreator = false;
	$scope.notCreator = false;
	$scope.isCreatorEditing = false;
	$scope.notCreatorEditing = false;
                $scope.notCreatorChoicedAndWaiting = false;
	$scope.choicing = false;
	$scope.resultWatching = false;
	$scope.game_room_id = -1;
	$scope.last_phase = 10000000;
	$scope.shuffled_definitions = [];
	$scope.choices = [];
	$scope.choice_results = [];
	$scope.endGameShow = false;
	$scope.submitting = false;
	
	
	
	$scope.getCreateGameForm = function() {
			$scope.preGameButtonsShow = false;
			$scope.createGameFormShow = true;
			$scope.backButtonShow   = true;
	};
	
	$scope.getJoinGameForm = function() {
			$scope.preGameButtonsShow = false;
			$scope.joinGameFormShow = true;
			$scope.backButtonShow   = true;
	};
	
	$scope.backAway = function() {
			$scope.preGameButtonsShow = true;
			$scope.joinGameFormShow = false;
			$scope.createGameFormShow = false;
			$scope.backButtonShow   = false;
	}
	
	$scope.addWord = function() {
			$scope.creator_words.push("");
	}
	
	$scope.excludeWord = function() {
			$scope.creator_words.pop();
	}
	
	$scope.sendWordsAndTrueDefinitions = function(game_room_id, words, true_definitions) {
			chatService.sendWordsAndTrueDefinitions(game_room_id, words, true_definitions).then(function (response) {
				// ответ сервера лежит в response.data
                var data = response.data;
				if (data.status !== 'ok') {
					alert('Error: что-то пошло не так');
				}
			});
	}
	
	$scope.sendWordsAndUserDefinitions = function(game_room_id, words, user_definitions) {
			return chatService.sendWordsAndUserDefinitions(game_room_id, words, user_definitions).then(function (response) {
				// ответ сервера лежит в response.data
                var data = response.data;
				if (data.status !== 'ok') {
					alert('Error: что-то пошло не так');
				}
				else {//если всё хорошо
					$scope.user_definitions = [];
				}
			});
	}
	
	$scope.sendUserDefinitions = function () {
			$scope.sendWordsAndUserDefinitions($scope.game_room_id, $scope.words, $scope.user_definitions);
			$scope.notCreatorEditing = false;
			$scope.notCreatorChoicedAndWaiting = true;
			
	}
	
	$scope.submitChoice = function (index) {
			if ($scope.submitting) {
				$scope.notCreatorChoicing = false;
				$scope.choiced = true;
				$scope.notCreatorChoicedAndWaiting = true;
				return 0;
			}
			$scope.submitting = true;
			angular.element('#definitionChoicingTable tr :eq(' + index + ')' ).addClass('on_click_class');
			//console.log($scope.shuffled_definitions[index]);
			chatService.submitChoice($scope.game_room_id, $scope.current_word, $scope.shuffled_definitions[index]).then(function(response) {
				var data = response.data;
				if (data.submitted) {
					$scope.notCreatorChoicing = false;
					$scope.choiced = true;
					$scope.notCreatorChoicedAndWaiting = true;
					$scope.submitting = false;
				}
				else {
					alert('Not submitted. Maybe you want choice your own definition');
					angular.element('#definitionChoicingTable tr :eq(' + index + ')' ).removeClass('on_click_class');
					$scope.submitting = false;
				}
			});
			
		};
	
	$scope.getChoices = function() {
		chatService.getChoices($scope.game_room_id, $scope.current_word).then(function (response) {
			var data = response.data;
			$scope.choices = data.choices;
		});
	}

	$scope.mouseEnter = function(index) { //вхождение мышки на строку таблицы
			angular.element('#definitionChoicingTable tr :eq(' + index + ')' ).addClass('mouse_over_class');
		};

	$scope.mouseLeave = function(index) { //уход мышки из строки таблицы
			angular.element('#definitionChoicingTable tr :eq(' + index + ')' ).removeClass('mouse_over_class');
		};
	
	$scope.createGame = function() {
		chatService.createGame('zavalinka', $scope.roomName).then(function (response) {
                // ответ сервера лежит в response.data
                var data = response.data;
				if (data.room_created) {
					$scope.game_room_id = data.game_room_id
					$scope.roomCreated = true;
					$scope.sendWordsAndTrueDefinitions($scope.game_room_id, $scope.creator_words, $scope.true_definitions);
					$scope.sendWordsAndUserDefinitions($scope.game_room_id, $scope.creator_words, $scope.creator_definitions);
					$scope.joinGameFormShow = false;
					$scope.createGameFormShow = false;
					$scope.backButtonShow   = false;
					$scope.joined = true;
					$scope.isCreator = true;
					$scope.creator_words = [];
					$scope.true_definitions = [];
					$scope.creator_definitions = [];
				}
				else {
					alert('Try another room name');
				}
				//обработка data
				
			});
	}; //createGame
	
	$scope.getWords = function() {
		chatService.getWords($scope.game_room_id).then(function (response) {
			var data = response.data;
			$scope.words = data.words;
		});	
	};
	
	$scope.joinGame = function() {
		chatService.joinGame($scope.roomName).then(function (response) {
			var data = response.data;
			if (data.joined) {
				$scope.game_room_id = data.game_room_id;
				$scope.joinGameFormShow = false;
				$scope.createGameFormShow = false;
				$scope.backButtonShow   = false;
				$scope.joined = true;
				$scope.notCreator = true;//по умолчанию не создатель.
				console.log($scope.words);
			}
			else {
				alert('Entering in this room has failed. May be room has not created');
			}
		});
	};
	
	$scope.startGame = function () {//отправляет отредактированные определения и выставляет phase = 1
			$interval.cancel($scope.editing);
			chatService.startGame($scope.game_room_id, $scope.definitions_for_creator_editing).then(function (response) {
			var data = response.data;
			if (data.started) {
				$scope.isCreatorEditing = false;
				$scope.definitions_for_creator_editing = [];
			}
			else {
				alert('Not started');
			}
		});
	};
	
	$scope.continueGame = function() {//увеличивает phase
		chatService.continueGame($scope.game_room_id).then(function (response) {
			var data = response.data;
			if (data.continued) {
			}
			else {
				alert('Not continued');
			}
		});
		
		
	};//startGame
	
	$scope.submitLogin = function() {

            if ($scope.working) return false;
            $scope.working = true;

            return chatService.login($scope.formName, $scope.formPassword).then(function (response) {

                $scope.working = false;

                var data = response.data;
             
                if (data.error || !(data.name)) {
                    alert(data.error);
                } else {
                    $scope.loggedIn = true;
                    $scope.myName = data.name;
                }
            });
    };//submitLogin
	
	$scope.submitLogout = function () {
            $scope.loggedIn = false;
            chatService.logout();
        };
	
	$scope.getDefinitionsForCreatorEditing = function() {
		if (!$scope.joined) return false;
		if (!$scope.isCreator) return false;
		return chatService.getDefinitionsForCreatorEditing($scope.game_room_id).then(function (response) {
				var data = response.data;
				var delta_length = data.definitions_for_creator_editing.length - $scope.definitions_for_creator_editing.length;
				if (delta_length > 0) {
					var concating_definitions = data.definitions_for_creator_editing.slice(-delta_length);
					$scope.definitions_for_creator_editing = $scope.definitions_for_creator_editing.concat(concating_definitions);
				}
		});
	}
	
	$scope.getChoiceResults = function() {
		return chatService.getChoiceResults($scope.game_room_id, $scope.current_word).then(function(response) {
				var data = response.data;
				$scope.choice_results = data.choice_results;
		});
	}
	
	$scope.getPhase = function() {
		if ((!$scope.joined) || ($scope.game_room_id < 0)) return false;
		return chatService.getPhase($scope.game_room_id).then(function (response) {
			var data = response.data;
			$scope.phase = data.phase;
			$scope.last_phase = data.last_phase;
			$scope.current_word = data.current_word;
			if ($scope.phase === 0) {//фаза приёма и проверки определений
				$scope.isCreatorEditing = $scope.isCreator;
				$scope.notCreatorEditing = $scope.notCreator && !$scope.notCreatorChoicedAndWaiting;
				if (($scope.notCreatorEditing) && ($scope.words.length === 0)) {
					$timeout(function() {$scope.getWords();}, 100);
				}
			}//end phase = 0
			if ($scope.phase > 0) {
				$scope.choicing = (($scope.phase % 2) === 1);
				$scope.resultWatching = (($scope.phase % 2) === 0);
				
				$scope.isCreatorChoiceWaiting = $scope.isCreator && $scope.choicing;
				$scope.notCreatorChoicing     = $scope.notCreator && $scope.choicing && (!$scope.choiced);
				
				$scope.isCreatorResultWatching = $scope.isCreator && $scope.resultWatching;
				$scope.notCreatorResultWatching = $scope.notCreator && $scope.resultWatching;
				
				if ($scope.notCreatorResultWatching) {
					$scope.notCreatorChoicedAndWaiting = false;
					$scope.choiced = false;
				}
				
				if ($scope.phase === $scope.last_phase) {
					$scope.getChoiceResults();
					
					$interval.cancel($scope.promiseGetZavalinkaUsers);
					$interval.cancel($scope.promiseGetPhase);
					$interval.cancel($scope.promiseChoicing);
					$interval.cancel($scope.promiseResultWatching);
					$scope.isCreatorResultWatching = false;
					$scope.endGameShow = true;
				}
			}
		});
	}
	
	$scope.getShuffledDefinitions = function() {
		chatService.getShuffledDefinitions($scope.game_room_id).then(function (response) {
                var data = response.data;
				//$scope.current_word = data.current_word;
				$scope.shuffled_definitions = data.shuffled_definitions;
            });	
	};
	
	$scope.getZavalinkaUsers = function() {
		if ((!$scope.joined) || ($scope.game_room_id < 0)) return false;
		chatService.getZavalinkaUsers($scope.game_room_id).then(function (response) {
                var data = response.data;
                $scope.zavalinka_users = data.game_users;
				$scope.isCreator        = data.is_creator;
				$scope.notCreator       = !data.is_creator;
				
            });	
	};
	
	$scope.init = function() {
			angular.element('nav, footer').css('display', 'none');
			$timeout(function() {
				resizeAll();
			}, 100);

            chatService.checkLogin().then(function (response) {
                var data = response.data;
                if (data.logged) {
                    $scope.loggedIn = true;
                    $scope.myName = data.loggedAs.name;
                } else {
                    $timeout(function () {
                        angular.element('#zavalinka_name').focus();
                    }, 50);
                }
            });
			
			$scope.promiseGetZavalinkaUsers = $interval(function () { $scope.getZavalinkaUsers(); }, 9000);
			$scope.promiseGetPhase = $interval(function () { $scope.getPhase(); }, 3000);
			
			$scope.editing = $interval(function () { 
				if (($scope.isCreatorEditing) && ($scope.phase === 0)) {
					$scope.getDefinitionsForCreatorEditing();
				}
			}, 2000);
			
			$scope.promiseChoicing = $interval(function() {
				if (($scope.choicing) && ($scope.shuffled_definitions.length === 0)) {
					$scope.getShuffledDefinitions();
					$scope.choice_results = [];
				}
				
				if ($scope.isCreatorChoiceWaiting) {
					$scope.getChoices();
				}
			}, 4000);
			
			$scope.promiseResultWatching = $interval(function() {
				if (($scope.resultWatching) && ($scope.choice_results.length === 0)) {
					$scope.getChoiceResults();
					$scope.shuffled_definitions = [];
				}
			}, 3000);
			
			
				
        }; //init
		
	$scope.init();
	
}]);//controller