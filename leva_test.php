<?php

require "utils.php";

echo levenshtein_utf8("Хруст", "Хрусть");
echo "\n";
echo levenshtein_utf8("хлеб", "пиво");
echo "\n";
echo levenshtein_utf8("банан", "бананан");
echo "\n";
echo levenshtein_utf8("еж", "ёж");
echo "\n";
