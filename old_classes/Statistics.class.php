<?php

use \Propel\Runtime\Propel;

class Statistics {

    public static function getTotalScore(User $u) {
        $n = $u->getRightAnswers()->count();
        return $n;
    }

    public static function getCurrentTournamentScore(User $u) {
        $current_tournament = Cfg::getTournamentNumber();
        $n = RightAnswerQuery::create()->filterByUser($u)->filterByType($current_tournament)->count();
        return $n;
    }

//----------------------------- Статистика по ответам начало ----------------------------------
    public static function getStatisticsInPeriod($min_time, $max_time, $number) {
        $statistics_objects = RightAnswerQuery::create()
                ->filterByCreatedAt([
                    'min' => $min_time,
                    'max' => $max_time])
                ->filterByType(0)
                ->join('User')
                ->withColumn('count(User.Id)', 'score')
                ->groupBy('RightAnswer.UserId')
                ->select(['User.Name', 'score'])
                ->orderBy('score', 'desc')
                ->limit($number)
                ->find();

        $statistics = [];
        foreach ($statistics_objects as $stat) {
            $as_array['user'] = $stat['User.Name'];
            $as_array['score'] = $stat['score'];
            $statistics [] = $as_array;
        }

        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsInPeriod

    public static function getStatisticsCurrentHour($number) {
        $date = getdate();
        $min_time = mktime($date["hours"], 0, 0, $date["mon"], $date["mday"], $date["year"]);
        return self::getStatisticsInPeriod($min_time, time(), $number);
    }

// end getStatisticsCurrentHour

    public static function getStatisticsPreviousHour($number) {
        //делаем так, а то предыдущий час может быть с предыдущего дня или даже года
        $date = getdate();
        $minutes = $date ["minutes"];
        $seconds = $date ["seconds"];
        $seconds_in_current_hour = $minutes * 60 + $seconds;
        $seconds_from_start_of_previous_hour = 3600 + $seconds_in_current_hour;
        return self::getStatisticsInPeriod(time() - $seconds_from_start_of_previous_hour, time() - $seconds_in_current_hour - 1, $number);
    }

// end getStatisticsPreviousHour

    public static function getStatisticsCurrentDay($number) {
        $date = getdate();
        $min_time = mktime(0, 0, 0, $date["mon"], $date["mday"], $date["year"]);
        return self::getStatisticsInPeriod($min_time, time(), $number);
    }

//end getStatisticsCurrentDay

    public static function getStatisticsPreviousDay($number) {
        //делаем так, а то предыдущий день может быть с предыдущего месяца или даже года
        $date = getdate();
        $minutes = $date ["minutes"];
        $seconds = $date ["seconds"];
        $hours = $date ["hours"];
        $seconds_in_current_day = $hours * 3600 + $minutes * 60 + $seconds;
        $seconds_from_start_of_previous_day = 3600 * 24 + $seconds_in_current_day;
        return self::getStatisticsInPeriod(time() - $seconds_from_start_of_previous_day, time() - $seconds_in_current_day - 1, $number);
    }

// end getStatisticsPreviousDay

    public static function getStatisticsCurrentMonth($number) {
        $date = getdate();
        $min_time = mktime(0, 0, 0, $date["mon"], 1, $date["year"]);
        return self::getStatisticsInPeriod($min_time, time(), $number);
    }

// getStatisticsCurrentMonth

    public static function getStatisticsPreviousMonth($number) {
        $date = getdate();
        $previous_month = $date['mon'] - 1;
        $year = $date['year'];
        if ($previous_month === 0) {
            $previous_month = 12;
            $year = $year - 1;
        }

        $min_time = mktime(0, 0, 0, $previous_month, 1, $year);
        $max_time = mktime(23, 59, 59, $previous_month + 1, 0, $year); //Последний день любого месяца можно вычислить как "нулевой" день следующего месяца
        return self::getStatisticsInPeriod($min_time, $max_time, $number);
    }

// end getStatisticsPreviousMonth

    public static function getStatisticsCurrentYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $current_year);
        return self::getStatisticsInPeriod($min_time, time(), $number);
    }

// end getStatisticsCurrentYear

    public static function getStatisticsPreviousYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $current_year - 1);
        $max_time = mktime(23, 59, 59, 12, 31, $current_year - 1);
        return self::getStatisticsInPeriod($min_time, $max_time, $number);
    }

// end getStatisticsPreviousYear

    public static function getStatisticsTotalScore($number) {
        return self::getStatisticsInPeriod(0, time(), $number);
    }

// end getStatisticsTotalScore
//----------------------------- Статистика по ответам конец -------------------------------------
//----------------------------- Статистика по лучшим промежуткам начало -------------------------

    public static function getStatisticsBestHours($min_date_string, $max_date_string, $number) {
        $con = Propel::getConnection();
        $query = "select hour as period, max(score) as score, user from "
                . "(select "
                . "DATE_FORMAT(right_answers.created_at, '%Y-%m-%d %H') as hour, users.name as user, count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where (DATE_FORMAT (right_answers.created_at,'%Y-%m-%d %H') >= :min "
                . "and DATE_FORMAT (right_answers.created_at,'%Y-%m-%d %H')   <= :max) "
                . "and right_answers.type = 0 "
                . "group by users.name, DATE_FORMAT(right_answers.created_at,'%Y-%m-%d %H') "
                . "order by count(right_answers.id) desc " //если не будет этого desc, то не всегда берутся соответствующие даты
                . ") as select_result1 "
                . "group by user "
                . "order by max(score) "
                . "desc limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':min', $min_date_string, PDO::PARAM_STR);
        $stmt->bindValue(':max', $max_date_string, PDO::PARAM_STR);
        $res = $stmt->execute();
        $statistics = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return ['best_periods' => $statistics];
    }

//end getStatisticsBestHours

    public static function getStatisticsBestDays($min_date_string, $max_date_string, $number) {
        $con = Propel::getConnection();
        $query = "select day as period, max(score) as score, user from "
                . "(select "
                . "DATE_FORMAT(right_answers.created_at, '%Y-%m-%d') as day, users.name as user, count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where (DATE_FORMAT (right_answers.created_at,'%Y-%m-%d') >= :min "
                . "and DATE_FORMAT (right_answers.created_at,'%Y-%m-%d')   <= :max) "
                . "and right_answers.type = 0 "
                . "group by users.name, DATE_FORMAT(right_answers.created_at,'%Y-%m-%d') "
                . "order by count(right_answers.id) desc " //если не будет этого desc, то не всегда берутся соответствующие даты
                . ") as select_result1 "
                . "group by user "
                . "order by max(score) "
                . "desc limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':min', $min_date_string, PDO::PARAM_STR);
        $stmt->bindValue(':max', $max_date_string, PDO::PARAM_STR);
        $res = $stmt->execute();
        $statistics = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return ['best_periods' => $statistics];
    }

//end getStatisticsBestHours

    public static function getStatisticsBestHoursTotal($number) {
        $min_date_string = '2015-08-02 00';
        $max_date_string = date("Y-m-d H", time());
        return self::getStatisticsBestHours($min_date_string, $max_date_string, $number);
    }

// end getStatisticsBestHoursTotal

    public static function getStatisticsBestHoursCurrentMonth($number) {
        $date = getdate();
        $current_month = $date['mon'];
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, $current_month, 1, $current_year);
        $min_date_string = date("Y-m-d H", $min_time);
        $max_date_string = date("Y-m-d H", time());
        return self::getStatisticsBestHours($min_date_string, $max_date_string, $number);
    }

// getStatisticsBestHoursCurrentMonth

    public static function getStatisticsBestHoursCurrentYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $current_year);
        $min_date_string = date("Y-m-d H", $min_time);
        $max_date_string = date("Y-m-d H", time());
        return self::getStatisticsBestHours($min_date_string, $max_date_string, $number);
    }

// getStatisticsBestHoursCurrentYear

    public static function getStatisticsBestDaysTotal($number) {
        $min_date_string = '2015-08-02';
        $max_date_string = date("Y-m-d", time());
        return self::getStatisticsBestDays($min_date_string, $max_date_string, $number);
    }

// getStatisticsBestDaysTotal

    public static function getStatisticsBestDaysCurrentYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $current_year);
        $min_date_string = date("Y-m-d", $min_time);
        $max_date_string = date("Y-m-d", time());
        return self::getStatisticsBestDays($min_date_string, $max_date_string, $number);
    }

// getStatisticsBestDaysCurrentYear

    public static function getStatisticsBestMonthsTotal($number) {
        $min_date_string = '2015-08';
        $max_date_string = date("Y-m", time());
        $con = Propel::getConnection();
        $query = "select month as period, max(score) as score, user from "
                . "(select "
                . "DATE_FORMAT(right_answers.created_at, '%Y-%m') as month, users.name as user, count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where DATE_FORMAT (right_answers.created_at,'%Y-%m') >= :min "
                . "and DATE_FORMAT (right_answers.created_at,'%Y-%m')   <= :max "
                . "and right_answers.type = 0 "
                . "group by users.name, DATE_FORMAT(right_answers.created_at,'%Y-%m') "
                . "order by count(right_answers.id) desc " //если не будет этого desc, то не всегда берутся соответствующие даты
                . ") as select_result1 "
                . "group by user "
                . "order by max(score) "
                . "desc limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':min', $min_date_string, PDO::PARAM_STR);
        $stmt->bindValue(':max', $max_date_string, PDO::PARAM_STR);
        $res = $stmt->execute();
        $statistics = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return ['best_periods' => $statistics];
    }

// getStatisticsBestMonthsTotal

    public static function getStatisticsBestYears($number) {
        $min_date_string = '2015';
        $max_date_string = date("Y", time());
        $con = Propel::getConnection();
        $query = "select year as period, max(score) as score, user from "
                . "(select "
                . "DATE_FORMAT(right_answers.created_at, '%Y') as year, users.name as user, count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where DATE_FORMAT (right_answers.created_at,'%Y') >= :min "
                . "and DATE_FORMAT (right_answers.created_at,'%Y')   <= :max "
                . "and right_answers.type = 0 "
                . "group by users.name, DATE_FORMAT(right_answers.created_at,'%Y') "
                . "order by count(right_answers.id) desc " //если не будет этого desc, то не всегда берутся соответствующие даты
                . ") as select_result1 "
                . "group by user "
                . "order by max(score) "
                . "desc limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':min', $min_date_string, PDO::PARAM_STR);
        $stmt->bindValue(':max', $max_date_string, PDO::PARAM_STR);
        $res = $stmt->execute();
        $statistics = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return ['best_periods' => $statistics];
    }

// getStatisticsBestYears
//----------------------------- Статистика по лучшим промежуткам конец ---------------------------
//----------------------------- Статистика по цепочкам начало ------------------------------------
    public static function getStatisticsChains($min_time, $max_time, $number) {
        $statistics_objects = RightAnswerQuery::create()
                ->filterByCreatedAt([
                    'min' => $min_time,
                    'max' => $max_time])
                ->filterByType(0)
                ->filterByNum(['min' => 2])
                ->join('User')
                ->withColumn('max(RightAnswer.Num)', 'score')
                ->withColumn('User.Name', 'user')
                ->groupBy('RightAnswer.UserId')
                ->select(['user', 'score'])
                ->orderBy('score', 'desc')
                ->limit($number)
                ->find();

        $statistics = [];
        foreach ($statistics_objects as $stat) {
            $as_array['user'] = $stat['user'];
            $as_array['score'] = $stat['score'];
            $statistics [] = $as_array;
        }

        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsChains

    public static function getStatisticsChainsTotal($number) {
        $min_time = mktime(0, 0, 0, 8, 2, 2015);
        return self::getStatisticsChains($mintime, time(), $number);
    }

// end getStatisticsChainsTotal

    public static function getStatisticsChainsCurrentMonth($number) {
        $date = getdate();
        $min_time = mktime(0, 0, 0, $date['mon'], 1, $date['year']);
        return self::getStatisticsChains($min_time, time(), $number);
    }

// end getStatisticsChainsCurrentMonth

    public static function getStatisticsChainsCurrentYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $date['year']);
        return self::getStatisticsChains($min_time, time(), $number);
    }

// end getStatisticsChainsCurrentYear
//----------------------------- Статистика по цепочкам конец ------------------------------------

    protected static function _functionForSort($a, $b) { //функция для сортировки usort, использующейся в рейтинге MQZ
        if ($a['score'] === $b['score']) {
            return 0;
        }
        return ($a['score'] < $b['score']) ? 1 : -1;
    }

// end _functionForSort

    protected static function _getAllUserNames() {
        return UserQuery::create()
                        ->select('User.Name')
                        ->find();
    }

// end _getAllUserNames

    public static function getHourScores($user_name, $number) {
        $con = Propel::getConnection();
        $query = "select  "
                . "count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where users.name = :p2 "
                . "and right_answers.type = 0 "
                . "group by DATE_FORMAT(right_answers.created_at,'%Y-%m-%d %H') "
                . "order by count(right_answers.id) desc "
                . "limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':p2', $user_name, PDO::PARAM_STR);
        $res = $stmt->execute();
        $hour_scores = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $hour_scores;
    }

// end getHourScores

    protected static function _getHoursHystogram($hour_scores) { //распределение часов по результатам
        $freq = [];
        foreach ($hour_scores as $hour_score) {
            $hs = (string) $hour_score['score'];
            $freq[$hs] = array_key_exists($hs, $freq) ? $freq[$hs] : 1;
        }
        return $freq;
    }

    public static function getStatisticsMQZ($number) {
        $user_names_objects = self::_getAllUserNames();
        $statistics = [];
        foreach ($user_names_objects as $user_name) {
            $hour_scores = self::getHourScores($user_name, $number); //все часы юзера
            $freq = self::_getHoursHystogram($hour_scores); //распределение часов по результатам
            $mqz = 1;
            $sum = 0;
            if (count($freq) > 0) { //юзеры без единого ответа не попадут в статистику
                for (reset($freq); ($key = key($freq)); next($freq)) { //алгоритм Альбы
                    $sum += $freq[$key];
                    if ($sum >= (int) $key) {
                        $mqz = (int) $key;
                        break;
                    }
                } // конец алгоритма Альбы
                $as_array ['score'] = $mqz;
                $as_array ['user'] = $user_name;
                $statistics [] = $as_array;
            }
        }// цикл обхода юзеров
        usort($statistics, 'self::_functionForSort');
        return ['statistics_in_period_array' => $statistics];
    }

//end getStatisticsMQZ

    public static function getStatisticsOnAnyFiftyChallenges($number) {
        $statistics_objects = UserQuery::create()
                ->select(['Name', 'BestFifty'])
                ->filterByBestFifty(['min' => 1])
                ->orderBy('BestFifty', 'desc')
                ->limit($number)
                ->find();

        $statistics = [];
        foreach ($statistics_objects as $stat) {
            $as_array['user'] = $stat['Name'];
            $as_array['score'] = $stat['BestFifty'];
            $statistics [] = $as_array;
        }

        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsOnAnyFiftyChallenges

    public static function getStatisticsInTournament($tournament_number, $number) {
        $statistics_objects = RightAnswerQuery::create()
                ->filterByType($tournament_number)
                ->join('User')
                ->withColumn('count(User.Id)', 'score')
                ->groupBy('RightAnswer.UserId')
                ->select(['User.Name', 'score'])
                ->orderBy('score', 'desc')
                ->limit($number)
                ->find();

        $statistics = [];
        foreach ($statistics_objects as $stat) {
            $as_array['user'] = $stat['User.Name'];
            $as_array['score'] = $stat['score'];
            $statistics [] = $as_array;
        }

        return ['statistics_in_period_array' => $statistics];
    }

    public static function getStatisticsLastTournament($number) {
        $tournament_number = Cfg::getTournamentNumber() - 1;
        return self::getStatisticsInTournament($tournament_number, $number);
    }

    public static function getStatisticsCurrentTournament($number) {
        $tournament_number = Cfg::getTournamentNumber();
        return self::getStatisticsInTournament($tournament_number, $number);
    }

}

//class
