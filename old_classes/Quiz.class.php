<?php

use \Propel\Runtime\ActiveQuery\Criteria;

class Quiz {
	
	/*public static function writeln ($file_name, $string1) {
		$file = fopen ($file_name, "a+t");
		fwrite ($file, $string1."\n");
		fclose ($file);
	}*/
	
	protected static function _incrementNumAsked(Question $question) {
	//Инкремент параметра num_asked у question. Отрабатывает для question в текущем challenge перед созданием нового challenge. Если количество онлайн-юзеров меньше заданного в конфиге, то инкремент не происходит.
		$min_users_for_updates = Cfg::getMinUsersForUpdates();
		$users_online_count    = UserQuery::create()->filterByOnline(true)->count();
		if ($users_online_count >= $min_users_for_updates) {
			$num_asked = $question->getNumAsked();
			$num_asked += 1;
			$question->setNumAsked($num_asked);
			$question->save();
		}
	}
	
	protected static function _incrementNumAnswered(Question $question) {
	//Инкремент параметра num_answered у question. Отрабатывает для question в текущем challenge во время проверки правильного ответа юзера. Если количество онлайн-юзеров меньше заданного в конфиге, то инкремент не происходит. Если строка с этим question в БД недавно обновлялась (чей-то верный ответ уже выполнил incrementNumAnswered), то инкремент не происходит.
		$min_users_for_updates = Cfg::getMinUsersForUpdates();
		$users_online_count    = UserQuery::create()->filterByOnline(true)->count();
		$current_challenge     = self::_getLastChallenge();
		
		if (($users_online_count >= $min_users_for_updates) &&
			($question->getUpdatedAt() < ($current_challenge->getCreatedAt() )))
		{
			$num_answered = $question->getNumAnswered();
			$num_answered += 1;
			$question->setNumAnswered($num_answered);
			$question->save();
		}
	}

    protected static function _getLastChallenge() {
        $last_challenge = ChallengeQuery::create()->orderById(Criteria::DESC)->findOne();
        return $last_challenge;
    }

    public static function getChallenge() {
        return self::_getLastChallenge();
    }

    public static function updateChallenge() {
		
        $last_challenge = self::_getLastChallenge();
        $qtime = Cfg::getQuestionTime();
        $created_time = $last_challenge->getCreatedAt();
		
		//на всякий случай. Если вдруг из-за каких-то причин ChallengeUpdated в конфиге не сбросится в 0, когда надо, то по прошествии двойного времени вопроса он сбрасывается в 0 и новый challenge будет создан, викторина продолжается.
		if ($created_time < new DateTime(2*$qtime . " seconds ago")) {
			Cfg::setChallengeUpdated(0);
		}

		$moment_ago = new DateTime($qtime . " seconds ago");
		
        if ($created_time < $moment_ago) {
			if (Cfg::getChallengeUpdated() > 0) {
				return 0;
			}
			Cfg::setChallengeUpdated(Cfg::getChallengeUpdated() + 1);
			
            self::_incrementNumAsked($last_challenge->getQuestion()); //инкремент предыдущего заданного вопроса. Если это делать раньше, то возникнут проблемы с update num_answered.
			
			$q = $last_challenge->getQuestion();
			$q->setUpdatedAt(new DateTime("now"));//установка обновления предыдущего вопроса, показывающего, что вопрос был задан. это важно для проверки внутри _excludeRecentQuestions
			$q->save();
		
			if (Cfg::getChallengeUpdated() > 1) {
				return 0;
			}
			Cfg::setChallengeUpdated(Cfg::getChallengeUpdated() + 1);


            $challenge = new Challenge();
            //$random_question = self::_getRandomQuestion();
			$quiz_mode = Cfg::getQuizMode();
			if ($quiz_mode === 'tournament') { //если идёт турнир, вопросы задаются по порядку
				if (!self::_isTournamentEnd()) {
					$next_question = self::_getTournamentQuestion();
				}
			}
			else { //если идёт регулярная викторина, то следующий вопрос по особому алгоритму
				$next_question = self::_getNextQuestion();
				//$next_question = self::_getRandomQuestion();
			}
			if ($next_question) {
				$challenge->setQuestion($next_question);
				$challenge->save();
			}
			else {
				throw new Exception("question has not selected");
			}
        }
    }
	
	protected static function _getCurrentDifficulty() { //получить сложность для вопроса. Находим остаток от деления challenge_id на длину кортежа сложности в конфиге. Например, если challenge_id = 2016, а кортеж сложности easy:medium:medium:easy:hard, то 2016 mod 5 = 1, то есть medium.
		$current_challenge = self::_getLastChallenge();
		$current_challenge_id = $current_challenge->getId();		
		$difficulty_sequence = Cfg::getDifficultySequence();
		$difficulty_elements = explode(':', $difficulty_sequence);
		$index = $current_challenge_id % count($difficulty_elements);
		$current_difficulty = $difficulty_elements[$index];
		return $current_difficulty;
	}
	
	public static function functionForSort(Question $a, Question $b) { //функция для сортировки usort, использующейся в _getNextQuestion (отвечабельность вопроса). Неравенства < и > именно такие, какие нужно.
		$a_asked    = $a->getNumAsked();
		$b_asked    = $b->getNumAsked();
		$a_answered = $a->getNumAnswered();
		$b_answered = $b->getNumAnswered();
		
		if ($a_asked*$b_asked === 0) {//какой-то из вопросов ни разу не задавался
			if ($a_asked === $b_asked) //оба ни разу не задавались
				return 0;
			return ($a_asked === 0) ? -1 : 1; //не заданный ни разу вопрос имеет приоритет
		} else {
			if (($a_answered === 0) && ($b_answered === 0)) {
				$condition = ($a_asked < $b_asked);
				return ($condition ? -1 : 1);
			}
			if ($a_answered/$a_asked === $b_answered/$b_asked)
				return 0;
				
			$condition = ($a_answered/$a_asked < $b_answered/$b_asked); //более отвечабельный вопрос имеет приоритет
			return ($condition ? 1 : -1);
		}
		
	}// end functionForSort
	
	protected static function _excludeRecentQuestions(array $questions) {
		if (gettype($questions[0]) !== 'object') {
			throw new InvalidArgumentException("Type of elements in array questions must be object");
		}

		if (!method_exists($questions[0], 'getPackageId')) {
			throw new InvalidArgumentException("Type of objects in array questions must be Question");
		}
		
		$not_recent_questions = [];
		$min_between_equal = Cfg::getMinTimeBetweenEqualQuestions();
		
		foreach ($questions as $q) {
			if ($q->getUpdatedAt() < (new DateTime($min_between_equal . " seconds ago"))) {
				$not_recent_questions []= $q;
			}
		}
		
		return $not_recent_questions;
	}//end _excludeRecentQuestions

	public static function log_questions(array $questions) {
		foreach ($questions as $q) {
			self::writeln('log.txt', $q->getNumAnswered() . ' ' . $q->getNumAsked() . ' ' . $q->getRightAnswer() );
		}
	}
	
	public static function _getNextQuestion() { //алгоритм: репозиторий ideas, папка Hr/func2
		$number = 99; //сколько выбираем рандомных вопросов, чтобы потом из них выбрать единственный
		$q_portion = ($number/3);
		//1. берём абсолютно случайно $number вопросов и сортируем по возрастанию сложности
		$questions = self::_getRandomQuestions($number);
		if (count($questions) !== $number) {
			throw new LengthException("_getRandomQuestions must return " . $number . " questions, but returned only " . count($questions));		
		}
		usort($questions, 'self::functionForSort');
		//2. Исходя из кортежа сложности находим текущую сложность и отбираем верхние, средние или нижние number/3 вопроса в отсортированном списке.
		$current_difficulty = self::_getCurrentDifficulty();
		if ($current_difficulty === 'easy') {
			$questions = array_slice($questions, 0, $q_portion);
		}
		elseif ($current_difficulty === 'medium') {
			$questions = array_slice($questions, $q_portion, $q_portion);
		}
		elseif ($current_difficulty === 'hard') {
			$questions = array_slice($questions, 2*$q_portion, $q_portion);
		}
		else {
			throw new UnexpectedValueException("difficulty_sequence has invalid element");
		}
		
		if (count($questions) !== $q_portion) {
			throw new LengthException("after array_slice must be remained " . $q_portion . " questions, but we have only " . count($questions));		
		}
		//3. убираем вопросы, которые недавно задавались
		$questions = self::_excludeRecentQuestions($questions);

		//4. Если ничего в списке не осталось, берём рандомный вопрос. А так берём из оставшихся случайный.
		if (count($questions) === 0) {
			return self::_getRandomQuestion();
		} else {
			$index = rand(0, count($questions) - 1);
			return $questions[$index];
		}
		
	}
	
	protected static function _announceResults() {
		Chat::sysMessage('Турнир завершён! Результаты:');
		$tournament_results = Statistics::getStatisticsLastTournament(10000)['statistics_in_period_array'];
		foreach ($tournament_results as $tr) {
			Chat::sysMessage('      '. $tr['user'] . ' ' . $tr['score']);
		}
	}
	
	protected static function _isTournamentEnd() {
		$index = Cfg::getTqi();
		$questions = self::getAllQuestionsIds();
		if ($index < count($questions)) {
			return false;
		}
		else {
			Cfg::setQuizMode('stop');
			Cfg::setTqi(0);
			$tournament_number = Cfg::getTournamentNumber();
			Cfg::setTournamentNumber($tournament_number + 1);
			self::_announceResults();
			return true;
		}
	}
	protected static function _getTournamentQuestion() {
		$index = Cfg::getTqi();
		$questions = self::getAllQuestionsIds();
		$id = $questions[$index];
		$question = QuestionQuery::create()->findOneById($id);
		Cfg::setTqi($index+1);
		return $question;
	}

//    protected static function _getRandomQuestion() {
//        // get array of ids
//        $ids = QuestionQuery::create()->select('id')->find()->toArray();
//        // get random element from it
//        $index_of_id = rand(0, count($ids) - 1);
//        // this is random id from questions
//        $id = $ids[$index_of_id];
//        // find question with this id
//        $question = QuestionQuery::create()->findOneById($id);
//        // return what we want
//        return $question;
//    }
	public static function getAllQuestionsIds() {
		$pstring = Cfg::getPackages(); // get string
		$blacklist = Cfg::getBlackList();
		$blacks = explode(':', $blacklist);

		if (($pstring === 'All') || ($pstring === 'all')) {
			$all_packages = PackageQuery::create()->find();
			foreach ($all_packages as $p) {
				$pnames []= $p->getName();
			}
		}
		else {
			$pnames = explode(':', $pstring); // array of names
		}
	
		$pnames = array_diff($pnames,$blacks);
        //now get packages
        $packages = PackageQuery::create()->filterByName($pnames)->find();

        $ids = array();
        foreach ($packages as $p) {
            $qs = $p->getQuestions();
            foreach($qs as $q) {
                $ids[] = $q->getId();
            }
        }
		return $ids;
	}
	
    protected static function _getRandomQuestion() {
        $ids = self::getAllQuestionsIds();
        $index_of_id = rand(0, count($ids) - 1);
        // this is random id from questions
        $id = $ids[$index_of_id];
        // find question with this id
        $question = QuestionQuery::create()->findOneById($id);
        // return what we want
        return $question;
    }
	
	protected static function _getRandomQuestions($number) {
		$ids = self::getAllQuestionsIds();
		$questions = [];
		for ($i = 0; $i < $number; $i++) {			
			$index_of_id = rand(0, count($ids) - 1);
			$id = $ids[$index_of_id];
			$question = QuestionQuery::create()->findOneById($id);
			$questions []= $question;
		}
		return $questions;
	}

    public static function currentQuestion() {
        $challenge = self::getChallenge();
        $current_question = $challenge->getQuestion();
        return $current_question;
    }

//    protected static function _isEqual($s1, $s2) {
//		$s11 = str_replace ('ё', 'е', $s1);
//		$s22 = str_replace ('ё', 'е', $s2);		
//		$s11 = str_replace ('Ё', 'Е', $s11);
//		$s22 = str_replace ('Ё', 'Е', $s22);
//
//        if ($s11 == $s22 ||
//                mb_convert_case($s11, MB_CASE_LOWER, "UTF-8") ==
//                mb_convert_case($s22, MB_CASE_LOWER, "UTF-8")) {
//            return true;
//        } else {
//            return false;
//        }
//    }
    
    protected static function _isEqual($s1, $s2) {
        return  str_replace('ё', 'е', mb_convert_case($s1, MB_CASE_LOWER, "UTF-8")) ===
                str_replace('ё', 'е', mb_convert_case($s2, MB_CASE_LOWER, "UTF-8"));
    }

	protected static function _setBestFiftyScore($user, $current_challenge_id) {
		$score_on_previous_49_challenges = 
				RightAnswerQuery::create()
				->filterByUser($user)
				->filterByChallengeId(['min' => $current_challenge_id - 49, 'max' => $current_challenge_id - 1])
				->count($con);
		
		$current_score_on_last_fifty = $score_on_previous_49_challenges + 1;
		$best_score_on_last_fifty    = $user->getBestFifty();
		if ($current_score_on_last_fifty > $best_score_on_last_fifty) {
			$user -> setBestFifty($current_score_on_last_fifty);
			$user -> save();
		}
	}

    public static function isRightAnswer($msg, Question $q) {
        if (self::_isEqual($msg, $q->getRightAnswer())) {
            return true;
        }

        $alt = $q->getAltAnswers();
        $alts = explode(':', $alt);

        foreach ($alts as $a) {
            if (self::_isEqual($msg, $a)) {
                return true;
            }
        }
        return false;
    }

    protected static function _answered(User $u, Challenge $c) {
        $a = RightAnswerQuery::create()->filterByUser($u)->filterByChallenge($c)->findOne();
        return $a ? true : false;
    }

    public static function checkAnswer(Message $msg) {
		$quiz_mode = Cfg::getQuizMode();
		if ($quiz_mode === 'stop') { //викторина остановлена
			return false;
		}
        if ($msg->getToId() != 0) { // private message
            return false;
        }
        $challenge = self::getChallenge();
        $text = $msg->getText();
        $user = $msg->getUser();
		$sys =  Chat::getSystemUser();
        $question = $challenge->getQuestion();
		
		$q_time = Cfg::getQuestionTime();
		$a_time = Cfg::getAnswerTime();
		$sec = $q_time - $a_time;

        if (self::isRightAnswer($text, $question)) {
            if (self::_answered($user, $challenge)) {//already?
			
			//преобразование сообщения пользователя в privateSysMessage ему же
				
				$toUser = $msg->getUser();
				$msg->setUser($sys);
				$msg->setText('От Вас уже принят точный ответ :)');
				$msg->setToId($toUser->getId());
				$msg->save();
                return true;
            }
			
			if ($challenge->getCreatedAt() < (new DateTime($sec . " seconds ago"))) {
				$toUser = $msg->getUser();
				$msg->setUser($sys);
				$msg->setText('О чём-то думать слишком поздно.');
				$msg->setToId($toUser->getId());
				$msg->save();
				return true;
			}
            $r = new RightAnswer();
            $r->setUser($user);
            $r->setChallenge($challenge);
			if ($quiz_mode === 'tournament') {
				$tournament_number = Cfg::getTournamentNumber();
				$r->setType($tournament_number);
			}
			
			$current_challenge_id = $challenge->getId();
			
			//Для статистики цепочек начало
			if ($quiz_mode  === 'regular') {
				$previous_challenge_id = $current_challenge_id - 1;
				$previous_right_answer = RightAnswerQuery::create()->filterByUser($user)->filterByChallengeId($previous_challenge_id)->findOne();
				if ($previous_right_answer) {
					$num_of_current_challenge = $previous_right_answer->getNum() + 1; //поле num в таблице
					$r ->setNum ($num_of_current_challenge);
				}
			}
			//Для статистики цепочек конец
			
			if ($quiz_mode  === 'regular') {
				self::_setBestFiftyScore($user, $current_challenge_id);//для статистики по ответам на произвольном участке в 50 вопросов подряд.
			}
            $r->save();
            if (!$r->getId()) {
                throw new Exception("answer not saved");
            }
			self::_incrementNumAnswered($question);
			$msg->setUser($sys);
			$msg->setToId($user->getId());
			
			if ($quiz_mode === 'regular') {
				$system_text = $text . ' - это верно! ' . 'Всего очков: ' .
					Statistics::getTotalScore($user) . '.';
				$system_text = (mb_strlen($system_text, 'utf-8') > 80)
					? 'Абсолютно точно!' : $system_text; /*. 'Всего очков: ' .
					Statistics::getTotalScore($user) . '.'
					: $system_text */
				$msg->setText($system_text);
			}
			else if ($quiz_mode === 'tournament') {
				$system_text = $text . ' - это верно! ' . 'Очки в турнире: ' .
					Statistics::getCurrentTournamentScore($user) . '.';
				$system_text = (mb_strlen($system_text, 'utf-8') > 80)
					? 'Абсолютно точно! ' . 'Очки в турнире: ' .
					Statistics::getCurrentTournamentScore($user) . '.'
					: $system_text;
				$msg->setText($system_text);
			}
			$msg->save();
            return true;
        } else {
            return false;
        }
    }
	
	public static function mb_str_split($string) { //стянуто отсюда: http://tutorialspots.com/php-function-mb_str_split-205.html
		return preg_split('/(?<!^)(?!$)/u', $string);
	}
	
	protected static function _commonLetters ($mas1, $mas2) {
		$result = [];
		for ($i = 0; $i<count($mas1); $i++) {
			$index = array_search ($mas1[$i], $mas2);
			if ($index !== false) {
				$result []= $mas1[$i];
				array_splice ($mas2, $index, 1);
			}
			
		}
		return $result;
	}

	
	protected static function _isNear($s1, $s2) {
		if (($s1 === '') || ($s2 === '')) return false;
        $t1 = mb_convert_case($s1, MB_CASE_LOWER, "UTF-8");
        $t2 = mb_convert_case($s2, MB_CASE_LOWER, "UTF-8");
		
		$l1 = iconv_strlen($t1, 'utf-8');
		$l2 = iconv_strlen($t2, 'utf-8');
		if (($l1 <= 3) || ($l2 <= 3)) return false;//слишком короткие не рассматриваем
		
		$pos1 = mb_strpos($t2,$t1,0,'utf-8'); //полные вхождения
		$pos2 = mb_strpos($t1,$t2,0,'utf-8');
		if (($pos1 !== false) || ($pos2 !== false)) return true;
		
		
		if (abs($l1-$l2) > 2) return false;//слишком разновеликие по длине не рассматриваем
		
		
		$mas1 = self::mb_str_split($t1);
		$mas2 = self::mb_str_split($t2);
		
		$common_letters = self::_commonLetters($mas1, $mas2);
		
		$minlen = min($l1,$l2);
		if (($minlen <6)  && (count($common_letters) >= $minlen)) return true;
		if (($minlen >=6) && ($minlen <10) && (count($common_letters) >= $minlen - 1)) return true;
		if (($minlen >=10) && (count($common_letters) >= $minlen - 2)) return true;
		
		return false;
    }
	
	public static function isNearAnswer($msg, Question $q) {
        if (self::_isNear($msg, $q->getRightAnswer())) {
            return true;
        }

        $alt = $q->getAltAnswers();
		if ($alt === '') return false;
        $alts = explode(':', $alt);
        foreach ($alts as $a) {
            if (self::_isNear($msg, $a)) {
                return true;
            }
        }
        return false;
    }
	
	public static function checkNearAnswer(Message $msg) {
		$quiz_mode = Cfg::getQuizMode();
		if ($quiz_mode === 'stop') { //викторина остановлена
			return false;
		}
        if ($msg->getToId() != 0) { // private message
            return false;
        }
		
		$challenge = self::getChallenge();
        $text = $msg->getText();
        $user = $msg->getUser();
        $question = $challenge->getQuestion();
		$current_challenge_id = $challenge->getId();
		
		if (self::isNearAnswer($text, $question)) {
			$sys =  Chat::getSystemUser();
			$msg->setUser($sys);
			$msg->setToId($user->getId());
			$system_text = $text . ' - это близко к ответу.';
			$system_text = (mb_strlen($system_text, 'utf-8') > 80)?
				'Ваше большое сообщение близко к ответу.' 
				: $system_text;
			$msg->setText($system_text);
			$msg->save();
			return true;
		} else {
			return false;
		}		
	}

    //api function
    public static function getQuestion() {
		
		$quiz_mode = Cfg::getQuizMode();
		if ($quiz_mode === 'stop') {
			$question_array = [
				'topic' => 'Режим игры',
				'text' => 'Викторина остановлена',
				'pictureUrl' => '../img/stop.jpg'
				];
			Cfg::setChallengeUpdated(0);
			return ['question' => $question_array];
		}
		
        //$current_question = self::currentQuestion();
		$challenge = self::getChallenge();
        $current_question = $challenge->getQuestion();
		
		if ($quiz_mode === 'tournament') {
			$tqi = '№' . (Cfg::getTqi()) . '. ';
		}
		else {
			$tqi = '';
		}
		
		$q_time = Cfg::getQuestionTime();
		$a_time = Cfg::getAnswerTime();
		$sec = $q_time - $a_time;
		$time_for_announced_param = round ($sec/2);
		
		if ($challenge->getCreatedAt() > (new DateTime($sec . " seconds ago"))) {
			$question_array = [
				'topic' => $tqi . $current_question->getTopic(),
				'text' => $current_question->getText(),
				'pictureUrl' => $current_question->getPackage()->getName() .
				'/' . $current_question->getPictureUrl()
			];
			if ($challenge->getCreatedAt() > (new DateTime($time_for_announced_param . " seconds ago"))) {
				Cfg::setAnnouncedRightUsers(0);
				Cfg::setChallengeUpdated(0);
			}
			
		} else {
			//показать ответ
			$question_array = [
				'topic' => 'Ответ: ' . $current_question->getRightAnswer(),
				'text' => $current_question->getComment()
				];
			$answer_picture = $current_question->getAnswerPictureUrl();
			$question_array['pictureUrl'] = ($answer_picture) ? 
					$current_question->getPackage()->getName() . '/answers/' . $answer_picture
					: $current_question->getPackage()->getName() . '/' 
						. $current_question->getPictureUrl();
			
			//объявить верно ответивших
			if (!Cfg::getAnnouncedRightUsers()) {
					Cfg::setAnnouncedRightUsers(1);
					self::announceRightUsers($challenge->getId());
					
			}
		}
				self::updateChallenge();

        return ['question' => $question_array];
    }
	
	public static function announceRightUsers($challenge_id) {
		//А также объявление засчитывавшихся ответов
		$q = self::currentQuestion();
		$right = $q->getRightAnswer();
		$alts = $q->getAltAnswers();
		$alts = str_replace (':', ', ', $alts);
		$rights = ($alts) ? ($right . ', ' . $alts) : ($right);
		$rights = $rights . '.';
		
		$right_answers = RightAnswerQuery::create()->filterByChallengeId($challenge_id)->find();
		$right_users = '';
		foreach ($right_answers as $right_answer) {
				$right_users = $right_users?
					$right_users . ', ' . $right_answer->getUser()->getName()
					: $right_answer->getUser()->getName();
		}
		if ($right_users) {
			$system_text = 'Верно ответили: ' . $right_users . '.';
			$system_text = (mb_strlen($system_text, 'utf-8') > 80) ? 'Верно ответили очень многие.' : $system_text;
			if (Cfg::getAnnouncedRightUsers() === 1) {
				//Chat::sysMessage('Засчитывались ответы: ' . $rights);
				Chat::sysMessage($system_text);
				Cfg::setAnnouncedRightUsers(2);
			}
		}
		else
		{
			if (Cfg::getAnnouncedRightUsers() === 1) {
				//Chat::sysMessage('Засчитывались ответы: ' . $rights);
				Chat::sysMessage('Никто не дал верный ответ.');
				Cfg::setAnnouncedRightUsers(2);
			}		
		}
		
	}

}//class

		/*require_once '../generated-classes/myvalidation.trait.php';
		require "../propinit.php";
		require "../utils.php";
		$questions = Quiz::_getNextQuestion();
		foreach ($questions as $q) {
			echo($q->getNumAnswered(). ' ' . $q->getNumAsked() . '<br/>');
		}*/
