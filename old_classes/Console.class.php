<?php

class Console {
    
    
    public static function checkCommand($input) {

        if ($input[0] != '!') { // not a command
            return false;
        }

        $tokens = explode(' ', $input);
        $first = $tokens[0];
        $method = substr($first, 1); // skip !

        if (method_exists('Console', $method)) {
            $params = array_slice($tokens, 1);
            self::$method($params);
        } else {
            self::commandNotFound($method);
        }

        return true;
    }

    public static function commandNotFound($cmd) {
        Chat::privateSysMessage('Команды ' . $cmd . ' не существует');
    }

    protected static function _checkAdmin() {
        $user = Chat::getCurrentUser();
        if ($user->getGroup() === 'admins') {
            return true;
        } else {
            Chat::privateSysMessage('Каждому своё.');
        }
    }

    public static function ping($params) {
        Chat::privateSysMessage('pong!');
    }

    public static function mode($params) {
        if (self::_checkAdmin()) {
            if ($params[0]) {
                Cfg::setQuizMode($params[0]);
            }
            Chat::privateSysMessage('quiz_mode = ' . Cfg::getQuizMode());
        }
    }

    public static function qtime($params) {
        if (self::_checkAdmin()) {
            if ($params[0]) {
                $t = (integer) $params[0];
                Cfg::setQuestionTime($t);
            }
            Chat::privateSysMessage('time = ' . Cfg::getQuestionTime());
        }
    }

    public static function atime($params) {
        if (self::_checkAdmin()) {
            if ($params[0]) {
                $t = (integer) $params[0];
                Cfg::setAnswerTime($t);
            }
            Chat::privateSysMessage('time = ' . Cfg::getAnswerTime());
        }
    }

    public static function syswrite($params) {
        Chat::privateSysMessage($params[0]);
    }

    public static function packages($params) {
        if (self::_checkAdmin()) {
            if ($params[0]) {
                Cfg::setPackages($params[0]);
            }
            Chat::privateSysMessage(Cfg::getPackages());
        }
    }

    public static function blacklist($params) {
        if (self::_checkAdmin()) {
            if ($params[0]) {
                Cfg::setBlackList($params[0]);
            }
            Chat::privateSysMessage(Cfg::getBlackList());
        }
    }

    public static function add_package($params) {
        $package = $params[0];
        if (self::_checkAdmin()) {
            $packages = Cfg::getPackages();
            $packages = $packages . ':' . $package;
            Cfg::setPackages($packages);
        }
        Chat::privateSysMessage($package . ' was successfully added');
    }

    public static function hello($p) {
        Chat::privateSysMessage('Hello Hrust!');
    }

    protected static function _stat_array_to_str($stat_array) {
        $result_string = '|';
        foreach ($stat_array as $stat) {
            $result_string = $result_string . $stat['user'] . ': ' . $stat['score'];
            $result_string .= "|";
        }
        return $result_string;
    }

    public static function stat() {
        $total5 = Statistics::getStatisticsTotalScore(5)["statistics_in_period_array"];
        $current_day5 = Statistics::getStatisticsCurrentDay(5)["statistics_in_period_array"];
        Chat::privateSysMessage('Топ-5 за всё время: ' . self::_stat_array_to_str($total5));
        Chat::privateSysMessage('Топ-5 текущего дня: ' . self::_stat_array_to_str($current_day5));
    }

    public static function min_users($params) {
        if (self::_checkAdmin()) {
            if ($params[0]) {
                Cfg::setMinUsersForUpdates($params[0]);
            }
            Chat::privateSysMessage('min_users_for_updates = ' . Cfg::getMinUsersForUpdates());
        }
    }

    public static function difficulty($params) {
        if (self::_checkAdmin()) {
            if ($params[0]) {
                Cfg::setDifficultySequence($params[0]);
            }
            Chat::privateSysMessage('difficulty_sequence = ' . Cfg::getDifficultySequence());
        }
    }

    public static function between_equal($params) {
        if (self::_checkAdmin()) {
            if ($params[0]) {
                Cfg::setMinTimeBetweenEqualQuestions($params[0]);
            }
            Chat::privateSysMessage('min_time_between_equal_questions = ' . Cfg::getMinTimeBetweenEqualQuestions());
        }
    }

    public static function tournament($params) {
        if (self::_checkAdmin()) {
            if ($params[0]) {
                Cfg::setTournamentNumber($params[0]);
            }
            Chat::privateSysMessage('tournament_number = ' . Cfg::getTournamentNumber() . ' tqi = ' . Cfg::getTqi());
        }
    }

    public static function tqi($params) { //tournament question index
        if (self::_checkAdmin()) {
            if (($params[0]) || ($params[0] === '0')) {
                Cfg::setTqi($params[0]);
            }
            Chat::privateSysMessage('tournament_question_index = ' . Cfg::getTqi());
        }
    }

}

//class

