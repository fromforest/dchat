<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cfg
 *
 * @author Alba
 */
class Cfg {

    public static function getQuizMode() {
        $c = ConfigQuery::create()->findOneById(1);
        return $c->getQuizMode();
    }

    public static function setQuizMode($s) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setQuizMode($s);
        $c->save();
    }

    public static function getQuestionTime() {
        $c = ConfigQuery::create()->findOneById(1);
        return $c->getQuestionTime();
    }

    public static function setQuestionTime($t) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setQuestionTime($t);
        $c->save();
    }

    public static function getAnswerTime() {
        $c = ConfigQuery::create()->findOneById(1);
        return $c->getAnswerTime();
    }

    public static function setAnswerTime($t) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setAnswerTime($t);
        $c->save();
    }

    public static function getPackages() {
        $c = ConfigQuery::create()->findOneById(1);
        $s = $c->getPackages();
        return $s;
    }

    public static function setPackages($ps) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setPackages($ps);
        $c->save();
    }

    public static function getBlackList() {
        $c = ConfigQuery::create()->findOneById(1);
        $s = $c->getBlackList();
        return $s;
    }

    public static function setBlackList($bls) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setBlackList($bls);
        $c->save();
    }

    public static function addPackage($p) {
        if (strpos($p, ':') !== false) {
            throw new Exception('Could not add package with : symbol.');
        }
        self::setPackages(self::getPackages() . ':' . $p);
    }

    public static function getMinUsersForUpdates() {
        $c = ConfigQuery::create()->findOneById(1);
        $s = $c->getMinUsersForUpdates();
        return $s;
    }

    public static function setMinUsersForUpdates($min_users) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setMinUsersForUpdates($min_users);
        $c->save();
    }

    public static function getDifficultySequence() {
        $c = ConfigQuery::create()->findOneById(1);
        $s = $c->getDifficultySequence();
        return $s;
    }

    public static function setDifficultySequence($ds) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setDifficultySequence($ds);
        $c->save();
    }

    public static function getMinTimeBetweenEqualQuestions() {
        $c = ConfigQuery::create()->findOneById(1);
        $s = $c->getMinTimeBetweenEqualQuestions();
        return $s;
    }

    public static function setMinTimeBetweenEqualQuestions($min_time) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setMinTimeBetweenEqualQuestions($min_time);
        $c->save();
    }

    public static function getTournamentQuestionIndex() {
        $c = ConfigQuery::create()->findOneById(1);
        return $c->getTournamentQuestionIndex();
    }

    public static function setTournamentQuestionIndex($index) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setTournamentQuestionIndex($index);
        $c->save();
    }

    public static function getTournamentNumber() {
        $c = ConfigQuery::create()->findOneById(1);
        return $c->getTournamentNumber();
    }

    public static function setTournamentNumber($number) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setTournamentNumber($number);
        $c->save();
    }

    public static function getTqi() { //tournament question index
        $c = ConfigQuery::create()->findOneById(1);
        return $c->getTqi();
    }

    public static function setTqi($number) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setTqi($number);
        $c->save();
    }

    public static function getAnnouncedRightUsers() {
        $c = ConfigQuery::create()->findOneById(1);
        return $c->getAnnouncedRightUsers();
    }

    public static function setAnnouncedRightUsers($num_announced) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setAnnouncedRightUsers($num_announced);
        $c->save();
    }

    public static function getChallengeUpdated() {
        $c = ConfigQuery::create()->findOneById(1);
        return $c->getChallengeUpdated();
    }

    public static function setChallengeUpdated($num_updated) {
        $c = ConfigQuery::create()->findOneById(1);
        $c->setChallengeUpdated($num_updated);
        $c->save();
    }

}
