<?php

use \Propel\Runtime\ActiveQuery\Criteria;
use \Propel\Runtime\Propel;

class SinglePlayer {
	
	/*public static function writeln ($file_name, $string1) {
		$file = fopen ($file_name, "a+t");
		fwrite ($file, $string1."\n");
		fclose ($file);
	}*/
	
	public static function getSinglePlayerLastTheme() {
		$user = Chat::getCurrentUser();
		$theme = '';
		if (!$user) {
            throw new Exception("Cannot submit chat. You are not logged in");
        }
		
		$spa = SinglePlayerAnswerQuery::create()->filterByUserId($user->getId())->orderById(Criteria::DESC)->findOne();
		if ($spa) {
			$question = QuestionQuery::create()->findOneById($spa->getQuestionId());
			$theme = $question->getTopic();
		}
		return ['theme' => $theme];
	}
	
	protected static function _functionForSort($a,$b) { //подумать над формулой, чтоб часто задаваемые вопросы меньше спрашивать.
		
		$a_asked = $a['num_asked'];
		$b_asked = $b['num_asked'];
		$a_answered = $a['num_answered'];
		$b_answered = $b['num_answered'];
		
		if ($a_asked*$b_asked === 0) {//какой-то из вопросов ни разу не задавался
			if ($a_asked === $b_asked) { //оба ни разу не задавались
				return 0;
			}
			return ($a_asked > $b_asked) ? 1 : -1; //не заданный ни разу вопрос имеет приоритет
		} else {
			if ($a_answered/($a_asked + 10) === $b_answered/($b_asked + 10)) { //что такое 10 - см. ideas func3, только там 3
				return 0;
			}
			$condition = ($a_answered/($a_asked + 10) < $b_answered/($b_asked + 10)); //менее отвечабельный вопрос имеет приоритет
			return ($condition ? -1 : 1);
		}
	}//end functionForSort
	
	protected static function _getQuestions($theme) {
		$questions = [];
		$ids = Quiz::getAllQuestionsIds();
		$questions_by_topic = QuestionQuery::create()->filterById($ids)->filterByTopic($theme)->find();
		$questions_by_tag1  =  QuestionQuery::create()->filterById($ids)->filterByTag1($theme)->find();
		$questions_by_tag2  =  QuestionQuery::create()->filterById($ids)->filterByTag2($theme)->find();
		foreach ($questions_by_topic as $q) {
			$questions []= $q;
		}
		foreach ($questions_by_tag1 as $q) {
			$questions []= $q;
		}
		
		foreach ($questions_by_tag2 as $q) {
			$questions []= $q;
		}
		
		return $questions;
	}
	
	protected static function _getNextSinglePlayerQuestion($theme, $is_random) {
		$user = Chat::getCurrentUser();
		
		$questions = self::_getQuestions($theme);
		if (count($questions) === 0) return false; //нет такой темы
		
		$question_objects = [];
		foreach ($questions as $q) {
			$q_con = SinglePlayerAnswerQuery::create()->filterByUserId($user->getId())->filterByQuestionId($q->getId());
			$q_asked = $q_con->count();
			$q_answered = $q_con->filterByIsRight(true)->count();
			$question_struct = ['question' => $q, 'num_asked' => $q_asked, 'num_answered' => $q_answered];
			$question_objects []= $question_struct;
		}
		
		if ($is_random) {
			$index = rand(0, count($question_objects) - 1);
			return $question_objects[$index]['question'];			
		}
		else {
			usort($question_objects, 'self::_functionForSort');
			return $question_objects[0]['question'];
		}
	}
	
	protected static function _getNoTheme() {
		return [
				'topic' => 'Тема отсутствует или недоступна',
				'text' => 'Введите тему',
				'pictureUrl' => '../img/no_theme.jpg'
			];
	}
	
	public static function getSinglePlayerQuestion($theme, $is_random) {
		$quiz_mode = Cfg::getQuizMode();
		if ($quiz_mode === 'tournament') {
			return ['question' => [
				'topic' => 'Сейчас идёт турнир',
				'text' => 'Дождитесь окончания турнира',
				'pictureUrl' => '../img/nowtournament.jpg'
			]];
		}
		$user = Chat::getCurrentUser();
		if (!$user) {
            throw new Exception("Cannot get question. You are not logged in");
        }
		
		if (($theme === '') || ($theme === ' ')) {
			$question_array = self::_getNoTheme();
			return ['question' => $question_array];
		}
		
		$question = self::_getNextSinglePlayerQuestion($theme, $is_random);
		if (!$question) {
			$question_array = self::_getNoTheme();
		}
		else {
			$spa = new SinglePlayerAnswer();
            $spa->setUserId($user->getId());
            $spa->setQuestionId($question->getId());
			$spa->save();
			
			$question_array = [
				'topic' => $question->getTopic(),
				'text' => $question->getText(),
				'pictureUrl' => $question->getPackage()->getName() .
				'/' . $question->getPictureUrl()
			];		
		}
		return ['question' => $question_array];
	}
	
	public static function getSinglePlayerAnswer($text) {
		$user = Chat::getCurrentUser();
		if (!$user) {
            throw new Exception("Cannot get answer. You are not logged in");
        }
		//spa = current challenge/answer, string in table "single_player_answers"
		$spa = SinglePlayerAnswerQuery::create()->filterByUserId($user->getId())->orderById(Criteria::DESC)->findOne();
		$question = QuestionQuery::create()->findOneById($spa->getQuestionId());
		
		
		$is_right = false;
		if ($text) {
			if (Quiz::isRightAnswer($text, $question)) {
				$is_right = true;
				$spa->setIsRight(true);
				$spa->save();
			}
		}
		
		$answer_array = [
			'is_right' => $is_right,
			'topic' => 'Ответ: ' . $question->getRightAnswer(),
			'text' => $question->getComment()
		];
		$answer_picture = $question->getAnswerPictureUrl();
		$answer_array['pictureUrl'] = ($answer_picture) ? 
					$question->getPackage()->getName() . '/answers/' . $answer_picture
					: $question->getPackage()->getName() . '/' 
						. $question->getPictureUrl();
						
		return ['answer' => $answer_array];
	}

	public static function getSinglePlayerAllThemes() {
		$themes = [];
		$theme_strings =[];
		
		$topic_strings = QuestionQuery::create()->select('topic')->distinct()->find();
		$tag1_strings  = QuestionQuery::create()->select('tag1')->distinct()->find();
		$tag2_strings  = QuestionQuery::create()->select('tag2')->distinct()->find();
		
		//$topic_strings = merge($topic_strings, $tag1_strings, $tag2_strings);
		//$topic_strings = array_unique($topic_strings);
		//Quiz::writeln('log.txt', gettype($topic_strings));

		foreach ($topic_strings as $topic) {
			if (strlen($topic . '') > 0) {
				$theme_strings []= $topic . '';
			}
		}

		foreach ($tag1_strings as $tag1) {
			if (strlen($tag1) > 0) {
				$theme_strings []= $tag1 . '';
			}
		}

		foreach ($tag2_strings as $tag2) {
			if (strlen($tag2) > 0) {
				$theme_strings []= $tag2 . '';
			}
		}
		
		$theme_strings = array_unique($theme_strings);
		foreach($theme_strings as $ts) {
			$as_array ['name'] = $ts;
			$themes []= $as_array;
		}
		return ['themes' => $themes];
	}

	
}//class
?>