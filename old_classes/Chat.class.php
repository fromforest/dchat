<?php

class Chat {

//    doesn't work don't know why
//      
//    public function __call($method, $arguments) {
//        //if function exists within this class call it
//        if (method_exists(self, $method)) {
//            $this->$method($arguments);
//        } else {
//            throw new Exception('Undefined function call.');
//        }
//    }

    protected static function _signUp($name, $password) {
        if (!$name || !$password) {
            throw new InvalidArgumentException('Please enter name and password.');
        }

        $user = new User(['Name' => $name, 'Password' => md5($password)]);
        $user->save();
        
        return $user;
    }

    protected static function _authenticate($name = "", $password = "") {
        $user = UserQuery::create()->filterByName($name)->filterByPassword(md5($password))->findOne();
        return $user ? $user : false;
    }

    //api
    public static function login() {
        $name = $_POST['name'];
        $password = $_POST['password'];

        if (!$name || !$password) {
            throw new Exception('Please enter name and password.');
        }

// 		if(!filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL)){
// 			throw new Exception('Your email is invalid.');
// 		}

        $existing_user = UserQuery::create()->findOneByName($name);

        //$user = self::_authenticate($name, $password); //вроде не нужно.
        if ($existing_user) {
            $user = self::_authenticate($name, $password);
            if (!$user) {
                throw new Exception('Wrong name/password.');
            }
        } else { // new user
            $user = self::_signUp($name, $password);
        }

        $user->setOnline(true);

        $_SESSION['user'] = [
            'name' => $name,
            'id' => $user->getId()
        ];

        return ['name' => $user->getName(), 'status' => 'login ok'];
    }

    //api
    public static function checkLogged() {
        $response = ['logged' => false];

        $user = self::getCurrentUser();

        if ($user) {
            $response['logged'] = true;
            $response['loggedAs'] = [
                'name' => $user->getName(),
                'id' => $user->getId()
            ];
        }
        return $response;
    }

    //api
    public static function logout() {
        $uname = $_SESSION['user']['name'];

        $u = UserQuery::create()->findOneByName($uname);
        if ($u) {
            $u->setOnline(false);
            $u->save();
        }

        $_SESSION = [];
        //session_destroy();
        unset($_SESSION);

        return ['status' => 1,
            'action' => 'log out'
        ];
    }

    public static function getSystemUser() {
        $sys = UserQuery::create()->findOneByName('System');
        if (!$sys) {
            throw new Exception('The system user not found.');
        }
        return $sys;
    }

    //todo
    public static function sysMessage($msg) {
        $sys = self::getSystemUser();
        //$message = new ChatLine();
        $message = new Message();
        //$message->setAuthor('System');
        $message->setUser($sys);
        $message->setText($msg);
        $message->save();
    }

    public static function privateSysMessage($msg) {
        $msg_stump = '';
        if (strlen($msg) > 80) {
            $msg_stump = substr($msg, 80);
            $msg = substr($msg, 0, 80);
        }

        $sys = self::getSystemUser();
        $toUser = self::getCurrentUser();
        //$message = new ChatLine();
        $message = new Message();
        //$message->setAuthor('System');
        $message->setUser($sys);
        $message->setText($msg);
        $message->setToId($toUser->getId());
        $message->save();

        if (strlen($msg_stump) > 0) {
            self::privateSysMessage($msg_stump);
        }
    }

    //api
    public static function submitChat() {
        $chatText = $_POST['chatText'];
        $user = self::getCurrentUser();
        if (!$user) {
            throw new Exception("Cannot submit chat. You are not logged in");
        }
        $username = $user->getName();
        $sys = self::getSystemUser();

        if (!$chatText) {
            throw new Exception('You haven\' entered a chat message.');
        }

        $is_command = Console::checkCommand($chatText);
        if ($is_command) {
            return [
                'status' => 1, // ok
                'insertID' => '',
                'time' => ''
            ];
        }

        $msg = new Message();
        $msg->setUser($user); //оставляем здесь setUser, потому что иначе Quiz::checkAnswer не ест msg

        $s = explode('$', $chatText);
        if ($s[0] == '@') { //private
            $toUser = UserQuery::create()->findOneByName($s[1]);
            $msg->setToId($toUser->getId());
            $msg->setText($s[2]);
        } else {
            $msg->setText($chatText);
        }

        // ------------ check right answer  -------------------

        if (!(Quiz::checkAnswer($msg))) {
            if (!(Quiz::checkNearAnswer($msg))) {
                $msg->save();
                //если ответ близкий, все дела объекта $msg ведутся внутри Quiz::checkNearAnswer
            }
            //если ответ верный, все преобразования и сохранение объекта $msg ведутся внутри Quiz::checkAnswer 
        }


        return [
            'status' => 1, // ok
            'insertID' => $msg->getId(),
            'time' => $msg->getCreatedAt()->format('H:i:s'),
            'text' => $msg->getText()
                /* 'time' => [
                  'hours' => $msg->getCreatedAt()->format('H'),
                  'minutes' => $msg->getCreatedAt()->format('i')
                  ] */
        ];
    }

    public static function getCurrentUser() {
        if (isset($_SESSION['user']))
            $username = $_SESSION['user']['name'];
        else
            return false;
        
        $user = UserQuery::create()->findOneByName($username);
        if (!$user) {
            $_SESSION = [];
            //session_destroy();
            unset($_SESSION);
            return false;
        }
        return $user;
    }

    public static function getUsers() {
        //global $database;
        $user = self::getCurrentUser();
        if (!$user) {
            throw new Exception("not logged in");
        }

        $user->setUpdatedAt(new DateTime("now"));
        $user->setOnline(true);
        $user->save();

        // Deleting chats older than 5 minutes and users inactive for 30 seconds
//                $database->query("DELETE FROM webchat_lines WHERE ts < SUBTIME(NOW(),'0:5:0')");
//                $database->query("DELETE FROM webchat_users WHERE last_activity < SUBTIME(NOW(),'0:0:30')");

        $old_lines = MessageQuery::create()->filterByCreatedAt('5 minutes ago', '<')->find();
        $old_lines->delete();

        $inactive_users = UserQuery::create()->filterByOnline(true)->filterByUpdatedAt('90 seconds ago', '<')->find();
//        $inactive_users->delete();

        foreach ($inactive_users as $u) {
            $u->setOnline(false);
            $u->save();
        }

        //$users = ChatUserQuery::create()->find(); //get all users
        $users_online = UserQuery::create()->findByOnline(true);

        $users_array = [];

        foreach ($users_online as $u) {
            $arr['name'] = $u->getName();
            $arr['id'] = $u->getId();
            $users_array[] = $arr;
        }

        return [
            'users' => $users_array
                //'total' => DB::query('SELECT COUNT(*) as cnt FROM webchat_users')->fetch_object()->cnt
                //'total' => count($users_array)
        ];
    }

    //api
    public static function getChats() {
        $lastID = $_GET['lastID'];
        $user = self::getCurrentUser();
        if (!$user) {
            return [ 'chats' => []];
        }

        $lastID = (int) $lastID;

        //$result = DB::query('SELECT * FROM webchat_lines WHERE id > '.$lastID.' ORDER BY id ASC');
        //$cs = ChatLine::find_by_sql('SELECT * FROM webchat_lines WHERE id > '.$lastID.' ORDER BY id ASC');

        $recent = MessageQuery::create()->filterById($lastID, '>'); //->find();
        $chat_objects = $recent
                ->condition('cond1', 'messages.to_id = ?', 0) //public
                ->condition('cond2', 'messages.to_id = ?', $user->getId()) //to me
                ->condition('cond3', 'messages.user_id = ?', $user->getId()) //from me
                ->where(['cond1', 'cond2', 'cond3'], 'or')
                ->find();

        $chats = [];
        foreach ($chat_objects as $chat) {

            // Returning the GMT (UTC) time of the chat creation:

            $chat_time = $chat->getCreatedAt()->format('Y-m-d H:i:s');

            $as_array['id'] = $chat->getId();
            $author = $chat->getUser();
            // we can have messages from deleted users
            if ($author) {
                $as_array['author'] = $author->getName(); // getAuthor();
            } else {
                $as_array['author'] = 'unknown';
            }
            $as_array['text'] = $chat->getText();
            $as_array['time'] = $chat_time;

            if ($chat->getToId() == 0) {
                $as_array['type'] = 'public';
            } else {
                $as_array['type'] = 'private';
                $to_name = UserQuery::create()->findOneById($chat->getToId())->getName();
                $as_array['text'] = $to_name . ', ' . $as_array['text'];
            }

            $chats[] = $as_array;
        }

        return ['chats' => $chats];
    }

    //api
    public static function getQuestion() {
        return Quiz::getQuestion();
    }

    //-----------------------------------Statistics---------------------------------------

    public static function getStatisticsTotalScore() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsTotalScore($length_of_top);
    }

    public static function getStatisticsCurrentHour() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsCurrentHour($length_of_top);
    }

    public static function getStatisticsForRightTable() {
        $length_of_top = 5;
        //$quiz_mode = Cfg::getQuizMode();
        return Statistics::getStatisticsCurrentHour($length_of_top);
    }

    public static function getStatisticsPreviousHour() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsPreviousHour($length_of_top);
    }

    public static function getStatisticsCurrentDay() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsCurrentDay($length_of_top);
    }

    public static function getStatisticsPreviousDay() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsPreviousDay($length_of_top);
    }

    public static function getStatisticsCurrentMonth() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsCurrentMonth($length_of_top);
    }

    public static function getStatisticsPreviousMonth() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsPreviousMonth($length_of_top);
    }

    public static function getStatisticsBestHoursTotal() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsBestHoursTotal($length_of_top);
    }

    public static function getStatisticsBestHoursCurrentMonth() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsBestHoursCurrentMonth($length_of_top);
    }

    public static function getStatisticsBestHoursCurrentYear() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsBestHoursCurrentYear($length_of_top);
    }

    public static function getStatisticsBestDaysTotal() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsBestDaysTotal($length_of_top);
    }

    public static function getStatisticsBestDaysCurrentYear() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsBestDaysCurrentYear($length_of_top);
    }

    public static function getStatisticsBestMonthsTotal() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsBestMonthsTotal($length_of_top);
    }

    public static function getStatisticsBestYears() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsBestYears($length_of_top);
    }

    public static function getStatisticsCurrentYear() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsCurrentYear($length_of_top);
    }

    public static function getStatisticsPreviousYear() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsPreviousYear($length_of_top);
    }

    public static function getStatisticsChainsTotal() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsChainsTotal($length_of_top);
    }

    public static function getStatisticsChainsCurrentMonth() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsChainsCurrentMonth($length_of_top);
    }

    public static function getStatisticsChainsCurrentYear() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsChainsCurrentYear($length_of_top);
    }

    public static function getStatisticsMQZ() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsMQZ($length_of_top);
    }

    public static function getStatisticsOnAnyFiftyChallenges() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsOnAnyFiftyChallenges($length_of_top);
    }

    public static function getStatisticsLastTournament() {
        $length_of_top = $_POST['length_of_top'];
        return Statistics::getStatisticsLastTournament($length_of_top);
    }

    //------------------------------------end Statistics-----------------------------------
    //------------------------------------single player-------------------------------------

    public static function getSinglePlayerQuestion() {
        $theme = $_POST['theme'];
        $is_random = $_POST['is_random'];
        return SinglePlayer::getSinglePlayerQuestion($theme, $is_random);
    }

    public static function getSinglePlayerAnswer() {
        $text = $_POST['text'];
        return SinglePlayer::getSinglePlayerAnswer($text);
    }

    public static function getSinglePlayerLastTheme() {
        return SinglePlayer::getSinglePlayerLastTheme();
    }

    public static function getSinglePlayerAllThemes() {
        return SinglePlayer::getSinglePlayerAllThemes();
    }

} //class

