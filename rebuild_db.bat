@cls
@echo This will destroy all data. Are you sure? Press Ctrl+C to abort.
@pause

@echo rebuilding database:

@echo generating sql...
call vendor\bin\propel.bat sql:build --overwrite

@echo creating models...
call vendor\bin\propel.bat model:build

@echo making autoloads...
call composer dump-autoload

@echo inserting sql...
call vendor\bin\propel.bat sql:insert

rem cd scripts

@echo creating users...
call php setup.php

@echo running seed script...
call php load_xml.php

rem cd ..

@echo done!

@pause