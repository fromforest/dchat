<?php

require 'pinit.php';

echo "deleting all questions...\n";

QuestionQuery::create()->deleteAll();

$xml = simplexml_load_file("tp1.xml") or die("Error: Cannot create object");

foreach ($xml->children() as $qx) {
    $q = new Question();
    $q->setId($qx->id);
    $q->setTopic($qx->topic);
    $q->setText($qx->text);
    $q->setPictureUrl($qx->picture_url);
    $q->setRightAnswer($qx->right_answer);
    $q->setComment($qx->comment);
    $q->save();
}

echo "questions created\n";

echo "deleting all challenges...\n";

ChallengeQuery::create()->deleteAll();
$c = new Challenge();
$c->setQuestionId(5);
$c->save();

echo "first challenge created\n";

//  echo "Child node: " . $child->text . "<br>";
//  echo "Child node: " . $child->picture_url . "<br>";
//print_r($xml);
?>