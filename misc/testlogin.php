<?php

require "pinit.php";

function _signUp($name, $password) {

    if (!$name || !$password) {
        throw new Exception('Please enter name and password.');
    }

    $user = new User();
    $user->setName($name);
    $user->setPassword(md5($password));
    $user->save();
}

function _authenticate($name = "", $password = "") {

    //$user = UserQuery::create()->findByName($name)->findByPassword($password)->limit(1)->find();
    $user = UserQuery::create()->filterByName($name)->filterByPassword(md5($password))->findOne();

    return $user ? $user : false;
}

//UserQuery::create()->deleteAll();
//
//_signUp('alba', '111');
//_signUp('belka', 'opex');
//_signUp('adele', 'opex');
//_signUp('hrooo', 'opex');
//_signUp('abc', 'opex');

echo "users created <br/>";

$inactive_users = UserQuery::create()->filterByUpdatedAt('10 seconds ago', '<')->find();
//$alba = _authenticate('alba', '111');
//$alba->setOnline(true);
//$alba->save();
foreach ($inactive_users as $u) {
    $u->setOnline(false);
    $u->save();
}
