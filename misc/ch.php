<?php
// test

require 'pinit.php';

use \Propel\Runtime\ActiveQuery\Criteria;

//$c = new Challenge();
//$c->setQuestionId(7);
//$c->save();

//print_r($q);

//$last = ChallengeQuery::create()->orderById(Criteria::DESC)->limit(1)->findOne();
//$last = ChallengeQuery::create()->orderById(Criteria::DESC)->findOne();

function _getRandomQuestion() {
    // get array of ids
    $ids = QuestionQuery::create()->select('id')->find()->toArray();
    // get random element from it
    $index_of_id = rand(0, count($ids) - 1);
    // this is random id from questions
    $id = $ids[$index_of_id];
    // find question with this id
    $question = QuestionQuery::create()->findOneById($id);
    // return what we want
    return $question->getText();
}

for ($i = 0; $i < 10; $i++) {
    echo _getRandomQuestion();
    echo "<br/>";
}

echo "--------------";

