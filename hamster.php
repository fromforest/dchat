<?php

interface IHamsterable {
    function hello();
}

class Hamster implements IHamsterable {
    public function hello() {
        return "Hello!";
    }
}

class ProxyHamster implements IHamsterable {
    public function __construct() {
        $this->hamster = new Hamster();
    }
    public function hello() {
        return $this->hamster->hello() . " I am proxy!";
    }
}

class ChildHamster extends Hamster {
    public function hello() {
        return "Hello from child!";
    }
}

function clientCode(IHamsterable $h) {
    echo $h->hello();    
}
 
$myHamster = new Hamster();
clientCode($myHamster);

$child = new ChildHamster();
clientCode($child);

$p = new ProxyHamster();
clientCode($p);
