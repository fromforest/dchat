#!/usr/bin/python
#coding: utf-8

import sys, re, os, codecs

num_args = len(sys.argv)

if num_args >= 2:
	text = sys.argv[1]
	print text
else:
	print "no text"

#text = '(((( Lots of punctuation )))'
#pattern = re.compile(r'(?P<word>\b\w+\b)')
#m = pattern.search(text)
#print m.group('word')

def parse_it(s, dt):
	s = s.replace ('vopros','?')
	pattern = re.compile('^(?P<topic>[^!^\$]*!)?(?P<tag1>[^!^\$]*!)?(?P<tag2>[^!^\$]*!)?(?P<question>[^!^\$]+)?\$(?P<answers>[^#^\$]+)(?P<comment>#.+)?.(?P<extension>jpg|jpeg)$')
	m = pattern.search(s)
	default_topic = dt
	topic = (m.group('topic') or default_topic).strip('!')
	tag1 = (m.group('tag1') or "").strip('!')
	tag2 = (m.group('tag2') or "").strip('!')
	question = m.group('question')
	answers = m.group('answers').split('&')
	right_answer = answers[0]
	alt_answers = ':'.join (answers[1:])
	comment = (m.group('comment') or u"#комментарий").strip('#')
	extension = m.group('extension')
	return (topic, tag1, tag2, question, right_answer, alt_answers, comment, extension)

def tag(tabs, t, content):
	return u'\t' * tabs + u'<%s>'%t + unicode(content) + u'</%s>'%t + u'\n'

# --------- параметры пакета ------------------------
package_author = u'Смусёнок'
package_id = 15
package_name = u'tp' + str(package_id)
package_description = u"покемоны от Смусёнка"

first_id = 100000

result_filename = 'tq/giraffe_package.xml'
#src_dir = './десерты'

pkg = codecs.open(result_filename, encoding = 'utf-8', mode = 'w')
pkg.write(u'<?xml version="1.0" encoding="UTF-8"?>\n')
pkg.write(u'\t' + u'<package>\n')
pkg.write(tag(2, u'id', package_id))
pkg.write(tag(2, u'name', package_name))
pkg.write(tag(2, u'description', package_description))

src_dir = './tq'
files = os.listdir(src_dir)

default_topic = u'десерты'
default_text = u'Как называется этот десерт?'
id = first_id

# for f in files:
# 	topic, tag1, tag2, question, right_answer, alt_answers, comment, extension = parse_it(f, "default topic")
# 	picture_url = 'pic' + str (id) + '.' + extension

# 	pkg.write('\t'*2 + '<question>')

# 	pkg.write('\t'*2 + '</question>')

pkg.write('\t' + '</package>\n')
pkg.close()

topic, tag1, tag2, question, right_answer, alt_answers, comment, extension = parse_it(text, "default topic")

print "topic: ", topic
print "tag1: ", tag1
print "tag2: ", tag2
print "question: ", question
print "right_answers: ", right_answer
print "alt_answers: ", alt_answers
print "comment: ", comment
print "extension: ", extension
