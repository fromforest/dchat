<?php

require_once 'propinit.php';

//echo "deleting all questions...\n";
//
//QuestionQuery::create()->deleteAll();

function loadPackage($fname) {
    $xml = simplexml_load_file("$fname") or die("Error: Cannot read xml!");

    $pid = (string) $xml->id;
    $package = PackageQuery::create()->findOneById($pid);

    if (!$package) { // not found
        $package = new Package();
        $package->setId($pid);
    }

    $package->setName($xml->name);
    $package->setDescription($xml->description);

    $package->save();
    
    foreach ($xml->question as $qx) {
        // need to cast xml element object to string!!!
        $id = (string) $qx->id;

        $q = QuestionQuery::create()->findOneById($id);
        if (!$q) { // not found
            $q = new Question();
            $q->setId($qx->id);
        }

        $q->setPackageId($pid);
        $q->setTopic($qx->topic);
        $q->setText($qx->text);
        $q->setPictureUrl($qx->picture_url);
		$q->setAnswerPictureUrl($qx->answer_picture_url);
        $q->setRightAnswer($qx->right_answer);
        $q->setComment($qx->comment);
        
        //необязательные поля
        $q->setAltAnswers($qx->alt_answers);
        $q->setAuthor($qx->author);
        $q->setTag1($qx->tag1);
        $q->setTag2($qx->tag2);
		if ($qx->type == 'text') {
			$q->setType('text');
		}
		else {
			$q->setType('image');
		}
			
        
        $q->save();
    }
}

loadPackage('questions/tp1/package.xml');
loadPackage('questions/tp2/package.xml');
loadPackage('questions/tp3/package.xml');
loadPackage('questions/tp4/package.xml');
loadPackage('questions/tp7/package.xml');
loadPackage('questions/tp8/package.xml');
loadPackage('questions/tp9/package.xml');
loadPackage('questions/tp10/package.xml');
loadPackage('questions/tp11/package.xml');
loadPackage('questions/tp12/package.xml');
loadPackage('questions/tp13/package.xml');
loadPackage('questions/tp14/package.xml');
loadPackage('questions/tp15/package.xml');
loadPackage('questions/tp16/package.xml');
loadPackage('questions/tp17/package.xml');
loadPackage('questions/tp18/package.xml');
loadPackage('questions/tp19/package.xml');
loadPackage('questions/tp20/package.xml');
loadPackage('questions/tp21/package.xml');
loadPackage('questions/tp22/package.xml');
loadPackage('questions/tp23/package.xml');
loadPackage('questions/tp24/package.xml');
loadPackage('questions/tp25/package.xml');
loadPackage('questions/tp26/package.xml');
loadPackage('questions/tp27/package.xml');
loadPackage('questions/tp28/package.xml');
loadPackage('questions/tp29/package.xml');
loadPackage('questions/tp30/package.xml');
loadPackage('questions/tp1000/package.xml');
loadPackage('questions/tp31/package.xml');
loadPackage('questions/tp32/package.xml');
loadPackage('questions/tp33/package.xml');
loadPackage('questions/tp34/package.xml');
loadPackage('questions/tp35/package.xml');
loadPackage('questions/tp36/package.xml');
loadPackage('questions/tp37/package.xml');
loadPackage('questions/tp38/package.xml');
loadPackage('questions/tp39/package.xml');
//loadPackage('questions/tp40/package.xml');
//loadPackage('questions/tp41/package.xml');
//loadPackage('questions/tp42/package.xml');
//loadPackage('questions/tp43/package.xml');
//loadPackage('questions/tp44/package.xml');
//loadPackage('questions/tp45/package.xml');
//loadPackage('questions/tp46/package.xml');
//loadPackage('questions/tp47/package.xml');
//loadPackage('questions/tp48/package.xml');

echo "questions created\n";

//echo "deleting all challenges...\n";
//
//ChallengeQuery::create()->deleteAll();
(new GConfig())->setChallengeUpdated(0);
$c = new Challenge();
$c->setQuestionId(101);
$c->save();

echo "first challenge created\n";

//  echo "Child node: " . $child->text . "<br>";
//  echo "Child node: " . $child->picture_url . "<br>";
//print_r($xml);
?>