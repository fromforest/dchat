<?php


require_once 'generated-classes/myvalidation.trait.php';
require "propinit.php";
require "utils.php";


error_reporting(E_ALL ^ E_NOTICE);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST))
    $_POST = json_decode(file_get_contents('php://input'), true);

session_name('webchat2');
session_start();

try {

    $response = array();

    //$_GET['action'] = 'getQuestion';

    $method = $_GET['action'];

    $dispatcher = new GDispatcher($_GET, $_POST);
    if (method_exists($dispatcher, $method)) {
        $response = $dispatcher->$method();
    } else {
        throw new Exception('No such API method.');
    }

//    if      (method_exists('Chat', $method)) {
//        $response = Chat::$method();
//    } else {
//        throw new Exception('Undefined function call.');
//    }

    echo json_encode($response);
} catch (Exception $e) {
    die(json_encode(array('error' => $e->getMessage())));
}
