<?php
require 'propinit.php';
use \Propel\Runtime\ActiveQuery\Criteria;
$words = ZavalinkaWordQuery::create()->select('word')->orderBy('word')->distinct()->find();
foreach($words as $w) {
	echo($w . '' . '<br/>');
	$kalashes = ZavalinkaWordQuery::create()->filterByWord($w . '')->select('definition')->find();
	foreach($kalashes as $k) {
		echo($k . '' . '<br/>');
	}
	echo('<br/>');
}
