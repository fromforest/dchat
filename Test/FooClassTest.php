<?php

//test for sumFoo

require_once __DIR__ . '/../propinit.php';
require_once "GTest.php";

class StupidTest extends GTest {

    // public function testTrueIsTrue()
    // example test

    public function testTrueIsTrue() {
        $foo = true;
        $this->assertTrue($foo);
    }

    // example function

    public function foo() {
        echo "not ok";
    }

    public function testSum() {
        $f = new FooClass();
        $res = $f->sum(2, 3);
        $this->assertEquals(2 + 3, $res);
    }

    //testing private method
    public function testPrivateSum() {

        $res = $this->oldinvokeMethod("FooClass", 'psum', array(1, 2));
        $this->assertEquals(3, $res, "1 + 2 = 3");
    }

}
