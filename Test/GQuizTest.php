<?php

//namespace Test;

require_once __DIR__ . "/../propinit.php";
require_once __DIR__ . "/../utils.php";
require_once "GTest.php";

use \Propel\Runtime\ActiveQuery\Criteria;

class GQuizTest extends GTest {
    private $gQuiz;
    // example test

    public function setUp() {
        $this->gQuiz = new GQuiz(new GChat, new GConfig);
    }
    
    public function testConstruct() {
            $g_chat = $this->getMock("GChat");
            $g_config = $this->getMock("GConfig");
            $g_quiz = new GQuiz($g_chat, $g_config);
            $this->assertEquals($g_quiz->stop_question['topic'], 'Режим игры');
    }
    
    //_incrementNumAsked tests ----------------------------------
    public function test_incrementNumAskedIncrement() {
            $users_online_count = (new GUserManager())->getCountOfUsersOnline();
            $min_users_for_updates = (new GConfig())->getMinUsersForUpdates();
            $this->assertTrue($users_online_count < $min_users_for_updates, 'Please check in table users, how many users is online');
            $how_many_users_create = $min_users_for_updates - $users_online_count;
            $users = $this->createUsersForTests($how_many_users_create);
            $question = (new GQuestionManager())->findQuestionById(101);
            $updated_at = $question->getUpdatedAt();
            $num_asked = $question->getNumAsked();
            $this->invokeMethod($this->gQuiz, '_incrementNumAsked', [$question]);
            $new_num_asked = $question->getNumAsked();
            $this->assertEquals($num_asked + 1, $new_num_asked, 'should be incremented');

            //восстановление прежних значений num_asked и updated_at
            $question->setNumAsked($num_asked);
            $question->save(); //при этой последовательности команд...
            $question->setUpdatedAt($updated_at); // updated_at действительно...
            $question->save(); //восстанавливается прежним.
            //удаление созданных юзеров
            $this->deleteUsers($users);
    } //method
    
    public function test_incrementNumAskedNoIncrement() {
            $users_online_count = (new GUserManager())->getCountOfUsersOnline();
            $min_users_for_updates = (new GConfig())->getMinUsersForUpdates();
            $this->assertTrue($users_online_count < $min_users_for_updates, 'Please check in table users, how many users is online');
            $how_many_users_create = $min_users_for_updates - $users_online_count - 1;
            $users = $this->createUsersForTests($how_many_users_create);
            $question = (new GQuestionManager())->findQuestionById(101);
            $updated_at = $question->getUpdatedAt();
            $num_asked = $question->getNumAsked();
            $this->invokeMethod($this->gQuiz, '_incrementNumAsked', [$question]);
            $new_num_asked = $question->getNumAsked();
            $this->assertEquals($num_asked, $new_num_asked, 'should not be incremented');

            //восстановление прежних значений num_asked и updated_at
            $question->setNumAsked($num_asked);
            $question->save(); //при этой последовательности команд...
            $question->setUpdatedAt($updated_at); // updated_at действительно...
            $question->save(); //восстанавливается прежним.
            //удаление созданных юзеров
            $this->deleteUsers($users);
    } //method
    
    //_incrementNumAnswered tests

     public function test_incrementNumAnsweredIncrement() {
            
            $users_online_count = (new GUserManager())->getCountOfUsersOnline();
            $min_users_for_updates = (new GConfig())->getMinUsersForUpdates();
            $this->assertTrue($users_online_count < $min_users_for_updates, 'Please check in table users, how many users is online');
            //для проведения теста необходимо чтобы не было много юзеров онлайн
            $how_many_users_create = $min_users_for_updates - $users_online_count;
            $users = $this->createUsersForTests($how_many_users_create);

            $question = (new GQuestionManager())->findQuestionById(101);
            $updated_at = $question->getUpdatedAt();
            $num_answered = $question->getNumAnswered();

            $challenge = new GChallenge();
            $challenge->setQuestion($question);
            $challenge->save();

            $this->invokeMethod($this->gQuiz, '_incrementNumAnswered', [$question]);
            $new_num_answered = $question->getNumAnswered();
            $this->assertEquals($num_answered + 1, $new_num_answered, 'should be incremented');
            $challenge->delete();
            //восстановление прежних значений num_answered и updated_at
            $question->setNumAnswered($num_answered);
            $question->save(); //при этой последовательности команд...
            $question->setUpdatedAt($updated_at); // updated_at действительно...
            $question->save(); //восстанавливается прежним.
            //удаление созданных юзеров
            $this->deleteUsers($users);
    }
    
     public function test_incrementNumAnsweredNoIncrement() {
            $users_online_count = (new GUserManager())->getCountOfUsersOnline();
            $min_users_for_updates = (new GConfig())->getMinUsersForUpdates();
            $this->assertTrue($users_online_count < $min_users_for_updates, 'Please check in table users, how many users is online');
            $how_many_users_create = $min_users_for_updates - $users_online_count;
            $users = $this->createUsersForTests($how_many_users_create);
            $question = (new GQuestionManager())->findQuestionById(101);
            $updated_at = $question->getUpdatedAt();
            $num_answered = $question->getNumAnswered();

            $challenge = new GChallenge();
            $challenge->setQuestion($question);
            $challenge->save();
            $question->setUpdatedAt(new DateTime("now"));
            $question->save();
            $this->invokeMethod($this->gQuiz, '_incrementNumAnswered', [$question]);
            $new_num_answered = $question->getNumAnswered();
            $this->assertEquals($num_answered, $new_num_answered, 'should not be incremented');

            $challenge->delete();
            //восстановление прежних значений num_answered и updated_at
            $question->setNumAnswered($num_answered);
            $question->save(); //при этой последовательности команд...
            $question->setUpdatedAt($updated_at); // updated_at действительно...
            $question->save(); //восстанавливается прежним.
            //удаление созданных юзеров
            $this->deleteUsers($users);	
     }
     
     // updateChallenge tests -------------------
     public function testUpdateChallenge_ResetChallengeUpdatedAfterDoubleQtime() {
            $question = (new GQuestionManager())->findQuestionById(101);
            $qtime = $this->gQuiz->config->getQuestionTime();
            $updated_at = $question->getUpdatedAt();
            $num_asked = $question->getNumAsked();

            $challenge = new GChallenge();
            $challenge->setQuestion($question);
            $challenge->save();

            $challenge->setCreatedAt(new Datetime((2*$qtime + 1) . " seconds ago"));
            $challenge->save();
            $this->gQuiz->config->setChallengeUpdated(6);

            $this->gQuiz->updateChallenge();
            $this->assertEquals($this->gQuiz->config->getChallengeUpdated(), 2, 'config parameter ChallengeUpdated should be reseted into 0 and then became 2');

            //восстановление прежних значений и удаление созданного challenge
            $challenge->delete();
            $question->setNumAsked($num_asked);
            $question->save();
            $this->gQuiz->config->setChallengeUpdated(0);
            $question->setUpdatedAt($updated_at);
            $question->save();
      } //method

      public function testUpdateChallenge_NotUpdateChallenge() {
            $question = (new GQuestionManager())->findQuestionById(101);
            $qtime = $this->gQuiz->config->getQuestionTime();
            $updated_at = $question->getUpdatedAt();
            $num_asked = $question->getNumAsked();

            $challenge = new GChallenge();
            $challenge->setQuestion($question);
            $challenge->save();

            $challenge->setCreatedAt(new Datetime(($qtime + 1) . " seconds ago"));
            $challenge->save();
            $this->gQuiz->config->setChallengeUpdated(1);

            $this->gQuiz->updateChallenge();
            $this->assertEquals($this->gQuiz->config->getChallengeUpdated(), 1, 'config parameter ChallengeUpdated should be equal 1 because check before challenge-updating must exit you from function');

            //восстановление прежних значений и удаление созданного challenge
            $challenge->delete();
            $question->setNumAsked($num_asked);
            $question->save();
            $this->gQuiz->config->setChallengeUpdated(0);
            $question->setUpdatedAt($updated_at);
            $question->save();
      }
      
      public function testUpdateChallenge_IsNewChallengeExist() {
            $question = (new GQuestionManager())->findQuestionById(101);
            $qtime = $this->gQuiz->config->getQuestionTime();
            $updated_at = $question->getUpdatedAt();
            $num_asked = $question->getNumAsked();

            $challenge = new GChallenge();
            $challenge->setQuestion($question);
            $challenge->save();

            $challenge->setCreatedAt(new Datetime(($qtime + 1) . " seconds ago"));
            $challenge->save();
            $this->gQuiz->config->setChallengeUpdated(0);

            $this->gQuiz->updateChallenge();
            $new_challenge = (new GChallengeManager)->getLastCreatedChallenge();

            $this->assertObjectHasAttribute("id", $new_challenge->getChallenge(), "should have id");
            $this->assertObjectHasAttribute("question_id", $new_challenge->getChallenge(), "should have question_id");
            $this->assertObjectHasAttribute("created_at", $new_challenge->getChallenge(), "should have created_at");
            $this->assertObjectHasAttribute("updated_at", $new_challenge->getChallenge(), "should have updated_at");
            $this->assertObjectNotHasAttribute("question", $new_challenge->getChallenge(), "should not have question, must be have only question_id");
            $this->assertEquals($new_challenge->getId(), $challenge->getId() + 1, 'id of new challenge should be equal previous challenge id + 1');
            $this->assertEquals($this->gQuiz->config->getChallengeUpdated(), 2, 'config parameter ChallengeUpdated should be equal 2 right after updateChallenge');

            //восстановление прежних значений и удаление созданного challenge
            $challenge->delete();
            $question->setNumAsked($num_asked);
            $question->save();
            $this->gQuiz->config->setChallengeUpdated(0);	
            $question->setUpdatedAt($updated_at);
            $question->save();
      }
      
      //functionForSortInRegular tests
    public function providerFunctionForSortInRegular() {
            $data = [];
                    $pairs = array(
                            ['num_answered' => 10, 'num_asked' => 900],
                            ['num_answered' => 50, 'num_asked' => 330],
                            ['num_answered' => 10, 'num_asked' => 10],
                            ['num_answered' => 80, 'num_asked' => 800],
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 1, 'num_asked' => 2],
                            ['num_answered' => 3, 'num_asked' => 9],
                            ['num_answered' => 4, 'num_asked' => 6],
                            ['num_answered' => 0, 'num_asked' => 1],
                            ['num_answered' => 0, 'num_asked' => 2]
                    );
                    $sorted_pairs = array(
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 10, 'num_asked' => 10],
                            ['num_answered' => 4, 'num_asked' => 6],
                            ['num_answered' => 1, 'num_asked' => 2],
                            ['num_answered' => 3, 'num_asked' => 9],
                            ['num_answered' => 50, 'num_asked' => 330],
                            ['num_answered' => 80, 'num_asked' => 800],
                            ['num_answered' => 10, 'num_asked' => 900],
                            ['num_answered' => 0, 'num_asked' => 1],
                            ['num_answered' => 0, 'num_asked' => 2]				
                    );
            $data []= array($pairs, $sorted_pairs);
                    $pairs = array(
                            ['num_answered' => 0, 'num_asked' => 6],
                            ['num_answered' => 0, 'num_asked' => 5],
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 1, 'num_asked' => 1],
                            ['num_answered' => 4, 'num_asked' => 10],
                            ['num_answered' => 10000, 'num_asked' => 10001],
                            ['num_answered' => 10000, 'num_asked' => 10002],
                            ['num_answered' => 5, 'num_asked' => 6]
                    );
                    $sorted_pairs = array(
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 1, 'num_asked' => 1],
                            ['num_answered' => 10000, 'num_asked' => 10001],
                            ['num_answered' => 10000, 'num_asked' => 10002],
                            ['num_answered' => 5, 'num_asked' => 6],
                            ['num_answered' => 4, 'num_asked' => 10],
                            ['num_answered' => 0, 'num_asked' => 5],
                            ['num_answered' => 0, 'num_asked' => 6]				
                    );
            $data []= array($pairs, $sorted_pairs);	
            return $data;
        }
	
     /**
     @dataProvider providerFunctionForSortInRegular
     **/
    public function testFunctionForSortInRegular($pairs, $sorted_pairs) {
            //подготовка данных
            $questions = [];
            $package = new GPackage();
            $package->setId(12349);
            $package->save();
            for ($i = 0; $i < 10; $i++) {
                    $question = new GQuestion();
                    $question->setId(123412341 + $i);
                    $question->setPackageId(12349);
                    
                    $question->setText('test text');
                    $question->setRightAnswer('test answer');
                    $question->setNumAnswered($pairs[$i]['num_answered']);
                    $question->setNumAsked($pairs[$i]['num_asked']);
                    $question->save();
                    $questions []= $question;
            }

            //выполнение и проверки
            usort($questions, 'GQuiz::functionForSortInRegular');
            for ($i = 0; $i < 10; $i++) {
                    $this->assertEquals($questions[$i]->getNumAnswered(), $sorted_pairs[$i]['num_answered'], 'must be equal');
                    $this->assertEquals($questions[$i]->getNumAsked(), $sorted_pairs[$i]['num_asked'], 'must be equal');
            }

            //удаление данных
            $package->delete();
    } //method

 // _excludeRecentQuestions tests

    public function test_excludeRecentQuestions_BadElementsInInputArray() {
            $strings = array();
            for ($i = 0; $i < 33; $i++) {
                    $strings []= 'string' . $i;
            }

            $msg = "Type of elements in array questions must be object";
            $this->setExpectedException("InvalidArgumentException", $msg);
            $this->invokeMethod($this->gQuiz, '_excludeRecentQuestions', [$strings]);
    }
    
    public function test_excludeRecentQuestions_NotQuestionsInInputArray() {
            $objects = array();
            for ($i = 0; $i < 33; $i++) {
                    $objects []= new DateTime("now");
            }

            $msg = "Type of objects in array questions must be GQuestion";
            $this->setExpectedException("InvalidArgumentException", $msg);
            $this->invokeMethod($this->gQuiz, '_excludeRecentQuestions', [$objects]);
    }
    
    public function test_excludeRecentQuestions_ExcludingLastHourQuestions() {
            $min_between_equal_bak = $this->gQuiz->config->getMinTimeBetweenEqualQuestions();

            $this->gQuiz->config->setMinTimeBetweenEqualQuestions(3600);

            //подготовка данных
            $first_id = 12345678;
            $package = new GPackage();
            $package->setId(12345);
            $package->save();
            $questions = [];
            $min_between_equal = 3600;
            $seconds_ago = [5, 3700, 800, 3800, 3900, 10, 0, 16, 3530, 3620];
            for ($i = 0; $i < 10; $i++) {
                    $question = new GQuestion();
                    $question->setId($first_id);
                    $question->setPackageId(12345);
                    $question->setText('mur');
                    $question->setRightAnswer('mur');
                    $question->save();
                    $question->setCreatedAt(new DateTime($seconds_ago[$i] . " seconds ago"));
                    $question->save();
                    $question->setUpdatedAt(new DateTime($seconds_ago[$i] . " seconds ago"));
                    $question->save();
                    $questions []= $question;
                    $first_id++;
            }
            $not_recent_questions = [$questions[1], $questions[3], $questions[4], $questions[9]];

            //вызов функции и проверки
            $results = $this->invokeMethod($this->gQuiz, '_excludeRecentQuestions', [$questions]);
            $this->assertEquals(count($results), count($not_recent_questions), 'must be equals');
            for ($i = 0; $i < count($results); $i++) {
                    $this->assertEquals($results[$i]->getId(), $not_recent_questions[$i]->getId(), 'must be equals');
            }

            //восстановление и удаление данных
            $this->gQuiz->config->setMinTimeBetweenEqualQuestions($min_between_equal_bak);
            $package->delete();
    }
    
    public function test_excludeRecentQuestions_ExcludingLast18000secQuestions() {
            $min_between_equal_bak = $this->gQuiz->config->getMinTimeBetweenEqualQuestions();
            $this->gQuiz->config->setMinTimeBetweenEqualQuestions(18000);

            //подготовка данных
            $first_id = 12345678;
            $package = new GPackage();
            $package->setId(12345);
            $package->save();
            $questions = [];
            $min_between_equal = 18000;
            $seconds_ago = [18010, 1800000, 800, 17990, 14000, 100000, 0, 1, 1010, 2020];
            for ($i = 0; $i < 10; $i++) {
                    $question = new GQuestion();
                    $question->setId($first_id);
                    $question->setPackageId(12345);
                    $question->setText('mur');
                    $question->setRightAnswer('mur');
                    $question->save();
                    $question->setCreatedAt(new DateTime($seconds_ago[$i] . " seconds ago"));
                    $question->save();
                    $question->setUpdatedAt(new DateTime($seconds_ago[$i] . " seconds ago"));
                    $question->save();
                    $questions []= $question;
                    $first_id++;
            }
            $not_recent_questions = [$questions[0], $questions[1], $questions[5]];

            //вызов функции и проверки
            $results = $this->invokeMethod($this->gQuiz, '_excludeRecentQuestions', [$questions]);
            $this->assertEquals(count($results), count($not_recent_questions), 'must be equals');
            for ($i = 0; $i < count($results); $i++) {
                    $this->assertEquals($results[$i]->getId(), $not_recent_questions[$i]->getId(), 'must be equals');
            }

            //восстановление и удаление данных
            $this->gQuiz->config->setMinTimeBetweenEqualQuestions($min_between_equal_bak);
            $package->delete();	
        } //method

        
        // _isEqual tests --------------------------
        public function provider_isEqual() {
            $data = array();
            for ($i = 0; $i < 10; $i++) {
                $data[] =  array('test', 'test', true);
                $data[] =  array('TEST', 'TEST', true);
                $data[] =  array('TeSt', 'tEsT', true);
                $data[] =  array('teST', 'TEst', true);

                $data[] =  array('привет', 'привет', true);
                $data[] =  array('привет', 'пРиВеТ', true);
                $data[] =  array('', '', true);
                $data[] =  array('а', 'б', false);
                $data[] =  array(' ', '  ', false);
                $data[] =  array('е', 'ё', true);
                $data[] =  array('ежик', 'ёжик', true);
                $data[] =  array('ЕЛОЧКА', 'ёлочка', true);
                $data[] =  array('ёг', 'йог', false);
                $data[] =  array('the WORD', 'THE word', true);
                $data[] =  array('еЁеЁеЁеЁ', 'еЕеЕеЕеЕ', true);
                $data[] =  array('чемодан', 'чюмодан', false);
                $data[] =  array('еЁеЁ еЁеЁ', 'еЕеЕ еЕеЕ', true);
                $data[] =  array('мАлЕнЬкАя БуКвА ё', 'МаЛеНьКаЯ бУкВа Ё', true);
                $data[] =  array('УПЫРЕНЫШ', 'УПЫРЁНЫШ', true);
                $data[] =  array('БОБРенок', 'бобрЁНОК', true);

            }
            return $data;
    }

    /**
     * @dataProvider provider_isEqual
     */
    public function test_isEqual($s1, $s2, $expected) {
        $actual = $this->invokeMethod($this->gQuiz, '_isEqual', [$s1, $s2]);
        $this->assertEquals($expected, $actual, "Should be equal");
    }

    public function provider_isNear() {
            $data = [];
            $data[] =  array('папоротник', 'папоротнег', true);
            $data[] =  array('папоротник', 'папаратник', true);
            $data[] =  array('папоротник', 'папоротаник', true);
            $data[] =  array('папоротник', 'папоротничанский', false);
            $data[] =  array('папоротник', 'партаник', false);

            $data[] =  array('хлюпик', 'хлюик', true);
            $data[] =  array('хлюпик', 'хшлюбик', true);
            $data[] =  array('хлюпик', 'хлюп', true);
            $data[] =  array('хлюпик', 'люблю', false);

            $data[] =  array('санкт-петербург', 'сакнат-петербург', true);
            $data[] =  array('санкт-петербург', 'петербург', true);
            $data[] =  array('санкт-петербург', 'питер', false);
            $data[] =  array('санкт-петербург', 'бранденбург', false);

            $data[] =  array('привет', 'приве', true);
            
            $data[] =  array('а', 'б', false);


            $data[] =  array('перец', 'еврей', false);
            $data[] =  array('перец', 'гвардеец', false);

            return $data;
    }
    
    /**
     * @dataProvider provider_isNear
     */
    public function test_isNear($s1, $s2, $expected) {
        $actual = $this->invokeMethod($this->gQuiz, '_isNear', [$s1, $s2]);
        $this->assertEquals($expected, $actual, "Should be equal");
    }
    
    public function test_getNextQuestionItCanReturnGQuestion() {
        $question = $this->invokeMethod($this->gQuiz, '_getNextQuestion');
        $this->assertTrue($question instanceof GQuestion);
    }
    
    public function test_getRandomQuestionItCanReturnGQuestion() {
        $question = $this->invokeMethod($this->gQuiz, '_getRandomQuestion');
        $this->assertTrue($question instanceof GQuestion);
    }
    
    public function test_getRandomQuestionsItCanReturnNeccesaryCountOfQuestions() {
        $number = 15;
        $questions = $this->invokeMethod($this->gQuiz, '_getRandomQuestions', [$number]);
        $this->assertEquals($number, count($questions));
    }
    
    public function test_currentQuestionItCanReturnGQuestion() {
        $question = $this->gQuiz->currentQuestion();
        $this->assertTrue($question instanceof GQuestion);
    }
    
    public function test_answeredWhenItCanReturnTrue() {
        //подготовка данных
        $user = $this->createUsersForTests(1) [0];
        $question = (new GQuestionManager())->findQuestionById(101);
        $challenge = new GChallenge();
        $challenge->setQuestion($question);
        $challenge->save();
        $right_answer = new GRightAnswer();
        $right_answer->setChallenge($challenge);
        $right_answer->setUser($user);
        $right_answer->save();
        
        //исполнение и проверка
        $response = $this->invokeMethod($this->gQuiz, '_answered', [$user, $challenge]);
        $this->assertEquals(True, $response);
        
        //очищение БД
        $right_answer->delete();
        $challenge->delete();
        $user->delete();       
    }
    
    public function test_answeredWhenItCanReturnFalse() {
        //подготовка данных
        $user = $this->createUsersForTests(1) [0];
        $question = (new GQuestionManager())->findQuestionById(101);
        $challenge = new GChallenge();
        $challenge->setQuestion($question);
        $challenge->save();
        
        $first_challenge = (new GChallengeManager)->findChallengeById(1);
        $right_answer = new GRightAnswer();
        $right_answer->setChallenge($first_challenge);
        $right_answer->setUser($user);
        $right_answer->save();
        
        //исполнение и проверка
        $response = $this->invokeMethod($this->gQuiz, '_answered', [$user, $challenge]);
        $this->assertEquals(False, $response);
        
        //очищение БД
        $right_answer->delete();
        $challenge->delete();
        $user->delete();       
    }
        
}//class

