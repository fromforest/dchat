<?php

require_once(__DIR__ . '\..\propinit.php');
require_once(__DIR__ . '\..\utils.php');
require_once "GTest.php";

//require "utils.php";

use \Propel\Runtime\ActiveQuery\Criteria;

class GChatTest extends GTest {
   private $g_chat;
   
   public function setUp() {
       $this->g_chat = new GChat();
   }
    
    // _signUp($name, $password) tests ---------------------------

    public function test_signUpCreateUser() {
        $name = "test" . generateRandomString(8);
        $password = "pwd" . generateRandomString(8);
        
        $user = $this->invokeMethod($this->g_chat, '_signUp', array($name, $password));
                
        $this->assertEquals($name, $user->getName(), "names should match");
        $this->assertEquals(md5($password), $user->getPassword(), "passwords should match");

        $user->delete();
    }
    
    public function test_signUpUserAttributes() {
        $name = "test" . generateRandomString(8);
        $password = "pwd" . generateRandomString(8);

        $user = $this->invokeMethod($this->g_chat, '_signUp', array($name, $password));

        $this->assertObjectHasAttribute("id", $user->getUser(), "should have id");
        $this->assertObjectHasAttribute("name", $user->getUser(), "should have name");
        $this->assertObjectHasAttribute("password", $user->getUser(), "should have password");
        $this->assertObjectHasAttribute("users_group", $user->getUser(), "should have users_group");
        // todo заменить users_group на group
        $this->assertObjectHasAttribute("online", $user->getUser(), "should have online");
        $this->assertObjectHasAttribute("best_fifty", $user->getUser(), "should have best_fifty");
        $this->assertObjectHasAttribute("created_at", $user->getUser(), "should have created_at");
        $this->assertObjectHasAttribute("updated_at", $user->getUser(), "should have updated_at");

        $this->assertObjectNotHasAttribute("iddqd", $user->getUser(), "should not have iddqd");

        $user->delete();
    }
    
    public function test_signUpUserClass() {
        $name = "test" . generateRandomString(8);
        $password = "pwd" . generateRandomString(8);

        $user = $this->invokeMethod($this->g_chat, '_signUp', array($name, $password));

        //$this->assertTrue($user instanceof User, "$user should be a User");
        $this->assertInstanceOf('GUser', $user, "user should be a GUser");
        $user->delete();
    }
    
     public function test_signUpLongNameUser() {
        $name = "test" . generateRandomString(16 + 1);
        $password = "pwd" . generateRandomString(8);

        $msg = "Could not create new User : name validation failed: This value is too long. ";
        $msg .= "It should have 16 characters or less.";
        $this->setExpectedException("Exception", $msg);

        $user = $this->invokeMethod($this->g_chat, '_signUp', array($name, $password));
    }
    
    public function test_signUpShortNameUser() {
        $name = generateRandomString(2);
        $password = "pwd" . generateRandomString(8);

        $msg = "Could not create new User : name validation failed: This value is too short. ";
        $msg .= "It should have 3 characters or more.";
        $this->setExpectedException("Exception", $msg);

        $user = $this->invokeMethod($this->g_chat, '_signUp', array($name, $password));
    }
    
    public function test_signUpEmptyName() {
        $name = "";
        $password = "pwd" . generateRandomString(8);

        $msg = "Please enter name and password.";
        $this->setExpectedException("Exception", $msg);

        $user = $this->invokeMethod($this->g_chat, '_signUp', array($name, $password));
    }
    
    public function test_signUpEmptyPassword() {
        $name = "test" . generateRandomString(8);
        $password = "";

        $msg = "Please enter name and password.";
        $this->setExpectedException("Exception", $msg);

        $user = $this->invokeMethod($this->g_chat, '_signUp', array($name, $password));
    }
    
    public function test_signUpOnlyOneArgument() {
        $this->setExpectedException("Exception", 'Missing argument 2 for GChat::_signUp()');
        $response = $this->invokeMethod($this->g_chat, '_signUp', ['a_parameter']);
    }
    
     // _authenticate($name = "", $password = "") tests ----------------------
    
    public function test_authenticateTestUser() {
        $user = $this->invokeMethod($this->g_chat, '_authenticate', array("test", "test"));
        $this->assertNotEquals(false, $user, "test user must be created");
    }
    
    public function test_authenticateWithNameAndPassword() {
        $name = "test" . generateRandomString(8);
        $password = "pwd" . generateRandomString(8);

        $created_user = $this->invokeMethod($this->g_chat, '_signUp', array($name, $password));

        $found_user = $this->invokeMethod($this->g_chat, '_authenticate', array($name, $password));

        $this->assertObjectHasAttribute("id", $found_user->getUser(), "should have id");
        $this->assertObjectHasAttribute("name", $found_user->getUser(), "should have name");
        $this->assertObjectHasAttribute("password", $found_user->getUser(), "should have password");
        $this->assertObjectHasAttribute("users_group", $found_user->getUser(), "should have users_group");
        // todo заменить users_group на group
        $this->assertObjectHasAttribute("online", $found_user->getUser(), "should have online");
        $this->assertObjectHasAttribute("best_fifty", $found_user->getUser(), "should have best_fifty");
        $this->assertObjectHasAttribute("created_at", $found_user->getUser(), "should have created_at");
        $this->assertObjectHasAttribute("updated_at", $found_user->getUser(), "should have updated_at");

        $this->assertObjectNotHasAttribute("iddqd", $found_user->getUser(), "should not have iddqd");

        $this->assertEquals($found_user->getId(), $created_user->getId(), "ids should match");
        $this->assertEquals($found_user->getName(), $created_user->getName(), "names should match");
        $this->assertEquals($found_user->getPassword(), $created_user->getPassword(), "passwords should match");

        $this->assertEquals($found_user->getOnline(), $created_user->getOnline(), "onlines should match");
        $this->assertEquals($found_user->getBestFifty(), $created_user->getBestFifty(), "best_fifty should match");
        $this->assertEquals($found_user->getCreatedAt(), $created_user->getCreatedAt(), "created_at should match");
        $this->assertEquals($found_user->getUpdatedAt(), $created_user->getUpdatedAt(), "updated_at should match");

        $created_user->delete();
    }
    
    public function test_authenticateTestUserWrongPassword() {
        $user = $this->invokeMethod($this->g_chat, '_authenticate', array("test", "test_wrong"));
        $this->assertFalse($user, "wrong password shall not pass");
    }
    
    // api
    // login() tests --------------------------------

    public function testloginTestUser() {
        $name = 'test';
        $password = 'test';
        $response = $this->g_chat->login($name, $password);
        //$this->assertNotNull($response, "must return user");

        $this->assertArrayHasKey("name", $response, "must have name");
        $this->assertArrayHasKey("status", $response, "must have status");

        $this->assertEquals('test', $response['name'], "Must be username");
        $this->assertEquals('login ok', $response['status'], "Must be ok");
    }
    
    public function testloginWrongPassword() {
        $this->setExpectedException("Exception", "Wrong name/password.");

        $name = 'test';
        $password = 'test_wrong';
        $response = $this->g_chat->login($name, $password);
    }
    
    public function testloginOneCreatedUser() {
        $name = 'test' . generateRandomString(8);
        $password  = generateRandomString(8);

        $response = $this->g_chat->login($name, $password);

        $this->assertArrayHasKey("name", $response, "must have name");
        $this->assertArrayHasKey("status", $response, "must have status");

        $this->assertEquals($name, $response['name'], "Must be username");
        $this->assertEquals('login ok', $response['status'], "Must be ok");
        
        $u = (new GUserManager)->findUserByName($name);
      
        $u->delete();
    }
    
    public function tenRandomNamesAndPasswords() {
        $data = array();
        for($i = 0; $i < 10; $i++) {
            array_push($data, array(
                        'test' . generateRandomString(8),
                        'pass' . generateRandomString(8)
                    )
            );
        }
        return $data;
    }
    
     /**
    * @dataProvider tenRandomNamesAndPasswords
    */
    public function testloginTenCreatedUsers($name, $pass)
        {
            $response = $this->g_chat->login($name, $pass);

            $this->assertArrayHasKey("name", $response, "must have name");
            $this->assertArrayHasKey("status", $response, "must have status");

            $this->assertEquals($name, $response['name'], "Must be username");
            $this->assertEquals('login ok', $response['status'], "Must be ok");

            $u = (new GUserManager)->findUserByName($name);
            $u->delete();
        }
    
    // checkLogged() tests -------------------------

    public function testcheckLoggedUserLogInAndLogOut() {       
        $name = 'test';
        $password = 'test';
        $this->g_chat->login($name, $password);
        $response = $this->g_chat->checkLogged();
        
        $this->assertTrue($response['logged'], "User must be logged in");
        $this->assertEquals('test', $response['loggedAs']['name'], "Names should match");
        
        $id =  (new GUserManager)->findUserByName($name)->getId();
        $this->assertEquals($id, $response['loggedAs']['id'], "Id's should match");
        
        $logout_response = $this->g_chat->logout();
        
        $response = $this->g_chat->checkLogged();
        $this->assertFalse($response['logged'], "Should not be logged in");
    }
    
    public function testgetSystemUser() {
        $sys = $this->g_chat->getSystemUser();
        $this->assertObjectHasAttribute("users_group", $sys->getUser(), "should have users_group");
    }
    
    public function testGChatHasMethodSubmitChat() {
        $this->assertTrue(method_exists($this->g_chat, 'submitChat'));
    }
    
}
