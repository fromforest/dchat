<?php


class GTest extends \PHPUnit_Framework_TestCase {
    
    // helper methods
    // they can call private methods
    
    
    public function oldinvokeMethod($class, $methodName, array $parameters = array()) {
        $reflection = new \ReflectionClass($class);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs(null, $parameters);
    }
    
    public function invokeMethod(&$object, $methodName, array $parameters = array()) {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $parameters);
    }
    
    public function testTrueIsTrue() {
        $foo = true;
        $this->assertTrue($foo);
    }
    
    public function createUsersForTests($how_many_users_create) {
        for ($i = 0; $i < $how_many_users_create; $i++) {
            $user = new GUser();
            $user->setName("test" . $i);
            $user->setPassword(md5("pass" . $i));
            $user->setOnline(true);
            $user->save();
            $users [] = $user;
        }
        return $users;
    }
    
    public function deleteUsers($users) {
        foreach ($users as $user) {
            if (!is_a($user, 'GUser')) {
                throw new InvalidArgumentException('input argument must be GUser');
            }
            $user->delete();
        }
    }
    
} // class
