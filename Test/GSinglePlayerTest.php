<?php

require_once "GTest.php";

class GSinglePlayerTest extends GTest {
    
    
    public function providerFunctionForSortInSinglePlayer() {
            $data = [];
                    $pairs = array(
                            ['num_answered' => 10, 'num_asked' => 900],
                            ['num_answered' => 50, 'num_asked' => 330],
                            ['num_answered' => 10, 'num_asked' => 10],
                            ['num_answered' => 80, 'num_asked' => 800],
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 1, 'num_asked' => 3],
                            ['num_answered' => 3, 'num_asked' => 9],
                            ['num_answered' => 4, 'num_asked' => 6],
                            ['num_answered' => 0, 'num_asked' => 1],
                            ['num_answered' => 1, 'num_asked' => 2]
                    );
                    $sorted_pairs = array(
                            ['num_answered' => 0, 'num_asked' => 0],
                            ['num_answered' => 0, 'num_asked' => 1], //0/11
                            ['num_answered' => 10, 'num_asked' => 900], //1/91
                            ['num_answered' => 1, 'num_asked' => 3],     //1/13
                            ['num_answered' => 1, 'num_asked' => 2],	//1/12
                            ['num_answered' => 80, 'num_asked' => 800], //8/81
                            ['num_answered' => 50, 'num_asked' => 330], //5/34
                            ['num_answered' => 3, 'num_asked' => 9],     //3/19
                            ['num_answered' => 4, 'num_asked' => 6],     //1/4
                            ['num_answered' => 10, 'num_asked' => 10] //1/2
                    );
            $data []= array($pairs, $sorted_pairs);
                    $pairs = array(
                            ['num_answered' => 0, 'num_asked' => 6],
                            ['num_answered' => 1, 'num_asked' => 5],
                            ['num_answered' => 1, 'num_asked' => 1],
                            ['num_answered' => 2, 'num_asked' => 2],
                            ['num_answered' => 3, 'num_asked' => 4],
                            ['num_answered' => 1, 'num_asked' => 18],
                            ['num_answered' => 4, 'num_asked' => 10],
                            ['num_answered' => 10000, 'num_asked' => 10001],
                            ['num_answered' => 10000, 'num_asked' => 10002],
                            ['num_answered' => 5, 'num_asked' => 6]
                    );
                    $sorted_pairs = array(
                            ['num_answered' => 0, 'num_asked' => 6],
                            ['num_answered' => 1, 'num_asked' => 18],
                            ['num_answered' => 1, 'num_asked' => 5],
                            ['num_answered' => 1, 'num_asked' => 1],
                            ['num_answered' => 2, 'num_asked' => 2],
                            ['num_answered' => 4, 'num_asked' => 10],
                            ['num_answered' => 3, 'num_asked' => 4],
                            ['num_answered' => 5, 'num_asked' => 6],
                            ['num_answered' => 10000, 'num_asked' => 10002],
                            ['num_answered' => 10000, 'num_asked' => 10001]                            
                    );
            $data []= array($pairs, $sorted_pairs);	
            return $data;
        }
        
     /**
     @dataProvider providerFunctionForSortInSinglePlayer
     **/
    public function testFunctionForSortInRegular($pairs, $sorted_pairs) {
            //подготовка данных
            $question_objects = [];
            for ($i = 0; $i < 10; $i++) {
                    $question_struct = ['question' => $this->getMock('GQuestion'), 
                                                   'num_asked' => $pairs[$i]['num_asked'],
                                                   'num_answered' => $pairs[$i]['num_answered'] ];
                    $question_objects []= $question_struct;
            }
                         
            //выполнение и проверки
            usort($question_objects, 'GSinglePlayer::functionForSortInSinglePlayer');
            for ($i = 0; $i < 10; $i++) {
                    $this->assertEquals($question_objects[$i]['num_answered'], $sorted_pairs[$i]['num_answered'], 'must be equal');
                    $this->assertEquals($question_objects[$i]['num_asked'], $sorted_pairs[$i]['num_asked'], 'must be equal');
            }

    } //method
}
