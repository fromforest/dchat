<?php

require_once "GTest.php";

class GTournamentTest extends GTest {
    private $gTournament;
    
    public function setUp() {
        $this->gTournament = new GTournament(new GConfig());
    }
    
    public function test_getTournamentQuestionResponseCanBeGQuestion() {
        $question = $this->invokeMethod($this->gTournament, '_getTournamentQuestion');
        $this->assertTrue($question instanceof GQuestion);
    }

    
} // class
