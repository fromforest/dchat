import os

def writeln (filename, element):
    f = open (filename, 'a', encoding = 'utf-8')
    f.write (str (element) + '\n')
    f.close ()
    
def readlist (f_in_filename):
    f_in = open (f_in_filename)
    f_in_list = f_in.readlines()
    for i in range (0,len (f_in_list)):
        f_in_list[i] = f_in_list [i][0:-1]
    return f_in_list
    
def del_spaces_and_tabs_in_start_of_line (string):
    if string == '':
        return string
    while string != '' and (string[0] == '\t' or string [0] == ' '):
        string = string[1:]
    return string
    
def del_spaces_and_tabs_in_end_of_line (string):
    if string == '':
        return string
    while string!= '' and (string[-1] == '\t' or string [-1] == ' ' or string [-1] == '\n'):
        string = string [:-1]
    return string

author = 'HRUST'
package_id = 1000
name = 'tp' + str(package_id)
first_id = 1000291
result = 'package.xml'
src_dir = './results'
os.chdir (src_dir)
files = os.listdir()
import random
random.shuffle(files)
description = 'text questions'

writeln(result, '<?xml version="1.0" encoding="UTF-8"?>')
writeln(result, '\t'+ '<package>')
writeln(result, '\t\t' + '<id>' + str (package_id) + '</id>')
writeln(result, '\t\t' + '<name>' + name + '</name>')
writeln(result, '\t\t' + '<description>' + description + '</description>')

default_text = ''
alt_answers = ''

#============================ основной цикл по файлам =======================
id = first_id

for file in files:
    if '.xml' in file:
        print ('delete xml file')
        exit
    try:
        topic = file.split('.')[0]
        strings = readlist(file)
        for line1 in strings:
            line = del_spaces_and_tabs_in_start_of_line(line1)
            line = del_spaces_and_tabs_in_end_of_line(line)
            if line == '':
                continue
            text_answer_comment = line.split('|')
            text = text_answer_comment[0]
            answer_comment = text_answer_comment[1].split('#')
            if len(answer_comment) == 1:
                answer_comment = text_answer_comment[1].split('-info-')
            answers = answer_comment[0].split('&')
            right_answer = answers[0]
            if len(answers) > 1:
                alt_answers = '&'.join(answers[1:])
            if len(answer_comment) == 2:
                comment = answer_comment[1]
            else:
                comment = default_text
            
             #======================= запись в файл =================================                
            writeln(result, '\t\t' + '<question>')
            
            writeln(result, '\t\t\t'+ '<id>' + str(id) + '</id>')
            writeln(result, '\t\t\t'+ '<type>' + 'text' + '</type>')
            writeln(result, '\t\t\t'+ '<topic>' + topic + '</topic>')
            writeln(result, '\t\t\t'+ '<text>' + text + '</text>')
            writeln(result, '\t\t\t'+ '<right_answer>' + right_answer + '</right_answer>')
            writeln(result, '\t\t\t'+ '<comment>' + comment + '</comment>')
            if alt_answers:
                writeln(result, '\t\t\t'+ '<alt_answers>' + alt_answers + '</alt_answers>')
            writeln(result, '\t\t\t'+ '<author>' + author + '</author>')
        
            writeln(result, '\t\t' + '</question>')
            id, alt_answers, comment = id + 1 , '' , ''
    except:
        print(file)

writeln(result, '\t'+'</package>')