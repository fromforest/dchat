<?php

class C {

	public function foo() { echo "foo"; }

}

if(true) {

    function bar() { echo "bar"; }

}

class C {
	
	public function baz() { echo "baz\n"; }

}

$c = new C();
$c->foo();

bar();

$c->baz();

