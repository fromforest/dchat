<?php

//namespace Test;

//require "propinit.php";
//require "utils.php";
require_once(__DIR__ . '\..\propinit.php');
require_once(__DIR__ . '\..\utils.php');

use \Chat;

class ChatTest extends \PHPUnit_Framework_TestCase {


    // helper method
    // this can call private methods
    public function invokeMethod($class, $methodName, array $parameters = array()) {
        $reflection = new \ReflectionClass($class);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs(null, $parameters);
    }

    // ------------------ Tests follow ---------------------------
    // _signUp($name, $password) tests ---------------------------

    public function test_signUpCreateUser() {
        $name = "test" . generateRandomString(8);
        $password = "pwd" . generateRandomString(8);

        $user = $this->invokeMethod("Chat", '_signUp', array($name, $password));
                
        $this->assertEquals($name, $user->getName(), "names should match");
        $this->assertEquals(md5($password), $user->getPassword(), "passwords should match");

        $user->delete();
    }
    
    public function test_signUpUserAttributes() {
        $name = "test" . generateRandomString(8);
        $password = "pwd" . generateRandomString(8);

        $user = $this->invokeMethod("Chat", '_signUp', array($name, $password));

        $this->assertObjectHasAttribute("id", $user, "should have id");
        $this->assertObjectHasAttribute("name", $user, "should have name");
        $this->assertObjectHasAttribute("password", $user, "should have password");
        $this->assertObjectHasAttribute("users_group", $user, "should have users_group");
        // todo заменить users_group на group
        $this->assertObjectHasAttribute("online", $user, "should have online");
        $this->assertObjectHasAttribute("best_fifty", $user, "should have best_fifty");
        $this->assertObjectHasAttribute("created_at", $user, "should have created_at");
        $this->assertObjectHasAttribute("updated_at", $user, "should have updated_at");

        $this->assertObjectNotHasAttribute("iddqd", $user, "should not have iddqd");

        $user->delete();
    }

    public function test_signUpUserClass() {
        $name = "test" . generateRandomString(8);
        $password = "pwd" . generateRandomString(8);

        $user = $this->invokeMethod("Chat", '_signUp', array($name, $password));

        //$this->assertTrue($user instanceof User, "$user should be a User");
        $this->assertInstanceOf('User', $user, "$user should be a User");

        $user->delete();
    }

    public function test_signUpLongNameUser() {
        $name = "test" . generateRandomString(16 + 1);
        $password = "pwd" . generateRandomString(8);

        $msg = "Could not create new User : name validation failed: This value is too long. ";
        $msg .= "It should have 16 characters or less.";
        $this->setExpectedException("Exception", $msg);

        $user = $this->invokeMethod("Chat", '_signUp', array($name, $password));
    }

    public function test_signUpShortNameUser() {
        $name = generateRandomString(2);
        $password = "pwd" . generateRandomString(8);

        $msg = "Could not create new User : name validation failed: This value is too short. ";
        $msg .= "It should have 3 characters or more.";
        $this->setExpectedException("Exception", $msg);

        $user = $this->invokeMethod("Chat", '_signUp', array($name, $password));
    }

    public function test_signUpEmptyName() {
        $name = "";
        $password = "pwd" . generateRandomString(8);

        $msg = "Please enter name and password.";
        $this->setExpectedException("Exception", $msg);

        $user = $this->invokeMethod("Chat", '_signUp', array($name, $password));
    }

    public function test_signUpEmptyPassword() {
        $name = "test" . generateRandomString(8);
        $password = "";

        $msg = "Please enter name and password.";
        $this->setExpectedException("Exception", $msg);

        $user = $this->invokeMethod("Chat", '_signUp', array($name, $password));
    }

    public function test_signUpOnlyOneArgument() {
        $this->setExpectedException("Exception", 'Missing argument 2 for Chat::_signUp()');
        $response = $this->invokeMethod("Chat", '_signUp', ['a_parameter']);
    }

    // _authenticate($name = "", $password = "") tests ----------------------

    public function test_authenticateTestUser() {
        $user = $this->invokeMethod("Chat", '_authenticate', array("test", "test"));
        $this->assertNotEquals(false, $user, "test user must be created");
    }

    public function test_authenticateWithNameAndPassword() {
        $name = "test" . generateRandomString(8);
        $password = "pwd" . generateRandomString(8);

        $created_user = $this->invokeMethod("Chat", '_signUp', array($name, $password));

        $found_user = $this->invokeMethod("Chat", '_authenticate', array($name, $password));

        $this->assertObjectHasAttribute("id", $found_user, "should have id");
        $this->assertObjectHasAttribute("name", $found_user, "should have name");
        $this->assertObjectHasAttribute("password", $found_user, "should have password");
        $this->assertObjectHasAttribute("users_group", $found_user, "should have users_group");
        // todo заменить users_group на group
        $this->assertObjectHasAttribute("online", $found_user, "should have online");
        $this->assertObjectHasAttribute("best_fifty", $found_user, "should have best_fifty");
        $this->assertObjectHasAttribute("created_at", $found_user, "should have created_at");
        $this->assertObjectHasAttribute("updated_at", $found_user, "should have updated_at");

        $this->assertObjectNotHasAttribute("iddqd", $found_user, "should not have iddqd");

        $this->assertEquals($found_user->getId(), $created_user->getId(), "ids should match");
        $this->assertEquals($found_user->getName(), $created_user->getName(), "names should match");
        $this->assertEquals($found_user->getPassword(), $created_user->getPassword(), "passwords should match");

        $this->assertEquals($found_user->getOnline(), $created_user->getOnline(), "onlines should match");
        $this->assertEquals($found_user->getBestFifty(), $created_user->getBestFifty(), "best_fifty should match");
        $this->assertEquals($found_user->getCreatedAt(), $created_user->getCreatedAt(), "created_at should match");
        $this->assertEquals($found_user->getUpdatedAt(), $created_user->getUpdatedAt(), "updated_at should match");

        $created_user->delete();
    }

    public function test_authenticateTestUserWrongPassword() {
        $user = $this->invokeMethod("Chat", '_authenticate', array("test", "test_wrong"));
        $this->assertFalse($user, "wrong password shall not pass");
    }

    // api
    // login() tests --------------------------------

    public function testloginTestUser() {
        $_POST['name'] = 'test';
        $_POST['password'] = 'test';
        $response = $this->chat->login();
        //$this->assertNotNull($response, "must return user");

        $this->assertArrayHasKey("name", $response, "must have name");
        $this->assertArrayHasKey("status", $response, "must have status");

        $this->assertEquals('test', $response['name'], "Must be username");
        $this->assertEquals('login ok', $response['status'], "Must be ok");
    }

    public function testloginWrongPassword() {
        $this->setExpectedException("Exception", "Wrong name/password.");

        $_POST['name'] = 'test';
        $_POST['password'] = 'test_wrong';
        $this->chat->login();
    }

    public function testloginOneCreatedUser() {
        $name = 'test' . generateRandomString(8);
        $_POST['name'] = $name;
        $_POST['password'] = generateRandomString(8);

        $response = $this->chat->login();

        $this->assertArrayHasKey("name", $response, "must have name");
        $this->assertArrayHasKey("status", $response, "must have status");

        $this->assertEquals($name, $response['name'], "Must be username");
        $this->assertEquals('login ok', $response['status'], "Must be ok");
        
        $u = UserQuery::create()->findOneByName($name);
        $u->delete();
        
        $_POST['name'] = '';
        $_POST['password'] = '';
    }

public function tenRandomNamesAndPasswords()
{
    $data = array();
    for($i = 0; $i < 10; $i++) {
        array_push($data, array(
                    'test' . generateRandomString(8),
                    'pass' . generateRandomString(8)
                )
        );
    }
    return $data;
}
    
    /**
    * @dataProvider tenRandomNamesAndPasswords
    */
public function testloginTenCreatedUsers($name, $pass)
{
        $_POST['name'] = $name;
        $_POST['password'] = $pass;

        $response = $this->chat->login();
        
//        echo "\nusername " . $name;
//        echo " pass " . $pass;

        $this->assertArrayHasKey("name", $response, "must have name");
        $this->assertArrayHasKey("status", $response, "must have status");

        $this->assertEquals($name, $response['name'], "Must be username");
        $this->assertEquals('login ok', $response['status'], "Must be ok");
        
        $u = UserQuery::create()->findOneByName($name);
        $u->delete();
        
        $_POST['name'] = '';
        $_POST['password'] = '';
       
}

    // checkLogged() tests -------------------------

    public function testcheckLoggedUserLogInAndLogOut() {
        
        $_POST['name'] = 'test';
        $_POST['password'] = 'test';
        $this->chat->login();
        //print_r($_SESSION['user']);
        //echo "------";
        $response = $this->chat->checkLogged();
        //print_r($response);
        
        $this->assertTrue($response['logged'], "User must be logged in");
        $this->assertEquals('test', $response['loggedAs']['name'], "Names should match");
        
        $id = UserQuery::create()->findOneByName('test')->getId();
        $this->assertEquals($id, $response['loggedAs']['id'], "Id's should match");
        
        $logout_response = $this->chat->logout();
        $_POST['name'] = '';
        $_POST['password'] = '';
        
        $response = $this->chat->checkLogged();
        $this->assertFalse($response['logged'], "Should not be logged in");
    }
    

}
