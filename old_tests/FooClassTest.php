<?php

//test for sumFoo

namespace Test;

use \FooClass;

class StupidTest extends \PHPUnit_Framework_TestCase {

    // public function testTrueIsTrue()
    // example test

    public function testTrueIsTrue() {
        $foo = true;
        $this->assertTrue($foo);
    }

    // example function

    public function foo() {
        echo "not ok";
    }

    // helper method
    // this can call private methods
    public function invokeMethod($class, $methodName, array $parameters = array()) {
        $reflection = new \ReflectionClass($class);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs(null, $parameters);
    }

    public function testSum() {
        $f = new FooClass();
        $res = $f->sum(2, 3);
        $this->assertEquals(2 + 3, $res);
    }

    //testing private method
    public function testPrivateSum() {

        $res = $this->invokeMethod("FooClass", 'psum', array(1, 2));
        $this->assertEquals(3, $res, "1 + 2 = 3");
    }

}
