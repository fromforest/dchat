<?php

//namespace Test;

require "propinit.php";

//require "utils.php";

use \Quiz;
use \Propel\Runtime\ActiveQuery\Criteria;

class QuizTest extends \PHPUnit_Framework_TestCase {

    // example test

    public function testTrueIsTrue() {
        $foo = true;
        $this->assertTrue($foo);
    }

    public function invokeMethod($class, $methodName, array $parameters = array()) {
        $reflection = new \ReflectionClass($class);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs(null, $parameters);
    }


    public function test_incrementNumAskedNoIncrement() {
        $users_online_count = UserQuery::create()->filterByOnline(true)->count();
        $min_users_for_updates = ConfigQuery::create()->findOneById(1)->getMinUsersForUpdates();
		$this->assertTrue($users_online_count < $min_users_for_updates, 'Please check in table users, how many users is online');
        $how_many_users_create = $min_users_for_updates - $users_online_count - 1;
        $users = [];
        for ($i = 0; $i < $how_many_users_create; $i++) {
            $user = new User(['Name' => "test" . $i, 'Password' => md5("pass" . $i)]);
            $user->setOnline(true);
            $user->save();
            $users [] = $user;
        }
        $question = QuestionQuery::create()->findOneById(101);
        $updated_at = $question->getUpdatedAt();
        $num_asked = $question->getNumAsked();
        $this->invokeMethod('Quiz', '_incrementNumAsked', [$question]);
        $new_num_asked = $question->getNumAsked();
        $this->assertEquals($num_asked, $new_num_asked, 'should not be incremented');

        //восстановление прежних значений num_asked и updated_at
        $question->setNumAsked($num_asked);
        $question->save(); //при этой последовательности команд...
        $question->setUpdatedAt($updated_at); // updated_at действительно...
        $question->save(); //восстанавливается прежним.
        //удаление созданных юзеров
        foreach ($users as $user) {
            $user->delete();
        }
    }


// updateChallenge tests -------------------

	public function testUpdateChallenge_ResetChallengeUpdatedAfterDoubleQtime() {
		$question = QuestionQuery::create()->findOneById(101);
		$config = ConfigQuery::create()->findOneById(1);
		$qtime = $config->getQuestionTime();
                                $updated_at = $question->getUpdatedAt();
		$num_asked = $question->getNumAsked();

		$challenge = new Challenge();
		$challenge->setQuestion($question);
		$challenge->save();
		
		$challenge->setCreatedAt(new Datetime((2*$qtime + 1) . " seconds ago"));
		$challenge->save();
		$config->setChallengeUpdated(6);
		$config->save();
		
		Quiz::updateChallenge();
		$this->assertEquals($config->getChallengeUpdated(), 2, 'config parameter ChallengeUpdated should be reseted into 0 and then became 2');
		
		//восстановление прежних значений и удаление созданного challenge
		$challenge->delete();
		$question->setNumAsked($num_asked);
		$question->save();
		$config->setChallengeUpdated(0);
		$config->save();
		$question->setUpdatedAt($updated_at);
		$question->save();
	}

	public function testUpdateChallenge_NotUpdateChallenge() {
		$question = QuestionQuery::create()->findOneById(101);
		$config = ConfigQuery::create()->findOneById(1);
		$qtime = $config->getQuestionTime();
                                $updated_at = $question->getUpdatedAt();
		$num_asked = $question->getNumAsked();

		$challenge = new Challenge();
		$challenge->setQuestion($question);
		$challenge->save();
		
		$challenge->setCreatedAt(new Datetime(($qtime + 1) . " seconds ago"));
		$challenge->save();
		$config->setChallengeUpdated(1);
		$config->save();
		
		Quiz::updateChallenge();
		$this->assertEquals($config->getChallengeUpdated(), 1, 'config parameter ChallengeUpdated should be equal 1 because check before challenge-updating must exit you from function');
		
		//восстановление прежних значений и удаление созданного challenge
		$challenge->delete();
		$question->setNumAsked($num_asked);
		$question->save();
		$config->setChallengeUpdated(0);
		$config->save();
		$question->setUpdatedAt($updated_at);
		$question->save();
	}

	public function testUpdateChallenge_IsNewChallengeExist() {
		$question = QuestionQuery::create()->findOneById(101);
		$config = ConfigQuery::create()->findOneById(1);
		$qtime = $config->getQuestionTime();
                                $updated_at = $question->getUpdatedAt();
		$num_asked = $question->getNumAsked();

		$challenge = new Challenge();
		$challenge->setQuestion($question);
		$challenge->save();
		
		$challenge->setCreatedAt(new Datetime(($qtime + 1) . " seconds ago"));
		$challenge->save();
		$config->setChallengeUpdated(0);
		$config->save();
		
		Quiz::updateChallenge();
		$new_challenge = ChallengeQuery::create()->orderById(Criteria::DESC)->findOne();
		
		$this->assertObjectHasAttribute("id", $new_challenge, "should have id");
		$this->assertObjectHasAttribute("question_id", $new_challenge, "should have question_id");
		$this->assertObjectHasAttribute("created_at", $new_challenge, "should have created_at");
		$this->assertObjectHasAttribute("updated_at", $new_challenge, "should have updated_at");
		$this->assertObjectNotHasAttribute("question", $new_challenge, "should not have question, must be have only question_id");
		$this->assertEquals($new_challenge->getId(), $challenge->getId() + 1, 'id of new challenge should be equal previous challenge id + 1');
		$this->assertEquals($config->getChallengeUpdated(), 2, 'config parameter ChallengeUpdated should be equal 2 right after updateChallenge');
		
		//восстановление прежних значений и удаление созданного challenge
		$challenge->delete();
		$question->setNumAsked($num_asked);
		$question->save();
		$config->setChallengeUpdated(0);
		$config->save();	
		$question->setUpdatedAt($updated_at);
		$question->save();
	}

//functionForSort tests
	public function providerFunctionForSort() {
		$data = [];
			$pairs = array(
				['num_answered' => 10, 'num_asked' => 900],
				['num_answered' => 50, 'num_asked' => 330],
				['num_answered' => 10, 'num_asked' => 10],
				['num_answered' => 80, 'num_asked' => 800],
				['num_answered' => 0, 'num_asked' => 0],
				['num_answered' => 1, 'num_asked' => 2],
				['num_answered' => 3, 'num_asked' => 9],
				['num_answered' => 4, 'num_asked' => 6],
				['num_answered' => 0, 'num_asked' => 1],
				['num_answered' => 0, 'num_asked' => 2]
			);
			$sorted_pairs = array(
				['num_answered' => 0, 'num_asked' => 0],
				['num_answered' => 10, 'num_asked' => 10],
				['num_answered' => 4, 'num_asked' => 6],
				['num_answered' => 1, 'num_asked' => 2],
				['num_answered' => 3, 'num_asked' => 9],
				['num_answered' => 50, 'num_asked' => 330],
				['num_answered' => 80, 'num_asked' => 800],
				['num_answered' => 10, 'num_asked' => 900],
				['num_answered' => 0, 'num_asked' => 1],
				['num_answered' => 0, 'num_asked' => 2]				
			);
		$data []= array($pairs, $sorted_pairs);
			$pairs = array(
				['num_answered' => 0, 'num_asked' => 6],
				['num_answered' => 0, 'num_asked' => 5],
				['num_answered' => 0, 'num_asked' => 0],
				['num_answered' => 0, 'num_asked' => 0],
				['num_answered' => 0, 'num_asked' => 0],
				['num_answered' => 1, 'num_asked' => 1],
				['num_answered' => 4, 'num_asked' => 10],
				['num_answered' => 10000, 'num_asked' => 10001],
				['num_answered' => 10000, 'num_asked' => 10002],
				['num_answered' => 5, 'num_asked' => 6]
			);
			$sorted_pairs = array(
				['num_answered' => 0, 'num_asked' => 0],
				['num_answered' => 0, 'num_asked' => 0],
				['num_answered' => 0, 'num_asked' => 0],
				['num_answered' => 1, 'num_asked' => 1],
				['num_answered' => 10000, 'num_asked' => 10001],
				['num_answered' => 10000, 'num_asked' => 10002],
				['num_answered' => 5, 'num_asked' => 6],
				['num_answered' => 4, 'num_asked' => 10],
				['num_answered' => 0, 'num_asked' => 5],
				['num_answered' => 0, 'num_asked' => 6]				
			);
		$data []= array($pairs, $sorted_pairs);	
		return $data;
	}
	
	/**
     * @dataProvider providerFunctionForSort
     */
	public function testFunctionForSort($pairs, $sorted_pairs) {
		//подготовка данных
		$questions = [];
		$package = new Package();
		$package->setId(12345);
		$package->save();
		for ($i = 0; $i < 10; $i++) {
			$question = new Question();
			$question->setId(123412341 + $i);
			$question->setPackageId(12345);
			$question->setText('test text');
			$question->setRightAnswer('test answer');
			$question->setNumAnswered($pairs[$i]['num_answered']);
			$question->setNumAsked($pairs[$i]['num_asked']);
			$question->save();
			$questions []= $question;
		}

		//выполнение и проверки
		usort($questions, 'Quiz::functionForSort');
		for ($i = 0; $i < 10; $i++) {
			$this->assertEquals($questions[$i]->getNumAnswered(), $sorted_pairs[$i]['num_answered'], 'must be equal');
			$this->assertEquals($questions[$i]->getNumAsked(), $sorted_pairs[$i]['num_asked'], 'must be equal');
		}

		//удаление данных
		$package->delete();
	}

// _excludeRecentQuestions tests

	public function test_excludeRecentQuestions_BadElementsInInputArray() {
		$strings = array();
		for ($i = 0; $i < 33; $i++) {
			$strings []= 'string' . $i;
		}

		$msg = "Type of elements in array questions must be object";
		$this->setExpectedException("InvalidArgumentException", $msg);
		$this->invokeMethod('Quiz', '_excludeRecentQuestions', [$strings]);
	}

	public function test_excludeRecentQuestions_NotQuestionsInInputArray() {
		$objects = array();
		for ($i = 0; $i < 33; $i++) {
			$objects []= new DateTime("now");
		}

		$msg = "Type of objects in array questions must be Question";
		$this->setExpectedException("InvalidArgumentException", $msg);
		$this->invokeMethod('Quiz', '_excludeRecentQuestions', [$objects]);
	}

	public function test_excludeRecentQuestions_ExcludingLastHourQuestions() {
		$config = ConfigQuery::create()->findOneById(1);
		$min_between_equal_bak = $config->getMinTimeBetweenEqualQuestions();
		
		$config->setMinTimeBetweenEqualQuestions(3600);
		$config->save();

		//подготовка данных
		$first_id = 12345678;
		$package = new Package();
		$package->setId(12345);
		$package->save();
		$questions = [];
		$min_between_equal = 3600;
		$seconds_ago = [5, 3700, 800, 3800, 3900, 10, 0, 16, 3530, 3620];
		for ($i = 0; $i < 10; $i++) {
			$question = new Question();
			$question->setId($first_id);
			$question->setPackageId(12345);
			$question->setText('mur');
			$question->setRightAnswer('mur');
			$question->save();
			$question->setCreatedAt(new DateTime($seconds_ago[$i] . " seconds ago"));
			$question->save();
			$question->setUpdatedAt(new DateTime($seconds_ago[$i] . " seconds ago"));
			$question->save();
			$questions []= $question;
			$first_id++;
		}
		$not_recent_questions = [$questions[1], $questions[3], $questions[4], $questions[9]];
		
		//вызов функции и проверки
		$results = $this->invokeMethod('Quiz', '_excludeRecentQuestions', [$questions]);
		$this->assertEquals(count($results), count($not_recent_questions), 'must be equals');
		for ($i = 0; $i < count($results); $i++) {
			$this->assertEquals($results[$i]->getId(), $not_recent_questions[$i]->getId(), 'must be equals');
		}
		
		//восстановление и удаление данных
		$config->setMinTimeBetweenEqualQuestions($min_between_equal_bak);
		$config->save();
		$package->delete();	
	}

	public function test_excludeRecentQuestions_ExcludingLast18000secQuestions() {
		$config = ConfigQuery::create()->findOneById(1);
		$min_between_equal_bak = $config->getMinTimeBetweenEqualQuestions();
		
		$config->setMinTimeBetweenEqualQuestions(18000);
		$config->save();

		//подготовка данных
		$first_id = 12345678;
		$package = new Package();
		$package->setId(12345);
		$package->save();
		$questions = [];
		$min_between_equal = 18000;
		$seconds_ago = [18010, 1800000, 800, 17990, 14000, 100000, 0, 1, 1010, 2020];
		for ($i = 0; $i < 10; $i++) {
			$question = new Question();
			$question->setId($first_id);
			$question->setPackageId(12345);
			$question->setText('mur');
			$question->setRightAnswer('mur');
			$question->save();
			$question->setCreatedAt(new DateTime($seconds_ago[$i] . " seconds ago"));
			$question->save();
			$question->setUpdatedAt(new DateTime($seconds_ago[$i] . " seconds ago"));
			$question->save();
			$questions []= $question;
			$first_id++;
		}
		$not_recent_questions = [$questions[0], $questions[1], $questions[5]];

		//вызов функции и проверки
		$results = $this->invokeMethod('Quiz', '_excludeRecentQuestions', [$questions]);
		$this->assertEquals(count($results), count($not_recent_questions), 'must be equals');
		for ($i = 0; $i < count($results); $i++) {
			$this->assertEquals($results[$i]->getId(), $not_recent_questions[$i]->getId(), 'must be equals');
		}
		
		//восстановление и удаление данных
		$config->setMinTimeBetweenEqualQuestions($min_between_equal_bak);
		$config->save();
		$package->delete();	
	}

// _isEqual tests --------------------------

    public function provider_isEqual() {
        $data = array();
        for ($i = 0; $i < 10; $i++) {
            $data[] =  array('test', 'test', true);
            $data[] =  array('TEST', 'TEST', true);
            $data[] =  array('TeSt', 'tEsT', true);
            $data[] =  array('teST', 'TEst', true);
            
            $data[] =  array('привет', 'привет', true);
            $data[] =  array('привет', 'пРиВеТ', true);
            $data[] =  array('', '', true);
            $data[] =  array('а', 'б', false);
            $data[] =  array(' ', '  ', false);
            $data[] =  array('е', 'ё', true);
            $data[] =  array('ежик', 'ёжик', true);
            $data[] =  array('ЕЛОЧКА', 'ёлочка', true);
            $data[] =  array('ёг', 'йог', false);
            $data[] =  array('the WORD', 'THE word', true);
            $data[] =  array('еЁеЁеЁеЁ', 'еЕеЕеЕеЕ', true);
            $data[] =  array('чемодан', 'чюмодан', false);
            $data[] =  array('еЁеЁ еЁеЁ', 'еЕеЕ еЕеЕ', true);
            $data[] =  array('мАлЕнЬкАя БуКвА ё', 'МаЛеНьКаЯ бУкВа Ё', true);
            $data[] =  array('УПЫРЕНЫШ', 'УПЫРЁНЫШ', true);
            $data[] =  array('БОБРенок', 'бобрЁНОК', true);
            
        }
        return $data;
    }

    /**
     * @dataProvider provider_isEqual
     */
	public function test_isEqual($s1, $s2, $expected) {
        $actual = $this->invokeMethod("Quiz", '_isEqual', [$s1, $s2]);
        $this->assertEquals($expected, $actual, "Should be equal");
    }

	public function provider_isNear() {
		$data = [];
		$data[] =  array('папоротник', 'папоротнег', true);
		$data[] =  array('папоротник', 'папаратник', true);
		$data[] =  array('папоротник', 'папоротаник', true);
		$data[] =  array('папоротник', 'папоротничанский', false);
		$data[] =  array('папоротник', 'партаник', false);
		
        $data[] =  array('хлюпик', 'хлюик', true);
		$data[] =  array('хлюпик', 'хшлюбик', true);
		$data[] =  array('хлюпик', 'хлюп', true);
		$data[] =  array('хлюпик', 'люблю', false);

        $data[] =  array('санкт-петербург', 'сакнат-петербург', true);
		$data[] =  array('санкт-петербург', 'петербург', true);
		$data[] =  array('санкт-петербург', 'питер', false);
		$data[] =  array('санкт-петербург', 'бранденбург', false);

        $data[] =  array('привет', 'приве', true);
        $data[] =  array('привет', 'пррива', true);
		$data[] =  array('привет', 'привееееет', true);
        
		$data[] =  array('а', 'б', false);
        
		$data[] =  array('перец', 'перце', true);
        $data[] =  array('перец', 'перчик', true);

        $data[] =  array('перец', 'еврей', false);
		$data[] =  array('перец', 'гвардеец', false);

		return $data;
	}

	/**
     * @dataProvider provider_isNear
     */
	public function test_isNear($s1, $s2, $expected) {
        $actual = $this->invokeMethod("Quiz", '_isNear', [$s1, $s2]);
        $this->assertEquals($expected, $actual, "Should be equal");
    }

}

//class
?>