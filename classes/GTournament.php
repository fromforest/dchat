<?php

class GTournament {
    public function __construct(GConfig $config) {
        $this->config = $config;
    }
    
   public function _isTournamentEnd() {
        $index = $this->config->getTqi();
        $questions = (new GQuestionManager())->getAllQuestionsIds('image');
        if ($index < count($questions)) {
            return false;
        } else {
            $this->config->setQuizMode('stop');
            $this->config->setTqi(0);
            $tournament_number = $this->config->getTournamentNumber();
            $this->config->setTournamentNumber($tournament_number + 1);
            $this->_announceResults();
            return true;
        }
    }

    public function _getTournamentQuestion() {
        $g_question_manager = new GQuestionManager();
        $index = $this->config->getTqi();
        $questions =  $g_question_manager->getAllQuestionsIds('image');
        $id = $questions[$index];
        $question = $g_question_manager->findQuestionById($id);
        $this->config->setTqi($index + 1);
        return $question;
    }
    
    protected function _announceResults() {
        $chat = new GChat();
        $chat->sysMessage('Турнир завершён! Результаты:');
        $statistics = new GStatistics();
        $tournament_results = $statistics->getStatisticsLastTournament(10000)['statistics_in_period_array'];
        foreach ($tournament_results as $tr) {
            $chat->sysMessage('      ' . $tr['user'] . ' ' . $tr['score']);
        }
    }
}
