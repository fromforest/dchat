<?php


class GGameRoom {
    private $_game_room;
    public function __construct() {
        $this->_game_room = new GameRoom();
    }
    
    public function getGameRoom() {
        return $this->_game_room;
    }
    
    public function getId() {
        return $this->_game_room->getId();
    }
    
    public function getPhase() {
        return $this->_game_room->getPhase();
    }
    
    public function getUserId() {
        return $this->_game_room->getUserId();
    }
    
    public function save() {
        $this->_game_room->save();
    }
    
    public function setGameRoom(GameRoom $game_room) {
        $this->_game_room = $game_room;
    }
    
    public function setPhase($number) {
        $this->_game_room->setPhase($number);
    }
    
    public function setUserId($user_id) {
        $this->_game_room->setUserId($user_id);
    }
    
    public function setRoomName($room_name) {
        $this->_game_room->setRoomName($room_name);
    }
    
    public function setType($type) {
        $this->_game_room->setType($type);
    }
}
