<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GUser {
    
    private $_user;
    
    function   __construct() {
        $this->_user = new User();
    }
    
    public function delete() {
        $this->_user->delete();
    }
    
    public function getBestFifty() {
        return $this->_user->getBestFifty();
    }
    
    public function getCreatedAt() {
        return $this->_user->getCreatedAt();
    }
    
    public function getGroup() {
        return $this->_user->getGroup();
    }
    
    public function getId() {
        return $this->_user->getId();
    }
    
    public function getLastGameRoomId() {
        return $this->_user->getLastGameRoomId();
    }
    
    public function getName() {
        return $this->_user->getName();
    }
    
    public function getOnline() {
        return $this->_user->getOnline();
    }
    
    public function getPassword() {
        return $this->_user->getPassword();
    }
    
    public function getUser() {
        return $this->_user;
    }
    
    public function getRightAnswersCount() {
        return $this->_user->getRightAnswers()->count();
    }
    
    public function getUpdatedAt() {
        return $this->_user->getUpdatedAt();
    }
    
    public function save() {
        $this->_user->save();
    }
    
    public function setUser(User $user) {
        $this->_user = $user;
    }
    
    public function setBestFifty($current_score_on_last_fifty) {
        $this->_user->setBestFifty($current_score_on_last_fifty);
    }
    
    public function setLastGameRoomId($game_room_id) {
        $this->_user->setLastGameRoomId($game_room_id);
    }
    
    public function setName($name) {
        $this->_user->setName($name);
    }
    
    public function setOnline($is_online) {
        $this->_user->setOnline($is_online);
    }
    
    public function setPassword($password) {
        $this->_user->setPassword($password);
    }
    
    public function setUpdatedAt(DateTime $datetime) {
        $this->_user->setUpdatedAt($datetime);
    }
    
} //class
