<?php


class GFile {
    public function __construct() {
        
    }
 
    
    //api
    public function sendFile($file, $topic, $tag1, $tag2, $text, $answers, $comment) {
        $g_config = new GConfig();
        $current_user = (new GChat())->getCurrentUser();
        if (!$current_user) {
            throw new Exception ("Please log in");
        }
        if($file["size"] > 1024*3*1024)
               {
                    echo("Too large");
                    throw new Exception ('Too large');
               }         
            // Проверяем загружен ли файл
            if(is_uploaded_file($file["tmp_name"]))
            {
              // Если файл загружен успешно, перемещаем его из временной директории в конечную
              $parts = explode('.', $file["name"]);
              $extension = $parts[ count($parts) - 1];
              $temp_uploaded_question_id = $g_config->getTempUploadedQuestionId();
              //$picture_url = 'pic' . $temp_uploaded_question_id . '.' . $extension; //часть пути для базы
              
              //код без заноса в БД, сохранение инфы в виде названия файла
              $picture_url = $topic . '!';
              if ($tag1) {
                $picture_url = $picture_url . $tag1 . '!';
              }
              if ($tag2) {
                $picture_url = $picture_url . $tag2 . '!';
              }
              $text = str_replace('?', 'vopros', $text);
              $picture_url = $picture_url . $text . '$' . $answers . '#' . $comment . '.' . $extension;
              $main_path = './questions/temp_uploaded/';
              $upload_path = $main_path . $picture_url;
              if (stripos(php_uname ($mode = "s"), "windows") !== False) {
                $upload_path = iconv('utf-8','windows-1251',$upload_path);
              }
              move_uploaded_file($file["tmp_name"], $upload_path);
              
              //парсинг answers
//              $answers_array = explode('&', $answers);
//              $answer = $answers_array[0];
//              $alt_answers = join(':', array_slice($answers_array, 1));
              
              //занос инфы в БД
//              $question = new GQuestion();
//              $package_id = (new GPackageManager())->getPackageIdByPackageName('temp_uploaded');
//              $question->setId($temp_uploaded_question_id);
//              $question->setPackageId($package_id);
//              $question->setTopic($topic);
//              if ($tag1) {
//                $question->setTag1($tag1);
//              }
//              if ($tag2) {
//                $question->setTag2($tag2);
//              }
//              $question->setText($text);
//              $question->setPictureUrl($picture_url);
//              $question->setRightAnswer($answer);
//              $question->setAltAnswers($alt_answers);
//              $question->setAuthor($current_user->getName());
//              $question->setComment($comment);
//              $question->save();
              
              $g_config->setTempUploadedQuestionId($temp_uploaded_question_id + 1);
              
            } else {
               throw new Exception ('ERROR: file has not uploaded');
            }

        return ['sent' => True, 'os' => php_uname ($mode = "s")];
    }
    
}
