<?php


class GGameManager {
    function __construct() {
        
    }
    
    
    public function isRoomNameExist ($room_name) {
        $game_room = GameRoomQuery::create()->findOneByRoomName($room_name);
        return $game_room ? True : False;
    }
    
    public function findGameRoomById($id) {
        $game_room = GameRoomQuery::create()->findOneById($id);
        if ($game_room) {
            $g_game_room = new GGameRoom();
            $g_game_room->setGameRoom($game_room);
            return $g_game_room;
        }
        else {
            return null;
        }
    }
    
    public function findGameRoomByName($room_name) {
        $game_room = GameRoomQuery::create()->findOneByRoomName($room_name);
        if ($game_room) {
            $g_game_room = new GGameRoom();
            $g_game_room->setGameRoom($game_room);
            return $g_game_room;
        }
        else {
            return null;
        }
    }
    
} //class
