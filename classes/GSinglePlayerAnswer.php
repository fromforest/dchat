<?php

class GSinglePlayerAnswer {
   private $_spa;
   
   function __construct () {
       $this->_spa = new SinglePlayerAnswer();
   }
     
   public function getNumAsked() {
	   return $this->_spa->getNumAsked();
   }
   
   public function getNumAnswered() {
	   return $this->_spa->getNumAnswered();
   }
   
   public function getQuestion() {
       $question = $this->_spa->getQuestion();
       $g_question = new GQuestion();
       $g_question->setQuestion($question);
       return $g_question;
   }
   
   public function getQuestionId() {
       return $this->_spa->getQuestionId();
   }
   
    public function getSinglePlayerAnswer() {
        return $this->_spa;
    }
   
   public function save() {
       $this->_spa->save();
   }
   
    public function setIsRight($is_right) {
        $this->_spa->setIsRight($is_right);
    }
	
	public function setNumAsked($num_asked) {
		$this->_spa->setNumAsked($num_asked);
	}
	
	public function setNumAnswered($num_answered) {
		$this->_spa->setNumAnswered($num_answered);
	}
    
    public function setSinglePlayerAnswer(SinglePlayerAnswer $spa) {
        $this->_spa = $spa;
    }
    
    public function setQuestionId($question_id) {
        $this->_spa->setQuestionId($question_id);
    }
    
    public function setUserId($user_id) {
        $this->_spa->setUserId($user_id);
    }
   
} //class
