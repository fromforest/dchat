<?php


class GPackageManager {
    
    public function __construct() {
        
    }
    
    public function getPackageIdByPackageName($package_name) {
        $package = PackageQuery::create()->findOneByName($package_name);
		if ($package) {
			return $package->getId();
		}
		else {
			return null;
		}
    }
    
}
