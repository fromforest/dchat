<?php

class GGame {
    function __construct() {
        
    }
    
     //api
    public function createGame($type, $room_name) {
        $user = (new GChat)->getCurrentUser();
        if (!$user) {
            throw new Exception("Problems with current user");
        }
        
        $checked = (new GGameManager())->isRoomNameExist($room_name);
        if ($checked) {
            return ["room_created" => false];
        }
     
        $g_room = new GGameRoom();
        $g_room->setUserId($user->getId());
        $g_room->setRoomName($room_name);
        $g_room->setType($type);
        $g_room->save();
        
        $user->setLastGameRoomId($g_room->getId());
        $user->save();
        
        $game_room_id = $g_room->getId();
        
        
        return ["room_created" => true,
                   "game_room_id"=>$game_room_id
                    ];
        
    } //createGame
    
    
    //ZAVALINKA
    
    //api
    
    public function continueGame($game_room_id) {
         $g_room = (new GGameManager())->findGameRoomById($game_room_id);
         $phase = $g_room->getPhase();
         $g_room->setPhase($phase + 1);
         $g_room->save();
         return ["continued" => True];
    }
    
    //api
    public function getChoices($game_room_id, $current_word) {
        $choiced_user_names = (new GZavalinkaManager())->getChoicedUserNamesByGameRoomIdAndWord($game_room_id, $current_word);
        //не путать с тем, что в getChoiceResults. Тут просто проголосовавшие, а там за кого голосовали
        $choices = [];
        foreach ($choiced_user_names as $name) {
            $choices []= ["name" => $name, "choiced" => True];
        }
        return ["choices" => $choices];
    }
    
    //api
    public function getChoiceResults($game_room_id, $current_word) {
        $choice_results = [];
        $zav_words = (new GZavalinkaManager())->findZavalinkaWordsByGameRoomIdAndWord($game_room_id, $current_word);
        foreach ($zav_words as $zw) {
            $choice_result = [];
            $author_id = $zw->getUserId();
            $author_name = (new GUserManager())->getUserNameById($author_id);
            $choiced_user_names_string = (new GZavalinkaManager())
                    ->getChoicedUserNamesString($game_room_id, $current_word, $author_id);
            $points = (new GZavalinkaManager())->getPoints($game_room_id, $author_id);
                    
            $choice_result["author"] = ($author_name === "System") ? 'Верный ответ' : $author_name;;
            $choice_result["definition"] = $zw->getDefinition();
            $choice_result["choiced_users"] = $choiced_user_names_string;
            $choice_result["points"] =  ($author_name === "System") ? '-' : $points;
            
            $choice_results []= $choice_result;
        }
        return ["choice_results" => $choice_results];
    }
    
    //api
    public function getGameUsers($game_room_id) {
        $user = (new GChat())->getCurrentUser();
         if (!$user) {
            throw new Exception('Some problems with user');
        }
        $g_room = (new GGameManager())->findGameRoomById($game_room_id);
        $game_users = (new GUserManager())->findUsersByLastGameRoomId($game_room_id);
        
        $is_creator = ($user->getId() === $g_room->getUserId());
        return ["game_users" => $game_users, "is_creator" => $is_creator];
    }
    
    //api
    public function getDefinitionsForCreatorEditing($game_room_id) {
        $definitions_for_creator_editing = (new GZavalinkaManager())->getDefinitionsForCreatorEditing($game_room_id);
        return ["definitions_for_creator_editing" => $definitions_for_creator_editing];
    }
    
    //api
    public function getPhase($game_room_id) {
        $g_room = (new GGameManager())->findGameRoomById($game_room_id);
        $phase = $g_room->getPhase();
        if ($phase > 0) {
            $g_zav_manager = (new GZavalinkaManager());
            $index = (integer) ($phase - 1) / 2;
            $words = $g_zav_manager->findWordsByGameRoomId($game_room_id);
            $current_word = $words[$index];
        }
        else {
            $current_word = "";
        }
        
        $last_phase = 2* ((new GZavalinkaManager())->findWordsCountByGameRoomId($game_room_id));
        return ["phase" => $phase, "current_word" => $current_word, "last_phase" => $last_phase];
    }
    
    //api
    public function getShuffledDefinitions($game_room_id) {
        $g_zav_manager = (new GZavalinkaManager());
        $g_room = (new GGameManager())->findGameRoomById($game_room_id);
        $phase = $g_room->getPhase();
        $index = (integer) ($phase - 1) / 2;
        $words = $g_zav_manager->findWordsByGameRoomId($game_room_id);
        $current_word = $words[$index];
        $definitions = $g_zav_manager->findDefinitionsByGameRoomIdAndWord($game_room_id, $current_word);
        //shuffle($definitions);
        rsort($definitions);
        return ["shuffled_definitions"=>$definitions];
    }
    
    //api
    public function getWords($game_room_id) {
        $words = (new GZavalinkaManager())->findWordsByGameRoomId($game_room_id);
        return ["words" => $words];
    }
    
    //api
    public function joinGame($room_name) {
        $user = (new GChat)->getCurrentUser();
        if (!$user) {
            throw new Exception("Problems with current user");
        }
        $g_game_room = (new GGameManager())->findGameRoomByName($room_name);
        if (!$g_game_room) {
            return ["joined"=>False];
        }
        else {
            $game_room_id = $g_game_room->getId();
            $user->setLastGameRoomId($game_room_id);
            $user->save();
            return ["joined"=>True, "game_room_id" => $game_room_id];
        }
    }
    
    //api
    public function sendWordsAndTrueDefinitions($game_room_id, $words, $true_definitions) {
        $sys = (new GChat)->getSystemUser(); //правильные определения будут под авторством юзера System
        if (count($words) !== count($true_definitions)) {
            throw new InvalidArgumentException('count of words and true definitions must be equal');
        }
        
        for ($i = 0; $i<count($words); $i++) {
            $zav_word = new GZavalinkaWord();
            $zav_word->setUserId($sys->getId());
            $zav_word->setWord($words[$i]);
            $zav_word->setDefinition($true_definitions[$i]);
            $zav_word->setGameRoomId($game_room_id);
            $zav_word->save();
        }
        
        return ['status' => 'ok'];
    }
    
    //api
    public function sendWordsAndUserDefinitions($game_room_id, $words, $user_definitions) {
        $user = (new GChat)->getCurrentUser();
        if (!$user) {
            throw new Exception('Some problems with user');
        }
        
         for ($i = 0; $i<count($words); $i++) {
            $checked = (new GZavalinkaManager())->findZavalinkaWordByGameRoomIdAndWordAndUserName($game_room_id, $words[$i], $user->getName());
            if (!$checked) {//already?
                $zav_word = new GZavalinkaWord();
                $zav_word->setUserId($user->getId());
                $zav_word->setWord($words[$i]);
                $zav_word->setDefinition($user_definitions[$i]);
                $zav_word->setGameRoomId($game_room_id);
                $zav_word->save();
            }
        }
        
        return ['status' => 'ok'];
        
    }
    
    //api
    public function startGame($game_room_id, $edited_users_definitions) {
         $g_room = (new GGameManager())->findGameRoomById($game_room_id);
         $g_room->setPhase(1);
         $g_room->save();
         writeln('log.txt', count($edited_users_definitions));
         //вставка отредактированных определений
         foreach($edited_users_definitions as $eud) {
             $word = $eud["word"];
             writeln('log.txt', $word);
             $user_name = $eud["user"];
             writeln('log.txt', $user_name);
             $g_zav_word = (new GZavalinkaManager())->
                     findZavalinkaWordByGameRoomIdAndWordAndUserName($game_room_id, $word, $user_name);
             $g_zav_word->setDefinition($eud["definition"]);
             $g_zav_word->save();
         }
        
        return ['started' => True];
    }
    
    public function submitChoice($game_room_id, $current_word, $choiced_definition) {
         $user = (new GChat)->getCurrentUser();
        if (!$user) {
            throw new Exception('Some problems with user');
        }
        $zavalinka_word = (new GZavalinkaManager)->
                findZavalinkaWordByGameRoomIdAndWordAndDefinition($game_room_id, $current_word, $choiced_definition);
        
        if ($user->getId() === $zavalinka_word->getUserId()) {
            return ["submitted" => False];
        }
        
        $zavalinka_choice = new GZavalinkaChoice();
        $zavalinka_choice->setGameRoomId($game_room_id);
        $zavalinka_choice->setUserId($user->getId());
        $zavalinka_choice->setWord($current_word);
        $zavalinka_choice->setForUserId($zavalinka_word->getUserId());
        $zavalinka_choice->save();
        
        return ["submitted" => True];
    }//method
    
} //class
