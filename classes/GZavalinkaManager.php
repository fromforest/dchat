<?php

class GZavalinkaManager {
    public function __construct() {
        
    }
    
    public function getDefinitionsForCreatorEditing($game_room_id) {
        $definitions_for_creator_editing = [];
        $zavalinka_words = ZavalinkaWordQuery::create()->filterByGameRoomId($game_room_id)->find();
        $zavalinka_structure = [];
        
        foreach($zavalinka_words as $zw) {
            if ($zw->getUserId() !== 1) { // без настоящих определений (то есть авторство = System)
                $u = UserQuery::create()->findOneById($zw->getUserId());
                $zavalinka_structure["user"] = $u->getName();
                $zavalinka_structure["word"] = $zw->getWord();
                $zavalinka_structure["definition"] = $zw->getDefinition();
                $definitions_for_creator_editing []= $zavalinka_structure;
            }
        }
        return $definitions_for_creator_editing;
    }//method
    
    public function findDefinitionsByGameRoomIdAndWord($game_room_id, $current_word) {
        $definitions = [];
        $definitions_objects = ZavalinkaWordQuery::create()->select('definition')->filterByGameRoomId($game_room_id)->filterByWord($current_word)->find();
        foreach($definitions_objects as $definition) {
            $definitions []= $definition . "";
        }
        return $definitions;
    }
    
    public function findWordsByGameRoomId($game_room_id) {
        $words = [];
        $word_objects = ZavalinkaWordQuery::create()->select('word')->filterByGameRoomId($game_room_id)->filterByUserId(1)->find();
        foreach($word_objects as $word) {
            $words []= $word . "";
        }
        return $words;
    }//method
    
    public function findWordsCountByGameRoomId($game_room_id) {
        $count = ZavalinkaWordQuery::create()->filterByGameRoomId($game_room_id)->filterByUserId(1)->count();
        return $count;
    }
    
    public function getChoicedUserNamesByGameRoomIdAndWord($game_room_id, $word) {
        $g_user_manager = new GUserManager();
        $user_ids = ZavalinkaChoiceQuery::create()->select('user_id')->filterByGameRoomId($game_room_id)
                ->filterByWord($word)->find();
        $user_names = [];
        foreach ($user_ids as $user_id) {
            $name = $g_user_manager->getUserNameById((integer)$user_id);
            $user_names []= $name;
        }
        return $user_names;
    }
    
    public function getChoicedUserNamesString($game_room_id, $word, $for_user_id) {
        $user_names_string = "";
        $choice_objects = ZavalinkaChoiceQuery::create()->filterByGameRoomId($game_room_id)
                ->filterByWord($word)->filterByForUserId($for_user_id)->find();
        foreach($choice_objects as $choice) {
            $user_id = $choice->getUserId();
            $name = (new GUserManager())->getUserNameById($user_id);
            $user_names_string = $user_names_string . $name . " ";
        }
        return $user_names_string;
    }
    
    public function getPoints($game_room_id, $user_id) {
        $where_choiced = ZavalinkaChoiceQuery::create()->filterByGameRoomId($game_room_id)
                ->filterByForUserId($user_id)->count();
        $where_right = ZavalinkaChoiceQuery::create()->filterByGameRoomId($game_room_id)
                ->filterByUserId($user_id)->filterByForUserId(1)->count();
        
        return $where_choiced + $where_right;
    }
    
    public function findZavalinkaWordsByGameRoomIdAndWord($game_room_id, $word) {
        $zavalinka_words = [];
        $zav_words = ZavalinkaWordQuery::create()->filterByGameRoomId($game_room_id)->filterByWord($word)->find();
        foreach ($zav_words as $zw) {
            $zavalinka_word = new GZavalinkaWord();
            $zavalinka_word->setZavalinkaWord($zw);
            $zavalinka_words []= $zavalinka_word;
        }
        return $zavalinka_words;
    }
    
    public function findZavalinkaWordByGameRoomIdAndWordAndUserName($game_room_id, $word, $user_name) {
        $user = (new GUserManager)->findUserByName($user_name);
        $user_id = $user->getId();
        $zavalinka_word = ZavalinkaWordQuery::create()->filterByGameRoomId($game_room_id)->filterByUserId($user_id)
                                        ->findOneByWord($word);
        if ($zavalinka_word) {
            $g_zavalinka_word = new GZavalinkaWord();
            $g_zavalinka_word->setZavalinkaWord($zavalinka_word);
            return $g_zavalinka_word;
        }
        else {
            return null;
        }
    }
    
    public function findZavalinkaWordByGameRoomIdAndWordAndDefinition($game_room_id, $word, $definition) {
        $zavalinka_word = ZavalinkaWordQuery::create()->filterByGameRoomId($game_room_id)
                ->filterByDefinition($definition)->findOneByWord($word);
        if ($zavalinka_word) {
            $g_zavalinka_word = new GZavalinkaWord();
            $g_zavalinka_word->setZavalinkaWord($zavalinka_word);
            return $g_zavalinka_word;
        }
        else {
            return null;
        }
    }//method

}//class