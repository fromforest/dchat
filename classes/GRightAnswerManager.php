<?php

use \Propel\Runtime\Propel;

class GRightAnswerManager {
    
    public function __construct() {
        
    }
    
    
    public function findRightAnswerByUserAndChallenge(GUser $u, GChallenge $ch) {
        $right_answer = RightAnswerQuery::create()->filterByUser($u->getUser())->filterByChallenge($ch->getChallenge())->findOne();
        if ($right_answer) {
                $g_right_answer = new GRightAnswer();
                $g_right_answer->setRightAnswer($right_answer);
                return $g_right_answer;
        }
        else {
                return null;
        }
    }
    
    public function getCurrentTournamentScore(GUser $user, $current_tournament) {
        return RightAnswerQuery::create()->filterByUser($user->getUser())->filterByType($current_tournament)->count();
    }
    
    public function getHourScores($user_name, $number) {
        $con = Propel::getConnection();
        $query = "select  "
                . "count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where users.name = :p2 "
                . "and right_answers.type = 0 "
                . "group by DATE_FORMAT(right_answers.created_at,'%Y-%m-%d %H') "
                . "order by count(right_answers.id) desc "
                . "limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':p2', $user_name, PDO::PARAM_STR);
        $res = $stmt->execute();
        $hour_scores = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $hour_scores;
    }
    
    public function getStatisticsBestDays($min_date_string, $max_date_string, $number) {
        $con = Propel::getConnection();
        $query = "select day as period, max(score) as score, user from "
                . "(select "
                . "DATE_FORMAT(right_answers.created_at, '%Y-%m-%d') as day, users.name as user, count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where (DATE_FORMAT (right_answers.created_at,'%Y-%m-%d') >= :min "
                . "and DATE_FORMAT (right_answers.created_at,'%Y-%m-%d')   <= :max) "
                . "and right_answers.type = 0 "
                . "group by users.name, DATE_FORMAT(right_answers.created_at,'%Y-%m-%d') "
                . "order by count(right_answers.id) desc " //если не будет этого desc, то не всегда берутся соответствующие даты
                . ") as select_result1 "
                . "group by user "
                . "order by max(score) "
                . "desc limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':min', $min_date_string, PDO::PARAM_STR);
        $stmt->bindValue(':max', $max_date_string, PDO::PARAM_STR);
        $res = $stmt->execute();
        $statistics = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $statistics;
    }
    
    public function getStatisticsBestHours($min_date_string, $max_date_string, $number) {
        $con = Propel::getConnection();
        $query = "select hour as period, max(score) as score, user from "
                . "(select "
                . "DATE_FORMAT(right_answers.created_at, '%Y-%m-%d %H') as hour, users.name as user, count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where (DATE_FORMAT (right_answers.created_at,'%Y-%m-%d %H') >= :min "
                . "and DATE_FORMAT (right_answers.created_at,'%Y-%m-%d %H')   <= :max) "
                . "and right_answers.type = 0 "
                . "group by users.name, DATE_FORMAT(right_answers.created_at,'%Y-%m-%d %H') "
                . "order by count(right_answers.id) desc " //если не будет этого desc, то не всегда берутся соответствующие даты
                . ") as select_result1 "
                . "group by user "
                . "order by max(score) "
                . "desc limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':min', $min_date_string, PDO::PARAM_STR);
        $stmt->bindValue(':max', $max_date_string, PDO::PARAM_STR);
        $res = $stmt->execute();
        $statistics = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $statistics;
    }
    
    public function getStatisticsBestMonthsTotal($number) {
        $min_date_string = '2015-08';
        $max_date_string = date("Y-m", time());
        $con = Propel::getConnection();
        $query = "select month as period, max(score) as score, user from "
                . "(select "
                . "DATE_FORMAT(right_answers.created_at, '%Y-%m') as month, users.name as user, count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where DATE_FORMAT (right_answers.created_at,'%Y-%m') >= :min "
                . "and DATE_FORMAT (right_answers.created_at,'%Y-%m')   <= :max "
                . "and right_answers.type = 0 "
                . "group by users.name, DATE_FORMAT(right_answers.created_at,'%Y-%m') "
                . "order by count(right_answers.id) desc " //если не будет этого desc, то не всегда берутся соответствующие даты
                . ") as select_result1 "
                . "group by user "
                . "order by max(score) "
                . "desc limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':min', $min_date_string, PDO::PARAM_STR);
        $stmt->bindValue(':max', $max_date_string, PDO::PARAM_STR);
        $res = $stmt->execute();
        $statistics = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $statistics;
    }
    
    public function getStatisticsBestYears($number) {
        $min_date_string = '2015';
        $max_date_string = date("Y", time());
        $con = Propel::getConnection();
        $query = "select year as period, max(score) as score, user from "
                . "(select "
                . "DATE_FORMAT(right_answers.created_at, '%Y') as year, users.name as user, count(right_answers.id) as score "
                . "from right_answers join users on right_answers.user_id = users.id "
                . "where DATE_FORMAT (right_answers.created_at,'%Y') >= :min "
                . "and DATE_FORMAT (right_answers.created_at,'%Y')   <= :max "
                . "and right_answers.type = 0 "
                . "group by users.name, DATE_FORMAT(right_answers.created_at,'%Y') "
                . "order by count(right_answers.id) desc " //если не будет этого desc, то не всегда берутся соответствующие даты
                . ") as select_result1 "
                . "group by user "
                . "order by max(score) "
                . "desc limit :p1";

        $stmt = $con->prepare($query);
        $stmt->bindValue(':p1', $number, PDO::PARAM_INT);
        $stmt->bindValue(':min', $min_date_string, PDO::PARAM_STR);
        $stmt->bindValue(':max', $max_date_string, PDO::PARAM_STR);
        $res = $stmt->execute();
        $statistics = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $statistics;
    }
    
    public function getStatisticsChains($min_time, $max_time, $number) {
        $statistics_objects = RightAnswerQuery::create()
                ->filterByCreatedAt([
                    'min' => $min_time,
                    'max' => $max_time])
                ->filterByType(0)
                ->filterByNum(['min' => 2])
                ->join('User')
                ->withColumn('max(RightAnswer.Num)', 'score')
                ->withColumn('User.Name', 'user')
                ->groupBy('RightAnswer.UserId')
                ->select(['user', 'score'])
                ->orderBy('score', 'desc')
                ->limit($number)
                ->find();

        $statistics = [];
        foreach ($statistics_objects as $stat) {
            $as_array['user'] = $stat['user'];
            $as_array['score'] = $stat['score'];
            $statistics [] = $as_array;
        }
        
        return $statistics;
    }
    
    public function getStatisticsInPeriod($min_time, $max_time, $number) {
        $statistics_objects = RightAnswerQuery::create()
                ->filterByCreatedAt([
                    'min' => $min_time,
                    'max' => $max_time])
                ->filterByType(0)
                ->join('User')
                ->withColumn('count(User.Id)', 'score')
                ->groupBy('RightAnswer.UserId')
                ->select(['User.Name', 'score'])
                ->orderBy('score', 'desc')
                ->limit($number)
                ->find();

        $statistics = [];
        foreach ($statistics_objects as $stat) {
            $as_array['user'] = $stat['User.Name'];
            $as_array['score'] = $stat['score'];
            $statistics [] = $as_array;
        }
        
        return $statistics;
    }
    
    public function getStatisticsInTournament($tournament_number, $number) {
        $statistics_objects = RightAnswerQuery::create()
                ->filterByType($tournament_number)
                ->join('User')
                ->withColumn('count(User.Id)', 'score')
                ->groupBy('RightAnswer.UserId')
                ->select(['User.Name', 'score'])
                ->orderBy('score', 'desc')
                ->limit($number)
                ->find();

        $statistics = [];
        foreach ($statistics_objects as $stat) {
            $as_array['user'] = $stat['User.Name'];
            $as_array['score'] = $stat['score'];
            $statistics [] = $as_array;
        }
        return $statistics;
    }
    
    public function getStringOfRightUsers($challenge_id) {
        $right_answers = RightAnswerQuery::create()->filterByChallengeId($challenge_id)->find();
        $right_users = '';
        foreach ($right_answers as $right_answer) {
            $right_users = $right_users ?
                    $right_users . ', ' . $right_answer->getUser()->getName() : $right_answer->getUser()->getName();
        }
        
        return $right_users;
    }
    
    public function setUpdatesForChains($current_challenge_id, GRightAnswer $r) {
        $previous_challenge_id = $current_challenge_id - 1;
        $previous_right_answer = RightAnswerQuery::create()->filterByUser($r->getUser()->getUser())->filterByChallengeId($previous_challenge_id)->filterByType(0)->findOne();
        if ($previous_right_answer) {
                $num_of_current_challenge = $previous_right_answer->getNum() + 1; //поле num в таблице
                $r->setNum($num_of_current_challenge);
                $r->save(); //подумать стоит ли убрать эту строку
        }
    }
}
