<?php

class GConfigManager {
    
    public function __construct() {
        
    }
    
    public function changeConfigTournament($tournament_number, $q_time, $a_time, $packages) {
        $config_tournament = $this->_getConfigTournamentByNumber($tournament_number);
        $config_tournament->setQuestionTime($q_time);
        $config_tournament->setAnswerTime($a_time);
        $config_tournament->setPackages($packages);
        $config_tournament->save();
    }
    
    public function createNewConfigTournament($tournament_number, $q_time, $a_time, $packages) {
        $config_tournament = new Config();
        $config_tournament->setId($tournament_number);
        $config_tournament->setQuestionTime($q_time);
        $config_tournament->setAnswerTime($a_time);
        $config_tournament->setPackages($packages);
        
        $config_tournament->setAnnouncedRightUsers(0);
        $config_tournament->setChallengeUpdated(0);
        $config_tournament->setBlackList("");
        $config_tournament->setDifficultySequence("");
        $config_tournament->setMinUsersForUpdates(0);
        $config_tournament->setTournamentNumber("");
        $config_tournament->setMinTimeBetweenEqualQuestions(0);
        $config_tournament->save();
        
        $ct = $this->_getConfigTournamentByNumber($tournament_number);
        if (!$ct) { // если не создался сволочь
             throw new Exception('Tournament config have not created');
        }
    }
//получить сложность для вопроса. Находим остаток от деления challenge_id на длину кортежа сложности в конфиге.
// Например, если challenge_id = 2016, а кортеж сложности easy:medium:medium:easy:hard, то 2016 mod 5 = 1, то есть medium.
    public function getCurrentDifficulty(GChallenge $current_challenge) {
        $config = new GConfig();
        $current_challenge_id = $current_challenge->getId();
        $difficulty_sequence = $config->getDifficultySequence();
        $difficulty_elements = explode(':', $difficulty_sequence);
        $index = $current_challenge_id % count($difficulty_elements);
        $current_difficulty = $difficulty_elements[$index];
        return $current_difficulty;
    }
    
    public function getTournamentParamsByTournamentNumber($tournament_number) {
        $ct = ConfigQuery::create()->findOneById($tournament_number);
        $q_time = $ct->getQuestionTime();
        $a_time = $ct->getAnswerTime();
        $packages = $ct->getPackages();
        $tournament_params = [
                "q_time" => $q_time,
                "a_time" => $a_time,
                "packages" => $packages
            ];
        return $tournament_params;
    }
    
    protected function _getConfigTournamentByNumber($tournament_number) {
        $ct = ConfigQuery::create()->findOneById($tournament_number);
        return $ct;
    }
    
    public function isTournamentExist($tournament_number) {
        $config_tournament = $this->_getConfigTournamentByNumber($tournament_number);
        return ($config_tournament ? True : False);
    }
    
    public function sendInfoTournament($tournament_number) {
        $chat = (new GChat());
        $config_tournament = $this->_getConfigTournamentByNumber($tournament_number);
        if (!$config_tournament) {
            $chat->privateSysMessage('Турнира с номером ' . $tournament_number .  ' не существует');
        }
        else {
        $chat->privateSysMessage('tournament_number = ' . $config_tournament->getId());
        $chat->privateSysMessage('q_time = ' . $config_tournament->getQuestionTime());
        $chat->privateSysMessage('a_time = ' . $config_tournament->getAnswerTime());
        $chat->privateSysMessage('packages = ' . $config_tournament->getPackages());
        }
        
    } // method
    
} //class
