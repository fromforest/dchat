<?php

use \Propel\Runtime\ActiveQuery\Criteria;

class GSinglePlayerManager {
    
function __construct() {
        
}    
    public function getLastSinglePlayerAnswer(GUser $user) {
        $spa = SinglePlayerAnswerQuery::create()->filterByUserId($user->getId())->orderByUpdatedAt(Criteria::DESC)->findOne();
        if ($spa) {
           $g_spa = new GSinglePlayerAnswer();
           $g_spa->setSinglePlayerAnswer($spa);
           return $g_spa;
        }
        else {
            return null;
        }
    }
    
    public function getSinglePlayerAskedAnsweredArray(GQuestion $q, GUser $user, $type) {
        if ($type === "image") {
            $q_con = SinglePlayerAnswerQuery::create()->filterByUserId($user->getId())->filterByQuestionId($q->getId());
        }
        else {
            $q_con = TextSinglePlayerAnswerQuery::create()->filterByUserId($user->getId())->filterByQuestionId($q->getId());
        }
		$q_asked = 0;
		$q_answered = 0;
        if ($q_con->count() > 0) {
			$q_asked = $q_con->findOne()->getNumAsked();
			$q_answered = $q_con->findOne()->getNumAnswered();
		}
		
        $question_struct = ['question' => $q, 'num_asked' => $q_asked, 'num_answered' => $q_answered];
        return $question_struct;
    }
	
	public function getSinglePlayerAnswerByUserAndQuestion(GUser $user, GQuestion $q, $type) {
		if ($type === "image") {
			$spa = SinglePlayerAnswerQuery::create()->filterByUserId($user->getId())->filterByQuestionId($q->getId())->findOne();
			if ($spa) {
				$g_spa = new GSinglePlayerAnswer();
				$g_spa->setSinglePlayerAnswer($spa);
				return $g_spa;
			}
			else {
				return null;
			}
		}//end for "image"
		else {
			$tspa = TextSinglePlayerAnswerQuery::create()->filterByUserId($user->getId())->filterByQuestionId($q->getId())->findOne();
			if ($tspa) {
			   $g_tspa = new GTextSinglePlayerAnswer();
			   $g_tspa->setTextSinglePlayerAnswer($tspa);
			   return $g_tspa;
			}
			else {
				return null;
			}
		}//end for "text"
		
	}//method
    
    
    //text Singleplayer
    
    public function getLastTextSinglePlayerAnswer(GUser $user) {
        $tspa = TextSinglePlayerAnswerQuery::create()->filterByUserId($user->getId())->orderByUpdatedAt(Criteria::DESC)->findOne();
        if ($tspa) {
           $g_tspa = new GTextSinglePlayerAnswer();
           $g_tspa->setTextSinglePlayerAnswer($tspa);
           return $g_tspa;
        }
        else {
            return null;
        }
    }
}
