<?php

class GChat {
    
    public function __construct() {
        
    }

    protected function _signUp($name, $password) {
        if (!$name || !$password) {
            throw new InvalidArgumentException('Please enter name and password.');
        }
        
        $user = new GUser();
        $user->setName($name);
        $user->setPassword(md5($password));
        $user->save();
        return $user;
    }

    protected function _authenticate($name = "", $password = "") {
        $user = (new GUserManager())->findUserByNameAndPassword($name, $password);
        return $user ? $user : false;
    }

    //api
    public function login($name, $password) {
        if (!$name || !$password) {
            throw new Exception('Please enter name and password.');
        }

// 		if(!filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL)){
// 			throw new Exception('Your email is invalid.');
// 		}
        $g_user_manager = new GUserManager();
        $existing_user = $g_user_manager->findUserByName($name);

        //$user = self::_authenticate($name, $password); //вроде не нужно.
        if ($existing_user) {
            $user = $this->_authenticate($name, $password);
            if (!$user) {
                throw new Exception('Wrong name/password.');
            }
        } else { // new user
            $user = $this->_signUp($name, $password);
        }

        $user->setOnline(true);

        $_SESSION['user'] = [
            'name' => $name,
            'id' => $user->getId()
        ];

        return ['name' => $user->getName(), 'status' => 'login ok'];
    }

    //api
    public function checkLogged() {
        $response = ['logged' => false];

        $user = $this->getCurrentUser();

        if ($user) {
            $response['logged'] = true;
            $response['loggedAs'] = [
                'name' => $user->getName(),
                'id' => $user->getId()
            ];
        }
        return $response;
    }

    //api
    public function logout() {
        $uname = $_SESSION['user']['name'];

        $u = (new GUserManager())->findUserByName($uname);
        if ($u) {
            $u->setOnline(false);
            $u->save();
        }

        $_SESSION = [];
        //session_destroy();
        unset($_SESSION);

        return ['status' => 1,
            'action' => 'log out'
        ];
    }

    public function getSystemUser() {
        $sys = (new GUserManager)->findUserByName('System');
        if (!$sys) {
            throw new Exception('The system user not found.');
        }
        return $sys;
    }

    //todo
    public function sysMessage($msg) {
        $sys = $this->getSystemUser();
        $message = new GMessage();
        $message->setUser($sys);
        $message->setText($msg);
        $message->save();
    }

    public function privateSysMessage($msg) {
        
        /*$msg_stump = '';
        if (strlen($msg) > 120) {
            $msg_stump = substr($msg, 120);
            $msg = substr($msg, 0, 120);
        }*/

        $sys = $this->getSystemUser();
        $toUser = $this->getCurrentUser();
        $message =new GMessage();
        $message->setUser($sys);
        $message->setText($msg);
        $message->setToId($toUser->getId());
        $message->save();

        /*if (strlen($msg_stump) > 0) {
            $this->privateSysMessage($msg_stump);
        }*/
    }

    //api
    public function submitChat($chatText) {
        $user = $this->getCurrentUser();
        if (!$user) {
            throw new Exception("Cannot submit chat. You are not logged in");
        }
        $username = $user->getName();
        $sys = $this->getSystemUser();

        if (!$chatText) {
            throw new Exception('You haven\' entered a chat message.');
        }

        $console = new GConsole($this, new GConfig());
        $is_command = $console->checkCommand($chatText);
        
        if ($is_command) {
            return [
                'status' => 1, // ok
                'insertID' => '',
                'time' => ''
            ];
        }

        $msg = new GMessage();
        $msg->setUser($user); //оставляем здесь setUser, потому что иначе GQuiz::checkAnswer не ест msg

        $s = explode('$', $chatText);
        if ($s[0] == '@') { //private
            $toUser = (new GUserManager())->findUserByName($s[1]);
            $msg->setToId($toUser->getId());
            $msg->setText($s[2]);
        } else {
            $msg->setText($chatText);
        }

        // ------------ check right answer  -------------------
        $quiz = new GQuiz($this, new GConfig());
        if (!($quiz->checkAnswer($msg))) {
            if (!($quiz->checkNearAnswer($msg))) {
                $msg->save();
                //если ответ близкий, все дела объекта $msg ведутся внутри Quiz::checkNearAnswer
            }
            //если ответ верный, все преобразования и сохранение объекта $msg ведутся внутри Quiz::checkAnswer 
        }


        return [
            'status' => 1, // ok
            'insertID' => $msg->getId(),
            'time' => $msg->getCreatedAtFormat('H:i:s'),
            'text' => $msg->getText()
        ];
    }

    public function getCurrentUser() {
        if (isset($_SESSION['user']))
            $username = $_SESSION['user']['name'];
        else
            return false;
        
        $user = (new GUserManager())->findUserByName($username);
        if (!$user) {
            $_SESSION = [];
            //session_destroy();
            unset($_SESSION);
            return false;
        }
        return $user;
    }
    
    //api
    public function getUsers() {
        //global $database;
        $user = $this->getCurrentUser();
        if (!$user) {
            throw new Exception("not logged in");
        }

        $user->setUpdatedAt(new DateTime("now"));
        $user->setOnline(true);
        $user->save();

        // Deleting chats older than 5 minutes and users inactive for 30 seconds
//                $database->query("DELETE FROM webchat_lines WHERE ts < SUBTIME(NOW(),'0:5:0')");
//                $database->query("DELETE FROM webchat_users WHERE last_activity < SUBTIME(NOW(),'0:0:30')");

        (new GMessageManager())->deleteOldMessages();
        $g_user_manager = new GUserManager();
        $g_user_manager->setInactiveUsersOffline();
        
        $users_array = $g_user_manager->getNamesAndIdsOfUsersOnline();

        return [
            'users' => $users_array
                //'total' => DB::query('SELECT COUNT(*) as cnt FROM webchat_users')->fetch_object()->cnt
                //'total' => count($users_array)
        ];
    }

    //api
    public function getChats($lastID) {
        $user = $this->getCurrentUser();
        if (!$user) {
            return [ 'chats' => []];
        }

        $lastID = (int) $lastID;

        //$result = DB::query('SELECT * FROM webchat_lines WHERE id > '.$lastID.' ORDER BY id ASC');
        //$cs = ChatLine::find_by_sql('SELECT * FROM webchat_lines WHERE id > '.$lastID.' ORDER BY id ASC');

        $chats = (new GMessageManager())->getAttributesOfRecentMessages($lastID, $user);

        return ['chats' => $chats];
    }

    //api
    public function getQuestion() {
        $g_quiz = new GQuiz($this, new GConfig);
        return $g_quiz->getQuestion();
    }


} //class
