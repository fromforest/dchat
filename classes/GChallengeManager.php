<?php
use \Propel\Runtime\ActiveQuery\Criteria;

class GChallengeManager {
    
    public function __construct() {
        
    }
    
    public function findChallengeById($challenge_id) {
        $challenge =  ChallengeQuery::create()->findOneById($challenge_id);
        if ($challenge) {
            $g_challenge = new GChallenge();
            $g_challenge->setChallenge($challenge);
            return $g_challenge;
        }
        else
        {
            return null;
        }
    }
    
    public function getLastCreatedChallenge() {
        $challenge =  ChallengeQuery::create()->orderById(Criteria::DESC)->findOne();
        $g_challenge = new GChallenge();
        $g_challenge->setChallenge($challenge);
        return $g_challenge;
    }
}
