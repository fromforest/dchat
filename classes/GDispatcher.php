<?php

//Giraffe dispatcher
use \Propel\Runtime\ActiveQuery\Criteria;


class GDispatcher {

    public function __construct($get, $post) {
        $this->get = $get;
        $this->post = $post;
    }

    // ---------------- Chat API -------------------------

    public function login() {
        $chat = new GChat();
        $name = $this->post['name'];
        $password = $this->post['password'];
        return $chat->login($name, $password);
    }

    public function checkLogged() {
        $chat = new GChat();
        return $chat->checkLogged();
    }

    public function logout() {
        $chat = new GChat();
        return $chat->logout();
    }

    public function submitChat() {
        $chat = new GChat();
        $chatText = $this->post['chatText'];
        return $chat->submitChat($chatText);
    }

    public function getChats() {
        $lastID = $this->get['lastID'];
        $chat = new GChat();
        return $chat->getChats($lastID);
    }

    public function getUsers() {
        $chat = new GChat();
        return $chat->getUsers();
    }

    // QUIZ

    public function getQuestion() {
        $chat = new GChat();
        return $chat->getQuestion();
    }

    // -------------------- Statistics API -----------------------------
	
    public function getStatisticsTotalScore() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsTotalScore($length_of_top);
    }
	
    public function getStatisticsCurrentHour() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsCurrentHour($length_of_top);
    }
	
    public function getStatisticsForRightTable() {
        $length_of_top = 5;
        $statistics = new GStatistics();
        return $statistics->getStatisticsCurrentHour($length_of_top);
    }

    public function getStatisticsPreviousHour() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsPreviousHour($length_of_top);
    }

    public function getStatisticsCurrentDay() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsCurrentDay($length_of_top);
    }

    public function getStatisticsPreviousDay() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsPreviousDay($length_of_top);
    }

    public function getStatisticsCurrentMonth() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsCurrentMonth($length_of_top);
    }

    public function getStatisticsPreviousMonth() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsPreviousMonth($length_of_top);
    }

    public function getStatisticsBestHoursTotal() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsBestHoursTotal($length_of_top);
    }

    public function getStatisticsBestHoursCurrentMonth() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsBestHoursCurrentMonth($length_of_top);
    }

    public function getStatisticsBestHoursCurrentYear() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsBestHoursCurrentYear($length_of_top);
    }

    public function getStatisticsBestDaysTotal() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsBestDaysTotal($length_of_top);
    }

    public function getStatisticsBestDaysCurrentYear() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsBestDaysCurrentYear($length_of_top);
    }

    public function getStatisticsBestMonthsTotal() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsBestMonthsTotal($length_of_top);
    }

    public function getStatisticsBestYears() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsBestYears($length_of_top);
    }

    public function getStatisticsCurrentYear() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsCurrentYear($length_of_top);
    }

    public function getStatisticsPreviousYear() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsPreviousYear($length_of_top);
    }

    public function getStatisticsChainsTotal() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsChainsTotal($length_of_top);
    }

    public function getStatisticsChainsCurrentMonth() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsChainsCurrentMonth($length_of_top);
    }

    public function getStatisticsChainsCurrentYear() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsChainsCurrentYear($length_of_top);
    }

    public function getStatisticsMQZ() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsMQZ($length_of_top);
    }

    public function getStatisticsOnAnyFiftyChallenges() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsOnAnyFiftyChallenges($length_of_top);
    }

    public function getStatisticsLastTournament() {
        $length_of_top = $this->post['length_of_top'];
        $statistics = new GStatistics();
        return $statistics->getStatisticsLastTournament($length_of_top);
    }
	
// -------------------- SinglePlayer API -------------------------------
	
    public function getSinglePlayerLastTheme() {
        $singlePlayer = new GSinglePlayer();
        return $singlePlayer->getSinglePlayerLastTheme();
    }
	
    public function getSinglePlayerQuestion() {
        $singlePlayer = new GSinglePlayer();
        $theme = $this->post['theme'];
        $is_random = $this->post['is_random'];
        return $singlePlayer->getSinglePlayerQuestion($theme, $is_random);
    }
	
    public function getSinglePlayerAnswer() {
        $singlePlayer = new GSinglePlayer();
        $text = $this->post['text'];
        return $singlePlayer->getSinglePlayerAnswer($text);
    }
	
    public function getSinglePlayerAllThemes() {
        $singlePlayer = new GSinglePlayer();
        return $singlePlayer->getSinglePlayerAllThemes();
    }
    
    //----- text Singleplayer API
    
    public function getTextSinglePlayerLastTheme() {
        $singlePlayer = new GSinglePlayer();
        return $singlePlayer->getTextSinglePlayerLastTheme();
    }
	
    public function getTextSinglePlayerQuestion() {
        $singlePlayer = new GSinglePlayer();
        $theme = $this->post['theme'];
        $is_random = $this->post['is_random'];
        return $singlePlayer->getTextSinglePlayerQuestion($theme, $is_random);
    }
	
    public function getTextSinglePlayerAnswer() {
        $singlePlayer = new GSinglePlayer();
        $text = $this->post['text'];
		$question_id = $this->post['question_id'];
		writeln('log.txt', 'in dispatcher: ' . $question_id);
        return $singlePlayer->getTextSinglePlayerAnswer($text, $question_id);
    }
	
    public function getTextSinglePlayerAllThemes() {
        $singlePlayer = new GSinglePlayer();
        return $singlePlayer->getTextSinglePlayerAllThemes();
    }
    
	//-----------------------Завалинка-------------------------------------------
    public function createGame() {
        $game = new GGame();
        $type = $this->post['type'];
        $room_name = $this->post['room_name'];
        return $game->createGame($type, $room_name);
    }
    
    public function getDefinitionsForCreatorEditing() {
        $game = new GGame();
        $game_room_id = $this->post['game_room_id'];
        return $game->getDefinitionsForCreatorEditing($game_room_id);
    }
    
    public function getPhase() {
         $game = new GGame();
        $game_room_id = $this->post['game_room_id'];
        return $game->getPhase($game_room_id);
    }
    
    public function getWords() {
        $game = new GGame();
        $game_room_id = $this->post['game_room_id'];
        return $game->getWords($game_room_id);
    }
    
    public function getZavalinkaUsers() {
        $game = new GGame();
        $game_room_id = $this->post['game_room_id'];
        return $game->getGameUsers($game_room_id);
    }
    
    
    public function joinGame() {
        $game = new GGame();
        $room_name = $this->post['room_name'];
        return $game->joinGame($room_name);
    }
	
    public function sendWordsAndTrueDefinitions() {
            $game = new GGame();
            $words = $this->post['words'];
            $game_room_id = $this->post['game_room_id'];
            $true_definitions = $this->post['true_definitions'];
            return $game->sendWordsAndTrueDefinitions($game_room_id, $words, $true_definitions);
    }
    
    public function sendWordsAndUserDefinitions() {
            $game = new GGame();
            $words = $this->post['words'];
            $game_room_id = $this->post['game_room_id'];
            $user_definitions = $this->post['user_definitions'];
            return $game->sendWordsAndUserDefinitions($game_room_id, $words, $user_definitions);
    }
    
    public function startGame() {
        $game = new GGame();
        $game_room_id = $this->post['game_room_id'];
        $edited_users_definitions = $this->post['edited_users_definitions'];
        return $game->startGame($game_room_id, $edited_users_definitions);
    }
    
    public function continueGame() {
        $game = new GGame();
        $game_room_id = $this->post['game_room_id'];
        return $game->continueGame($game_room_id);
    }
    
    public function getChoices() {
        $game = new GGame();
        $game_room_id = $this->post["game_room_id"];
        $current_word = $this->post["current_word"];
        return $game->getChoices($game_room_id, $current_word);
    }
    
    public function getChoiceResults() {
        $game = new GGame();
        $game_room_id = $this->post['game_room_id'];
        $current_word  = $this->post['current_word'];
        return $game->getChoiceResults($game_room_id, $current_word);
    }
    
    public function getShuffledDefinitions() {
        $game = new GGame();
        $game_room_id = $this->post['game_room_id'];
        return $game->getShuffledDefinitions($game_room_id);
    }
    
    public function submitChoice() {
        $game = new GGame();
        $game_room_id = $this->post['game_room_id'];
        $current_word = $this->post['current_word'];
        $choiced_definition = $this->post['choiced_definition'];
        return $game->submitChoice($game_room_id, $current_word, $choiced_definition);
    }
    
    // завалинка end
    
    public function sendFile() {
        //$file = $this->post['file'];
        return (new GFile())->sendFile(
                                                    $_FILES["filename"],
                                                    $_POST["topic"],
                                                    $_POST["tag1"],
                                                    $_POST["tag2"],
                                                    $_POST["questionText"],
                                                    $_POST["answer"],
                                                    $_POST["comment"]
                                                    );
    }

} //class
