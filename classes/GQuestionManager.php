<?php

class GQuestionManager {
    
    public function __construct() {
        
    }
    
    
    public function findQuestionById($id) {
        $question = QuestionQuery::create()->findOneById($id);
        if ($question) {
            $g_question = new GQuestion();
            $g_question->setQuestion($question);
            return $g_question;
        }
        else {
            return null;
        }
    }
    
    public function getAllQuestionsIds($type) {
        $config = new GConfig();
        $pstring = $config->getPackages(); // get string
        $blacklist = $config->getBlackList();
        $blacks = explode(':', $blacklist);

        if (($pstring === 'All') || ($pstring === 'all')) {
            $all_packages = PackageQuery::create()->find();
            foreach ($all_packages as $p) {
                
                $pnames [] = $p->getName();
            }
        } else {
            $pnames = explode(':', $pstring); // array of names
        }
        
        $pnames = array_diff($pnames, $blacks);
        
        //now get packages
        $packages = PackageQuery::create()->filterByName($pnames)->find();

        $ids = array();
        foreach ($packages as $p) {
            $package_ids = $this->_getQuestionsIdsByPackageAndType($p, $type);
            /*foreach ($qs as $q) {
                $ids[] = $q->getId();
            }*/
			$ids = array_merge($ids, $package_ids);
        }
        return $ids;
    }
    
    private function _getQuestionsByPackageAndType(Package $p, $type) {
        $questions = QuestionQuery::create()->filterByType($type)->filterByPackageId($p->getId());
        return $questions;
    }
	
	private function _getQuestionsIdsByPackageAndType(Package $p, $type) {
		$ids = array();
		$id_objects = QuestionQuery::create()->select('Id')->filterByType($type)->filterByPackageId($p->getId())->find();
		foreach ($id_objects as $id) {
			$ids []= $id;
		}
        return $ids;
	}

    
    public function findQuestionsByTopicAndTag1AndTag2($theme, $type) {
        $questions = [];
		//$start = microtime($get_as_float = true);
		//writeln('log.txt', 'start ' . $start);
        $ids = $this->getAllQuestionsIds($type);
		//$period1 = microtime($get_as_float = true);
        //writeln('log.txt', 'getAllQuestionsIds ' . $period1);
        $questions_by_topic = QuestionQuery::create()->filterById($ids)->filterByTopic($theme)->find();
        $questions_by_tag1  =  QuestionQuery::create()->filterById($ids)->filterByTag1($theme)->find();
        $questions_by_tag2  =  QuestionQuery::create()->filterById($ids)->filterByTag2($theme)->find();
        //$period2 = microtime($get_as_float = true);
		//writeln('log.txt', 'finds in database ' . $period2);
        foreach ($questions_by_topic as $q) {
            $g_question = new GQuestion();
            $g_question->setQuestion($q);
            $questions []= $g_question;
        }
        foreach ($questions_by_tag1 as $q) {
            $g_question = new GQuestion();
            $g_question->setQuestion($q);
            $questions []= $g_question;
        }
        foreach ($questions_by_tag2 as $q) {
            $g_question = new GQuestion();
            $g_question->setQuestion($q);
            $questions []= $g_question;
        }
        //$period3 = microtime($get_as_float = true);
		//writeln('log.txt', 'copying ' . $period3);
        return $questions;
    }
	
	public function getStringForWhereByBlackList() {
		$config = new GConfig();
		$gpm = new GPackageManager();
        $blacklist = $config->getBlackList();
        $blacks = explode(':', $blacklist);
		$string_for_where = "";
		foreach ($blacks as $b) {
			$b_id = $gpm->getPackageIdByPackageName($b);
			if ($string_for_where != "") {
				$string_for_where = $string_for_where . " and ";
			}
			$string_for_where = $string_for_where . 'questions.package_id != "' . $b_id . '"';
		}
		//$string_for_where = 'questions.package_id != "18" and questions.package_id != "1" ';
		return $string_for_where;
	}
    
    public function getAllThemeStrings($type) {
		$theme_strings =[];
        $string_for_where = $this->getStringForWhereByBlackList();
		
        $topic_strings = QuestionQuery::create()->filterByType($type)->where($string_for_where)->select('topic')->distinct()->find();
        $tag1_strings  = QuestionQuery::create()->filterByType($type)->where($string_for_where)->select('tag1')->distinct()->find();
        $tag2_strings  = QuestionQuery::create()->filterByType($type)->where($string_for_where)->select('tag2')->distinct()->find();

        foreach ($topic_strings as $topic) {
                if (strlen($topic . '') > 0) {
                    $theme_strings []= $topic . '';
                }
        }

        foreach ($tag1_strings as $tag1) {
                if (strlen($tag1) > 0) {
                    $theme_strings []= $tag1 . '';
                }
        }

        foreach ($tag2_strings as $tag2) {
                if (strlen($tag2) > 0) {
                     $theme_strings []= $tag2 . '';
                }
        }
        return $theme_strings;
    }
    
}//class
