<?php

class GConsole {
    
    public function __construct(GChat $chat, GConfig $config) {
        $this->chat = $chat;
        $this->config = $config;
    }
    
    public function checkCommand($input) {

        if ($input[0] != '!') { // not a command
            return false;
        }

        $tokens = explode(' ', $input);
        $first = $tokens[0];
        $method = substr($first, 1); // skip !

        if (method_exists($this, $method)) {
            $params = array_slice($tokens, 1);
            $this->$method($params);
        } else {
            $this->commandNotFound($method);
        }

        return true;
    }

    public function commandNotFound($cmd) {
        $this->chat->privateSysMessage('Команды ' . $cmd . ' не существует');
    }

    protected function _checkAdmin() {
        $user = $this->chat->getCurrentUser();
        if ($user->getGroup() === 'admins') {
            return true;
        } else {
            $this->chat->privateSysMessage('Not enough privileges.');
        }
    }

    public function ping($params) {
        $this->chat->privateSysMessage('pong!');
    }

    public function mode($params) {
        if ($this->_checkAdmin()) {
            if ($params[0]) {
                $this->config->setQuizMode($params[0]);
            }
            $this->chat->privateSysMessage('quiz_mode = ' . $this->config->getQuizMode());
        }
    }

    public function qtime($params) {
        if ($this->_checkAdmin()) {
            if ($params[0]) {
                $t = (integer) $params[0];
                $this->config->setQuestionTime($t);
            }
            $this->chat->privateSysMessage('time = ' . $this->config->getQuestionTime());
        }
    }

    public function atime($params) {
        if ($this->_checkAdmin()) {
            if ($params[0]) {
                $t = (integer) $params[0];
                $this->config->setAnswerTime($t);
            }
            $this->chat->privateSysMessage('time = ' . $this->config->getAnswerTime());
        }
    }

    public function syswrite($params) {
        $this->chat->privateSysMessage($params[0]);
    }

    public function packages($params) {
        if ($this->_checkAdmin()) {
            if ($params[0]) {
                $this->config->setPackages($params[0]);
            }
            $this->chat->privateSysMessage($this->config->getPackages());
        }
    }

    public function blacklist($params) {
        if ($this->_checkAdmin()) {
            if ($params[0]) {
                $this->config->setBlackList($params[0]);
            }
            $this->chat->privateSysMessage($this->config->getBlackList());
        }
    }

    public function add_package($params) {
        $package = $params[0];
        if ($this->_checkAdmin()) {
            $packages = $this->config->getPackages();
            $packages = $packages . ':' . $package;
            $this->config->setPackages($packages);
        }
        $this->chat->privateSysMessage($package . ' was successfully added');
    }

    public function hello($p) {
        $this->chat->privateSysMessage('Hello Hrust!');
    }

    protected function _stat_array_to_str($stat_array) {
        $result_string = '|';
        foreach ($stat_array as $stat) {
            $result_string = $result_string . $stat['user'] . ': ' . $stat['score'];
            $result_string .= "|";
        }
        return $result_string;
    }

    public function stat() {
        $statistics = new GStatistics();
        $total5 = $statistics->getStatisticsTotalScore(5)["statistics_in_period_array"];
        $current_day5 = $statistics->getStatisticsCurrentDay(5)["statistics_in_period_array"];
        $this->chat->privateSysMessage('Топ-5 за всё время: ' . $this->_stat_array_to_str($total5));
        $this->chat->privateSysMessage('Топ-5 текущего дня: ' . $this->_stat_array_to_str($current_day5));
    }

    public function min_users($params) {
        if ($this->_checkAdmin()) {
            if ($params[0]) {
                $this->config->setMinUsersForUpdates($params[0]);
            }
            $this->chat->privateSysMessage('min_users_for_updates = ' . $this->config->getMinUsersForUpdates());
        }
    }
    
    protected function _checkConfigTournamentParams($params) {
        //если не 4 параметра, выдаём подсказку
        if (count($params) !== 4) {
            $this->chat->privateSysMessage('Give me accurately 4 parameters: tournament_number, q_time, a_time, packages');
            return False;
        }
        else {
        //если же всё в порядке, преобразуем данные
            $tournament_number = (integer) $params[0];
            $q_time                      = (integer) $params[1];
            $a_time                      = (integer) $params[2];
            $packages                   = $params[3];
            
            //если после преобразования мы где-то получили 0, то значит входные данные были кривые
            if (($tournament_number * $q_time * $a_time) === 0) {
                $this->chat->privateSysMessage('You are bad. Give me 4 good parameters: tournament_number, q_time, a_time, packages');
                return False;
            }
            
            if ($tournament_number === 1) {
                $this->chat->privateSysMessage("You are stupid. You want change main config");
                return False;
            }
        }
        
        return True; //по умолчанию всё ок
    }
    
    public function config_tournament($params) {
        if ($this->_checkConfigTournamentParams($params)) { //если проверка корректности данных дала True, то всё ок.
            
            $tournament_number = (integer) $params[0];
            $q_time                      = (integer) $params[1];
            $a_time                      = (integer) $params[2];
            $packages                   = $params[3];
            
            $is_exist = (new GConfigManager())->isTournamentExist ($tournament_number);
            
            if (!$is_exist) { //если с таким номером турнира нет то создание конфига для нового турнира
                (new GConfigManager())->createNewConfigTournament($tournament_number, $q_time, $a_time, $packages);
                
            }
            else { //если турнир с таким номером есть, то просто редактируем запись в базе
                (new GConfigManager())->changeConfigTournament($tournament_number, $q_time, $a_time, $packages);
            }
                    
            $this->info_tournament([$tournament_number]);

        }
    }
    
    public function info_tournament($params) {
        $tournament_number = $params[0];
        (new GConfigManager())->sendInfoTournament($tournament_number);
    }

    public function difficulty($params) {
        if ($this->_checkAdmin()) {
            if ($params[0]) {
                $this->config->setDifficultySequence($params[0]);
            }
            $this->chat->privateSysMessage('difficulty_sequence = ' . $this->config->getDifficultySequence());
        }
    }

    public function between_equal($params) {
        if ($this->_checkAdmin()) {
            if ($params[0]) {
                $this->config->setMinTimeBetweenEqualQuestions($params[0]);
            }
            $this->chat->privateSysMessage('min_time_between_equal_questions = ' . $this->config->getMinTimeBetweenEqualQuestions());
        }
    }

    public function tournament($params) {
        if ($this->_checkAdmin()) {
            if ( ($params[0]) && ($this->config->getQuizMode() === "stop")) {
                $tournament_number = (integer) $params[0];
                if (($tournament_number === 0) || ($tournament_number === 1)) {
                        $this->chat->privateSysMessage($tournament_number . 'is invalid argument');
                }
                else {
                        $g_config_manager = new GConfigManager();
                        $this->config->setTournamentNumber($tournament_number);
                        $tournament_params = $g_config_manager->getTournamentParamsByTournamentNumber($tournament_number);
                        $this->config->setQuestionTime($tournament_params["q_time"]);
                        $this->config->setAnswerTime($tournament_params["a_time"]);
                        $this->config->setPackages($tournament_params["packages"]);
                        $g_config_manager->sendInfoTournament($tournament_number);
                }
            }
            else {
                $this->chat->privateSysMessage("Either no parameters or quiz_mode is not stop");
            }
            $this->chat->privateSysMessage('tournament_number = ' . $this->config->getTournamentNumber() . ' tqi = ' . $this->config->getTqi());
        }
    }

    public function tqi($params) { //tournament question index
        if ($this->_checkAdmin()) {
            if (($params[0]) || ($params[0] === '0')) {
                $this->config->setTqi($params[0]);
            }
            $this->chat->privateSysMessage('tournament_question_index = ' . $this->config->getTqi());
        }
    }

} //class

