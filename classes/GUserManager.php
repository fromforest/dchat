<?php

//класс является прослойкой между Propel и нашим серверным кодом по всему, что связано с таблицей users в БД.

class GUserManager {

    public function findUserByNameAndPassword($name = "", $password = "") { //используется в GChat::_authenticate 
        $user = UserQuery::create()->filterByName($name)->filterByPassword(md5($password))->findOne();
        if ($user) {
            $g_user = new GUser();
            $g_user->setUser($user);
            return $g_user;
        }
        else {
            return null;
        }
    }
    
    public function findUserByName($name = "") { //используется в GChat::login
        $user = UserQuery::create()->findOneByName($name);
        if ($user) {
            $g_user = new GUser();
            $g_user->setUser($user);
            return $g_user;
        }
        else {
            return null;
        }
    }
    
    public function findUsersByLastGameRoomId($game_room_id) {
        $game_users = UserQuery::create()->filterByLastGameRoomId($game_room_id)->find();
        $user_names = [];
        foreach ($game_users as $gu) {
            $user_names []= $gu->getName();
        }
        return $user_names;
    }
    
    public function getAllUserNames() {
        return UserQuery::create()
                        ->select('User.Name')
                        ->find();
    }
    
    public function getCountOfUsersOnline() {
        return UserQuery::create()->filterByOnline(true)->count();
    }
    
    public function getNamesAndIdsOfUsersOnline() {
        $users_online = UserQuery::create()->findByOnline(true);

        $users_array = [];

        foreach ($users_online as $u) {
            $arr['name'] = $u->getName();
            $arr['id'] = $u->getId();
            $users_array[] = $arr;
        }
        
        return $users_array;
    }
    
    public function getStatisticsOnAnyFiftyChallenges($number) {
        $statistics_objects = UserQuery::create()
                ->select(['Name', 'BestFifty'])
                ->filterByBestFifty(['min' => 1])
                ->orderBy('BestFifty', 'desc')
                ->limit($number)
                ->find();

        $statistics = [];
        foreach ($statistics_objects as $stat) {
            $as_array['user'] = $stat['Name'];
            $as_array['score'] = $stat['BestFifty'];
            $statistics [] = $as_array;
        }
        
        return $statistics;
    }
    
    public function getUserNameById($id) {
        $name = UserQuery::create()->select('name')->findOneById($id);
        return $name . "";
    }
    
    public function setBestFiftyScore($current_challenge_id, GUser $g_user) {
        $score_on_previous_49_challenges = RightAnswerQuery::create()
                ->filterByUser($g_user->getUser())
                ->filterByChallengeId(['min' => $current_challenge_id - 49, 'max' => $current_challenge_id - 1])
                ->filterByType(0)
                ->count();

        $current_score_on_last_fifty = $score_on_previous_49_challenges + 1;
        $best_score_on_last_fifty = $g_user->getBestFifty();
        if ($current_score_on_last_fifty > $best_score_on_last_fifty) {
            $g_user->setBestFifty($current_score_on_last_fifty);
            $g_user->save();
        }
    }
    
    public function setInactiveUsersOffline() {
        $inactive_users = UserQuery::create()->filterByOnline(true)->filterByUpdatedAt('90 seconds ago', '<')->find();

        foreach ($inactive_users as $u) {
            $u->setOnline(false);
            $u->save();
        }
    }

} //class
