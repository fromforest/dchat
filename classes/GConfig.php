<?php

class GConfig{
    
    private $_c;
    public function __construct() {
        $this->_c = ConfigQuery::create()->findOneById(1);
    }

    public function getQuizMode() {     
        return $this->_c->getQuizMode();
    }

    public function setQuizMode($s) {
        $this->_c->setQuizMode($s);
        $this->_c->save();
    }

    public function getQuestionTime() {
        return $this->_c->getQuestionTime();
    }

    public function setQuestionTime($t) {
        $this->_c->setQuestionTime($t);
        $this->_c->save();
    }

    public function getAnswerTime() {
        return $this->_c->getAnswerTime();
    }

    public function setAnswerTime($t) {
        $this->_c->setAnswerTime($t);
        $this->_c->save();
    }

    public function getPackages() {
        $s = $this->_c->getPackages();
        return $s;
    }

    public function setPackages($ps) {
        $this->_c->setPackages($ps);
        $this->_c->save();
    }

    public function getBlackList() {
        $s = $this->_c->getBlackList();
        return $s;
    }

    public function setBlackList($bls) {
        $this->_c->setBlackList($bls);
        $this->_c->save();
    }

    public function addPackage($p) {
        if (strpos($p, ':') !== false) {
            throw new Exception('Could not add package with : symbol.');
        }
        $this->setPackages($this->getPackages() . ':' . $p);
    }

    public function getMinUsersForUpdates() {
        $s = $this->_c->getMinUsersForUpdates();
        return $s;
    }

    public function setMinUsersForUpdates($min_users) {
        $this->_c->setMinUsersForUpdates($min_users);
        $this->_c->save();
    }

    public function getDifficultySequence() {
        $s = $this->_c->getDifficultySequence();
        return $s;
    }

    public function setDifficultySequence($ds) {
        $this->_c->setDifficultySequence($ds);
        $this->_c->save();
    }

    public function getMinTimeBetweenEqualQuestions() {
        $s = $this->_c->getMinTimeBetweenEqualQuestions();
        return $s;
    }

    public function setMinTimeBetweenEqualQuestions($min_time) {
        $this->_c->setMinTimeBetweenEqualQuestions($min_time);
        $this->_c->save();
    }

    public function getTournamentQuestionIndex() {
        return $this->_c->getTournamentQuestionIndex();
    }

    public function setTournamentQuestionIndex($index) {
        $this->_c->setTournamentQuestionIndex($index);
        $this->_c->save();
    }

    public function getTournamentNumber() {
        return $this->_c->getTournamentNumber();
    }

    public function setTournamentNumber($number) {
        $this->_c->setTournamentNumber($number);
        $this->_c->save();
    }

    public function getTqi() { //tournament question index
        return $this->_c->getTqi();
    }

    public function setTqi($number) {
        $this->_c->setTqi($number);
        $this->_c->save();
    }

    public function getAnnouncedRightUsers() {
        return $this->_c->getAnnouncedRightUsers();
    }

    public function setAnnouncedRightUsers($num_announced) {
        $this->_c->setAnnouncedRightUsers($num_announced);
        $this->_c->save();
    }

    public function getChallengeUpdated() {
        return $this->_c->getChallengeUpdated();
    }

    public function setChallengeUpdated($num_updated) {
        $this->_c->setChallengeUpdated($num_updated);
        $this->_c->save();
    }
    
    public function getTempUploadedQuestionId() {
        return $this->_c->getTempUploadedQuestionId();
    }
    
    public function setTempUploadedQuestionId($id) {
        $this->_c->setTempUploadedQuestionId($id);
        $this->_c->save();
    }

}
