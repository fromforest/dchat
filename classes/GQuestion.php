<?php


class GQuestion {
    private $_question;
    
    function   __construct() {
        $this->_question = new Question();
    }
    
    public function getAltAnswers() {
        return $this->_question->getAltAnswers();
    }
    
    public function getAnswerPictureUrl() {
        return $this->_question->getAnswerPictureUrl();
    }
    
    public function getComment() {
        return $this->_question->getComment();
    }
    
    public function getId() {
        return $this->_question->getId();
    }
    
    public function getNumAnswered() {
        return $this->_question->getNumAnswered();
    }
    
    public function getNumAsked() {
        return $this->_question->getNumAsked();
    }
    
    public function getPackage() {
        $package = $this->_question->getPackage();
        $g_package = new GPackage();
        $g_package->setPackage($package);
        return $g_package;
    }
    
    public function getPackageName() {
        return $this->_question->getPackage()->getName();
    }
    
    public function getPictureUrl() {
        return $this->_question->getPictureUrl();
    }
    
    public function getQuestion() {
        return $this->_question;
    }
    
    public function getRightAnswer() {
        return $this->_question->getRightAnswer();
    }
    
    
    public function getText() {
        return $this->_question->getText();
    }
    
    public function getTopic() {
        return $this->_question->getTopic();
    }
    
    public function getUpdatedAt() {
        return $this->_question->getUpdatedAt();
    }
    
    public function save() {
         $this->_question->save();
    }
    
    public function setAuthor($author) {
        $this->_question->setAuthor($author);
    }
    
    public function setCreatedAt(DateTime $created_at) {
        $this->_question->setCreatedAt($created_at);
    }
    
    public function setComment($comment) {
        $this->_question->setComment($comment);
    }
    
    public function setNumAnswered($num_answered) {
        $this->_question->setNumAnswered($num_answered);
    }
    
    public function setNumAsked($num_asked) {
        $this->_question->setNumAsked($num_asked);
    }
    
    public function setPackageId($package_id) {
        $this->_question->setPackageId($package_id);
    }
    
    public function setPictureUrl($picture_url) {
        $this->_question->setPictureUrl($picture_url);
    }
    
    public function setId($id) {
        $this->_question->setId($id);
    }

    public function setQuestion(Question $question) {
        $this->_question = $question;
    }
    
    public function setRightAnswer($right_answer) {
        $this->_question->setRightAnswer($right_answer);
    }
    
    public function setAltAnswers($alt_answers) {
        $this->_question->setAltAnswers($alt_answers);
    }
    
    public function setText($text) {
        $this->_question->setText($text);
    }
    
    public function setTopic($topic) {
        $this->_question->setTopic($topic);
    }
    
    public function setTag1($tag1) {
        $this->_question->setTag1($tag1);
    }
    
    public function setTag2($tag2) {
        $this->_question->setTag2($tag2);
    }
    
    public function setUpdatedAt(DateTime $datetime) {
        $this->_question->setUpdatedAt($datetime);
    }
        
}
