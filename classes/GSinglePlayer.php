<?php

use \Propel\Runtime\ActiveQuery\Criteria;
use \Propel\Runtime\Propel;

class GSinglePlayer {
	
        public function __construct() {
        
        }
        
        //api
        public function getSinglePlayerLastTheme() {
                $chat = new GChat();
	$user = $chat->getCurrentUser();
                $theme = '';
	if (!$user) {
                    throw new Exception("Cannot submit chat. You are not logged in");
                }
		
	$last_singleplayer_answer = (new GSinglePlayerManager())->getLastSinglePlayerAnswer($user); //любой ответ, не обязательно правильный
	if ($last_singleplayer_answer) {
                        $question_id = $last_singleplayer_answer->getQuestionId();
                        $question = (new GQuestionManager())->findQuestionById($question_id);
                        $theme = $question->getTopic();
	}
		return ['theme' => $theme];
        }
	
        public static function functionForSortInSinglePlayer($a,$b) { //подумать над формулой, чтоб часто задаваемые вопросы меньше спрашивать.
		
	$a_asked = $a['num_asked'];
	$b_asked = $b['num_asked'];
                $a_answered = $a['num_answered'];
	$b_answered = $b['num_answered'];
		
	if ($a_asked*$b_asked === 0) {//какой-то из вопросов ни разу не задавался
                    if ($a_asked === $b_asked) { //оба ни разу не задавались
                        return 0;
                    }
                    return ($a_asked > $b_asked) ? 1 : -1; //не заданный ни разу вопрос имеет приоритет
	} else {
                    if ($a_answered/($a_asked + 10) === $b_answered/($b_asked + 10)) { //что такое 10 - см. ideas func3, только там 3
						if ($a_answered === 0) { //чтобы один и тот же неберущийся вопрос подряд не шел, а они сменяли друг друга
							$condition = $a_asked < $b_asked;
							return ($condition? -1 : 1);
						}
						else {
							return 0;
						}
                    }
                    $condition = ($a_answered/($a_asked + 10) < $b_answered/($b_asked + 10)); //менее отвечабельный вопрос имеет приоритет
                    return ($condition ? -1 : 1);
	}
        }//end functionForSortInSinglePlayer
	
        protected function _getNextSinglePlayerQuestion($theme, $is_random, $type) {
	$chat = new GChat();
	$user = $chat->getCurrentUser();
                $g_question_manager = new GQuestionManager();
                $g_singleplayer_manager = new GSinglePlayerManager();
                
	$questions = $g_question_manager->findQuestionsByTopicAndTag1AndTag2($theme, $type);
				$questions_count = count($questions);
                $question_objects = [];
                foreach ($questions as $q) {
                        $question_struct = $g_singleplayer_manager->getSinglePlayerAskedAnsweredArray($q, $user, $type);
                        $question_objects []= $question_struct;
                }
		
                if ($is_random) {
                        $index = rand(0, count($question_objects) - 1);
                        return array($question_objects[$index]['question'], 1);			
	}
                else {
                        usort($question_objects, 'self::functionForSortInSinglePlayer');
						//foreach ($question_objects as $qo) {
						//	writeln('log.txt', $qo['question']->getText() . ' ' . $qo['num_answered'] . ' ' . $qo['num_asked']);
						//}
                        return array ($question_objects[0]['question'], $questions_count);
	}
        }//end  _getNextSinglePlayerQuestion
	
        protected function _getNoTheme() {
	return [
                    'topic' => 'Тема отсутствует или недоступна',
                    'text' => 'Введите тему',
                    'pictureUrl' => '../img/no_theme.jpg'
	];
        }
        
        protected function _getTextNoTheme() {
	return [
                    'topic' => 'Тема отсутствует или недоступна',
                    'text' => 'Введите тему'
	];
        }
        
        //api
        public function getSinglePlayerQuestion($theme, $is_random) {
	$config = new GConfig();
	$quiz_mode = $config->getQuizMode();
	if ($quiz_mode === 'tournament') {
                    return ['question' => [
                                    'topic' => 'Сейчас идёт турнир',
                                    'text' => 'Дождитесь окончания турнира',
                                    'pictureUrl' => '../img/nowtournament.jpg'
                                    ]
                              ];
	}
	$chat = new GChat();
	$user = $chat->getCurrentUser();
	if (!$user) {
                    throw new Exception("Cannot get question. You are not logged in");
                }
		
                if (($theme === '') || ($theme === ' ')) {
                    $question_array = $this->_getNoTheme();
                    return ['question' => $question_array];
	}
		
	$question_and_count_array = $this->_getNextSinglePlayerQuestion($theme, $is_random, 'image');
	$question = $question_and_count_array[0];
	$questions_count = $question_and_count_array[1];
	
	if (!$question) {
                    $question_array = $this->_getNoTheme();
                    return ['question' => $question_array];
	}
	else {
                    //$spa = (new GSinglePlayerManager())->createSinglePlayerAnswer($question->getId(), $user->getId());
					$gsm = new GSinglePlayerManager();
					$spa = $gsm->getSinglePlayerAnswerByUserAndQuestion($user, $question, "image");
					if (!$spa) {
						$spa = new GSinglePlayerAnswer();
						$spa->setQuestionId($question->getId());
						$spa->setUserId($user->getId());
						$spa->setNumAsked(1);
						$spa->save();
					}
                    else {
						$spa->setNumAsked($spa->getNumAsked() + 1);
						$spa->save();
					}
                    $question_array = [
                        'topic' => $question->getTopic(),
                        'text' => $question->getText(),
                        'pictureUrl' => $question->getPackageName() .
		'/' . $question->getPictureUrl()
                    ];		
                    return ['question' => $question_array, 'questions_count' => $questions_count];
	}
	
        } //end getSinglePlayerQuestion
	
        public function getSinglePlayerAnswer($text) {
	$chat = new GChat();
	$user = $chat->getCurrentUser();
	if (!$user) {
                    throw new Exception("Cannot get answer. You are not logged in");
                }
	//spa = current challenge/answer, string in table "single_player_answers"
	$spa = (new GSinglePlayerManager())->getLastSinglePlayerAnswer($user);
	$question = $spa->getQuestion();
		
	$is_right = false;
	if ($text) {
                    $quiz = new GQuiz(new GChat(), new GConfig());
                    if ($quiz->isRightAnswer($text, $question)) {
						$is_right = true;
                        $spa->setNumAnswered($spa->getNumAnswered() + 1);
                        $spa->save();
                    }
	}
		
	$answer_array = [
                    'is_right' => $is_right,
                    'topic' => 'Ответ: ' . $question->getRightAnswer(),
                    'text' => $question->getComment()
	];
	$answer_picture = $question->getAnswerPictureUrl();
	$answer_array['pictureUrl'] = ($answer_picture) ? 
	$question->getPackageName() . '/answers/' . $answer_picture
                        : $question->getPackageName() . '/' 
		. $question->getPictureUrl();
						
	return ['answer' => $answer_array];
        } //end getSinglePlayerAnswer
        
        //api
        public function getSinglePlayerAllThemes() {
	$themes = [];
	$theme_strings = (new GQuestionManager())->getAllThemeStrings('image');
		
	$theme_strings = array_unique($theme_strings);
	foreach($theme_strings as $ts) {
                    $as_array ['name'] = $ts;
                    $themes []= $as_array;
	}
	return ['themes' => $themes];
        }
        
        //text SinglePlayer api
        
        public function getTextSinglePlayerLastTheme() {
                $chat = new GChat();
	$user = $chat->getCurrentUser();
                $theme = '';
	if (!$user) {
                    throw new Exception("Cannot submit chat. You are not logged in");
                }
		
	$last_text_singleplayer_answer = (new GSinglePlayerManager())->getLastTextSinglePlayerAnswer($user); //любой ответ, не обязательно правильный
	if ($last_text_singleplayer_answer != null) {
                        $question_id = $last_text_singleplayer_answer->getQuestionId();
                        $question = (new GQuestionManager())->findQuestionById($question_id);
                        $theme = $question->getTopic();
	}
                
                return ['theme' => $theme];
                
        }//method
        
        public function getTextSinglePlayerQuestion($theme, $is_random) {
	$config = new GConfig();
	$chat = new GChat();
	$user = $chat->getCurrentUser();
	if (!$user) {
                    throw new Exception("Cannot get question. You are not logged in");
                }
		
                if (($theme === '') || ($theme === ' ')) {
                    $question_array = $this->_getTextNoTheme();
                    return ['question' => $question_array];
	}
		
	$question_and_count_array = $this->_getNextSinglePlayerQuestion($theme, $is_random, 'text');
	$question = $question_and_count_array[0];
	$questions_count = $question_and_count_array[1];
	
	if (!$question) {
                    $question_array = $this->_getTextNoTheme();
                    return ['question' => $question_array];
	}
	else {
                    //$spa = (new GSinglePlayerManager())->createSinglePlayerAnswer($question->getId(), $user->getId());
            
                    $gsm = new GSinglePlayerManager();
					$tspa = $gsm->getSinglePlayerAnswerByUserAndQuestion($user, $question, "text");
					if (!$tspa) {
						$tspa = new GTextSinglePlayerAnswer();
						$tspa->setQuestionId($question->getId());
						$tspa->setUserId($user->getId());
						$tspa->setNumAsked(1);
						$tspa->save();
					}
                    else {
						$tspa->setNumAsked($tspa->getNumAsked() + 1);
						$tspa->save();
					}
                    
                    $question_array = [
						'id' => $question->getId(),
                        'topic' => $question->getTopic(),
                        'text' => $question->getText()
                    ];		
                    return ['question' => $question_array, 'questions_count' => $questions_count];
	}
	
        } //end getTextSinglePlayerQuestion
        
        public function getTextSinglePlayerAnswer($text, $question_id) {
			$chat = new GChat();
			$user = $chat->getCurrentUser();
			if (!$user) {
							throw new Exception("Cannot get answer. You are not logged in");
						}
			//spa = current challenge/answer, string in table "single_player_answers"
			
			//прежде запрос шёл по последнему обновленному сочетанию user/question в таблице TextSinglePlayerQuestion
			//$tspa = (new GSinglePlayerManager())->getLastTextSinglePlayerAnswer($user);
			//$question = $tspa->getQuestion();
			writeln('log.txt', 'id question: ' . $question_id . '');
			$question = (new GQuestionManager())->findQuestionById($question_id);
			$tspa = (new GSinglePlayerManager())->getSinglePlayerAnswerByUserAndQuestion($user, $question, 'text');
			$is_right = false;
			if ($text) {
							$quiz = new GQuiz(new GChat(), new GConfig());
							if ($quiz->isRightAnswer($text, $question)) {
								$is_right = true;
								$tspa->setNumAnswered($tspa->getNumAnswered() + 1);
								$tspa->save();
							}
			}
				
			$answer_array = [
							'is_right' => $is_right,
							'topic' => 'Ответ: ' . $question->getRightAnswer(),
							'text' => $question->getComment()
			];
			
						
			return ['answer' => $answer_array];
        } //end getTextSinglePlayerAnswer
        
    public function getTextSinglePlayerAllThemes() {
			$themes = [];
			$theme_strings = (new GQuestionManager())->getAllThemeStrings('text');
				
			$theme_strings = array_unique($theme_strings);
			foreach($theme_strings as $ts) {
							$as_array ['name'] = $ts;
							$themes []= $as_array;
			}
			return ['themes' => $themes];
        }//method
	
}//class