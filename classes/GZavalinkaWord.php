<?php


class GZavalinkaWord {
    private $_zw;
    
    public function __construct() {
        $this->_zw = new ZavalinkaWord();
    }
    
    public function getDefinition() {
        return $this->_zw->getDefinition();
    }
    
    public function getUserId() {
        return $this->_zw->getUserId();
    }
    
    public function getZavalinkaWord() {
        return $this->_zw;
    }
    
    public function setUserId($user_id) {
        $this->_zw->setUserId($user_id);
    }
     
    public function setWord($word) {
        $this->_zw->setWord($word);
    }
    
    public function setZavalinkaWord(ZavalinkaWord $zw) {
        $this->_zw = $zw;
    }
      
    public function setDefinition($definition) {
        $this->_zw->setDefinition($definition);
    }
     
    public function setGameRoomId($game_room_id) {
        $this->_zw->setGameRoomId($game_room_id);
    }
     
    public function save() {
        $this->_zw->save();
    }
}
