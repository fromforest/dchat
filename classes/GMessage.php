<?php

class GMessage {
    private $_message;
    function   __construct() {
        $this->_message = new Message();
    }
    
    public function getCreatedAtFormat($format) {
        return $this->_message->getCreatedAt()->format($format);
    }
    
    public function getId() {
        return $this->_message->getId();
    }
    
    public function getMessage() {
        return $this->_message;
    }
    
    public function getText() {
        return $this->_message->getText();
    }
    
    public function getToId() {
        return $this->_message->getToId();
    }
    
    public function getQuestion() {
        $question = $this->_message->getQuestion();
        $g_question = new GQuestion();
        $g_question->setQuestion($question);
        return $g_question;
    }
    
    public function getUser() {
        $user = $this->_message->getUser();
        $g_user = new GUser();
        $g_user->setUser($user);
        return $g_user;
    }
    
    public function save() {
        $this->_message->save();
    }
    
    public function setMessage(Message $message) {
        $this->_message = $message;
    }
    
    public function setText($text) {
        $this->_message->setText($text);
    }
    
    public function setToId($id) {
        $this->_message->setToId($id);
    }
    
    public function setUser(GUser $author) {
        $this->_message->setUser($author->getUser());
    }

}
