<?php

class GMessageManager {
    
    public function __construct() {
        
    }

    public function deleteOldMessages() {
        $old_lines = MessageQuery::create()->filterByCreatedAt('5 minutes ago', '<')->find();
        $old_lines->delete();
    }
    
    public function getAttributesOfRecentMessages($lastID, GUser $user) {
        $recent = MessageQuery::create()->filterById($lastID, '>'); //->find();
        $chat_objects = $recent
                ->condition('cond1', 'messages.to_id = ?', 0) //public
                ->condition('cond2', 'messages.to_id = ?', $user->getId()) //to me
                ->condition('cond3', 'messages.user_id = ?', $user->getId()) //from me
                ->where(['cond1', 'cond2', 'cond3'], 'or')
                ->find();

        $chats = [];
        foreach ($chat_objects as $chat) {

            // Returning the GMT (UTC) time of the chat creation:

            $chat_time = $chat->getCreatedAt()->format('Y-m-d H:i:s');

            $as_array['id'] = $chat->getId();
            $author = $chat->getUser();
            // we can have messages from deleted users
            if ($author) {
                $as_array['author'] = $author->getName(); // getAuthor();
            } else {
                $as_array['author'] = 'unknown';
            }
            $as_array['text'] = $chat->getText();
            $as_array['time'] = $chat_time;

            if ($chat->getToId() == 0) {
                $as_array['type'] = 'public';
            } else {
                $as_array['type'] = 'private';
                $to_name = UserQuery::create()->findOneById($chat->getToId())->getName();
                $as_array['text'] = $to_name . ', ' . $as_array['text'];
            }

            $chats[] = $as_array;
        }
        
        return $chats;
    }
    
}//class
