<?php

use \Propel\Runtime\ActiveQuery\Criteria;
//require_once __DIR__ . '/../utils.php';

class GQuiz {
    
    public $stop_question;
    public $alreadyAnsweredText;
    public $tooLateText;
    
    public function __construct(GChat $chat, GConfig $config) {
        $this->chat = $chat;
        $this->config = $config;
        $this->stop_question = [
                'topic' => 'Режим игры',
                'text' => 'Викторина остановлена',
                'pictureUrl' => '../img/stop.jpg'
            ];
        $this->alreadyAnsweredText = 'От Вас уже принят точный ответ :)';
        $this->tooLateText = 'Вы не успели.';
    }

    protected function _incrementNumAsked(GQuestion $question) {
        //Инкремент параметра num_asked у question. Отрабатывает для question в текущем challenge перед
        // созданием нового challenge. Если количество онлайн-юзеров меньше заданного в конфиге,
        //  то инкремент не происходит.
        $min_users_for_updates = $this->config->getMinUsersForUpdates();
        $users_online_count = (new GUserManager())->getCountOfUsersOnline();
        if ($users_online_count >= $min_users_for_updates) {
            $num_asked = $question->getNumAsked();
            $num_asked += 1;
            $question->setNumAsked($num_asked);
            $question->save();
        }
    }

    protected function _incrementNumAnswered(GQuestion $question) {
        //Инкремент параметра num_answered у question.
        //Отрабатывает для question в текущем challenge во время проверки правильного ответа юзера.
        //Если количество онлайн-юзеров меньше заданного в конфиге, то инкремент не происходит.
        // Если строка с этим question в БД недавно обновлялась (чей-то верный ответ уже выполнил incrementNumAnswered),
        //  то инкремент не происходит.
        $min_users_for_updates = $this->config->getMinUsersForUpdates();
        $users_online_count = (new GUserManager())->getCountOfUsersOnline();
        $current_challenge = $this->_getLastChallenge();

        if (($users_online_count >= $min_users_for_updates) &&
                ($question->getUpdatedAt() < ($current_challenge->getCreatedAt() ))) {
            $num_answered = $question->getNumAnswered();
            $num_answered += 1;
            $question->setNumAnswered($num_answered);
            $question->save();
        }
    }

    protected function _getLastChallenge() {
        $last_challenge = (new GChallengeManager())->getLastCreatedChallenge();
        return $last_challenge;
    }

    public function getChallenge() {
        return $this->_getLastChallenge();
    }

    public function updateChallenge() {
        $last_challenge = $this->_getLastChallenge();
        $qtime = $this->config->getQuestionTime();
        $created_time = $last_challenge->getCreatedAt();

        //на всякий случай. Если вдруг из-за каких-то причин ChallengeUpdated в конфиге не сбросится в 0, когда надо, то по прошествии двойного времени вопроса он сбрасывается в 0 и новый challenge будет создан, викторина продолжается.
        if ($created_time < new DateTime(2 * $qtime . " seconds ago")) {
            $this->config->setChallengeUpdated(0);
        }

        $moment_ago = new DateTime($qtime . " seconds ago");
        
        if ($created_time < $moment_ago) {
            if ($this->config->getChallengeUpdated() > 0) { //если какой-то юзерский запрос уже создаёт новый challenge
                return 0;
            }
            else {
                $this->config->setChallengeUpdated($this->config->getChallengeUpdated() + 1); //обновление в конфиге для предыдущей проверки что мы уже тут
            }
            
            $this->_incrementNumAsked($last_challenge->getQuestion()); //инкремент предыдущего заданного вопроса. Если это делать раньше, то возникнут проблемы с update num_answered.

            $q = $last_challenge->getQuestion();
            $q->setUpdatedAt(new DateTime("now")); //установка обновления предыдущего вопроса, показывающего, что вопрос был задан. это важно для проверки внутри _excludeRecentQuestions
            $q->save();
                
            
            if ($this->config->getChallengeUpdated() > 1) { //ещё одна аналогичная проверка
                return 0;
            }
            else {
                $this->config->setChallengeUpdated($this->config->getChallengeUpdated() + 1);
            }
            $this->createNextChallenge();
        }
    }

    public function createNextChallenge() {
        $challenge = new GChallenge();
        $quiz_mode = $this->config->getQuizMode();
            if ($quiz_mode === 'tournament') { //если идёт турнир, вопросы задаются по порядку
                $tournament = new GTournament($this->config);
                if (!$tournament->_isTournamentEnd()) {
                    $next_question = $tournament->_getTournamentQuestion();
                }
            } else { //если идёт регулярная викторина, то следующий вопрос по особому алгоритму
                $next_question = $this->_getNextQuestion();
                //$next_question = self::_getRandomQuestion();
            }
            if ($next_question) {
                $challenge->setQuestion($next_question);
                $challenge->save();
            } else {
                throw new UnexpectedValueException("question has not selected");
            }
    }

    //функция для сортировки usort, использующейся в _getNextQuestion (отвечабельность вопроса). 
    //Неравенства < и > именно такие, какие нужно.
    public static function functionForSortInRegular(GQuestion $a, GQuestion $b) { 
        $a_asked = $a->getNumAsked();
        $b_asked = $b->getNumAsked();
        $a_answered = $a->getNumAnswered();
        $b_answered = $b->getNumAnswered();

        if ($a_asked * $b_asked === 0) {//какой-то из вопросов ни разу не задавался
            if ($a_asked === $b_asked) //оба ни разу не задавались
                return 0;
            return ($a_asked === 0) ? -1 : 1; //не заданный ни разу вопрос имеет приоритет
        } else {
            if (($a_answered === 0) && ($b_answered === 0)) {
                return (($a_asked < $b_asked) ? -1 : 1);
            }
            if ($a_answered / $a_asked === $b_answered / $b_asked)
                return 0;

            $condition = ($a_answered / $a_asked < $b_answered / $b_asked); //более отвечабельный вопрос имеет приоритет
            return ($condition ? 1 : -1);
        }
    }

    protected function _excludeRecentQuestions(array $questions) {
        if (gettype($questions[0]) !== 'object') {
            throw new InvalidArgumentException("Type of elements in array questions must be object");
        }

        if (!method_exists($questions[0], 'getNumAsked')) {
            throw new InvalidArgumentException("Type of objects in array questions must be GQuestion");
        }

        $not_recent_questions = [];
        $min_between_equal = $this->config->getMinTimeBetweenEqualQuestions();

        foreach ($questions as $q) {
            if ($q->getUpdatedAt() < (new DateTime($min_between_equal . " seconds ago"))) {
                $not_recent_questions [] = $q;
            }
        }

        return $not_recent_questions;
    }

    protected function _getNextQuestion() { //алгоритм: репозиторий ideas, папка Hr/func2
        $number = 99; //сколько выбираем рандомных вопросов, чтобы потом из них выбрать единственный
        $q_portion = ($number / 3);
        //1. берём абсолютно случайно $number вопросов и сортируем по возрастанию сложности
        $questions = $this->_getRandomQuestions($number);
        if (count($questions) !== $number) {
            throw new LengthException("_getRandomQuestions must return " . $number . " questions, but returned only " . count($questions));
        }
        usort($questions, 'self::functionForSortInRegular');
        //2. Исходя из кортежа сложности находим текущую сложность и отбираем верхние, средние или нижние number/3 вопроса в отсортированном списке.
        $current_challenge = $this->getChallenge();
        $current_difficulty = (new GConfigManager())->getCurrentDifficulty($current_challenge);
        if ($current_difficulty === 'easy') {
            $questions = array_slice($questions, 0, $q_portion);
        } elseif ($current_difficulty === 'medium') {
            $questions = array_slice($questions, $q_portion, $q_portion);
        } elseif ($current_difficulty === 'hard') {
            $questions = array_slice($questions, 2 * $q_portion, $q_portion);
        } else {
            throw new UnexpectedValueException("difficulty_sequence has invalid element");
        }

        if (count($questions) !== $q_portion) {
            throw new LengthException("after array_slice must be remained " . $q_portion . " questions, but we have only " . count($questions));
        }
        //3. убираем вопросы, которые недавно задавались
        $questions = $this->_excludeRecentQuestions($questions);

        //4. Если ничего в списке не осталось, берём рандомный вопрос. А так берём из оставшихся случайный.
        if (count($questions) === 0) {
            return $this->_getRandomQuestion();
        } else {
            $index = rand(0, count($questions) - 1);
            return $questions[$index];
        }
    }

    protected function _getRandomQuestion() {
        $g_question_manager = new GQuestionManager();
        $ids = $g_question_manager->getAllQuestionsIds('image');
        $index_of_id = rand(0, count($ids) - 1);
        // this is random id from questions
        $id = $ids[$index_of_id];
        // find question with this id
        $question = $g_question_manager->findQuestionById($id);
        // return what we want
        return $question;
    }

    protected function _getRandomQuestions($number) {
        $g_question_manager = new GQuestionManager();
        $ids = $g_question_manager->getAllQuestionsIds('image');
        $questions = [];
        for ($i = 0; $i < $number; $i++) {
            $index_of_id = rand(0, count($ids) - 1);
            $id = $ids[$index_of_id];
            $question = $g_question_manager->findQuestionById($id);
            $questions [] = $question;
        }
        return $questions;
    }

    public function currentQuestion() {
        $challenge = $this->getChallenge();
        $current_question = $challenge->getQuestion();
        return $current_question;
    }

    protected function _isEqual($s1, $s2) {
        return str_replace('ё', 'е', mb_convert_case($s1, MB_CASE_LOWER, "UTF-8")) ===
                str_replace('ё', 'е', mb_convert_case($s2, MB_CASE_LOWER, "UTF-8"));
    }

    public function isRightAnswer($msg, GQuestion $q) {
        if ($this->_isEqual($msg, $q->getRightAnswer())) {
            return true;
        }

        $alt = $q->getAltAnswers();
        $alts = explode(':', $alt);

        foreach ($alts as $a) {
            if ($this->_isEqual($msg, $a)) {
                return true;
            }
        }
        return false;
    }

    protected function _answered(GUser $u, GChallenge $c) {
        $a = (new GRightAnswerManager())->findRightAnswerByUserAndChallenge($u, $c);
        return $a ? true : false;
    }

    public function checkAnswer(GMessage $msg) {
        $quiz_mode = $this->config->getQuizMode();
        if ($quiz_mode === 'stop') { //викторина остановлена
            return false;
        }
        if ($msg->getToId() != 0) { // private message
            return false;
        }
        $challenge = $this->getChallenge();
        $text = $msg->getText();
        $user = $msg->getUser();
        $sys = $this->chat->getSystemUser();
        $question = $challenge->getQuestion();

        $q_time = $this->config->getQuestionTime();
        $a_time = $this->config->getAnswerTime();
        $sec = $q_time - $a_time;

        if ($this->isRightAnswer($text, $question)) {
            if ($this->_answered($user, $challenge)) {//already?
                $this->_setTextThatAlreadyAnswered($sys, $msg);
                return true;
            }

            if ($challenge->getCreatedAt() < (new DateTime($sec . " seconds ago"))) {
                $this->_setTextThatTooLateAnswer($sys, $msg);
                return true;
            }
            
            $this->_createNewRightAnswerAndSetUpdatesForStatistics($user, $challenge, $quiz_mode);
            $this->_incrementNumAnswered($question);
            $this->_setTextThatAnswerIsRight($user, $sys, $msg, $quiz_mode);
            return true;
        } else {
            return false;
        }
    }
    
    protected function _setTextThatAlreadyAnswered(GUser $sys, GMessage $msg) {
        //преобразование сообщения пользователя в privateSysMessage ему же
                $toUser = $msg->getUser();
                $msg->setUser($sys);
                $msg->setText($this->alreadyAnsweredText);
                $msg->setToId($toUser->getId());
                $msg->save();
    }
    
    protected function _setTextThatTooLateAnswer(GUser $sys, GMessage $msg) {
                $toUser = $msg->getUser();
                $msg->setUser($sys);
                $msg->setText($this->tooLateText);
                $msg->setToId($toUser->getId());
                $msg->save();
    }
    
    protected function _setTextThatAnswerIsRight(GUser $user, GUser $sys, GMessage $msg, $quiz_mode) {
            $statistics = new GStatistics();
            $msg->setUser($sys);
            
            $text = $msg->getText();
            $msg->setToId($user->getId());
            
            if ($quiz_mode === 'regular') {
                $system_text = $text . ' - это верно! ' . 'Всего очков: ' .
                        $statistics->getTotalScore($user) . '.';
                $system_text = (mb_strlen($system_text, 'utf-8') > 120) ? 'Абсолютно точно!' : $system_text; /* . 'Всего очков: ' .
                  Statistics::getTotalScore($user) . '.'
                  : $system_text */
                $msg->setText($system_text);
            }
            else if ($quiz_mode === 'tournament') {
                $system_text = $text . ' - это верно! ' . 'Очки в турнире: ' .
                        $statistics->getCurrentTournamentScore($user) . '.';
                $system_text = (mb_strlen($system_text, 'utf-8') > 120) ? 'Абсолютно точно! ' . 'Очки в турнире: ' .
                        $statistics->getCurrentTournamentScore($user) . '.' : $system_text;
                $msg->setText($system_text);
            }
            $msg->save();
    }
    
    protected function _createNewRightAnswerAndSetUpdatesForStatistics(GUser $user, GChallenge $challenge, $quiz_mode) {
        $g_right_answer_manager = new GRightAnswerManager();
        $r = new GRightAnswer();
            $r->setUser($user);
            $r->setChallenge($challenge);
            if ($quiz_mode === 'tournament') {
                $tournament_number = $this->config->getTournamentNumber();
                $r->setType($tournament_number);
            }
            
            //Для статистики цепочек и для статистики по ответам на произвольном участке в 50 вопросов подряд.
            $current_challenge_id = $challenge->getId();        
            if ($quiz_mode === 'regular') {
                $g_right_answer_manager->setUpdatesForChains($current_challenge_id, $r);
                (new GUserManager())->setBestFiftyScore($current_challenge_id, $user);
            }
            //
            
            $r->save();
            if (!$r->getId()) {
                throw new UnexpectedValueException("answer not saved");
            }
    }

    protected function _isNear($s1, $s2) {
        if (($s1 === '') || ($s2 === '')) {return false;}
        
        $t1 = mb_convert_case($s1, MB_CASE_LOWER, "UTF-8");
        $t2 = mb_convert_case($s2, MB_CASE_LOWER, "UTF-8");

        $l1 = iconv_strlen($t1, 'utf-8');
        $l2 = iconv_strlen($t2, 'utf-8');
        
        if (($l1 <= 3) || ($l2 <= 3)) {return false;} //слишком короткие не рассматриваем

        $pos1 = mb_strpos($t2, $t1, 0, 'utf-8'); //полные вхождения
        $pos2 = mb_strpos($t1, $t2, 0, 'utf-8');
        if (($pos1 !== false) || ($pos2 !== false)) {return true;}
        
        if (levenshtein_utf8($t1, $t2)/max($l1,$l2)  <= 0.34) {return true;}

        return false;
    }

    public function isNearAnswer($text, GQuestion $q) {
        if ($this->_isNear($text, $q->getRightAnswer())) {
            return true;
        }

        $alt = $q->getAltAnswers();
        if ($alt === '') {return false;}
        $alts = explode(':', $alt);
        foreach ($alts as $a) {
            if ($this->_isNear($text, $a)) {return true;}
        }
        return false;
    }

    public function checkNearAnswer(GMessage $msg) {
        $quiz_mode = $this->config->getQuizMode();
        if ($quiz_mode === 'stop') { //викторина остановлена
            return false;
        }
        if ($msg->getToId() != 0) { // private message
            return false;
        }

        $challenge = $this->getChallenge();
        $text = $msg->getText();
        $user = $msg->getUser();
        $question = $challenge->getQuestion();

        if ($this->isNearAnswer($text, $question)) {
            $sys = $this->chat->getSystemUser();
            $msg->setUser($sys);
            $msg->setToId($user->getId());
            $system_text = $text . ' - это близко к ответу.';
            $system_text = (mb_strlen($system_text, 'utf-8') > 80) ?
                    'Ваше большое сообщение близко к ответу.' : $system_text;
            $msg->setText($system_text);
            $msg->save();
            return true;
        } else {
            return false;
        }
    }

    //api function
    public function getQuestion() {
        $quiz_mode = $this->config->getQuizMode();
        
        if ($quiz_mode === 'stop') {
            $question_array = $this->stop_question;
            $this->config->setChallengeUpdated(0);
            return ['question' => $question_array];
        }
        
        $challenge = $this->getChallenge();
        $current_question = $challenge->getQuestion();
        
        $tqi = ($quiz_mode === 'tournament') ?    '№' . ($this->config->getTqi()) . '. '     :    '';
        $q_time = $this->config->getQuestionTime();
        $a_time = $this->config->getAnswerTime();
        $sec = $q_time - $a_time;
        $time_for_announced_param = round($sec / 2); 

        if ($challenge->getCreatedAt() > (new DateTime($sec . " seconds ago"))) { //фаза вопроса
            $question_array = $this->_makeQuestionArray($current_question, $tqi);
            //на середине фазы вопроса возвращаем параметры конфига в 0
            if ($challenge->getCreatedAt() > (new DateTime($time_for_announced_param . " seconds ago"))) {
                $this->config->setAnnouncedRightUsers(0);
                $this->config->setChallengeUpdated(0);
            }
        } else {           //фаза ответа
            $question_array = $this->_makeAnswerArray($current_question);

            //объявить верно ответивших
            if (!$this->config->getAnnouncedRightUsers()) {
                $this->config->setAnnouncedRightUsers(1);
                $this->announceRightUsers($challenge->getId());
            }
        }
        $this->updateChallenge();

        return ['question' => $question_array];
    }
    
    protected function _makeQuestionArray(GQuestion $current_question, $tqi) {
        return [
                'topic' => $tqi . $current_question->getTopic(),
                'text' => $current_question->getText(),
                'pictureUrl' => $current_question->getPackageName() .
                '/' . $current_question->getPictureUrl()
            ];
    }
    
    protected function _makeAnswerArray(GQuestion $current_question) {
        $answer_array = [
                'topic' => 'Ответ: ' . $current_question->getRightAnswer(),
                'text' => $current_question->getComment()
            ];
            $answer_picture = $current_question->getAnswerPictureUrl();
            $answer_array['pictureUrl'] = ($answer_picture) ?
                    $current_question->getPackageName() . '/answers/' . $answer_picture : $current_question->getPackageName() . '/'
                    . $current_question->getPictureUrl();
        return $answer_array;
    }

    public function announceRightUsers($challenge_id) {
        //А также объявление засчитывавшихся ответов
        $q = $this->currentQuestion();
        $right = $q->getRightAnswer();
        $alts = $q->getAltAnswers();
        $alts = str_replace(':', ', ', $alts);
        $rights = ($alts) ? ($right . ', ' . $alts) : ($right);
        $rights = $rights . '.';

        $right_users = (new GRightAnswerManager())->getStringOfRightUsers($challenge_id);
        if ($right_users) {
            $system_text = 'Верно ответили: ' . $right_users . '.';
            $system_text = (mb_strlen($system_text, 'utf-8') > 120) ? 'Верно ответили очень многие.' : $system_text;
            if ($this->config->getAnnouncedRightUsers() === 1) {
                //Chat::sysMessage('Засчитывались ответы: ' . $rights);
                $this->chat->sysMessage($system_text);
                $this->config->setAnnouncedRightUsers(2);
            }
        } else {
            if ($this->config->getAnnouncedRightUsers() === 1) {
                //Chat::sysMessage('Засчитывались ответы: ' . $rights);
                $this->chat->sysMessage('Никто не дал верный ответ.');
                $this->config->setAnnouncedRightUsers(2);
            }
        }
    }

}

//class

		/*require_once '../generated-classes/myvalidation.trait.php';
		require "../propinit.php";
		require "../utils.php";
		$questions = Quiz::_getNextQuestion();
		foreach ($questions as $q) {
			echo($q->getNumAnswered(). ' ' . $q->getNumAsked() . '<br/>');
		}*/
