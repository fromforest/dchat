<?php

use \Propel\Runtime\Propel;

class GStatistics {
	
    public function __construct() {
        
    }

    public function getTotalScore(GUser $u) {
        $n = $u->getRightAnswersCount();
        return $n;
    }

    public function getCurrentTournamentScore(GUser $u) {
        $current_tournament = (new GConfig())->getTournamentNumber();
        $n = (new GRightAnswerManager())->getCurrentTournamentScore ($u, $current_tournament);
        return $n;
    }

//----------------------------- Статистика по ответам начало ----------------------------------
    public function getStatisticsInPeriod($min_time, $max_time, $number) {
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod($min_time, $max_time, $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsInPeriod

    public function getStatisticsCurrentHour($number) {
        $date = getdate();
        $min_time = mktime($date["hours"], 0, 0, $date["mon"], $date["mday"], $date["year"]);
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod($min_time, time(), $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsCurrentHour

    public function getStatisticsPreviousHour($number) {
        //делаем так, а то предыдущий час может быть с предыдущего дня или даже года
        $date = getdate();
        $minutes = $date ["minutes"];
        $seconds = $date ["seconds"];
        $seconds_in_current_hour = $minutes * 60 + $seconds;
        $seconds_from_start_of_previous_hour = 3600 + $seconds_in_current_hour;
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod(time() - $seconds_from_start_of_previous_hour, time() - $seconds_in_current_hour - 1, $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsPreviousHour

    public function getStatisticsCurrentDay($number) {
        $date = getdate();
        $min_time = mktime(0, 0, 0, $date["mon"], $date["mday"], $date["year"]);
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod($min_time, time(), $number);
        return ['statistics_in_period_array' => $statistics];
    }

//end getStatisticsCurrentDay

    public function getStatisticsPreviousDay($number) {
        //делаем так, а то предыдущий день может быть с предыдущего месяца или даже года
        $date = getdate();
        $minutes = $date ["minutes"];
        $seconds = $date ["seconds"];
        $hours = $date ["hours"];
        $seconds_in_current_day = $hours * 3600 + $minutes * 60 + $seconds;
        $seconds_from_start_of_previous_day = 3600 * 24 + $seconds_in_current_day;
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod(time() - $seconds_from_start_of_previous_day, time() - $seconds_in_current_day - 1, $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsPreviousDay

    public function getStatisticsCurrentMonth($number) {
        $date = getdate();
        $min_time = mktime(0, 0, 0, $date["mon"], 1, $date["year"]);
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod($min_time, time(), $number);
        return ['statistics_in_period_array' => $statistics];
    }

// getStatisticsCurrentMonth

    public function getStatisticsPreviousMonth($number) {
        $date = getdate();
        $previous_month = $date['mon'] - 1;
        $year = $date['year'];
        if ($previous_month === 0) {
            $previous_month = 12;
            $year = $year - 1;
        }

        $min_time = mktime(0, 0, 0, $previous_month, 1, $year);
        $max_time = mktime(23, 59, 59, $previous_month + 1, 0, $year); //Последний день любого месяца можно вычислить как "нулевой" день следующего месяца
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod($min_time, $max_time, $number);
        return ['statistics_in_period_array' => $statistics];
    } // end getStatisticsPreviousMonth

    public function getStatisticsCurrentYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $current_year);
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod($min_time, time(), $number);
        return ['statistics_in_period_array' => $statistics];
    }// end getStatisticsCurrentYear

    public function getStatisticsPreviousYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $current_year - 1);
        $max_time = mktime(23, 59, 59, 12, 31, $current_year - 1);
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod($min_time, $max_time, $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsPreviousYear

    public function getStatisticsTotalScore($number) {
        $statistics = (new GRightAnswerManager())->getStatisticsInPeriod(0, time(), $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsTotalScore
//----------------------------- Статистика по ответам конец -------------------------------------
//
//----------------------------- Статистика по лучшим промежуткам начало -------------------------

    public function getStatisticsBestHours($min_date_string, $max_date_string, $number) {
        $statistics = (new GRightAnswerManager())->getStatisticsBestHours($min_date_string, $max_date_string, $number);
        return ['best_periods' => $statistics];
    }

//end getStatisticsBestHours

    public function getStatisticsBestDays($min_date_string, $max_date_string, $number) {
        $statistics = (new GRightAnswerManager())->getStatisticsBestDays($min_date_string, $max_date_string, $number);
        return ['best_periods' => $statistics];
    }

//end getStatisticsBestHours

    public function getStatisticsBestHoursTotal($number) {
        $min_date_string = '2015-08-02 00';
        $max_date_string = date("Y-m-d H", time());
        $statistics =  (new GRightAnswerManager())->getStatisticsBestHours($min_date_string, $max_date_string, $number);
        return ['best_periods' => $statistics];
    }

// end getStatisticsBestHoursTotal

    public function getStatisticsBestHoursCurrentMonth($number) {
        $date = getdate();
        $current_month = $date['mon'];
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, $current_month, 1, $current_year);
        $min_date_string = date("Y-m-d H", $min_time);
        $max_date_string = date("Y-m-d H", time());
        $statistics =  (new GRightAnswerManager())->getStatisticsBestHours($min_date_string, $max_date_string, $number);
        return ['best_periods' => $statistics];
    }

// getStatisticsBestHoursCurrentMonth

    public function getStatisticsBestHoursCurrentYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $current_year);
        $min_date_string = date("Y-m-d H", $min_time);
        $max_date_string = date("Y-m-d H", time());
        $statistics =  (new GRightAnswerManager())->getStatisticsBestHours($min_date_string, $max_date_string, $number);
        return ['best_periods' => $statistics];
    } // getStatisticsBestHoursCurrentYear

    public function getStatisticsBestDaysTotal($number) {
        $min_date_string = '2015-08-02';
        $max_date_string = date("Y-m-d", time());
        $statistics =  (new GRightAnswerManager())->getStatisticsBestDays($min_date_string, $max_date_string, $number);
        return ['best_periods' => $statistics];
    } // getStatisticsBestDaysTotal

    public function getStatisticsBestDaysCurrentYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $current_year);
        $min_date_string = date("Y-m-d", $min_time);
        $max_date_string = date("Y-m-d", time());
        $statistics =  (new GRightAnswerManager())->getStatisticsBestDays($min_date_string, $max_date_string, $number);
        return ['best_periods' => $statistics];
    }

// getStatisticsBestDaysCurrentYear

    public function getStatisticsBestMonthsTotal($number) {
        $statistics = (new GRightAnswerManager())->getStatisticsBestMonthsTotal($number);
        return ['best_periods' => $statistics];
    }

// getStatisticsBestMonthsTotal

    public function getStatisticsBestYears($number) {
        $statistics = (new GRightAnswerManager())->getStatisticsBestYears($number);
        return ['best_periods' => $statistics];
    }

// getStatisticsBestYears
//----------------------------- Статистика по лучшим промежуткам конец ---------------------------
//----------------------------- Статистика по цепочкам начало ------------------------------------
    public function getStatisticsChains($min_time, $max_time, $number) {
        $statistics = (new GRightAnswerManager())->getStatisticsChains($min_time, $max_time, $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsChains

    public function getStatisticsChainsTotal($number) {
        $min_time = mktime(0, 0, 0, 8, 2, 2015);
        $statistics = (new GRightAnswerManager())->getStatisticsChains($mintime, time(), $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsChainsTotal

    public function getStatisticsChainsCurrentMonth($number) {
        $date = getdate();
        $min_time = mktime(0, 0, 0, $date['mon'], 1, $date['year']);
        $statistics = (new GRightAnswerManager())->getStatisticsChains($min_time, time(), $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsChainsCurrentMonth

    public function getStatisticsChainsCurrentYear($number) {
        $date = getdate();
        $current_year = $date['year'];
        $min_time = mktime(0, 0, 0, 1, 1, $date['year']);
        $statistics = (new GRightAnswerManager())->getStatisticsChains($min_time, time(), $number);
        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsChainsCurrentYear
//----------------------------- Статистика по цепочкам конец ------------------------------------

    protected function _functionForSort($a, $b) { //функция для сортировки usort, использующейся в рейтинге MQZ
        if ($a['score'] === $b['score']) {
            return 0;
        }
        return ($a['score'] < $b['score']) ? 1 : -1;
    }

// end _functionForSort

    public function getHourScores($user_name, $number) {
        $hour_scores = (new GRightAnswerManager)->getHourScores($user_name, $number);        
        return $hour_scores;
    }

// end getHourScores

    protected function _getHoursHystogram($hour_scores) { //распределение часов по результатам
        $freq = [];
        foreach ($hour_scores as $hour_score) {
            $hs = (string) $hour_score['score'];
            $freq[$hs] = array_key_exists($hs, $freq) ? $freq[$hs] : 1;
        }
        return $freq;
    }

    public function getStatisticsMQZ($number) {
        $user_names_objects = (new GUserManager())->getAllUserNames();
        $statistics = [];
        foreach ($user_names_objects as $user_name) {
            $hour_scores = $this->getHourScores($user_name, $number); //все часы юзера
            $freq = $this->_getHoursHystogram($hour_scores); //распределение часов по результатам
            $mqz = 1;
            $sum = 0;
            if (count($freq) > 0) { //юзеры без единого ответа не попадут в статистику
                for (reset($freq); ($key = key($freq)); next($freq)) { //алгоритм Альбы
                    $sum += $freq[$key];
                    if ($sum >= (int) $key) {
                        $mqz = (int) $key;
                        break;
                    }
                } // конец алгоритма Альбы
                $as_array ['score'] = $mqz;
                $as_array ['user'] = $user_name;
                $statistics [] = $as_array;
            }
        }// цикл обхода юзеров
        usort($statistics, 'self::_functionForSort');
        return ['statistics_in_period_array' => $statistics];
    }

//end getStatisticsMQZ

    public function getStatisticsOnAnyFiftyChallenges($number) {
        $statistics = (new GUserManager())->getStatisticsOnAnyFiftyChallenges($number);

        return ['statistics_in_period_array' => $statistics];
    }

// end getStatisticsOnAnyFiftyChallenges

    public function getStatisticsInTournament($tournament_number, $number) {
        $statistics = (new GRightAnswerManager())->getStatisticsInTournament($tournament_number, $number);
        return ['statistics_in_period_array' => $statistics];
    }

    public function getStatisticsLastTournament($number) {
        $config = new GConfig();
        $tournament_number = $config->getTournamentNumber() - 1;
        $statistics = (new GRightAnswerManager())->getStatisticsInTournament($tournament_number, $number);
        return ['statistics_in_period_array' => $statistics];
    }

    public function getStatisticsCurrentTournament($number) {
        $config = new GConfig();
        $tournament_number = $config->getTournamentNumber();
        return (new GRightAnswerManager())->getStatisticsInTournament($tournament_number, $number);
    }

}

//class
