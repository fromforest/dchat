<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GTextSinglePlayerAnswer
 *
 * @author HRUST
 */
class GTextSinglePlayerAnswer {
    private $_tspa;
   
   function __construct () {
       $this->_tspa = new TextSinglePlayerAnswer();
   }
   
   public function getNumAsked() {
	   return $this->_tspa->getNumAsked();
   }
   
   public function getNumAnswered() {
	   return $this->_tspa->getNumAnswered();
   }
      
   public function getQuestion() {
       $question = $this->_tspa->getQuestion();
       $g_question = new GQuestion();
       $g_question->setQuestion($question);
       return $g_question;
   }
   
   public function getQuestionId() {
       return $this->_tspa->getQuestionId();
   }
   
    public function getTextSinglePlayerAnswer() {
        return $this->_tspa;
    }
   
   public function save() {
       $this->_tspa->save();
   }
   
    public function setIsRight($is_right) {
        $this->_tspa->setIsRight($is_right);
    }
	
	public function setNumAsked($num_asked) {
		$this->_tspa->setNumAsked($num_asked);
	}
	
	public function setNumAnswered($num_answered) {
		$this->_tspa->setNumAnswered($num_answered);
	}
    
    public function setTextSinglePlayerAnswer(TextSinglePlayerAnswer $tspa) {
        $this->_tspa = $tspa;
    }
    
    public function setQuestionId($question_id) {
        $this->_tspa->setQuestionId($question_id);
    }
    
    public function setUserId($user_id) {
        $this->_tspa->setUserId($user_id);
    }
}
