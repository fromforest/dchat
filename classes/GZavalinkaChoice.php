<?php


class GZavalinkaChoice {
    private $_zc;
    
    public function __construct() {
        $this->_zc = new ZavalinkaChoice();
    }
    
    public function getGameRoomId() {
        return $this->_zc->getGameRoomId();
    }
    
    public function getForUserId() {
        return $this->_zc->getForUserId();
    }
    
    public function getId() {
        return $this->_zc->getId();
    }
    
    public function getUserId() {
        return $this->_zc->getUserId();
    }
    
    public function getWord() {
        return $this->_zc->getWord();
    }
    
    public function getZavalinkaChoice() {
        return $this->_zc;
    }
    
    public function save() {
        $this->_zc->save();
    }
    
    public function setGameRoomId($game_room_id) {
        $this->_zc->setGameRoomId($game_room_id);
    }
    
    public function setForUserId($for_user_id) {
        $this->_zc->setForUserId($for_user_id);
    }
    
    public function setUserId($user_id) {
        $this->_zc->setUserId($user_id);
    }
    
    public function setWord($word) {
        $this->_zc->setWord($word);
    }
    
    public function setZavalinkaChoice(ZavalinkaChoice $zc) {
        $this->_zc = $zc;
    }
    
}//class 
