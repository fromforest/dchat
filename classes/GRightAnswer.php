<?php

class GRightAnswer {
   private $_right_answer;
   
   function __construct() {
       $this->_right_answer = new RightAnswer();
   }
   
   function delete() {
       $this->_right_answer->delete();
   }
   
   function getId() {
       return $this->_right_answer->getId();
   }
   
   public function getRightAnswer() {
       return $this->_right_answer;
   }
   
   function getUser() {
       $user = $this->_right_answer->getUser();
       $g_user = new GUser();
       $g_user->setUser($user);
       return $g_user;
   }
   
   public function save() {
       $this->_right_answer->save();
   }
   
   public function setChallenge(GChallenge $g_challenge) {
       $this->_right_answer->setChallenge($g_challenge->getChallenge());
   }
   
   public function setNum($number) { //num отображает текущую цепочку верных ответов юзера
       $this->_right_answer->setNum($number);
   }
   
   public function setRightAnswer(RightAnswer $right_answer) {
       $this->_right_answer = $right_answer;
   }
   
   public function setType($tournament_number) { //если $tournament_number == 0, то это регулярная игра
       $this->_right_answer->setType($tournament_number);
   }
   
   public function setUser(GUser $g_user) {
       $this->_right_answer->setUser($g_user->getUser());
   }
}
