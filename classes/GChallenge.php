<?php


class GChallenge {
    
    private $_challenge;
    
    function   __construct() {
        $this->_challenge = new Challenge();
    }
    
    public function delete() {
        $this->_challenge->delete();
    }
    
    function getChallenge() {
        return $this->_challenge;
    }
    
    public function getCreatedAt() {
       return $this->_challenge->getCreatedAt();
    }
    
    public function getId() {
        return $this->_challenge->getId();
    }
    
    public function getQuestion() {
        $question = $this->_challenge->getQuestion();
        $g_question = new GQuestion();
        $g_question->setQuestion($question);
        return $g_question;
    }
    
    public function save() {
        $this->_challenge->save();
    }
    
    public function setChallenge(Challenge $challenge) {
        $this->_challenge = $challenge;
    }
    
    public function setCreatedAt(DateTime $created_at) {
        $this->_challenge->setCreatedAt($created_at);
    }
    
    public function setQuestion(GQuestion $g_question) {
        $this->_challenge->setQuestion($g_question->getQuestion());
    }
    
}
