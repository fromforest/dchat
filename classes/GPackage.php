<?php



class GPackage {

    private $_package;

    function __construct() {
        $this->_package = new Package();
    }
    
    public function delete() {
        $this->_package->delete();
    }
    
    public function getId() {
        return $this->_package->getId();
    }

    public function getPackage() {
        return $this->_package;
    }
    
    public function save() {
        $this->_package->save();
    }
    
    public function setId($id) {
        $this->_package->setId($id);
    }

    public function setPackage(Package $package) {
        $this->_package = $package;
    }
    
} //class
