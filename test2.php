<?php

require "propinit.php";
require "utils.php";

function invokeMethod($class, $methodName, array $parameters = array()) {
        $reflection = new \ReflectionClass($class);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs(null, $parameters);
    }

$_POST['name'] = 'te';
$_POST['password'] = '12312';

$response = invokeMethod("Chat", "_authenticate", array('test', 'test'));
print_r($response);

