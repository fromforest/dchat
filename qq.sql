select  
DATE_FORMAT(right_answers.created_at,'%Y-%m-%d %H') as hour, users.name, count(right_answers.id) as score 
from 
right_answers join users 
on 
right_answers.user_id = users.id 
group by users.name, DATE_FORMAT(right_answers.created_at,'%Y-%m-%d %H') 
order by count(right_answers.id) 
desc 
limit 5;