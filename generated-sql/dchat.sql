
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- messages
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER,
    `to_id` INTEGER DEFAULT 0 NOT NULL,
    `text` VARCHAR(255) NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `messages_i_d404ac` (`created_at`),
    INDEX `messages_fi_69bd79` (`user_id`),
    CONSTRAINT `messages_fk_69bd79`
        FOREIGN KEY (`user_id`)
        REFERENCES `users` (`id`)
        ON DELETE SET NULL
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- users
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    `password` VARCHAR(32) NOT NULL,
    `users_group` VARCHAR(32) DEFAULT 'muggles' NOT NULL,
    `online` TINYINT(1) DEFAULT 0 NOT NULL,
    `best_fifty` INTEGER DEFAULT 0 NOT NULL,
    `last_game_room_id` INTEGER,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `users_u_d94269` (`name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- packages
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `packages`;

CREATE TABLE `packages`
(
    `id` INTEGER NOT NULL,
    `name` VARCHAR(32) NOT NULL,
    `description` VARCHAR(500),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- questions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions`
(
    `id` INTEGER NOT NULL,
    `package_id` INTEGER NOT NULL,
    `type` VARCHAR(32) DEFAULT 'image' NOT NULL,
    `topic` VARCHAR(100) NOT NULL,
    `text` VARCHAR(2000) NOT NULL,
    `picture_url` VARCHAR(255) NOT NULL,
    `answer_picture_url` VARCHAR(255),
    `right_answer` VARCHAR(100) NOT NULL,
    `alt_answers` VARCHAR(255),
    `author` VARCHAR(64),
    `tag1` VARCHAR(100),
    `tag2` VARCHAR(100),
    `comment` VARCHAR(2000),
    `num_asked` INTEGER DEFAULT 0 NOT NULL,
    `num_answered` INTEGER DEFAULT 0 NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `questions_fi_57a14d` (`package_id`),
    CONSTRAINT `questions_fk_57a14d`
        FOREIGN KEY (`package_id`)
        REFERENCES `packages` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- challenges
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `challenges`;

CREATE TABLE `challenges`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `question_id` INTEGER NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `challenges_fi_83fc93` (`question_id`),
    CONSTRAINT `challenges_fk_83fc93`
        FOREIGN KEY (`question_id`)
        REFERENCES `questions` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- right_answers
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `right_answers`;

CREATE TABLE `right_answers`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER,
    `challenge_id` INTEGER NOT NULL,
    `num` INTEGER DEFAULT 1 NOT NULL,
    `type` INTEGER NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `right_answers_fi_69bd79` (`user_id`),
    INDEX `right_answers_fi_ec5f27` (`challenge_id`),
    CONSTRAINT `right_answers_fk_69bd79`
        FOREIGN KEY (`user_id`)
        REFERENCES `users` (`id`)
        ON DELETE SET NULL,
    CONSTRAINT `right_answers_fk_ec5f27`
        FOREIGN KEY (`challenge_id`)
        REFERENCES `challenges` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- single_player_answers
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `single_player_answers`;

CREATE TABLE `single_player_answers`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `question_id` INTEGER NOT NULL,
    `num_asked` INTEGER DEFAULT 0 NOT NULL,
    `num_answered` INTEGER DEFAULT 0 NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `single_player_answers_fi_83fc93` (`question_id`),
    CONSTRAINT `single_player_answers_fk_83fc93`
        FOREIGN KEY (`question_id`)
        REFERENCES `questions` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- text_single_player_answers
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `text_single_player_answers`;

CREATE TABLE `text_single_player_answers`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `question_id` INTEGER NOT NULL,
    `num_asked` INTEGER DEFAULT 0 NOT NULL,
    `num_answered` INTEGER DEFAULT 0 NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `text_single_player_answers_fi_83fc93` (`question_id`),
    CONSTRAINT `text_single_player_answers_fk_83fc93`
        FOREIGN KEY (`question_id`)
        REFERENCES `questions` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- game_rooms
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `game_rooms`;

CREATE TABLE `game_rooms`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `type` VARCHAR(100) DEFAULT '' NOT NULL,
    `room_name` VARCHAR(100) DEFAULT '' NOT NULL,
    `phase` INTEGER DEFAULT 0 NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `game_rooms_u_3fdef3` (`room_name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- zavalinka_words
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `zavalinka_words`;

CREATE TABLE `zavalinka_words`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `word` VARCHAR(255) NOT NULL,
    `user_id` INTEGER NOT NULL,
    `definition` VARCHAR(1000) NOT NULL,
    `game_room_id` INTEGER NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- zavalinka_choices
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `zavalinka_choices`;

CREATE TABLE `zavalinka_choices`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `word` VARCHAR(255) NOT NULL,
    `user_id` INTEGER NOT NULL,
    `for_user_id` INTEGER NOT NULL,
    `game_room_id` INTEGER NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- config
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config`
(
    `id` INTEGER NOT NULL,
    `quiz_mode` VARCHAR(1024) DEFAULT 'regular' NOT NULL,
    `question_time` INTEGER DEFAULT 30 NOT NULL,
    `answer_time` INTEGER DEFAULT 7 NOT NULL,
    `announced_right_users` INTEGER DEFAULT 1 NOT NULL,
    `challenge_updated` INTEGER DEFAULT 0 NOT NULL,
    `packages` VARCHAR(2048) DEFAULT 'tp1:tp2:tp18' NOT NULL,
    `black_list` VARCHAR(1024) DEFAULT 'tp18:temp_uploaded' NOT NULL,
    `difficulty_sequence` VARCHAR(1024) DEFAULT 'easy:medium:easy:hard' NOT NULL,
    `min_users_for_updates` INTEGER DEFAULT 5 NOT NULL,
    `tqi` INTEGER DEFAULT 0 NOT NULL,
    `tournament_number` VARCHAR(1024) DEFAULT '' NOT NULL,
    `temp_uploaded_question_id` INTEGER DEFAULT 1000000 NOT NULL,
    `reserve_string` VARCHAR(255) DEFAULT '0' NOT NULL,
    `reserve_int` INTEGER DEFAULT 0 NOT NULL,
    `reserve5` INTEGER DEFAULT 0 NOT NULL,
    `min_time_between_equal_questions` INTEGER DEFAULT 360 NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
